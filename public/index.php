<?php

use enums\ServiceEnums;
use enums\AppEnums;
error_reporting( E_ALL );
ini_set( 'display_errors', '1' );

define( 'APP_ROOT', dirname( __DIR__ ) . '/' );

//将出错信息输出到一个文本文件
ini_set( 'error_log', APP_ROOT . '/logs/php_error_log.txt' );

//定义常量 APP_ROOT web root

define( 'APP_MODULE', APP_ROOT . 'apps/' );

define( 'APP_MODE', 'dev' );//dev deploy
// use
// Phalcon\Mvc\Dispatcher as MvcDispatcher,
// Phalcon\Events\Manager as EventsManager,
// Phalcon\Mvc\Dispatcher\Exception as DispatchException;
		
$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

$loader->registerNamespaces( array(
	'libraries' => APP_ROOT . 'libraries/',
//	'utils'		=> APP_ROOT . 'apps/utils/',
	'apis'		=> APP_ROOT . 'apis/',
//	'bizs'      => APP_ROOT . 'bizs',
	'enums'		=> APP_ROOT . 'enums/',
	'listeners'	=> APP_ROOT . 'listeners/',
    'vendors'   => APP_ROOT . 'vendors/',
    'models'    => APP_ROOT . 'models/',
    'helpers'   => APP_ROOT . 'helpers/',
    'vos'   => APP_ROOT . 'vos/',
));

$loader->register();

$arrInitCfg = array(
		array( 'name' => 'ipCfg', 'file' => 'iplocation.php', 'inDi' => true ),
		array( 'name' => 'lockCfg', 'file' => 'lock.php', 'inDi' => true ),
		array( 'name' => 'ueditorCfg', 'file' => 'ueditor.php', 'inDi' => true ),
		array( 'name' => 'sessionCfg', 'file' => 'session.php', 'inDi' => true ),
		array( 'name' => 'dbCfg', 'file' => 'db.php', 'inDi' => true ),
		array( 'name' => 'queueCfg', 'file' => 'queue.php', 'inDi' => true ),
		array( 'name' => 'urlCfg', 'file' => 'url.php', 'inDi' => true ),
		array( 'name' => 'config', 'file' => 'config.php', 'inDi' => true ),
		array( 'name' => 'cacheCfg', 'file' => 'cache.php', 'inDi' => true ),
		array( 'name' => 'logCfg', 'file' => 'log.php', 'inDi' => true ),
		array( 'name' => 'staticCfg', 'file' => 'static.php', 'inDi' => true ),
		array( 'name' => 'ossCfg', 'file' => 'oss.php', 'inDi' => true ),
		array( 'name' => 'smsCfg', 'file' => 'sms.php', 'inDi' => true ),
        array( 'name' => 'appVer', 'file' => 'appVer.php', 'inDi' => true )
);

$di = new \Phalcon\Di\FactoryDefault();
//set in di
foreach( $arrInitCfg as $cfg )
{
	if( file_exists( APP_ROOT . 'config/' . APP_MODE . '/' . $cfg[ 'file' ] ))
	{
		if( $cfg[ 'inDi' ] )
		{
			$di->set( $cfg['name'], require APP_ROOT . 'config/' . APP_MODE . '/' . $cfg['file'], true );
		}
		else
		{
			${$cfg[ 'name' ]} = require APP_ROOT . 'config/' . APP_MODE . '/' . $cfg['file'];
		}
	}
	else
	{
		if( $cfg[ 'inDi' ] )
		{
			$di->set( $cfg['name'], require APP_ROOT . 'config/' . $cfg['file'], true  );
		}
		else
		{
			${$cfg[ 'name' ]} = require APP_ROOT . 'config/' . $cfg['file'];
		}
	}
}

try
{
    //加载composer
    require_once APP_ROOT . '/vendor/autoload.php';

    require APP_ROOT . 'config/services.php';//加载公共services
	
	$app = new \Phalcon\Mvc\Application($di);
	
	// Register the installed modules
	$app->registerModules( array(
	        'admin' => 
			array(
	            'className' => 'apps\admin\Module',
	            'path' => APP_ROOT . 'apps/admin/Module.php'
	        ),
	        'common' => 
			array(
	            'className' => 'apps\common\Module',
	            'path' => APP_ROOT . 'apps/common/Module.php'
	        ),
    	    'cms' =>
    	    array(
    	        'className' => 'apps\cms\Module',
    	        'path' => APP_ROOT . 'apps/cms/Module.php'
    	    ),
    	    'install' =>
    	    array(
    	        'className' => 'apps\install\Module',
    	        'path' => APP_ROOT . 'apps/install/Module.php'
    	    ),
	       
	));

    echo $app->handle()->getContent();
}
catch( \Exception $e )
{
	if( AppEnums::APP_MODE_PRODUCTION == APP_MODE )
	{
		if( isset( $di[ ServiceEnums::SERVICE_LOG_ERR ] ) )
		{
			$di[ ServiceEnums::SERVICE_LOG_ERR ]->error( $e->getMessage() );
			$di[ ServiceEnums::SERVICE_LOG_ERR ]->error( $e->getTraceAsString() . ',参数为：' . var_export( $_SERVER, true  ) );
		}
	}
	else
	{
		echo $e->getMessage(), '<br>';
		echo $e->getTraceAsString();
	}


	echo '系统出错请联系管理员解决';
    
}

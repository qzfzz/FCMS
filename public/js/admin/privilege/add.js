    //滚动条
     var optionScroll = { //滚动条到搜索位置
         color: "rgba(0,0,0,0.2)",
         size: "3px",
         height:'400px'
     }
    $("#pri_modal .modal-body").slimscroll( optionScroll );
     
     //数据树
     var objOption = {   levels:1,
             color: "#428bca",
             expandIcon: 'glyphicon glyphicon-chevron-right',
             collapseIcon: 'glyphicon glyphicon-chevron-down',
             showCheckbox: true,
             highlightSelected:false,
             multiSelect: false,
             data : []
         };
     var objTree = {};
     var arrAclChecked = [];
     isInitTree = false; //数据树是否生成
     
     var objPageAdd = $( '#page_add' );
     
     //对话框窗口打开
     objPageAdd.on( 'click', '#add_acl', function(){
         var name = $.trim( $( 'input[name=name]' ).val());
         if( ! name )
         {
             toastr.error( '请先填写菜单名' );
             return false;
         }
         $( '.modal-title' ).text( name );
         arrAclChecked = [];
         if( isInitTree )
         {
             objTree.treeview( 'uncheckAll' ); //取消所有选中的
             $( '#pri_modal' ).modal( 'show' );
             return false;
         }
         
         $( '#loading' ).show();
         $.get( ajaxUrl + '/getAcl', function( ret ){
             if( ! ret.status && ! isInitTree)
             {
                 objOption.data = ret.data;
                 objTree = $( '#pri_tree' ).treeview( objOption );
                 var name = $.trim( $( '#ppri' ).text()); //获得上一级的
                 name = "articles";
                 var nodes = objTree.treeview('search', [ name, { ignoreCase: true, exactMatch: true } ]);

                 $('#pri_tree').treeview('expandNode', [ nodes, { levels: 99, silent: true } ]);
                 isInitTree = true;
                 setTreeEvent( objTree );
             
                 $( '#pri_modal' ).modal( 'show' );
             }
             else
             {
                 toastr.error( ret.msg );
             }
             
         }, 'json' ).error( function(){
             toastr.error( '网络不通' );
         }).complete( function(){
             $( '#loading' ).hide(); 
             //isInitTree = false;
         });
     });
     
     //设置treeviw事件
     function setTreeEvent( objTree )
     {
         //选中acl事件
         objTree.on('nodeChecked', function(event, data) {
         
             var objAction = data;
             if( ! objAction.parentId )
             {
                 objTree.treeview( 'uncheckNode', objAction.nodeId );
                 toastr.error( '请选择action' );
                 return false;
             }
             var objController = objTree.treeview( 'getNode', objAction.parentId );
             var objModule = objTree.treeview( 'getParent', objAction.parentId );
             if( ! objController )
             {
                 toastr.error( objAction.text + '缺少控制器，请选择action' );
                 return false;
             }
             if( ! objModule )
             {
                 toastr.error( objAction.text + '缺少模块，请选择action' );
                 return false;
             }
             var obj = {};
             obj.id = objAction.id;
             obj.action = objAction.text;
             obj.controller = objController.text;
             obj.module = objModule.text;
             
             arrAclChecked[ data.id ] = obj;
          });    
           
         //取消acl事件
         objTree.on('nodeUnchecked', function(event, data) {
             delete arrAclChecked[ data.id ];
          });  

          //选中事件
          var preId = 0;
          objTree.on( 'nodeSelected', function( event,data ){
             preId = data.nodeId;
             objTree.treeview( 'expandNode', data.nodeId );
         });

          //没选中事件
          objTree.on( 'nodeUnselected', function( event,data ){
             setTimeout( function(){
                 if( preId == data.nodeId )
                 {
                     objTree.treeview( 'collapseNode', data.nodeId );
                 }
                 
             }, 0 ); 
         });
     }   
     
     //搜索acl
     $( '#keyword' ).keyup( function( e ){
         if( e.keyCode == 13 )
         {
             $( this ).change();
         }
     });
     $( '#keyword' ).change( function(){
         var keyword = $.trim( $( this ).val());
         if( ! keyword )
         {
             objTree.treeview( 'collapseAll' );
             return false;
         }
         if( ! isInitTree )
         {
             toastr.error( '无数据可搜索' );
             return;
         }
         objTree.treeview( 'clearSearch' );
         objTree.treeview('collapseAll', { silent: true });
         var nodes = objTree.treeview('search', [ keyword, { ignoreCase: true, exactMatch: true } ]);
         $('#pri_tree').treeview('expandNode', [ nodes, { levels: 99, silent: true } ]);

         $( '.search-result:first' ).attr( 'id', 'scroll_position' );
         location = '#scroll_position'; //跳到那个元素跟前
         $("#pri_modal .modal-body").slimscroll( optionScroll );
     });
     
     //对话框关闭 , 保存选中acl到表格
      $(  '#save_acl' ).click( function(){
         if( ! arrAclChecked.length )
         {
             $( '#pri_modal' ).modal( 'hide' );
             return false;               
         }
         var objTable = $( '#pri_table' );
         //添加到表格中，首先判断表格中是否已经存在
         var arrAclExist = [];
         objTable.find( 'tr' ).each( function(){
             var id = $.trim( $( this ).attr( 'data-id' ));
             arrAclExist.push( id );
         });
         var strHtml = ''; 
         for( var i in arrAclChecked )
         {
             var obj = arrAclChecked[ i ];
             if( arrAclExist.indexOf( obj.id ) !== -1 )
             {
                 continue;
             }
                 
             strHtml += '<tr data-id="' + obj.id + '"><td>' + obj.module + '</td><td>' + obj.controller + '</td><td>' + obj.action +
                        '</td><td><i class="glyphicon text-success glyphicon-ok"></i></td><td><i class="glyphicon glyphicon-remove"></i></td></tr>'; 
         }       
         objTable.find( '.no_acl' ).remove();
         objTable.append( strHtml ); 

         //检查是否有默认的，没有就设置第一个为默认的
         if( ! objTable.find( 'tr.is_defalut' ).length )
         {
             objTable.find( 'tr:first' ).addClass( 'is_default' );
         }
         
         $( '#pri_modal' ).modal( 'hide' );
     });

     //选择默认acl
     objPageAdd.on( 'click', 'tr', function(){
         $( this ).siblings().removeClass( 'is_default' );
         $( this ).addClass( 'is_default' );
     });

     //要删除的id
     objPageAdd.on( 'click', '.glyphicon-remove', function(){
         var obj = $( this ).parents( 'tr' );
         var paId = obj.attr( 'data-paid' ); 
         if( ! paId )
         {
             obj.remove();
             return true;    
         }
         $.confirm( { title: '确定要删除', 'confirm': function(){
             arrDel.push( paId );    
             obj.remove();
         } });
         return false;
     });

    /*-----------------保存数据-------------------------*/
    //校验数据是否正确
    objPageAdd.on( 'blur', ':text', function(){
       var value = $.trim( $( this ).val());
       var status = true;
       if( value )
       {
           if( value.length > 32 )
           {
               error( this, '输入字符不可超过32个' );
               return false;
           }
           var name = $( this ).attr( 'name' ); //排序
           if( name === 'sort')
           {
                if( ! /^[0-9]*$/.test( value ))
                {
                    error( this, '请输入正整数' );
                }
           }
       }
       else
       {
           error( this, '输入字符不可为空' );
       }
       
       
    });
    
    //保存表单数据
    objPageAdd.on( 'click', '#save', function(){

        var sort = $.trim( $( 'input[name=sort]' ).val() );
        if( isNaN( sort ))
        {
            toastr.error( '请输入数字' );
            return false;
        }
        var data = $( '#form' ).serialize();
        data += '&' + 'key=' + csrf.key + '&token=' + csrf.token;
        //删除的id
        for( var i in arrDel )
        {
            data += '&acl[del][]=' + arrDel[i];
        }
        
        var defaultId = 0;   
        //新添加的acl, 以及默认的acl
        $( '#pri_table' ).find( 'tr' ).each( function(){
               
              var paId = $.trim( $( this ).attr( 'data-paid' ));  //pri_acl的id
              var id = $.trim( $( this ).attr( 'data-id' )); //新加的acl的id
              if( $( this ).hasClass( 'is_default'))
              {
                 defaultId = id; //默认的acl
                 data += '&defaultId=' + id;
              }
              
              if( ! paId && id ) 
              {
                  data += '&acl[add][]=' + id;
              }
        });
        
        //发送数据
        $.post( ajaxUrl + '/save', data, function( ret ){
            setCSRF( ret );
            if( ! ret.status )
            { 
                showPagelist();
            }
            else
            {
               toastr.error( ret.msg );
            }
        }, 'json');
    } ).error( function(){
        toastr.error( '网络不通' );
    });
    
    objPageAdd.on( 'click', '#cancel', function(){
        showPagelist();
    });
    
    function setCSRF( data ) 
    {
       csrf.key = data.key;
       csrf.token = data.token;
    }
    
    function error( _this, msg )
    {
        var obj = $( _this ).parents( '.form-group' );
        obj.removeClass( 'has-success' ).addClass( 'has-error' );
        $( this ).siblings( 'span' ).removeClass( 'glyphicon-ok' ).addClass( 'glyphicon-remove' );
        if( msg )
        {
            obj.find( '.help-block' ).remove();
            obj.append( '<div class="col-xs-6 help-block">' + msg + '</div>' );
        }
    }

    function success( _this )
    {
        $( _this ).parents( '.form-group' ).addClass( 'has-success' ).removeClass( 'has-error' )
            .find( '.help-block' ).remove();
        $( _this ).siblings( 'span' ).addClass( 'glyphicon-ok' ).removeClass( 'glyphicon-remove' );
    }

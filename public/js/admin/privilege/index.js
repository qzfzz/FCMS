var objList = $( '#page_list' );
    
    //删除权限
    objList.delegate( '.remove', 'click',function(){
       var obj = $( this ).parents( 'tr' );
       var id = obj.attr( 'data-id' ); 
       var data = $.extend( csrf, { id : id });
       
       $.confirm(  { title: '确认要删除吗？',confirm: function(){
            
           $.post( ajaxUrl + '/delete', data, function( ret ){
               if( ! ret.status )
               {
                   obj.remove();
               }
               else
               {
                   toastr.error( ret.msg );
               }
               csrf.key = ret.key;
               csrf.token = ret.token;
           }, 'json' );
       }});
       return false;
    });
    
    var isAdd = true;
    //编辑权限
    objList.delegate( '.edit' ,'click', function(){
        var objParent = $( this ).parents( 'tr' );
        var id = objParent.attr( 'data-id' );
        var url = ajaxUrl + '/edit/id/' + id;
        objParent.addClass( 'current' ).siblings().removeClass( 'current' );
        isAdd = false;
        getAddView( url );
        
        return false;
    });
    
    //添加子权限
    objList.delegate( '.add', 'click', function(){
        var objParent = $( this ).parents( 'tr' );
        var pid = objParent.attr( 'data-id' );
        var url = ajaxUrl + '/add/pid/' + $( this ).parents( 'tr' ).attr( 'data-id' );
        objParent.addClass( 'current' ).siblings().removeClass( 'current' );
        isAdd = true;
        getAddView( url );
        return false;
    });

    //添加根权限
    $( '#add_page' ).click( function(){
        var url = ajaxUrl + '/add' ;
        isAdd = true;
        getAddView( url );
        return false;
    });

    //获取添加页面
    function getAddView( url )
    {
        $.get( url, function( ret ){
            if( ! ret.status )
            {
                $( '#page_add' ).html( ret.view );
                showPageAdd();
            }
            else
            {
                toastr.error( ret.msg );
            }
            
        }, 'json' ).error( function(){
            toastr.error( '网络不通' );
        });
    }
  
    var scrollTop = 0;
    var arrDel = [];
    //显示添加页面
    function showPageAdd( title )
    {
        scrollTop = document.body.scrollTop;
        var title = isAdd ? '添加' : '编辑';
        //页签显示
        $( 'a#add_page' ).text( title ).parent().addClass( 'active' ).siblings().removeClass( 'active' );
        $( '#page_list' ).hide();
        $( '#page_add' ).show();//页面显示
        arrDel = []; //清空删除
    }
    
    //显示列表页面
    function showPagelist()
    {
        //页签显示
        $( 'a#list_page' ).parent().addClass( 'active' ).siblings().removeClass( 'active' );
        $( 'a#add_page' ).text( '添加' );
        //页面显示
        $( '#page_add' ).hide();
        $( '#page_list' ).show();
        
        $( 'body' ).scrollTop( scrollTop );
    }
    
    //点击列表页
    $( '#list_page' ).click( function(){
        showPagelist();
    });
    
    //一级的点击
    objList.delegate( '.first', 'click', function(){
        //二级的状态都归位
        $( '.second' ).hide().find( '.name .glyphicon' ).removeClass( 'glyphicon-menu-down' ).addClass( 'glyphicon-menu-right' );
        showSub( $( this ), 'second' );
    } );
    //二级的点击
    objList.delegate( '.second', 'click', function(){
        showSub( $( this ), 'third' );
    } );

    //获取数据
    function showSub( obj, level )
    {
        var id = obj.attr( 'data-id' );
        var pid = obj.attr( 'data-pid' );
        if( ! id )
        {
            return false;
        }

        //同级别的状态
        obj.siblings( '.pid' + pid ).find( '.name .glyphicon' ).removeClass( 'glyphicon-menu-down' ).addClass( 'glyphicon-menu-right' );
        //当前的状态
        var icon = obj.find( '.name .glyphicon' );
        if( icon.hasClass( 'glyphicon-menu-right'))
        {
            icon.removeClass( 'glyphicon-menu-right' ).addClass( 'glyphicon-menu-down');//此时处于展开状态
        }
        else
        {
            icon.removeClass( 'glyphicon-menu-down' ).addClass( 'glyphicon-menu-right'); //处于原始状态
        }
        $( '.third, .add' ).hide();
        obj.find( '.add' ).show();
        
        var sub = $( '.pid' +　id );
        if( sub.length ) //已经存在子菜单了
        {
            if(  icon.hasClass( 'glyphicon-menu-right')) //处于原始状态
            {
                sub.hide();//子菜单恢复
            }
            else
            {
                sub.show();//子菜单展开
            }   
            return false;
        }

        $.get( ajaxUrl + '/getSub', { id : id }, function( ret ){
            if( ! ret.status )
            {
                var target = obj.next();
                for( var i in ret.data )
                {
                    var clone = $( '.template' ).clone().removeClass( 'template').addClass( level + ' pid' + id ) 
                            .attr({ 'data-id' :  ret.data[i].id, 'data-pid' : id });
                    clone.find( '.pri-name' ).text( ret.data[i].name );
                    clone.find( '.pri-sort').text( ret.data[i].sort );
                    if( target.length )
                    {
                        target.before( clone );
                    }
                    else
                    {
                       $( 'tbody' ).append( clone );
                    }
                }
            }
            else
            {
                toastr.error( ret.msg );
            }
        }, 'json');
    }
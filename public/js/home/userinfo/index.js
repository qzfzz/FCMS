/**
 * 
 * 个人信息管理中心
 * 
 * 个人信息
 * 		修改昵称
 * 		修改密码
 * 		修改邮箱
 * 		修改手机号码
 * 
 * @date 2016-05-06
 * 
 */

$(function(){
	/*---------------清除所有input错误样式信息-----------------*/
	$( 'input' ).each(function(){
		$(this).focus(function(){
			$( this ).parent( 'div.form-group' ).removeClass( 'has-error' );
		});
	});
	
	var passLevel = checkStrong( pass );
	switch( parseInt( passLevel ) )
	{
		case 1:
			var strLevel = '★☆☆☆';
		break;
		case 2:
			var strLevel = '★★☆☆';
		break;
		case 3:
			var strLevel = '★★★☆';
		break;
		case 4:
			var strLevel = '★★★★';
		break;
	}
	$( '#set_password' ).find( 'div.user_row_content span' ).html( strLevel );
	
});
/*---------------点击每行内容出现下拉菜单-----------------*/
$( 'div.fshop_content_row' ).each( function(){
	$( this ).click( function(){
		if( 'none' == $( this ).next( "div.fshop_content_dropdown" ).css( 'display' ) ){
			$( 'div.fshop_content_dropdown' ).fadeOut( 0 );
			$( this ).next( "div.fshop_content_dropdown" ).fadeIn( 300 );
		}else if( 'block' == $( this ).next( "div.fshop_content_dropdown" ).css( 'display' ) ){
			$( this ).next( "div.fshop_content_dropdown" ).fadeOut( 300 );
		}
	} );
} );

/*---------------点击取消收回下拉菜单-----------------*/
$( 'button.concel' ).click( function(){
	$( this ).parents( "div.fshop_content_dropdown" ).fadeOut( 300 );
} );

/*---------------点击确定修改用户昵称-----------------*/
$( 'button.confirm_nickname' ).click(function(){
	var editName = $( 'input#user_nickname' ).val();
	if( !editName || editName.length < 0 )
	{
		$( 'input#user_nickname' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( $( 'input#user_nickname' ).attr( 'placeholder' ) );
		return false;
	}
	if( editName && editName == setName  )
	{
		$( 'input#user_nickname' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( '请勿重复设置正在使用的昵称.' );
		return false;
	}
	
	var objParams = {};
	objParams.username = editName;
	objParams.key = key;
	objParams.token = token;
	
	$.post( '/home/userinfo/upUsername', objParams, function( ret ){
		if( 1 != ret.status )
		{
			$( '#set_nickname' ).find( 'div.user_row_content' ).html( ret.username );
			$( 'button.confirm_nickname' ).parents( 'div.fshop_content_dropdown' ).fadeOut( 300 );
		}
		else
			toastr.error( ret.msg );
		
		key = ret.key;
		token = ret.token;
	}, 'json' );
	
});
/*---------------点击确定修改用户密码-----------------*/
$( 'button.confirm_password' ).click(function(){
	var oldPass = $( 'input#user_password' ).val();
	var newPass = $( 'input#user_newpassword' ).val();
	var cfmPass = $( 'input#user_repassword' ).val();
	if( !oldPass || oldPass.length < 0 )
	{
		$( 'input#user_password' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( $( 'input#user_password' ).attr( 'placeholder' ) );
		return false;
	}
	if( !newPass || newPass.length < 0 )
	{
		$( 'input#user_newpassword' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( $( 'input#user_newpassword' ).attr( 'placeholder' ) );
		return false;
	}
	if( !cfmPass || cfmPass.length < 0 )
	{
		$( 'input#user_repassword' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( $( 'input#user_repassword' ).attr( 'placeholder' ) );
		return false;
	}
	if( MD5( newPass ) != MD5( cfmPass )  )
	{
		$( 'input#user_repassword' ).parents( 'div.form-group' ).addClass( 'has-error' );
		
		toastr.error( '两次输入密码不一致,请重新输入.' );
		$( 'input#user_repassword' ).focus();
		return false;
	}
	
	var level = checkStrong( newPass );
	
	var objParams = {};
	objParams.oldpass = MD5( oldPass );
	objParams.passwd = MD5( newPass );
	objParams.key = key;
	objParams.token = token;
	
	$.post( '/home/userinfo/upPasswd', objParams, function( ret ){
		if( 1 != ret.status )
		{
			toastr.success( ret.msg );
			switch( parseInt( level ) )
			{
				case 1:
					var strLevel = '★☆☆☆';
				break;
				case 2:
					var strLevel = '★★☆☆';
				break;
				case 3:
					var strLevel = '★★★☆';
				break;
				case 4:
					var strLevel = '★★★★';
				break;
			}
			$( '#set_password' ).find( 'div.user_row_content span' ).html( strLevel );
			$( 'button.confirm_email' ).parents( 'div.fshop_content_dropdown' ).fadeOut( 300 );
		}
		else
			toastr.error( ret.msg );
		
		key = ret.key;
		token = ret.token;
	}, 'json' );
	
});
/*---------------点击确定修改用户邮箱-----------------*/
$( 'button.confirm_email' ).click(function(){
	var editEmail = $( 'input#user_email' ).val();
	var regEmail = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
	if( !editEmail || editEmail.length < 0 )
	{
		$( 'input#user_email' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( $( 'input#user_email' ).attr( 'placeholder' ) );
		return false;
	}
	if( !regEmail.test( editEmail ) )
	{
		$( 'input#user_email' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( '请输入有效的邮箱地址！' );
		return false;
	}
	var objParams = {};
	objParams.email = editEmail;
	objParams.key = key;
	objParams.token = token;
	
	$.post( '/home/userinfo/upEmail', objParams, function( ret ){
		if( 1 != ret.status )
		{
			$( '#set_email' ).find( 'div.user_row_content' ).html( ret.phone );
			$( 'button.confirm_email' ).parents( 'div.fshop_content_dropdown' ).fadeOut( 300 );
		}
		else
			toastr.error( ret.msg );
		
		key = ret.key;
		token = ret.token;
	}, 'json' );
	
});
/*---------------点击确定修改用户手机号码-----------------*/
$( 'button.confirm_phone' ).click(function(){
	var editPhone = $( 'input#user_phone' ).val();
	var regPhone = /^(\+86)?1[34578]{1}\d{9}$/;
	if( !editPhone || editPhone.length < 0 )
	{
		$( 'input#user_phone' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( $( 'input#user_phone' ).attr( 'placeholder' ) );
		return false;
	}
	if( !regPhone.test( editPhone ) )
	{
		$( 'input#user_phone' ).parents( 'div.form-group' ).addClass( 'has-error' );
		toastr.error( '请输入正确的手机号码！' );
		return false;
	}
	var objParams = {};
	objParams.phone = editPhone;
	objParams.key = key;
	objParams.token = token;
	
	$.post( '/home/userinfo/upPhone', objParams, function( ret ){
		if( 1 != ret.status )
		{
			$( '#set_phone' ).find( 'div.user_row_content' ).html( ret.phone );
			$( 'button.confirm_phone' ).parents( 'div.fshop_content_dropdown' ).fadeOut( 300 );
		}
		else
			toastr.error( ret.msg );
		
		key = ret.key;
		token = ret.token;
	}, 'json' );
	
});

function checkStrong(sValue) 
{
    var modes = 0;
    //正则表达式验证符合要求的
    if (sValue.length < 1) return modes;
    if (/\d/.test(sValue)) modes++; //数字
    if (/[a-z]/.test(sValue)) modes++; //小写
    if (/[A-Z]/.test(sValue)) modes++; //大写  
    if (/\W/.test(sValue)) modes++; //特殊字符
    
   //逻辑处理
    switch (modes) {
        case 1:
            return 1;
        break;
        case 2:
            return 2;
        case 3:
        case 4:
            return sValue.length < 12 ? 3 : 4
        break;
    }
}
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>modal page dialog</title>
<link rel="stylesheet" href="/css/admin/base.css">
<link rel="stylesheet" type="text/css" href="/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet"  href="/css/admin/font-awesome.min.css" >
<link rel="stylesheet" type="text/css" href="/bootstrap/colorpickersliders/bootstrap.colorpickersliders.css">

<style>
.pos{
	overflow-y:scroll;
}
div.sel_skin{
	width:100%;
	height:auto;
	overflow:hidden;
}
div.text{
	margin:0;
	height:40px;
	padding-top:15px;
	border-bottom:2px solid #E8E8E8;
	font-size:14px;
	font-weight:bold;
}
ul li{
	float:left;
	text-align:Justify;
	text-justify:inter-ideograph;
	list-style:none;
	margin:5px 0 0 10px;
}
div.cont{
	margin: 0 auto;
	height:50px;
	padding-top:10px;
}
div.title{
	float:left;
	width:80px;
}
div.sel_cont{
	float:left;
}

/* -------- 复选框和伪单选框按钮 -------------- */
input[type="checkbox"] {
	display: none;
}

input[type="checkbox"] + label {
	cursor: pointer;
	font-size: 1em;
}

/* ==================================================================== */
/* CHECKBOX        ---------------------------------------------------- */
/* ==================================================================== */
[id^="checkbox-"] + label {
	background-color: #FFF;
	border: 2px solid #D6846A;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
	padding: 9px;
	border-radius: 5px;
	display: inline-block;
	overflow: hidden;
	position: relative;
	margin-right: 30px;
}

[id^="checkbox-"] + label:active {
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}

[id^="checkbox-"] + label:after {
	position: absolute;
	top: 99px;
	-webkit-transition: all 0.3s linear;
	transition: all 0.3s linear;	
	content: '\2715';
	color: #947975;
	width: 100%;
	text-align: center;
	font-size: 1.4em;
	padding: 1px 0 0 0;
	left: 0px;

}

[id^="checkbox-"]:checked + label:after {
	top: 0px;
}
[id^="checkbox-"]:checked + label {
	background-color: #F7F2EC;
	border: 2px solid #C47D62;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
}
.checkbox:checked + label:after {
	left: 0px;
}
.checkbox + label {
	width: 30px;
	height: 30px;
}


/* ==================================================================== */
/* radio           ---------------------------------------------------- */
/* ==================================================================== */
[id^="checkboxradio-"] + label {
	background-color: #FFF;
	border: 1px solid #C1CACA;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
	padding: 9px;
	border-radius: 1000px;
	display: inline-block;
	position: relative;
	margin-top:10px;
}

[id^="checkboxradio-"] + label:active {
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.2);
}

[id^="checkboxradio-"]:checked + label {
	background-color: #ECF2F7;
	border: 1px solid #92A1AC;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
	color: #243441;
}

[id^="checkboxradio-"]:checked + label:before {
	content: ' ';
	border-radius: 100px;
	position: absolute;
	background: #253C4B;
	opacity: 0.8;
	display: none;
	box-shadow: inset 0 15px 23px -10px rgba(187, 230, 240, 0.3), 0 2px 2px rgba(0, 0, 0, 0.1);
	top: 3px;
	left: 3px;
	width: 12px;
	height: 12px;
}

[id^="checkboxradio-"]:checked + label:before {
	content: ' ';
	display: block;
}

/* -------- 复选框和伪单选框按钮 -------------- */


div.row label{
	font-weight:400;
	float:left;
	padding-top:2%;
	margin-right:2%;
}
div.row.margin{
	margin-left:1%;
}
div.row.pos{
	margin-top:1%;
}
.form-control-sm {
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display: block;
    font-size: 14px;
    height: 34px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 60%;
}
div .chkshow input[type="checkbox"]{
	display: block;
}
div.mt-chechbox input[type="checkbox"]{
	float:left;
}
.form-control-slm {
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display: block;
    font-size: 14px;
    height: 34px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 10%;
}
div.checkbox, div.radio {
    display: block;
    margin-bottom: 10px;
    margin-top: 0;
    position: relative;
}
div#add-links span{
	cursor:pointer;
}
.laypos img{
	cursor:pointer;
}
.laypos img:hover{
	border:1px solid #4cae4c;
}
.laypos img.active{
	border:1px solid #4cae4c;
}
.laypos p{
	margin-top: -5%;
	text-align:center;
	padding-right:50%;
}
#s-hot{
	padding-left:2%;
}

/*  --------- 图标晃动 --------------*/
.operate:hover {
    cursor:pointer;
    color:green;
}
.skinimg font{
	padding-left:8%;
}
.skinimg i:hover{-webkit-animation: tada 1s .2s ease both;-moz-animation: tada 1s .2s ease both;}
@-webkit-keyframes tada{
	0%{-webkit-transform:scale(1);}
	10%, 
	20%{-webkit-transform:scale(0.9) rotate(-3deg);}
	30%, 50%, 70%, 
	90%{-webkit-transform:scale(1.1) rotate(3deg);}
	40%, 60%, 
	80%{-webkit-transform:scale(1.1) rotate(-3deg);}
	100%{-webkit-transform:scale(1) rotate(0);}
}
@-moz-keyframes tada{
	0%{-moz-transform:scale(1);}
	10%, 
	20%{-moz-transform:scale(0.9) rotate(-3deg);}
	30%, 50%, 70%, 
	90%{-moz-transform:scale(1.1) rotate(3deg);}
	40%, 60%, 
	80%{-moz-transform:scale(1.1) rotate(-3deg);}
	100%{-moz-transform:scale(1) rotate(0);}
}
</style>
</head>
<body>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#layoutModal">
  	弹出配置信息格式dialog
</button>

<div class="modal fade" id="layoutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
		   <ul class="nav nav-tabs nav-justified">
			  <li role="presentation" class="active"><a href="#skin" data-toggle='tab'>皮肤</a></li>
			  <li role="presentation"><a href="#modal" data-toggle='tab'>模板</a></li>
			  <li role="presentation"><a href="#setting" data-toggle='tab'>配置</a></li>
			  <li role="presentation"><a href="#tab" data-toggle='tab'>选项卡</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="skin">
					<div class="sel_skin">
						<div class="text">皮肤选择</div>
						<div class="cont">
							<div class="col-md-2 col-sm-3 col-xs-2">皮肤模板</div>
							<div class="col-md-9 col-sm-8 col-xs-9" id="skin_list">
								<input id="checkbox-1" class="checkbox" type="checkbox">
								<label for="checkbox-1"></label>
								<input id="checkbox-2" class="checkbox" type="checkbox">
								<label for="checkbox-2"></label>
								<input id="checkbox-3" class="checkbox" type="checkbox">
								<label for="checkbox-3"></label>
								<input id="checkbox-4" class="checkbox" type="checkbox">
								<label for="checkbox-4"></label>
							</div>
						</div>
					</div>
					<div class="sel_skin">
						<div class="text">个性化皮肤</div>
						<div class="cont">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row margin">
									<label for="border-size">边框颜色</label>
									<input type="text" class="form-control-sm pickupColor" id="border-color" name="" value="" data-color-format="hex" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row margin">
									<label for="border-size">边框大小</label>
									<div class="input-group">
										<input type="text" class="form-control" id="border-size" name="" value="" />
										<span class="input-group-addon">px</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row margin">
									<label for="border-margin">模块间距</label>
									<div class="input-group">
										<input type="text" class="form-control" id="border-margin" name="" value="" />
										<span class="input-group-addon">px</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="sel_skin">
						<div class="text">标题栏</div>
						<div class="cont">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row margin">
									<label for="title">标题</label>
									<input type="text" class="form-control-sm" id="title" name="" value="" />
								</div>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div class="row margin" style="margin-top:4%; margin-left:5%;">
									<label style="margin-top:3%;" for="color">颜色</label>
									<input type="text" class="form-control-sm pickupColor" style="margin-left:22%;" id="color" name="" value="" data-color-format="hex" />
								</div>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div class="row margin" style="margin-top:4%;">
									<label style="margin-top:3%;" for="font-size">字号</label>
									<input type="text" class="form-control-sm" id="font-size" name="" value="" />
								</div>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div class="row margin" style="margin-top:4%;">
									<label style="margin-top:3%;" for="position">位置</label>
									<select class="form-control-sm" id="position">
										<option value="0">不选择</option>
										<option value="1">居左</option>
										<option value="2">居中</option>
										<option value="3">居右</option>
									</select>
								</div>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6">
								<div style="float:left; width:50px;">
									<input type="checkbox" id="checkboxradio-2" />
									<label for="checkboxradio-2"></label>加粗
								</div>
								<div style="float:left;">
									<input type="checkbox" id="checkboxradio-3" />
									<label for="checkboxradio-3"></label>下划线
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row margin" style="margin-top:1%;">
									<label for="url_links">链接</label>
									<input type="text" class="form-control-sm" id="url_links" name="" value="http://" />
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row margin chkshow">
									<label for="url_links">边框</label>
									<div class="checkbox">
										<label><input type="checkbox" name="" value="" /> 上</label>
										<label><input type="checkbox" name="" value="" /> 右</label>
										<label><input type="checkbox" name="" value="" /> 下</label>
										<label><input type="checkbox" name="" value="" /> 左</label>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row margin" style="margin-top:1%;">
									<label for="bg-color">背景颜色</label>
									<input type="text" class="form-control-sm pickupColor" id="bg-color" name="" value="" data-color-format="hex" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="modal">
					<ul class="nav nav-pills">
					  <li role="presentation" class="active"><a href="#lately" data-toggle='tab'>最近使用</a></li>
					  <li role="presentation"><a href="#my" data-toggle='tab'>我的创建</a></li>
					  <li role="presentation"><a href="#system" data-toggle='tab'>系统模板</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="lately">
							<div class="row pos">
								<div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="1" class="thumbnail" />
						       		<p>玫瑰红</p>
						        </div>
						        <div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="2" class="thumbnail" />
						       		<p>最新使用-魅蓝</p>
						        </div>
						        <div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="3" class="thumbnail" />
						       		<p>最新使用-玫瑰白</p>
						        </div>
							</div>
						</div>
						<div class="tab-pane" id="my">
							<div class="row pos">
								<div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="1" class="thumbnail" />
						       		<p>我的创建-白白白</p>
						        </div>
						        <div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="2" class="thumbnail" />
						       		<p>我的创建-绿绿绿</p>
						        </div>
						        <div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="3" class="thumbnail" />
						       		<p>我的创建-澄澄橙</p>
						        </div>
							</div>
						</div>
						<div class="tab-pane" id="system">
							<div class="row pos">
								<div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="1" class="thumbnail" />
						       		<p>系统模板-深空灰</p>
						        </div>
						        <div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="2" class="thumbnail" />
						       		<p>系统模板-土豪金</p>
						        </div>
						        <div class="col-xs-4 col-md-4 laypos">
						       		<img src="/img/mall/1.png" data-id="3" class="thumbnail" />
						       		<p>系统模板-玫瑰金</p>
						        </div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="setting">
					<div class="sel_skin">
						<!---- 搜索配置  ---->
<!-- 						<div class="cont"> -->
<!-- 							 <div class="col-md-12 col-sm-12 col-xs-12"> -->
<!-- 								<div class="row margin"> -->
<!-- 									<label for="keys">默认关键字配置</label> -->
<!-- 									<input type="text" class="form-control-sm" id="keys" name="" value="" /> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 							<div class="col-md-12 col-sm-12 col-xs-12"> -->
<!-- 								<div class="row margin" style="margin-top: 1%;"> -->
<!-- 									<label for="s-hot">配置热搜词</label> -->
<!-- 									<div class="input-group" id="s-hot"> -->
<!-- 										<div class="col-md-4 col-sm-4 col-xs-3"><input type="text" class="form-control" name="" value="" /></div> -->
<!-- 										<div class="col-md-4 col-sm-4 col-xs-3"><input type="text" class="form-control" name="" value="" /></div> -->
<!-- 										<div class="col-md-4 col-sm-4 col-xs-3"><input type="text" class="form-control" name="" value="" /></div> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 							<div class="col-md-12 col-sm-12 col-xs-12"> -->
<!-- 								<div class="row margin chkshow"> -->
<!-- 									<label for="s_price">价格刷选</label> -->
<!-- 									<div class="checkbox" style="margin-left:14%"> -->
<!-- 										<label><input type="checkbox" name="" id="s_price" value="" /></label> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<!----  搜索配置 ---->
						<!----  商品配置  ---->
						<iframe id="add_turn" name="add_turn" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" src="http://shop.xxxx.dev/public/tpl/dialog/iframe_goods.php" class="col-md-12 col-sm-12 col-xs-12"></iframe>
						<!----  商品配置  ---->
						<!----  导航配置  ---->
<!-- 						<div class="table-responsive"> -->
<!-- 							<table class="table table-hover table-bordered" style="margin-top:2%"> -->
<!-- 								<tr> -->
<!-- 									<th>导航名称</th> -->
<!-- 									<th>链接地址</th> -->
<!-- 									<th>操作</th> -->
<!-- 								</tr> -->
<!-- 								<tr id="insert_form"> -->
<!-- 									<td><input type="text" name="nav_name" class="form-control" id="nav_name" placeholder="请输入导航名称" value="" /></td> -->
<!-- 									<td><input type="text" name="nav_url" class="form-control" id="nav_url" placeholder="请输入导航链接地址" value="http://" /></td> -->
<!-- 									<td class="skinimg text-center"> -->
<!-- 										<font size="4"><i class="fa fa-plus-square operate"></i></font> -->
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 							</table> -->
<!--  						</div> -->
						<!----  导航配置  ---->
						<!--  自定义区域配置  -->
<!-- 						<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;"> -->
<!-- 							<script type="text/plain" id="custom_content"></script> -->
<!-- 						</div> -->
						<!--  自定义区域配置  -->
						<!--  轮播图配置  -->
<!-- 						<iframe id="add_turn" name="add_turn" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" src="http://shop.xxxx.dev/public/tpl/dialog/iframe_trun.php" class="col-md-12 col-sm-12 col-xs-12"></iframe> -->
						<!--  轮播图配置  -->
					</div>
				</div>
				<div class="tab-pane" id="tab">
					<div class="sel_skin">
						<div class="text">添加名称和链接 <a href="#" class="pull-right">更新全部</a></div>
						<div class="cont" id="add-links">
						
							<div class="form-group">
								<div class="col-sm-6 col-md-4">
									<input type="text" class="form-control" name="link-name" value="" />
								</div>
								<div class="input-group">
									<input type="text" class="form-control" name="link-url" value="" />
									<span class="input-group-addon">
										<i class="fa fa-plus-square"></i>
									</span>
								</div>
							</div>
							
						</div>
						
					</div>
					<div class="sel_skin">
						<div class="text">位置</div>
						<div class="cont">
							<div class="col-md-4 col-sm-6">
								<div class="row margin chkshow" style="margin-top:6%;">
									<div class="checkbox">
										<label><input type="checkbox" name="" value="" /> 开启多选项卡</label>
									</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6">
								<div class="row margin" style="margin-top:4%;">
									<label style="margin-top:3%;" for="margin-size">边距大小</label>
									<div class="input-group">
										<input type="text" class="form-control" id="margin-size" name="" value="" />
										<span class="input-group-addon">px</span>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="row margin" style="margin-top:1%;">
									<label for="url_links">标题边框显示</label>
									<div class="radio">
										<label><input type="radio" name="title-border" value="1" /> 左上</label>
										<label><input type="radio" name="title-border" value="2" /> 左下</label>
										<label><input type="radio" name="title-border" value="3" /> 右上</label>
										<label><input type="radio" name="title-border" value="4" /> 右下</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning pull-left" data-option="refresh">重新载入</button>
        <button type="button" class="btn btn-success pull-left" data-option="save">保存修改</button>
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消操作</button>
      </div>
    </div>
  </div>
</div>

<script src="/js/jquery/jquery-1.11.1.min.js"></script>
<script src="/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script type='text/javascript' src='/bootstrap/colorpickersliders/tinycolor.js'></script>
<script src="/bootstrap/colorpickersliders/bootstrap.colorpickersliders.js"></script>

<script src="/ueditor/ueditor.config.js"></script>
<script src="/ueditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript">


</script>
</body>
</html>
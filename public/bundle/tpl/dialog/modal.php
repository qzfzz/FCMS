<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>modal page dialog</title>
<link rel="stylesheet" href="/css/admin/base.css">
<link rel="stylesheet" type="text/css" href="/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet"  href="/css/admin/font-awesome.min.css" >
<style>
.pos{
	height:480px;
	overflow-y:scroll;
}
.line_buttom{
	height:110px;
	overflow:hidden;
	border-bottom:1px dashed #ccc;
}
.line_buttom.pos{
	margin-top:5%;
}
p.text{
	font-size:12px;
}
div.pos_btn{
	padding-top:5%;
}
.button{
	width:50px;
	height:25px;;
	background-color:RGB(252,246,234);
	border: 1px solid #ccc;
}
</style>
</head>
<body>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#layoutModal">
  	弹出布局格式dialog
</button>

<div class="modal fade" id="layoutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width: 700px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">添加模块</h4>
      </div>
      <div class="modal-body pos">
		    <ul class='nav nav-pills nav-stacked  col-md-4'>
		      <li class='active'><a href='#tab1' data-toggle='tab'>店铺基础模块</a></li>
		      <li><a href='#tab2' data-toggle='tab'>选项二</a></li>
		      <li><a href='#tab3' data-toggle='tab'>选项三</a></li>
		      <li><a href='#tab4' data-toggle='tab'>选项四</a></li>
		      <li><a href='#tab5' data-toggle='tab'>选项五</a></li>
		    </ul>
		    <div class='tab-content col-md-8'>
		      <div class='tab-pane active' id='tab1'>
		      	<div class="line_buttom">
		      		<img src="/img/mall/cat1.png" class="thumbnail col-sm-1 col-md-2"/>
			      	<div class="col-xs-8 col-xs-8">
		      			<h5><strong>商品类目</strong></h5>
		      			<p class="text">
		      				只适用于商家。展示商家在后台添加的自定义商品类目。用户点击这些类目 可以快速查找到该类目下的所有商品。（支持到三级类目）
		      			</p>
		      		</div>
		      		<div class="col-xs-2 col-md-2 pos_btn"><button type="button" class="button">加载</button></div>
		      	</div>
		      	<div class="line_buttom pos">
		      		<img src="/img/mall/cat1.png" class="thumbnail col-sm-1 col-md-2" />
			      	<div class="col-xs-8 col-xs-8">
		      			<h5><strong>h5. Bootstrap heading</strong></h5>
		      			<p class="text">
		      				o呵呵呵呵呵呵额呵呵呵呵呵enenenennenenenene
		      			sadjkasdjl
		      			</p>
		      		</div>
		      		<div class="col-xs-2 col-md-2 pos_btn"><button type="button" class="button">加载</button></div>
		      	</div>
		      	<div class="line_buttom pos">
		      		<img src="/img/mall/cat1.png" class="thumbnail col-sm-1 col-md-2" />
			      	<div class="col-xs-8 col-xs-8">
		      			<h5><strong>h5. Bootstrap heading</strong></h5>
		      			<p class="text">
		      				o呵呵呵呵呵呵额呵呵呵呵呵enenenennenenenene
		      			sadjkasdjl
		      			</p>
		      		</div>
		      		<div class="col-xs-2 col-md-2 pos_btn"><button  type="button" class="button">加载</button></div>
		      	</div>
		      	<div class="line_buttom pos">
		      		<img src="/img/mall/cat1.png" class="thumbnail col-sm-1 col-md-2" />
			      	<div class="col-xs-8 col-xs-8">
		      			<h5><strong>h5. Bootstrap heading</strong></h5>
		      			<p class="text">
		      				o呵呵呵呵呵呵额呵呵呵呵呵enenenennenenenene
		      			sadjkasdjl
		      			</p>
		      		</div>
		      		<div class="col-xs-2 col-md-2 pos_btn"><button  type="button" class="button">加载</button></div>
		      	</div>
		      </div>
		      <div class='tab-pane line_buttom' id='tab2'></div>
		      <div class='tab-pane line_buttom' id='tab3'>我是选项卡三的内容</div>
		      <div class='tab-pane line_buttom' id='tab4'>我是选项卡四的内容</div>
		      <div class='tab-pane line_buttom' id='tab5'>我是选项卡五的内容</div>
		    </div> 
      </div>
    </div>
  </div>
</div>

<script src="/js/jquery/jquery-1.11.1.min.js"></script>
<script src="/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</body>
</html>
<!doctype html>
<html>
<head>
<title>轮播图设置</title>
<link rel="stylesheet" href="/css/admin/base.css">
<link rel="stylesheet" type="text/css" href="/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet"  href="/css/admin/font-awesome.min.css" >
<style>
/*  --------- 图标晃动 --------------*/
.operate:hover {
    cursor:pointer;
    color:green;
}
div .position{
	color: #b2b2b2;
	line-height: 34px;
	margin: 0;
    font-size:12px;
	marin-bottom:1%;
}
.skinimg font{
	padding-left:8%;
}
.skinimg i:hover{-webkit-animation: tada 1s .2s ease both;-moz-animation: tada 1s .2s ease both;}
@-webkit-keyframes tada{
	0%{-webkit-transform:scale(1);}
	10%, 
	20%{-webkit-transform:scale(0.9) rotate(-3deg);}
	30%, 50%, 70%, 
	90%{-webkit-transform:scale(1.1) rotate(3deg);}
	40%, 60%, 
	80%{-webkit-transform:scale(1.1) rotate(-3deg);}
	100%{-webkit-transform:scale(1) rotate(0);}
}
@-moz-keyframes tada{
	0%{-moz-transform:scale(1);}
	10%, 
	20%{-moz-transform:scale(0.9) rotate(-3deg);}
	30%, 50%, 70%, 
	90%{-moz-transform:scale(1.1) rotate(3deg);}
	40%, 60%, 
	80%{-moz-transform:scale(1.1) rotate(-3deg);}
	100%{-moz-transform:scale(1) rotate(0);}
}
</style>
</head>
<body style="min-height:500px">
<div class="table-responsive" id="content">

	<div class="btn-group" role="group" style="margin-top:2%">
	  <button type="button" class="btn btn-default id_input">ID录入</button>
	  <button type="button" class="btn btn-default select_input">筛选录入</button>
	  <button type="button" class="btn btn-danger">删除</button>
	</div>
	
	<table class="table table-hover table-bordered" style="margin-top:1%" id="goods_list">
		<tr>
			<th><input type="checkbox" name="chkall" id="chkall" /> 选择</th>
			<th>商品ID</th>
			<th>商品名称</th>
			<th>价格</th>
			<th>库存量</th>
		</tr>
		<tr>
			<td><input type="checkbox" name="chkitem[]" value="1"/></td>
			<td>890</td>
			<td>橙子</td>
			<td>8.8</td>
			<td>1000</td>
		</tr>
	</table>
	<nav class="text-right" >
      <ul class="pagination pagination-sm" style="margin-top:0"> 
           <li class="disabled"><a href="#" >&laquo;</a></li>
           <li><a href="#">1</a></li>
           <li><a href="#"><span >2</span></a></li>
           <li class="active"><a href="#"><span >3</span></a></li>
           <li><a href="4">4</a></li>
           <li><a>...</a></li>
           <li><a href="#">15</a></li>
           <li class="disabled"><a href="#" >&raquo;</a></li>
       </ul>
    </nav>
</div>
</body>

<div class="modal fade add_push_goods_input">
  <div class="modal-dialog" style="margin-top:20%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">添加推荐商品</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
    		<div class="col-md-12 col-sm-12 col-xs-12 position"><i class="fa fa-info-circle text-danger"></i> 请输入商品编号(SKU ID) 编号与编号之间用英文逗号隔开,最多可录入200个ID</div>
      		<div class="col-md-12 col-sm-12 col-xs-12">
	        	<textarea class="form-control" rows="3"></textarea>
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-left">录入</button>
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade add_push_goods_select">
 <div class="modal-dialog modal-lg" role="document" style="margin-top:20%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">筛选录入推荐商品</h4>
      </div>
      <div class="modal-body">
      	<div class="row" style="margin-buttom:2%">
    		<div class="col-md-12 col-sm-12 col-xs-12" >
    			<div class="input-group">
    				<label>请选择筛选条件：</label>
    				<select name="sel_cond">
    					<option value="0">销量</option>
    					<option value="1">价格</option>
    				</select>
    				<select id="sel_cate">
    					<option value="0">请选择分类</option>
    					<option value="1">毛衣</option>
    					<option value="2">皮鞋</option>
    					<option value="3">围巾</option>
    				</select>
    				<select id="sel_sec_cate">
    					<option value="0">二级分类</option>
    					<option value="1">红毛衣</option>
    					<option value="2">白毛衣</option>
    					<option value="3">绿毛衣</option>
    				</select>
    				<button type="button" class="btn btn-sm btn-warning">开始筛选</button>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-12 col-sm-12 col-xs-12">
	        		<div class="table-responsive">
						<table class="table table-hover table-bordered" style="margin-top:2%">
							<tr>
								<th>ID</th>
								<th>商品名称</th>
								<th>商品价格</th>
								<th>库存剩余量</th>
							</tr>
							<tr>
								<td>1000</td>
								<td>橙子</td>
								<td>8.8</td>
								<td>1000</td>
							</tr>
						</table>
					</div>
      			</div>
    		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-left">录入</button>
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
      </div>
    </div>
  </div>
</div>


<script src="/js/jquery/jquery-1.11.1.min.js"></script>
<script src="/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){
	//id 录入
	$( 'button.id_input' ).click(function(){
		$( 'div.add_push_goods_input' ).modal({
			backdrop:false,
			keyboard:true,
		});
	});
	//筛选录入
	$( 'button.select_input' ).click(function(){
		$( 'div.add_push_goods_select' ).modal({
			backdrop:false,
			keyboard:true,
		});
	});

	//全选
	$( '#chkall' ).click(function(){
		var chkstate = $( this ).prop( 'checked' );
		if( false == chkstate )
			 $( 'input[name="chkitem[]"]' ).prop( 'checked', false );
		else
			 $( 'input[name="chkitem[]"]' ).prop( 'checked', true );
	});
});

</script>
</html>
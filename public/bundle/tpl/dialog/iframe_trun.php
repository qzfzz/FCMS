<!doctype html>
<html>
<head>
<title>轮播图设置</title>
<link rel="stylesheet" href="/css/admin/base.css">
<link rel="stylesheet" type="text/css" href="/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet"  href="/css/admin/font-awesome.min.css" >
<style>
/*  --------- 图标晃动 --------------*/
.operate:hover {
    cursor:pointer;
    color:green;
}
.skinimg font{
	padding-left:8%;
}
.skinimg i:hover{-webkit-animation: tada 1s .2s ease both;-moz-animation: tada 1s .2s ease both;}
@-webkit-keyframes tada{
	0%{-webkit-transform:scale(1);}
	10%, 
	20%{-webkit-transform:scale(0.9) rotate(-3deg);}
	30%, 50%, 70%, 
	90%{-webkit-transform:scale(1.1) rotate(3deg);}
	40%, 60%, 
	80%{-webkit-transform:scale(1.1) rotate(-3deg);}
	100%{-webkit-transform:scale(1) rotate(0);}
}
@-moz-keyframes tada{
	0%{-moz-transform:scale(1);}
	10%, 
	20%{-moz-transform:scale(0.9) rotate(-3deg);}
	30%, 50%, 70%, 
	90%{-moz-transform:scale(1.1) rotate(3deg);}
	40%, 60%, 
	80%{-moz-transform:scale(1.1) rotate(-3deg);}
	100%{-moz-transform:scale(1) rotate(0);}
}
</style>
</head>
<body style="min-height:500px">
<div class="table-responsive" id="content">
	<table class="table table-hover table-bordered" style="margin-top:2%">
		<tr>
			<th>大图</th>
			<th>小图</th>
			<th>标题</th>
			<th>链接</th>
			<th width="80px;">操作</th>
		</tr>
		<tr id="insert_turn_pic">
			<td id="ins_big_pic">
				<div class="panel panel-default">
				  <div class="panel-heading text-center" style="cursor:pointer;">选择图片</div>
				  <div class="panel-body"></div>
				</div>
				<input type="hidden" name="pic_big_val" id="pic_big_val" value="" />
			</td>
			<td id="ins_sm_pic">
				<div class="panel panel-default">
				  <div class="panel-heading text-center" style="cursor:pointer;">选择图片</div>
				  <div class="panel-body"></div>
				</div>
				<input type="hidden" name="pic_sm_val" id="pic_sm_val" value="" />
			</td>
			<td style="padding-top:3%"><input type="text" name="title" class="form-control" id="title" value="" /></td>
			<td style="padding-top:3%"><input type="text" name="url" class="form-control" id="url" value="http://" /></td>
			<td class="skinimg text-center" style="padding-top:4%">
				<font size="4"><i class="fa fa-plus-square operate"></i></font>
			</td>
		</tr>
		
	</table>
</div>
<script type="text/plain" id="upload_pic_big"></script>
<script type="text/plain" id="upload_pic_sm"></script>
</body>
<script src="/js/jquery/jquery-1.11.1.min.js"></script>
<script src="/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script type='text/javascript' src='/bootstrap/colorpickersliders/tinycolor.js'></script>
<script src="/bootstrap/colorpickersliders/bootstrap.colorpickersliders.js"></script>

<script src="/u/ueditor.config.js"></script>
<script src="/u/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="/u/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript">
var big_editor = UE.getEditor('upload_pic_big',{ 
	//sid: '<?php //echo $this->session->getId();?>',
	bizt:'shop_fixture'
 });

big_editor.ready(function () {
	big_editor.hide();
	big_editor.addListener('beforeInsertImage', function (t, arg) {     //侦听图片上传
         
    	 $( '#icon' ).val( arg[0].src );
     });
	big_editor.setDisabled( [ 'insertimage' ]);
 });

var sm_editor = UE.getEditor('upload_pic_sm',{ 
	//sid: '<?php //echo $this->session->getId();?>',
	bizt:'shop_fixture'
});

sm_editor.ready(function () {
	sm_editor.hide();
	sm_editor.addListener('beforeInsertImage', function (t, arg) {     //侦听图片上传
     
	 $( '#icon' ).val( arg[0].src );
 });
	sm_editor.setDisabled( [ 'insertimage' ]);
});

$(function(){
	//轮播图
	$( '#ins_big_pic div.panel-heading' ).click(function(){
		var myBigImage = big_editor.getDialog("insertimage");
		myBigImage.open();
	});
	$( '#ins_sm_pic div.panel-heading' ).click(function(){
		var mySmImage = sm_editor.getDialog("insertimage");
		mySmImage.open();
	});
	//添加
	$(function(){
		$( 'i.fa-plus-square' ).click(function(){
			var set_name = $( '#title' ).val();
			var set_url	 = $( '#url' ).val();
			if( false != set_name && false != set_url )
			{
				var strAppend = '<tr><td id="ins_big_pic"><div class="panel panel-default">'+
					  			'<div class="panel-body">big_pic1</div> '+
								'</div></td>'+
								'<td><div class="panel panel-default">'+
					  			'<div class="panel-body">sm_pic1</div>'+
								'</div></td>'+
								'<td class="text-center" style="padding-top:3%">'+set_name+'</td>'+
								'<td class="text-center" style="padding-top:3%">'+set_url +'</td>'+
								'<td class="skinimg text-center" style="padding-top:3%"><font size="4"><i class="fa fa-arrow-up operate" onclick="upNav(this);"></i></font>'+
								'<font size="4"><i class="fa fa-arrow-down operate" onclick="downNav(this);"></i></font>'+
								'<font size="4"><i class="fa fa-times operate" onclick="removeNav(this);"></i></font></td></tr>';

				$( '#insert_turn_pic' ).after( strAppend );
			}
		});
	});
});

//上移
function upNav( item )
{
	//是否为第一个
	var top = $( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).prev( 'tr' ).attr( 'id' );
	if( "undefined" != typeof( top ) )
		return false;
	
	var innerHtml = $( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).html();
	$( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).prev( 'tr' ).before( '<tr class="text-center">' +innerHtml +'</tr>' );
	$( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).remove();
}
//下移
function downNav( item )
{
	var innerHtml = $( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).html();
	$( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).next( 'tr' ).after( '<tr class="text-center">' +innerHtml +'</tr>' );
	//是否为最后一个
	var strNextTr = $( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).next( 'tr' ).html();
	if( "undefined" != typeof( strNextTr ) )
		$( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).remove();
}
//删除
function removeNav( item )
{
	$( item ).parent( 'font' ).parent( 'td' ).parent( 'tr' ).remove();
}
</script>
</html>
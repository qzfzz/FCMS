<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>layout page dialog</title>
<link rel="stylesheet" href="/css/admin/base.css">
<link rel="stylesheet" type="text/css" href="/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet"  href="/css/admin/font-awesome.min.css" >
<style>
.pos{
	overflow-y:scroll;
}
.laypos{
	float:left;
	cursor:pointer;
}
.laypos img:hover{
	border:1px solid #4cae4c;
}
.laypos img.active{
	border:1px solid #4cae4c;
}
.laypos p{
	text-align:center;
	margin-top: -10%;
}
</style>
</head>
<body>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#layoutModal">
  	弹出布局格式dialog
</button>

<div class="modal fade" id="layoutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">添加布局</h4>
      </div>
      
      <div class="modal-body pos">
        <div class="col-xs-6 col-md-4 laypos">
       		<img src="/img/mall/1.png" data-id="1" class="thumbnail" />
       		<p>三栏布局(180x390x180)</p>
        </div>
        <div class="col-xs-6 col-md-4 laypos">
       		<img src="/img/mall/1.png" data-id="2" class="thumbnail" />
       		<p>三栏布局(180x390x180)</p>
        </div>
        <div class="col-xs-6 col-md-4 laypos">
       		<img src="/img/mall/1.png" data-id="3" class="thumbnail active" />
       		<p>三栏布局(180x390x180)</p>
        </div>
        <div class="col-xs-6 col-md-4 laypos">
       		<img src="/img/mall/1.png" data-id="4" class="thumbnail" />
       		<p>三栏布局(180x390x180)</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-left">添加</button>
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
      </div>
    </div>
  </div>
</div>

<script src="/js/jquery/jquery-1.11.1.min.js"></script>
<script src="/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){
	$( 'div.laypos' ).each(function(){
		$(this).find( 'img.thumbnail' ).click(function(){
			$( 'div.laypos img.thumbnail' ).removeClass( 'active' );
			$(this).addClass( 'active' );
		});
	});
});
</script>
</body>
</html>
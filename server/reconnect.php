<?php
againContent:
	$iGone = 0;
	try
	{
		return $this->application->handle( $_SERVER[ 'PATH_INFO' ])->getContent();
	}
	catch ( Exception $e )
	{
		if( preg_match( '/SQLSTATE\[/', $e->getMessage() ))
		{//db gone away
			againMysql:
			try
			{
	
				$config = $this->di->get('config');
				$this->application->db->connect(array(
						'adapter'  => $config->database->adapter,
						'host'     => $config->database->host,
						'username' => $config->database->username,
						'password' => $config->database->password,
						'dbname'   => $config->database->dbname,
						'charset'  => $config->database->charset
				));
	
				goto againContent;
			}
			catch( Exception $e )
			{
				if( preg_match( '/^SQLSTATE\[/', $e->getMessage() ))
				{
					if( ++$iGone > 10 )
					{//向运维人员发送邮箱或警告
						$response->status( 500 );
						$response->end( 'mysql:server internal error' );
						return;
					}
					goto againMysql;
				}
	
				$response->end( __LINE__ . $e->getMessage() );
				return;
			}
		}
		else if( preg_match( '/Redis server went away/', $e->getMessage() ) ||
		preg_match( '/Connection lost/', $e->getMessage() ) )//))
		{//reconnect redis server
			$config = $this->di->get('config');
	
			$iRedisGone = 0;
			do
			{
				unset( $this->application->nredis );
				$this->application->nredis = new \Redis();
				if( $this->application->nredis->pconnect(  '127.0.0.1', 6379 ) )
				{
					goto againContent;
				}
			}
			while( ++$iRedisGone < 9 );//连接10次
			 
			// error and send message to administrator
			$response->status( 500 );
			$response->end( 'redis:server internal error' );
			return;
		}
	
	// 	$response->end( __LINE__ .$e->getMessage() . $e->getTraceAsString() );
	// 	return;
	
	}
<?php

namespace helpers;
use enums\HttpServerEnums;
if( ! APP_ROOT  ) return 'Direct Access Deny!';

/**
 *
 * @author fzq
 * @date 2016-06-27
 */
class CommUtils
{

	function __construct( )
	{
	
	}

	/**
	 * Generates an UUID with dash
	 * 32位+4
	 * @author     Anis uddin Ahmad <admin@ajaxray.com>
	 * @param      string  an optional prefix
	 * @return     string  the formatted uuid
	 */
	public static function getUUID()
	{
		return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), 
				mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), 
				mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
// 		mt_srand( $_SERVER[ 'REQUEST_TIME' ] );
// 		$chars = md5( uniqid( mt_rand(), true ) );
// 		$uuid = substr( $chars, 0, 8 ) . '-';
// 		$uuid .= substr( $chars, 8, 4 ) . '-';
// 		$uuid .= substr( $chars, 12, 4 ) . '-';
// 		$uuid .= substr( $chars, 16, 4 ) . '-';
// 		$uuid .= substr( $chars, 20, 12 );
// 		return $prefix . $uuid;
	}
	
	/**
	 * Generates an UUID with no dash
	 * 32位
	 * @author     Anis uddin Ahmad <admin@ajaxray.com>
	 * @param      string  an optional prefix
	 * @return     string  the formatted uuid
	 */
	public static function getUUIDNoDash( $prefix = '' )
	{
		return sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535),
				 mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), 
				mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
// 		mt_srand( $_SERVER[ 'REQUEST_TIME' ] );
// 		$chars = md5( uniqid( mt_rand(), true ) );
		
// 		return $prefix . $chars;
	}

	/**
	 * Generates a uniqe id
	 * 最多32位
	 * @author     Anis uddin Ahmad <admin@ajaxray.com>
	 * @param      string  an optional prefix
	 * @return     string  the formatted uuid
	 */
	public static function getUNID( $prefix = '' )
	{
// 		$chars = uniqid( mt_srand( $_SERVER[ 'REQUEST_TIME' ] ), true );
// 		$uuid = str_replace( '.', '', $chars );
		
// 		return $prefix . $uuid;
		return sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), 
				mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151),
				 mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}

	/**
	 * 取得指定长度的random num
	 * @param number $iLen
	 * @return string
	 */
	public static function getRandNum( $iLen = 8 )
	{
		if( $iLen > 32 )
		{
			$iLen = 32;
		}
		
		$chars = md5( uniqid() );
		$gmp = gmp_init( '0x' . $chars );
		
		return substr( gmp_strval( $gmp , 10), 0, $iLen );//任何16进制字符串转成任何10进制字符串
	}
	/**
	 * 取得函数签名
	 * @param string $strClsSerialID
	 * @param string $strClass
	 * @param string $strFunc
	 * @return string
	 */
	public static function getFuncSignature( $strClsSerialID, $strClass, $strFunc )
	{
		return $strClsSerialID . $strClass . $strFunc;
	}

	/**
	 * 判断用户是否使用微信登录
	 */
	public static function isWeChatLogin()
	{
		if( strpos( $_SERVER[ 'HTTP_USER_AGENT' ], 'MicroMessenger' ) !== false )
		{//
			return true;
		}
		
		return false;
	}
	
	/**
	 * 0 for common(php-fpm, libphp...)
	 * 1 for swoole
	 */
	public static function getServerType()
	{
		if( class_exists( 'HttpServer' ) )
		{
			return HttpServerEnums::SERVER_TYPE_SWOOLE;
		}
		
		return HttpServerEnums::SERVER_TYPE_COMMON;
	}
}



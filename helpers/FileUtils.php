<?php
/**
 *
 * @author fzq
 * @date 2016-06-27
 */
namespace helpers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class FileUtils
{

	public static function getFileExt( $strFileName )
	{
		$info = pathinfo( $strFileName );
		
		return $info['extension'];
	}
	
}


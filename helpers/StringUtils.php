<?php
namespace helpers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

/**
 * @author Bruce
 * date 2014-11-05
 */
class StringUtils
{
	/**
	 * trans html tags to plain text
	 * 
	 * @param string $strHtml
	 * @return string
	 */
	public static function transHtmlTagsToPlainText( $strHtml )
	{
		$strHtml = str_replace( '&nbsp;', ' ', $strHtml );
		$strHtml = str_replace( "<br>", "\r\n", $strHtml );
		$strHtml = strip_tags( $strHtml );
		
		return $strHtml;
	}
	
	/**
	 * 取得指定长度的字符串
	 * 字符串  = 数字 + 字幕
	 * @param number $iLen
	 * @return string
	 */
	public static function getRandomString( $iLen = 8 )
	{
	    $prefix = 'bwdy_';
	     
	    $str = null;
	    $strPol = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
	    $max = intval( strlen( $strPol ) - 1 );
	     
	    for( $i=0 ;$i< $iLen; ++$i )
	    {
	        $str .= $strPol[ rand( 0, $max ) ];//rand($min,$max)生成介于min和max两个数之间的一个随机整数
	    }
	     
	    return $prefix . $str;
	}
}


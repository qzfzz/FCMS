<?php
/**
 *
 * @author fzq
 * @date 2016-06-27
 */
namespace helpers;
!defined( 'APP_ROOT' ) && exit( 'Direct Access Deny!' );

/**
 * 格式化时间工具类
 * @author Bruce
 * date 2014-11-05
 */
class TimeUtils
{

	function __construct( )
	{
	
	}
	
	/**
	 * 格式：2013-03-21 19：21：38 
	 */
	public static function getFullTime( $bRefresh = false )
	{
	    if( $bRefresh )
	    {
	        return date( 'Y-m-d H:i:s', time() );
	    }
	    
	    return date( 'Y-m-d H:i:s', isset( $_SERVER[ 'REQUEST_TIME' ] ) ? $_SERVER[ 'REQUEST_TIME' ] : $_SERVER[ 'request_time'] );
	}
	
	/**
	 * 格式: 134734948234.12
	 */
	public static function getFloatTime( $bRefresh = false )
	{
	    if( $bRefresh )
	    {
	        return number_format( time() , 2, '.', '' );
	    }
	    
	    return number_format( ( isset( $_SERVER[ 'REQUEST_TIME' ] ) ? $_SERVER[ 'REQUEST_TIME' ] : $_SERVER[ 'request_time'] ) , 2, '.', '' );
	    
	}
	
	/**
	 * 返回当前时间戳
	 */
	public static function getTimeStemp( $bRefresh = false )
	{
	    if( $bRefresh )
	    {
	        return time();
	    }
    	
	    return isset( $_SERVER[ 'REQUEST_TIME' ] ) ? $_SERVER[ 'REQUEST_TIME' ] : $_SERVER[ 'request_time'];
	}
	
	/**
	 * 取time search string
	 */
	public static function getTimeSearchStr( $timeFrom, $timeTo, $strKey )
	{
		$strSearch = ' ';
	
		if( $timeFrom != '' && $timeFrom != false && $timeTo != '' && $timeTo != false )
		{ //from to
			$strSearch .= ' ' . $strKey . ' between \'' . $timeFrom . '\' and \'' . $timeTo . '\'';
		}
		else if( $timeFrom != '' && $timeFrom != false && ( $timeTo == '' || $timeTo == false) )
		{ //from
			$strSearch .= '' . $strKey . ' > \'' . $timeFrom . '\'';
		}
		else if( $timeTo != '' && $timeTo != false && ($timeFrom == '' || $timeFrom != false) )
		{ //to
			$strSearch .=  '' . $strKey . ' <\'' . $timeTo . '\'';
		}
	
		return $strSearch;
	}
	
	/**
	 * get year
	 * format: 2014
	 * @return string
	 */
	public static function getYear( $bRefresh = false )
	{
	    if( $bRefresh )
	    {
	        return date( 'Y', time() );
	    }
	    
		return date( 'Y', isset( $_SERVER[ 'REQUEST_TIME' ] ) ? $_SERVER[ 'REQUEST_TIME' ] : $_SERVER[ 'request_time'] );
	}

	/**
	 * get int time
	 * @return int time
	 */
	public static function getIntTime( $bRefresh = false )
	{
	    if( $bRefresh )
	        return time();
	    
		return isset( $_SERVER[ 'REQUEST_TIME' ] ) ? $_SERVER[ 'REQUEST_TIME' ] : $_SERVER[ 'request_time'];
	}
}


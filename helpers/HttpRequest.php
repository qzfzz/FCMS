<?php

namespace helpers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

/**
 * 用于http请求
 * 
 * @author fzq
 * @date 2017-04-02
 */
class HttpRequest //implements \Phalcon\Di\InjectionAwareInterface
{
    /**
     * http method get 
     */
    const REQUEST_TYPE_GET = 0;
    
    /**
     * http method post 
     */
    const REQUEST_TYPE_POST = 1;
    
    /**
     * http method put
     */
    const REQUEST_TYPE_PUT = 2;
    
    /**
     * http method delete
     */
    const REQUEST_TYPE_DEL = 3;
    
	private static $_instance = null;
	
	private function __construct() 
	{
	}
	
	public static function getInstance()
	{
		if( !self::$_instance )
		{
			self::$_instance = new HttpRequest();
		}
		
		return self::$_instance;
	} 
	
	private function arrayToUrlEncodedStr( array $arrParams )
	{
	    assert( $arrParams );
	    
	    $queryString = '';
	    foreach( $arrParams as $k => $v )
	    {
	       if( is_array( $v ))
	       {
	           $queryString .=  $this->arrayToUrlEncodedStr( $v ) . '&'; 
	       }
	       else
	       {
	           $queryString .= urlencode( $k ) . '=' . urlencode( $v ). '&';
	       }
	    }
	    
	    if( $queryString != '' )
	    {
	        return substr( $queryString, 0, -1 );
	    }
	    
	    return null;
	}
	
	public function request( $strUrl, $params, $iRequestType = self::REQUEST_TYPE_GET )
	{
	    $httpInfo = array();
	    $ch = curl_init();
	     
	    if(stripos( $strUrl, 'https://' ) !== false )
	    {
	        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
	        curl_setopt( $ch, CURLOPT_SSLVERSION, 1 );
	    }
	    
	    curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
	    curl_setopt( $ch, CURLOPT_USERAGENT , 'FCMS' );
	    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
	    curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
	    
	    curl_setopt( $ch , CURLOPT_URL , $strUrl );
	     
	    $strQuerystring = $this->arrayToUrlEncodedStr( $params );
	    
	    switch( $iRequestType )
	    {
	        case self::REQUEST_TYPE_GET:
	            if( $params )
	            {
	                curl_setopt( $ch , CURLOPT_URL , $strUrl . '?' . $params );
	            }
	            else
	            {
	                curl_setopt( $ch , CURLOPT_URL , $strUrl );
	            }
	            break;
	        case self::REQUEST_TYPE_POST:
	            curl_setopt( $ch , CURLOPT_POST , true );
	            curl_setopt( $ch , CURLOPT_POSTFIELDS , $strQuerystring );
                break;	            
	        case self::REQUEST_TYPE_PUT:
	            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );
	            curl_setopt( $ch , CURLOPT_POSTFIELDS , $strQuerystring );
	             
	            break;
	        case self::REQUEST_TYPE_DEL:
	            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );
	            curl_setopt( $ch , CURLOPT_POSTFIELDS , $strQuerystring );
	             
	            break;
	  
	    }
	    
	    $response = curl_exec( $ch );
	    if ( $response === FALSE ) 
	    {
	        //echo "cURL Error: " . curl_error($ch);
	        return false;
	    }
	    
	    $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
	    $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
	    curl_close( $ch );
	    return $response;
	}
	
	public function get( $strUrl, $params )
	{
	    return $this->request( $strUrl, $params, self::REQUEST_TYPE_GET );
	}
	
	public function post( $strUrl, $params )
	{
	    return $this->request( $strUrl, $params, self::REQUEST_TYPE_POST );
	}
	
	public function put( $strUrl, $params )
	{
	    return $this->request( $strUrl, $params, self::REQUEST_TYPE_PUT );
	}
	
	public function delete( $strUrl, $params )
	{
	    return $this->request( $strUrl, $params, self::REQUEST_TYPE_DEL );
	}
	
		
// 	public function setDI(\Phalcon\DiInterface $dependencyInjector) 
// 	{
// 		$this->_di = $dependencyInjector;
// 	}

// 	public function getDI() 
// 	{
// 		return $this->_di;
// 	}

}
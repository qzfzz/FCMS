<?php

namespace vos;

class ArticleCateVO
{
	
	/**
	 * 主键
	 * @var integer
	 */
	public $id;
	
	/**
	 * 添加时间
	 * @var string
	 */
	public $addtime;
	
	/**
	 * 更新时间
	 * @var string
	 */
	public $uptime;
	
	/**
	 * 删除标记
	 * @var integer
	 */
	public $delsign;
	
	/**
	 * 描述
	 * @var string
	 */
	public $descr;
	
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var string
	 */
	public $title;
	
	/**
	 * @var string
	 */
	public $keywords;
	
	/**
	 * @var int
	 */
	public $type;
	
	/**
	 * @var string
	 */
	public $description;
	
	/**
	 * @var int
	 */
	public $nofollow;
	
	/**
	 * @var string
	 */
	public $img;
	
	/**
	 * @var int
	 */
	public $parent_id;

    /**
     * @var string
     */
	public $url;

	
	public function setData( $data )
	{
		if( empty( $data ) )
        {
            return false;
        }

		foreach( $data as $key=>$row )
		{
			$this->$key = $row;
		}
	}
	
}

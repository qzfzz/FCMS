<?php

namespace vos;

/**
 * 友情链接
 * @author Carey
 * @date 2015-10-23
 */
class FriendLinkVO
{
	
	/**
	 *  id
	 * @var integer
	 */
	public $id;
	
	/**
	 * 添加时间
	 * @var string
	 */
	public $addtime;
	
	/**
	 * 更新时间
	 * @var string
	 */
	public $uptime;
	
	/**
	 * 删除标记
	 * @var integer
	 */
	public $delsign;
	
	/**
	 * 备注信息
	 * @var string
	 */
	public $descr;
	
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var string
	 */
	public $title;
	
	/**
	 * @var int
	 */
	public $nofollow;
	
	/**
	 * @var string
	 */
	public $icon;
	
	/**
	 * @var string
	 */
	public $url;
	
	/**
	 * @var int
	 */
	public $sort;
	


	public function setData( $data )
	{
		if( empty( $data ) )
        {
            return false;
        }

		foreach( $data as $key=>$row )
		{
			$this->$key = $row;
		}
	}
	
}

?>
<?php

namespace vos;

class MenuVO
{
	
	/**
	 *  id
	 * @var integer
	 */
	public $id;
	
	/**
	 * 添加时间
	 * @var string
	 */
	public $addtime;
	
	/**
	 * 更新时间
	 * @var string
	 */
	public $uptime;
	
	/**
	 * 删除标记
	 * @var integer
	 */
	public $delsign;
	
	/**
	 * 备注信息
	 * @var string
	 */
	public $descr;
	
	/**
	 * @var int
	 */
	public $cid;
	
	/**
	 * @var int
	 */
	public $pid;
	
	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $url;
	
	/**
	 * @var int
	 */
	public $relid;
	
	/**
	 * @var int
	 */
	public $target;
	
	/**
	 * @var string
	 */
	public $icon;
	
	/**
	 * @var int
	 */
	public $is_show;

	public function setData( $data )
	{
		if( empty( $data ) )
        {
            return false;
        }
		
		foreach( $data as $key=>$row )
		{
			$this->$key = $row;
		}
	}
}

?>
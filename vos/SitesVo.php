<?php

namespace vos;

/**
 * 站点配置
 * @author Carey
 * @date 2015-10-23
 */
class SitesVo
{
	
	/**
	 *  id
	 * @var integer
	 */
	public $id;
	
	/**
	 * 添加时间
	 * @var string
	 */
	public $addtime;
	
	/**
	 * 更新时间
	 * @var string
	 */
	public $uptime;
	
	/**
	 * 删除标记
	 * @var integer
	 */
	public $delsign;
	
	/**
	 * 备注信息
	 * @var string
	 */
	public $descr;
	
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var string
	 */
	public $domain;
	
	/**
	 * @var string
	 */
	public $logo;
	
	/**
	 * @var string
	 */
	public $seokey;
	
	/**
	 * @var string
	 */
	public $seodescr;
	
	/**
	 * @var string
	 */
	public $copyright;
	
	/**
	 * @var string
	 */
	public $trace_code;
	
	/**
	 * @var int
	 */
	public $is_main;


	public function setData( $data )
	{
		if( empty( $data ) )
        {
            return false;
        }

		foreach( $data as $key=>$row )
		{
			$this->$key = $row;
		}
	}
	
}

?>
<?php

namespace vos;

/**
 * 广告
 * @author Carey
 * @date 2015-10-22
 */
class AdsVo
{
	
	/**
	 * 广告id
	 * @var integer
	 */
	public $id;
	
	/**
	 * 
	 * @var integer
	 */
	public $media_type;
	
	/**
	 * 广告名称
	 * @var string
	 */
	public $name;
	
	/**
	 * 链接地址
	 * @var string
	 */
	public $url;
	
	/**
	 * 开始时间
	 * @var integer
	 */
	public $begin_time;
	
	/**
	 * 失效时间
	 * @var integer
	 */
	public $end_time;
	
	/**
	 * 点击次数
	 * @var integer
	 */
	public $click_count;
	
	/**
	 * 是否开启
	 * @var integer
	 */
	public $enabled;
	
	/**
	 *  分类id
	 * @var integer
	 */
	public $cat_id;
	
	/**
	 * 排序位置
	 * @var integer
	 */
	public $sort_order;
	
	/**
	 * 标题
	 * @var string
	 */
	public $title;
	
	/**
	 * 更新时间
	 * @var string
	 */
	public $uptime;
	
	/**
	 * 删除标记
	 * @var integer
	 */
	public $delsign;
	
	/**
	 * 
	 * @var integer
	 */
	public $click_left;
	
	/**
	 * 
	 * @var int
	 */
	public $cid;
	
	/**
	 * 
	 * @var integer
	 */
	public $weight;
	
	/**
	 * 添加人id
	 * @var integer
	 */
	public $user_id;
	
	/**
	 * 描述
	 * @var string
	 */
	public $descr;
	
	/**
	 * 添加时间
	 * @var string
	 */
	public $addtime;
	
	/**
	 * 
	 * @var string
	 */
	public $src;
	
	/**
	 * 关联商铺id
	 * @var integer
	 */
	public $shopid;
	
	/**
	 * 是否允许追踪
	 * @var integer
	 */
	public $nofollow;

	
	public function setData( $data )
	{
		if( empty( $data ) )
        {
            return false;
        }
		
		foreach( $data as $key=>$row )
		{
			$this->$key = $row;
		}
	}
	
}

?>
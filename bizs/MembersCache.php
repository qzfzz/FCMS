<?php
use apps\admin\models\MemMembers;
use enums\ServiceEnums;
class MembersCache implements \Phalcon\Di\InjectionAwareInterface
{
	const USERS_MEM_MEMBERS_ID = 'h_mem_members_id_';
	
	private $_di = null;
	/* 
	 * @see \Phalcon\Di\InjectionAwareInterface::setDI()
	 */
	public function setDI(\Phalcon\DiInterface $dependencyInjector) 
	{
		$this->_di = $dependencyInjector;
	}

	/* 
	 * @see \Phalcon\Di\InjectionAwareInterface::getDI()
	 */
	public function getDI() 
	{
		return $this->_di;
	}

	public function initCacheData()
	{
		$redis = $this->_di->get( ServiceEnums::SERVICE_NATIVE_REDIS );
		
		$allMems = MemMembers::find();
		
		foreach( $allMems as $mem )
		{
			var_dump( $mem->toArray() );
		}
	}
}
<?php

namespace apps\cwechat;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Events\Manager as EventsManager;

class Module implements ModuleDefinitionInterface
{

	public function registerAutoloaders(\Phalcon\DiInterface $di = null )
	{
		$loader = new \Phalcon\Loader();
		
		$loader->registerNamespaces( 
				array( 'apps\cwechat\controllers' => APP_ROOT . '/apps/cwechat/controllers/','apps\cwechat\models' => APP_ROOT . '/apps/cwechat/models/','apps\cwechat\plugins' => APP_ROOT . '/apps/cwechat/plugins/','apps\cwechat\libraries' => APP_ROOT . '/apps/cwechat/libraries/',
						'apps\cwechat\listeners' => APP_ROOT . 'apps/cwechat/listeners/','apps\cwechat\controllers' => APP_ROOT . '/apps/cwechat/controllers/','apps\cwechat\enums' => APP_ROOT . '/apps/cwechat/enums/' 
				) );
		
		$loader->register();
	}

	/**
	 * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
	 */
	public function registerServices(\Phalcon\DiInterface $di = null )
	{
		// Registering a dispatcher
		$di->set( 'dispatcher', function ( )
		{
					
			$dispatcher = new \Phalcon\Mvc\Dispatcher();
			
			// Attach a event listener to the dispatcher
			$eventManager = new \Phalcon\Events\Manager();
			// $eventManager->attach('dispatch', new \Acl( 'admin' ));
			
			$eventManager->attach( 'dispatch:beforeException', 
					function ( $event, $dispatcher, $exception )
					{
						
						if( $exception instanceof \Phalcon\Mvc\Dispatcher\Exception )
						{
							$dispatcher->forward( array( 'module' => 'cwechat','controller' => 'error','action' => 'err404' 
							) );
							
							return false;
						}
					} );
			
			$eventManager->attach( 'dispatch:beforeDispatchLoop', 
					function ( $event, $dispatcher )
					{
						$keyParams = array();
						$params = $dispatcher->getParams();
						
						foreach( $params as $k => $v )
						{
							if( $k & 1 )
							{
								$keyParams[ $params[ $k - 1 ] ] = $v;
							}
						}
						
						$dispatcher->setParams( $keyParams );
					} );
			
			$dispatcher->setEventsManager( $eventManager );
			$dispatcher->setDefaultNamespace( "apps\\cwechat\\controllers\\" );
			return $dispatcher;
		} );
		
		// Registering the view component
		$di->set( 'view', function ( )
		{
			$view = new \Phalcon\Mvc\View();
			$view->setViewsDir( '../apps/cwechat/views/' );
			$view->registerEngines( 
					array( 
							'.volt' => function ( $view, $di )
							{
								$volt = new VoltEngine( $view, $di );
								$volt->setOptions( array( 'compiledPath' => APP_ROOT . '/apps/cwechat/cache/','compiledSeparator' => '_','compileAlways' => true 
								) );
								return $volt;
							} 
					// '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
												) );
			return $view;
		} );
		
		$config = $di->get( 'config' );
		
		$di->set( 'db', function ( ) use($config ) {
			$db = new \Phalcon\Db\Adapter\Pdo\Mysql( 
				array( 'adapter' => $this['dbCfg']->db->adapter,'host' => $this['dbCfg']->db->host,'username' => $this['dbCfg']->db->username,'password' => $this['dbCfg']->db->password,'dbname' => $this['dbCfg']->db->dbname,'charset' => $this['dbCfg']->db->charset 
				) );
					
			$eventsMgr = new EventsManager();
			$dbListener = new \apps\cwechat\listeners\DbListener();
					
			$eventsMgr->attach( 'db', $dbListener );
					
			$db->setEventsManager( $eventsMgr );

            return $db;
         }, true );
		
		$di->set( 'wechat', function(){
			
		}, true );
	}

}
        
<?php

namespace apps\admin\enums;

class MessageEnums
{
	
    /**
     * 错误消息
     */
    const MESSAGE_ERROR = 1;
    
    /**
     * 正确消息
     */
    const MESSAGE_SUCCESS = 0;
	
}


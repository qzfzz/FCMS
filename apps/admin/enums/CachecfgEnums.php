<?php

namespace apps\admin\enums;

class CachecfgEnums {
	
	/*------------自动化静态化配置-------------*/
	/**
	 * 基本配置
	 * @var integer
	 */
	const CACHE_CONFIG_BASE = 1;
	
	/**
	 * 驱动配置
	 * @var unknown
	 */
	const CACHE_CONFIG_DRIVER = 2;
	
	/**
	 * 存储位置
	 * @var unknown
	 */
	const CACHE_CONFIG_STORAGE = 3;
	
	
	/*------------ 静态化类型 ----------------*/
	/**
	 * 栏目页
	 * @var integer
	 */
	const STATIC_TYPE_COLUMN = 1;
	
	/**
	 * 列表页
	 * @var unknown
	 */
	const STATIC_TYPE_LISR = 2;
	
	/**
	 * 详细页
	 * @var unknown
	 */
	const STATIC_TYPE_DETAIL = 3;
	
	
	
}


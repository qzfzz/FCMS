<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;;

class Sensitive extends Model{
	
	/**
	 * @var integer
	 */
	public $id;
	
	/**
	 * @var string
	 */
	public $addtime;
	
	/**
	 * @var string
	 */
	public $uptime;
	
	/**
	 * @var integer
	 */
	public $delsign;
	
	/**
	 * @var string
	 */
	public $descr;
	
	/**
	 * @var string
	 */
	public $word;
	
	/**
	 * @var string
	 */
	public $rword;
	
	/**
	 * @var int
	 */
	public $uid;
	
	public function columnMap() {
		return array (
			'id' 		  => 'id',
			'addtime' => 'addtime',
			'uptime' => 'uptime',
			'delsign' 	  => 'delsign',
			'descr' 	  => 'descr',
			'word' 		  => 'word',
			'rword' 	  => 'rword',
			'uid' 		  => 'uid',
		);
	}
	
	public function initialize()
	{
		$this->useDynamicUpdate( TRUE );
		
		$this->addBehavior( new SoftDelete( array (
			'field' => 'delsign',
			'value' => DBEnums::DELSIGN_YES,
		) ) );
		
		$this->belongsTo( 'uid', '\apps\admin\models\PriUsers', 'id', array (
			'alias' => 'adminuser',
		) );
		$this->setSource( $this->di[ 'dbCfg' ][ 'db' ][ 'prefix' ] . 'sens_wd' );
	}
	
	
	
	public function insert( $val = array() )
	{
		if( is_array( $val ) && !empty( $val ) )
		{
			$model = new Sensitive();
			if( $model->save( $val ) )
			{
				return true;
			}
		}
		return false;
	}
}


<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }
class CommonDistrictDic extends \Phalcon\Mvc\Model {
	
	/** /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $level;


    /**
     *
     * @var integer
     */
    public $upid;

    /**
     *
     * @var integer
     */
    public $displayorder;
    
    /**
     * 全名
     */
    public $fullname;

    /**
     * 省id 
     */
    public $province_id;
    
    /**
     * 省
     */
    public $province;
    
    /**
     * 市ID
     */
    public $city_id;
    
    /**
     * 市
     */
    public $city;
    
    
    /**
     * 区/县 ID
     */
    public $district_id;
    
    /**
     * 区/县
     */
    public $district;
    
    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id'            => 'id', 
            'name'          => 'name', 
            'level'         => 'level', 
            'upid'          => 'upid', 
            'displayorder'  => 'displayorder',
        	'fullname' => 'fullname',
        	'province_id' => 'province_id',
        	'city_id' => 'city_id',
        	'district_id' => 'district_id',
        	'province' => 'province',
        	'city' => 'city',
        	'district' => 'district'
        );
    }
    
    public function initialize()
    {
    	$this->useDynamicUpdate( true );
    	
    	$this->setSource( 'common_district_dic' );
    }

    
    /**
     * 获取全部省 返回
     * @return boolean|unknown
     */
    public function getProvince()
    {
        $phql = 'SELECT id,name FROM apps\admin\models\CommonDistrictDic WHERE upid=:upid: and level=:level:';
        $result = $this->_modelsManager->executeQuery( $phql, array( 'upid' => 0 , 'level' => 1 ) );
        return $result;
    }
}

<?php

namespace apps\admin\models;

use enums\DBEnums;;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }
class PriRolesPris extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var integer
	 */
	public $roleid;
	
	/**
	 *
	 * @var integer
	 */
	public $priid;
	public function initialize() {
		$this->useDynamicUpdate( true );
		$this->addBehavior( new \Phalcon\Mvc\Model\Behavior\SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		
		$this->belongsTo( 'roleid', 'apps\admin\models\PriRoles', 'id', array (
				'alias' => 'priRoles' 
		) );
		$this->belongsTo( 'priid', 'apps\admin\models\PriPris', 'id', array (
				'alias' => 'priPris' 
		) );

		$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
	}
	
	/**
	 * 添加多个角色权限项
	 *
	 * @param type $roleId        	
	 * @param type $pri        	
	 * @return type
	 */
	
	public function addData( $roleId, $pri ) {
		$addtime = date( 'Y-m-d H:i:s' );
		$query = $this->modelsManager->createQuery( "insert into apps\admin\models\PriRolesPris (roleid, addtime, delsign, priid) values ('$roleId', '$addtime', 0, :priid:)" );
		foreach( $pri as $id ) 
		{
			if( ! empty( $id ) )
			{
				$status = $query->execute( [ 'priid' => $id ] )->success();
			}
		}
		return $status;
	}
	
	/**
	 * 获得角色下的所有的权限项
	 * @param int $roleId 角色id
	 */
	public function getRolePris( $roleId )
	{
		if( ! $roleId )
		{
			return array();
		}
		$phql = 'SELECT p.id,p.name, p.pid FROM \apps\admin\models\PriRolesPris AS rp '.
				' INNER JOIN \apps\admin\models\PriPris AS p ON p.id = rp.priid AND p.delsign=' . DBEnums::DELSIGN_NO .
				' WHERE rp.roleid=?0 AND rp.delsign=' .DBEnums::DELSIGN_NO;
		$objPris = $this->getModelsManager()->executeQuery( $phql, array( $roleId ));
	
		$arrPris = array();
		foreach( $objPris as $objPri )
		{
			$arrPris[ $objPri->pid ][ $objPri->id ] = [ 'id' => $objPri->id, 'name' => $objPri->name,
					'pid' => $objPri->pid ];
		}
		return $arrPris;
	}
	
	/**
	 * 获得角色下所有的acl
	 * @param int $roleId
	 */
	public function getRoleAcl( $roleId )
	{
		//所有的acl
		$objAcl = new PriAcl();
		$arrAcl = $objAcl->getAllAcl();
		
		$phql = 'SELECT a.acl_id FROM \apps\admin\models\PriRolesPris AS rp '.
				' LEFT JOIN \apps\admin\models\PriPrisAcl AS a ON a.pri_id = rp.priid AND a.delsign=' . DBEnums::DELSIGN_NO .
				' WHERE rp.roleid=?0 AND rp.delsign=' .DBEnums::DELSIGN_NO;
		$objRoleAcl = $this->getModelsManager()->executeQuery( $phql, array( $roleId ));
		
		$arrResource = array();
		foreach( $objRoleAcl as $item )
		{
			$actionId = $item->acl_id;
			if( ! empty( $arrAcl[ $actionId ][ 'name' ] ) )
			{
				$actionName = $arrAcl[ $actionId ][ 'name' ];
				$controllerId = $arrAcl[ $actionId ][ 'pid' ];
				if( ! empty( $arrAcl[ $controllerId ][ 'name' ]))
				{
					$controllerName = lcfirst( $arrAcl[ $controllerId ][ 'name' ]);
					$arrResource[ $controllerName ][] = $actionName;
				}
			}
		}
		return $arrResource;
	}
}

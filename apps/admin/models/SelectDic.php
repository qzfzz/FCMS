<?php

namespace apps\admin\models;

use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;;

class SelectDic extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $title;
	
	/**
	 *
	 * @var string
	 */
	public $key;
	
	/**
	 *
	 * @var string
	 */
	public $value;
	
	/**
	 *
	 * @var integer
	 */
	public $valtype;
	
	/**
	 *
	 * @var string
	 */
	public $cateid;
	
	
	public $sort;
	
	/**
	 * Independent Column Mapping.
	 * Keys are the real names in the table and the values their names in the application
	 *
	 * @return array
	 */
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'delsign' => 'delsign',
				'descr' => 'descr',
				'title' => 'title',
				'key' => 'key',
				'value' => 'value',
				'valtype' => 'valtype',
		        'cateid' => 'cateid',
				'sort' => 'sort'
		);
	}
	public function initialize() {
		$this->useDynamicUpdate( true );
		
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES 
		) ) );
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2016年8月19日' )
	 * @comment( comment = '根据分类id获取该类下所有配置项' )
	 */
	public static  function getSelsByCateId( $catid )
	{
	    if( !$catid )
	        return false;
	
	        $where = array(
	            'columns'    => 'id,title,key,value',
	            'conditions' => 'delsign=:del: and cateid=:cateid:',
	            'bind'       => array( 'del' => DBEnums::DELSIGN_NO, 'cateid'=> $catid ),
	        );
	        $res = self::find( $where );
	         
	        return $res;
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2016年8月22日' )
	 * @comment( comment = '通过 id 获取配置项' )
	 */
	public static function getSelById( $id )
	{
	    if( !$id )
	        return false;
	
	        $where = array(
	            'columns'    => 'id,title,key,value',
	            'conditions' => 'delsign=:del: and id=:optid:',
	            'bind'       => array( 'del' => DBEnums::DELSIGN_NO, 'optid' => $id ),
	        );
	        $res = self::findFirst( $where );
	
	        return $res;
	}
	

}

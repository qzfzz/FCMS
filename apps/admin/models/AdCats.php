<?php

namespace apps\admin\models;

use enums\DBEnums;;
use Phalcon\Mvc\Model\Behavior\SoftDelete;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }
class AdCats extends \Phalcon\Mvc\Model 
{
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var integer
	 */
	public $pid;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var integer
	 */
	public $width;
	
	/**
	 *
	 * @var integer
	 */
	public $height;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var double
	 */
	public $base_price;
	
	/**
	 *
	 * @var double
	 */
	public $click_price;
	
	private $categorys;
	
	/**
	 * Independent Column Mapping.
	 * Keys are the real names in the table and the values their names in the application
	 *
	 * @return array
	 */
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'name' => 'name',
				'pid' => 'pid',
				'descr' => 'descr',
				'width' => 'width',
				'height' => 'height',
				'delsign' => 'delsign',
				'base_price' => 'base_price',
				'click_price' => 'click_price' 
		);
	}
	public function initialize() {
		$this->useDynamicUpdate( TRUE );
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
	}
	
	/**
	 * 更新
	 */
	public function updateCats( $data ) {
		$phql = 'update \apps\admin\models\AdCats set name=:name:, pid=:pid:, uptime=:uptime:, width=:width:, height=:height: where id=:id:';
		$result = $this->_modelsManager->executeQuery( $phql, $data );
		return $result->success();
	}
	
	/**
	 * 删除
	 */
	public function deleteCats( $data ) {
		$phql = 'update \apps\admin\models\AdCats set  delsign=' . DBEnums::DELSIGN_YES . ', uptime=:uptime: where id=:id:';
		$result = $this->_modelsManager->executeQuery( $phql, $data );
		return $result->success();
	}
	
	public function getCategoryTree( $pid = 0 )
	{
	    if( !$this->categorys )
	    {
	        $where = 'delsign=' . DBEnums::DELSIGN_NO;
	        $objCates = AdCats::find( array( $where, 'columns' => 'id,pid,name',
	            'order'   => 'pid' ) );
	        if( $objCates )
	            $this->categorys = $objCates->toArray();
	    }
	
	    $arrCates = array();
	    foreach( $this->categorys as $cate )
	    {
	        if( $cate[ 'pid' ] == $pid )
	        {
	            $arrCates[ $cate[ 'id' ] ] = $cate;
	            $children = $this->getCategoryTree( $cate[ 'id' ] );
	
	            if( !empty( $children ) )
	            {
	                $arrCates[ $cate[ 'id' ] ][ 'sub' ] = $children;
	            }
	        }
	    }
	    return $arrCates;
	}
}

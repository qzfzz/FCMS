<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;;

class SlideGroup extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var integer
	 */
	public $type;
	
	/**
	 *
	 * @var integer
	 */
	public $width;
	
	/**
	 *
	 * @var integer
	 */
	public $height;
	
	/**
	 *
	 * @var integer
	 */
	public $size;
	
	/**
	 *
	 * @var int
	 */
	public $islimit;
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'delsign' => 'delsign',
				'descr' => 'descr',
				'name' => 'name',
				'type' => 'type',
				'size' => 'size',
				'width' => 'width',
				'height' => 'height',
				'islimit' => 'islimit' 
		);
	}
	public function initialize() {
		$this->useDynamicUpdate( true );
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES 
		) ) );
		
		$this->hasMany( 'id', '\apps\admin\models\Slide', 'groupid', array (
				'alias' => 'slide' 
		) );
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
	}
}

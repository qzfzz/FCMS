<?php
namespace apps\admin\models;
use enums\DBEnums;
use enums\PriEnums;

use Phalcon\Mvc\Model\Behavior\SoftDelete;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class PriPrisAcl extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Primary
     * @Identity
     * @Column(type="string", length=20, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    public $delsign;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $pri_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $acl_id;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
//     public function getSource()
//     {
//         return 'bwyt_pri_pris_acl';
//     }
    
    public function initialize()
    {
    	$this->useDynamicUpdate( true );
    	
    	$this->addBehavior( new SoftDelete( array (
    			'field' => 'delsign',
    			'value' => DBEnums::DELSIGN_YES
    	) ) );
    	
    	$this->addBehavior( new SoftDelete( array (
    			'field' => 'is_default',
    			'value' => PriEnums::DEFAULT_ACL_NOT
    	) ) );
    	
    	$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
    }

    /**
     * 获得权限所对应的acl
     * @param int $priId
     */
    public function getPriAcl( $priId )
    {
    	$objAcl = new PriAcl();
    	$arrAcl = $objAcl->getAllAcl();
    	//找出当前的
    	$priAcl = PriPrisAcl::find( array( 'pri_id=?0 and delsign=' . DBEnums::DELSIGN_NO, 'bind' => array( $priId ))  );
    	$arrRet = array();
    	foreach( $priAcl as $item )
    	{
    		$arr = array();
    		$arr[ 'paId' ] = $item->id; //中间表id
    		$actionId = $item->acl_id;
    		$arr[ 'aclId' ] = $actionId; //acl表的id
    		$arr[ 'is_default' ] = $item->is_default; //是否默认
    		
    		if( ! empty( $arrAcl[ $actionId ] ))
    		{
    			$arr[ 'action' ] = $arrAcl[ $actionId ][ 'name' ]; //动作
    			$controllerId = $arrAcl[ $actionId ][ 'pid' ];
    			 
    			if( ! empty( $arrAcl[ $controllerId ]))
    			{
    				$arr[ 'controller' ] = $arrAcl[ $controllerId ][ 'name' ];
    				$moduleId = $arrAcl[ $controllerId ][ 'pid' ];
    	
    				if( ! empty( $arrAcl[ $moduleId ]))
    				{
    					$arr[ 'module' ] = $arrAcl[ $moduleId ][ 'name' ];
    				}
    			}
    		}
    		$arrRet[] = $arr;
    	}
    	return $arrRet;
    }

    /**
     * 保存priAcl
     * @param int $priId  权限项id
     * @param array $arrAcl 添加，删除的aclId
     * @param int $defaultId 默认的aclId 
     */
    public function savePriAcl( $priId, $arrAcl, $defaultId )
    {
    	if( ! $priId )
    	{
    		return array( 'msg' => '非法的权限id' );
    	}
    	
    	//添加acl
    	if( ! empty( $arrAcl[ 'add']))
    	{   
    		foreach ( $arrAcl[ 'add'] as $item )
    		{
    			if( ! $item )
    			{
    				continue;
    			}
    			//在添加之前判断是否已经存在
    			$objAcl = PriPrisAcl::findFirst( array( 'pri_id=' . $priId . ' and acl_id=' . $item ));
    			if( $objAcl ) //存在
    			{
    				if( $objAcl->delsign == DBEnums::DELSIGN_YES ) //被删除， 恢复
    				{
    					$status = $objAcl->save( array( 'delsign' => DBEnums::DELSIGN_NO ));
    					if( ! $status )
    					{
    						return array( 'msg' => '还原acl失败');
    					}
    				}
    				continue;
    			}
    			//新建
    			$obj  = new PriPrisAcl();
    			$status = $obj->save( array( 'pri_id' => $priId, 'acl_id' => $item ));
    			if( ! $status )
    			{
    				return array( 'msg' => '保存权限acl失败' );
    			}
    		}
    	}
    	
    	//删除acl
    	if( ! empty( $arrAcl[ 'del']))
    	{
    		$strAcl = implode( ',', $arrAcl[ 'del' ] );
    		$objAcl = PriPrisAcl::find( array( 'id in (' . $strAcl . ')' ));
    		if( ! $objAcl->delete() )
    		{
    			return  array( 'msg' => '删除权限acl失败' );
    		}
    	}
    	
    	//先清其他的默认
    	$objPriAcl = PriPrisAcl::find( array( 'pri_id=?0 and is_default=' . PriEnums::DEFAULT_ACL_YES, 'bind' => array( $priId ) ));
    	$status = $objPriAcl->update( array( 'is_default' => PriEnums::DEFAULT_ACL_NOT )); 
    	if( ! $status )
    	{
    		return array( 'msg' => '清除默认失败' );
    	}
    	
    	//添加默认的acl
    	if( $defaultId )
    	{
	    	$condition = array( 'pri_id=?0 and acl_id=?1 and delsign=' . DBEnums::DELSIGN_NO, 'bind' => array( $priId, $defaultId ) );
    	}
    	else //没有默认的就用第一个
    	{
    		$condition = array( 'pri_id=?0 and delsign=' . DBEnums::DELSIGN_NO, 'bind' => array( $priId ));
    	}
    	
    	$objPriAcl = PriPrisAcl::findFirst( $condition ) ; 
    	if( $objPriAcl )
    	{
    		$status = $objPriAcl->update( array( 'is_default' => PriEnums::DEFAULT_ACL_YES ));
    		if( ! $status )
    		{
    			return array( 'msg' => '保存默认acl失败' );
    		}
    	}
    	return; //无消息就是好消息
    }
    
}

<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;;

class SiteSetting extends Model 
{
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $domain;
	
	/**
	 *
	 * @var string
	 */
	public $logo;
	
	/**
	 *
	 * @var string
	 */
	public $seokey;
	
	/**
	 *
	 * @var string
	 */
	public $seodescr;
	
	/**
	 *
	 * @var string
	 */
	public $copyright;
	
	/**
	 *
	 * @var string
	 */
	public $trace_code;
	
	/**
	 *
	 * @var int
	 */
	public $is_main;
	
	
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'delsign' => 'delsign',
				'descr' => 'descr',
				'name' => 'name',
				'domain' => 'domain',
				'logo' => 'logo',
				'seokey' => 'seokey',
				'seodescr' => 'seodescr',
				'copyright' => 'copyright',
				'trace_code' => 'trace_code',
				'is_main' => 'is_main' 
		);
	}
	public function initialize() {
		$this->useDynamicUpdate( true );
		
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		
		$this->setSource( $this->di[ 'dbCfg' ][ 'db'][ 'prefix'] . 'site_setting' );
	}
}


<?php
namespace apps\admin\models;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class LogAdminLogin extends \Phalcon\Mvc\Model
{

  /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var datetime
     */
    public $last_login_time;

    /**
     *
     * @var string
     */
    public $last_login_ip;

    /**
     *
     * @var datetime
     */
    public $last_logout_time;
    
    /**
     *
     * @var string
     */
    public $last_login_city;
    
    /**
     *
     * @var string
     */
    public $device;
    
    
    /**
     *
     * @var string
     */
    public $useragent;
    
    
    /**
     *	@var string 
     */
    public $sid;
    
    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
//     public function getSource()
//     {
//         return 'xxxx_log_admin_login';
//     }
    
    public function initialize()
    {
    	$this->useDynamicUpdate( true );
    	
    	$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
    	 
    }
    
}

<?php

namespace apps\admin\models;

use enums\DBEnums;;

use helpers\TimeUtils;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\SoftDelete;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }
class ArticleCats extends Model 
{
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $title;
	
	/**
	 *
	 * @var string
	 */
	public $keywords;
	
	/**
	 *
	 * @var integer
	 */
	public $type;
	
	/**
	 *
	 * @var string
	 */
	public $description;
	
	/**
	 *
	 * @var integer
	 */
	public $nofollow;
	
	/**
	 *
	 * @var string
	 */
	public $img;

    /**
     *
     * @var string
     */
    public $url;


	/**
	 *
	 * @var integer
	 */
	public $parent_id;
	
	private $categorys = null;
	
	/**
	 * Independent Column Mapping.
	 * Keys are the real names in the table and the values their names in the application
	 *
	 * @return array
	 */
//	public function columnMap() {
//		return array (
//				'id' => 'id',
//				'addtime' => 'addtime',
//				'uptime' => 'uptime',
//				'delsign' => 'delsign',
//				'descr' => 'descr',
//				'name' => 'name',
//				'title' => 'title',
//				'keywords' => 'keywords',
//				'type' => 'type',
//				'description' => 'description',
//				'nofollow' => 'nofollow',
//				'img' => 'img',
//				'parent_id' => 'parent_id',
//                'url' => 'url'
//		);
//	}

    public function beforeValidataionOnCreate()
    {
        $this->uptime = $this->addtime = TimeUtils::getFullTime();
        $this->delsign = DBEnums::DELSIGN_NO;
    }


    public function beforeValidataionOnUpdate()
    {
        $this->uptime = TimeUtils::getFullTime();
    }

	public function initialize() {
		$this->useDynamicUpdate( true );
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		
		$this->hasMany( 'id', '\apps\admin\models\Articles', 'cat_id', array (
				'alias' => 'articlelist' 
		) );
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
	}
	
	/**
	 * 更新
	 */
//	public function updateCats( $data ) {
//		$phql = 'update \apps\admin\models\ArticleCats set name=:name:, parent_id=:parent_id:, uptime=:uptime:, descr=:descr: where id=:id:';
//		$result = $this->_modelsManager->executeQuery( $phql, $data );
//		return $result->success();
//	}
	
	/**
	 * 删除
	 */
	public function deleteCats( $data ) {
		$phql = 'update \apps\admin\models\ArticleCats set  delsign=' . DBEnums::DELSIGN_YES . ', uptime=:uptime: where id=:id:';
		$result = $this->_modelsManager->executeQuery( $phql, $data );
		return $result->success();
	}
	
	public function getCategoryTree( $pid = 0 )
	{

	    if( ! $this->categorys )
	    {
	        $where =  'delsign=' . DBEnums::DELSIGN_NO;
	        $objCates = ArticleCats::find( array(  $where, 'columns' => 'id,parent_id as pid,name','order' => 'parent_id' ));
	        if( $objCates )
	        {
	            $this->categorys = $objCates->toArray();
	        }
	    }
	     
	    assert( $this->categorys );
	    
	    $arrCates = array();
	    foreach( $this->categorys as $cate )
	    {
	        if( $cate[ 'pid' ] == $pid )
	        {
	            $arrCates[ $cate[ 'id' ] ] = $cate;
	            $children = $this->getCategoryTree( $cate[ 'id' ] );
	
	            if( ! empty( $children ) )
	            {
	                $arrCates[ $cate[ 'id' ] ][ 'sub' ] = $children;
	            }
	        }
	    }
	     
	    return $arrCates;
	}
	
	public static function checkExist( $strName )
	{
	    $where = array(
	        'conditions'=>'delsign=:del: and name=:name:',
	        'bind'		=> array( 'del' => DBEnums::DELSIGN_NO, 'name' => $strName ),
	    );
	    
	    $status = ArticleCats::findFirst( $where );
	    if( $status )
	    {
	        return true;
	    }
	    else
	    {
	       return false;       
	    }
	}
}

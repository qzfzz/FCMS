<?php
namespace apps\admin\models;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;;

class SysDicCategory extends Model
{
    public $id;
    
    public $addtime;
    
    public $uptime;
    
    public $delsign;
    
    public $name;
    
    public $sort;
    
    public $descr;
    
    public $en_name;
    
    public $getter;
    
    public $getter_descr;
    
    public $is_enum;
    
    public function initialize()
    {
        $this->useDynamicUpdate( TRUE );
        $this->addBehavior( new SoftDelete( array (
            'field' => 'delsign',
            'value' => DBEnums::DELSIGN_YES
        ) ) );
        
        $this->setSource( $this->di['dbCfg']['db']['prefix'] . 'system_category' );
    }
    
    public function columnsMap()
    {
        return array(
            'id'            => 'id',
            'addtime'   => 'addtime',
            'uptime'   => 'uptime',
            'delsign'       => 'delsign',
            'name'          => 'name',
            'sort'          => 'sort',
            'descr'         => 'descr',
        	'en_name'		=> 'en_name',
        	'getter'	=> 'getter',
        	'getter_descr' => 'getter_descr',
        	'is_enum' => 'is_enum'	
        );
    }
    
    public static  function getAll()
    {
        $where = array(
            'columns'   => 'id,name',
            'conditions'=> 'delsign=:del:',
            'bind'       => array( 'del' => DBEnums::DELSIGN_NO ),
        	'order' => 'uptime desc'	
        );
        $res = self::find(  $where );
        return $res;
    }
}


<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;
use enums\PriEnums;
use Phalcon\Mvc\Model\Resultset;

class PriRoles extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var int
	 */
	public $type;
	
	/**
	 * Independent Column Mapping.
	 * Keys are the real names in the table and the values their names in the application
	 *
	 * @return array
	 */
	public function initialize() {
		$this->useDynamicUpdate( true );
		
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		
	
		$this->hasMany( 'id', 'apps\admin\models\PriRolesPris', 'roleid', array (
				'alias' => 'priRolesPris' 
		) );
		
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
	}
	
	/**
	 * 获取角色
	 */
	public function findRoles() {
		return self::find( array (
				'delsign' => DBEnums::DELSIGN_NO,
				'columns' => 'id, name' 
		) )->toArray();
	}
	
	/**
	 * 获得角色
	 * @return \Phalcon\Mvc\Model\ResultsetInterface
	 */
	public function getRoles()
	{
		$condition = 'delsign=' . DBEnums::DELSIGN_NO  . ' and id !=' .PriEnums::SUPER_ADMIN_ID ;
		 
		return PriRoles::find( array(
				$condition,
				'columns' => 'id,name',
				'hydration' => Resultset::HYDRATE_ARRAYS
		));
	}
}

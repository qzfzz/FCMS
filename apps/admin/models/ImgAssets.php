<?php
namespace apps\admin\models;
if( ! APP_ROOT  ) return 'Direct Access Deny!';

use Phalcon\Mvc\Model;

class ImgAssets extends Model
{
    
    public $id;
    
    public $addtime;
    
    public $delsign;
    
    public $descr;
    
    public $filename;
    
    public $biz_type;
    
    public $size;
    
    public $type;
    
    public $bucket;
    
    public $object;
    
    public $height;
    
    public $width;
    
    public $mem_type;
    
    public $mem_id;
    
    public $path_name;
    
    public function initialize()
    {
        $this->useDynamicUpdate( true );
         
        $this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
    }
}


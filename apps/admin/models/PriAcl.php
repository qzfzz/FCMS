<?php
namespace apps\admin\models;
use enums\DBEnums;
use enums\PriEnums;
use enums\ServiceEnums;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }


class PriAcl extends \Phalcon\Mvc\Model
{

    public $id;

    
    public $name;
    
    
    public $pid;

    
    public $type;
    
    public $delsign;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
//     public function getSource()
//     {
//         return 'xxxx_pri_acl';
//     }

    /**
	 * 获取所有的acl
	 */
	public function getAllAcl()
	{
		$arrAcl = $this->di[ ServiceEnums::SERVICE_DATA_CACHE ]->get( PriEnums::PREFIX_ACL_ADMIN );
		if( ! $arrAcl )
		{
			$arrAcl = array();
			$phql = 'SELECT * FROM \apps\admin\models\PriAcl WHERE delsign=' . DBEnums::DELSIGN_NO;
			$arrPriAcl = $this->getModelsManager()->executeQuery( $phql )->toArray();
			foreach( $arrPriAcl as $item )
			{
				$arrAcl[ $item[ 'id' ] ] = $item;
			}
			$this->di[ ServiceEnums::SERVICE_DATA_CACHE ]->save( PriEnums::PREFIX_ACL_ADMIN, $arrAcl );
		}
		return $arrAcl;
	}
	
	public function initialize()
	{
	    $this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
	}

}

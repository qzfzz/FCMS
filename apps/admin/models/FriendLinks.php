<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;;

/**
 * 友情链接
 *
 * @author Carey
 *         date 2015/10/22
 */
class FriendLinks extends Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $title;
	
	/**
	 *
	 * @var int
	 */
	public $nofollow;
	
	/**
	 *
	 * @var int
	 */
	public $urltype;
	
	/**
	 *
	 * @var int
	 */
	public $target;
	
	/**
	 *
	 * @var string
	 */
	public $url;
	
	/**
	 *
	 * @var int
	 */
	public $sort;
	
	/**
	 *
	 * @var string
	 */
	public $icon;
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'delsign' => 'delsign',
				'descr' => 'descr',
				'name' => 'name',
				'title' => 'title',
				'nofollow' => 'nofollow',
				'urltype' => 'urltype',
				'target' => 'target',
				'url' => 'url',
				'icon' => 'icon',
				'sort' => 'sort' 
		);
	}
	public function initialize() {
		$this->useDynamicUpdate( TRUE );
		
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		$this->setSource( $this->di[ 'dbCfg' ][ 'db'][ 'prefix'] . 'friendly_links' );
	}
}


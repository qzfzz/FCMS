<?php
namespace apps\admin\models; 

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;
use enums\PriEnums;

class PriUsers extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $addtime;

    /**
     *
     * @var string
     */
    public $uptime;

    /**
     *
     * @var integer
     */
    public $delsign;

    /**
     *
     * @var string
     */
    public $descr;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $nickname;

    /**
     *
     * @var string
     */
    public $loginname;

    /**
     *
     * @var string
     */
    public $birthdate;

    /**
     *
     * @var string
     */
    public $pwd;

    /**
     *
     * @var string
     */
    public $email;


    /**
     *
     * @var integer
     */
    public $role_id;

    /**
     *
     * @var string
     */
    public $forget_code;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     * @var string
     */
    public $avatar;
    
    /**
     * Validations and business logic
     */
    public function validation()
    {

    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'addtime' => 'addtime', 
            'uptime' => 'uptime', 
            'delsign' => 'delsign', 
            'descr' => 'descr', 
            'name' => 'name', 
            'nickname' => 'nickname', 
            'loginname' => 'loginname', 
            'birthdate' => 'birthdate', 
            'pwd' => 'pwd', 
            'email' => 'email', 
            'role_id' => 'role_id', 
            'forget_code' => 'forget_code', 
            'status' => 'status',
        	'avatar'	=> 'avatar',
        );
	}
	
	public function initialize()
    {
		$this->useDynamicUpdate( true );
		
        $this->addBehavior( new SoftDelete( array(
            'field' => 'delsign',
            'value' => DBEnums::DELSIGN_YES,
        ) ) );
        
        //与角色关联
        $this->belongsTo( 'role_id', '\apps\admin\models\PriRoles', 'id', [ 'alias' => 'priRoles' ]);
        
        $this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
    }
    
    /**
     * 添加新数据
     */
    public function beforeCreate()
    {
        $this->addtime = $this->uptime = date( 'Y-m-d H:i:s' );
        $this->delsign = DBEnums::DELSIGN_NO;
        $this->status = PriEnums::STUATUS_NOEMAL;
    }
    /**
     * 更新数据
     */
    public function beforeUpdate()
    {
        $this->uptime = date( 'Y-m-d H:i:s' );
    }
    
    /**
     * 获取用户
     */
    public function findUsers()
    {
        return self::find( array(
            'delsign' => DBEnums::DELSIGN_NO,
            'columns' => 'id, name, groupid'
        ))->toArray();
    }
    
}
<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;
use enums\PriEnums;

class PriPris extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var integer
	 */
	public $pid;
	
	/**
	 *
	 * @var integer
	 */
	public $display;
	
	/**
	 *
	 * @var string
	 */
	public $src;
	
	/**
	 *
	 * @var int
	 */
	public $sort;
	
	/**
	 *
	 * @var tinyint
	 */
	public $loadmode;
	
	/**
	 *
	 * @var tinyint
	 */
	public $type;
	
	/**
	 *
	 * @var int
	 */
	public $apid;
	
	/**
	 *
	 * @var int
	 */
	public $is_pc;
	
	/**
	 *
	 * @var string
	 */
	public $icon;
	
	/**
	 * Independent Column Mapping.
	 * Keys are the real names in the table and the values their names in the application
	 *
	 * @return array
	 */
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'delsign' => 'delsign',
				'descr' => 'descr',
				'name' => 'name',
				'pid' => 'pid',
				'display' => 'display',
				'src' => 'src',
				'sort' => 'sort',
				'loadmode' => 'loadmode',
				'type' => 'type',
				'apid' => 'apid', 
				'is_pc' => 'is_pc',
				'icon'  => 'icon'
		);
	}
	public function initialize() {
		$this->useDynamicUpdate( true );
		
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
		$this->hasMany( 'id', 'apps\admin\models\PriRolesPris', 'priid', array (
				'alias' => 'priRolesPris' 
		) );
	}
	
// 	/**
// 	 * 获得角色下的左边菜单
// 	 * @param int $roleId 角色id
// 	 */
// 	public function getLeftMenu( $roleId )
// 	{
// 		if( ! $roleId )
// 		{
// 			return;
// 		}
	
// 		if( $roleId != PriEnums::SUPER_ADMIN_ID ) //普通管理员
// 		{
// 			$phql = 'SELECT p.id,p.name, p.pid, p.sort, p.loadmode, IFNULL( a.acl_id, 0 ) AS acl_id FROM \apps\admin\models\PriRolesPris AS rp '.
// 					' INNER JOIN \apps\admin\models\PriPris AS p ON p.id = rp.priid AND p.delsign=' . DBEnums::DELSIGN_NO . ' AND p.display=' . PriEnums::IS_MENU_YES .
// 					' LEFT JOIN \apps\admin\models\PriPrisAcl AS a ON a.pri_id = p.id AND a.delsign=' . DBEnums::DELSIGN_NO . ' AND a.is_default=' . PriEnums::DEFAULT_ACL_YES .
// 					' WHERE rp.roleid=?0 AND rp.delsign=' .DBEnums::DELSIGN_NO . ' group by p.id';
// 		}
// 		else //超级管理员
// 		{
// 			$phql = 'SELECT p.id, p.name, p.pid, p.sort, p.loadmode, IFNULL( a.acl_id, 0 ) AS acl_id FROM \apps\admin\models\PriPris as p '.
// 					' LEFT JOIN \apps\admin\models\PriPrisAcl AS a ON a.pri_id = p.id AND a.delsign=' . DBEnums::DELSIGN_NO . ' and a.is_default=' . PriEnums::DEFAULT_ACL_YES .
// 					' WHERE p.delsign=' .DBEnums::DELSIGN_NO . ' and p.display=' .PriEnums::IS_MENU_YES . ' group by p.id';
// 		}
	
// 		$arr = $this->getModelsManager()->executeQuery( $phql, array( $roleId ))->toArray();
// 		$arrPris = array();
// 		foreach( $arr as $item )
// 		{
// 			$arrPris[ $item[ 'id'] ] = $item;
// 		}
		
// 		//获取所有的acl;
// 		$objAcl = new PriAcl();
// 		$arrAcl = $objAcl->getAllAcl(); 
		
// 		//生成菜单
// 		$arrMenu = array();
// 		foreach( $arrPris as $key => $item )
// 		{
// 			if( ! empty( $arrPris[ $item[ 'pid'] ] ) ) //有父菜单
// 			{
// 				$arrPris[ $key ]  += $this->getSingleAcl( $arrAcl, $item[ 'acl_id'] ); //actionId
// 				unset( $arrPris[ $key ][ 'acl_id'] );
// 				$arrPris[ $item[ 'pid'] ][ 'sub' ][ $item[ 'id' ]] = &$arrPris[ $key ];
// 			}
// 			else //没有父菜单
// 			{
// 				unset( $arrPris[ $key ][ 'acl_id'] );
// 				$arrMenu[ $item[ 'id' ]] = &$arrPris[ $key ];
// 			}
// 		}
		
// 		return $arrMenu;
// 	}
	
	
	
// 	/**
// 	 * 根据单个action 获取
// 	 * @param array $arrAcl 单个acl
// 	 * @param int $actionId 
// 	 */
// 	public function getSingleAcl( $arrAcl, $actionId )
// 	{
// 		$arr = array( 'module' => '', 'src' => '');
// 		if( empty( $arrAcl[ $actionId ] ))
// 		{
// 			return $arr;
// 		}
		
// 		$strAction = $arrAcl[ $actionId ][ 'name' ];
// 		$controllerId  = $arrAcl[ $actionId ][ 'pid' ];
// 		if( ! empty( $arrAcl[ $controllerId ])) //控制器
// 		{
// 			$strController = $arrAcl[ $controllerId ][ 'name' ];
// 			$moduleId = $arrAcl[ $controllerId ][ 'pid' ];
// 			if( ! empty( $arrAcl[ $moduleId ])) //模块
// 			{
// 				$arr[ 'module' ] = $arrAcl[ $moduleId ][ 'name' ];
// 				$arr[ 'src' ] = lcfirst(  $strController ) . '/' . $strAction;	
// 			}
// 		}
// 		return $arr;
// 	}

	public function beforeCreate()
	{
		$this->uptime = $this->addtime = date( 'Y-m-d H:i:s' );
		$this->delsign = DBEnums::DELSIGN_NO;
	}
	
	public function beforeUpdate()
	{
		$this->uptime = date( 'Y-m-d H:i:s' );
	}
}

<?php
namespace apps\admin\models;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class AccountAreaCodeDic extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=32, nullable=true)
     */
    public $name;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $pid_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=6, nullable=true)
     */
    public $sort;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */

   public function initialize()
   {
       $this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
   }
   

}

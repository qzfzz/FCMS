<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;;

/**
 * 页面缓存
 *
 * @author Carey
 *         @date 2015-10-30
 */
class StaticCache extends Model
{
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var integer
	 */
	public $cache_time;
	
	/**
	 *
	 * @var integer
	 */
	public $type;
	
	/**
	 *
	 * @var string
	 * @return multitype:string
	 */
	public $cfgtype;
	
	/**
	 *
	 * @var string
	 * @return multitype:string
	 */
	public $prefix;
	
	/**
	 * 配置类型
	 */
	public $mold;
	
	/**
	 * 所属配置
	 */
	public $kind;
	
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'delsign'   => 'delsign',
				'descr'     => 'descr',
				'name'      => 'name',
				'cache_time' => 'cache_time',
				'type'      => 'type',
				'cfgtype'   => 'cfgtype',
				'prefix'    => 'prefix',
		        'mold'    => 'mold',
		        'kind'    => 'kind',
		);
	}
	public function initialize()
    {
		$this->useDynamicUpdate( TRUE );
		
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . 'cache_static_manage' );
	}
}


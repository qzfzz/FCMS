<?php

namespace apps\admin\models;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Model;

/**
 * 系统后台主页配置项
 *
 * @author Carey
 */
class SysIndeCfg extends Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $icon;
	
	/**
	 *
	 * @var color
	 */
	public $color;
	
	/**
	 *
	 * @var int
	 */
	public $line;
	
	/**
	 *
	 * @var int
	 */
	public $size;
	
	/**
	 *
	 * @var int
	 */
	public $display;
	
	/**
	 *
	 * @var int
	 */
	public $groupid;
	
	/**
	 *
	 * @var int
	 */
	public $sort;
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'delsign' => 'delsign',
				'descr' => 'descr',
				'name' => 'name',
				'icon' => 'icon',
				'color' => 'color',
				'line' => 'line',
				'size' => 'size',
				'display' => 'display',
				'role_id' => 'role_id',
				'sort' => 'sort' 
		);
	}
	public function initialize() {
		$this->useDynamicUpdate( TRUE );
		
		$this->belongsTo( 'role_id', 'apps\admin\models\PriRoles', 'id', array (
				'alias' => 'priRoles' 
		) );
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . 'sys_index_cfg' );
	}
}

?>
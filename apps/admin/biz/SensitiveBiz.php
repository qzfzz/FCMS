<?php
namespace apps\admin\bizs;

use Phalcon\Di\InjectionAwareInterface;
use enums\DBEnums;;
use apps\admin\models\Sensitive;
use apps\admin\models\InternalMessages;
use apps\admin\models\InternalNotifies;
use apps\admin\models\Advices;
use apps\admin\models\OrdersCommentsReply;
use apps\admin\models\OrdersComments;
use apps\admin\models\Products;
use enums\SensitiveEnums;

/* 
 * 敏感词业务
*/
class SensitiveBiz implements InjectionAwareInterface
{
    private $_di;
    
    public function __construct( $di )
    {
    	$this->_di = $di;
    }
    
    /* (non-PHPdoc)
     * @see \Phalcon\Di\InjectionAwareInterface::setDI()
     */
    public function setDI( \Phalcon\DiInterface $dependencyInjector) {
        // TODO Auto-generated method stub
        $this->di = $dependencyInjector;
    }
    
    /* (non-PHPdoc)
     * @see \Phalcon\Di\InjectionAwareInterface::getDI()
     */
    public function getDI() {
        // TODO Auto-generated method stub
        return $this->di;
    }
    
    /**
     * @author( author='New' )
     * @date( date = '2016-8-26' )
     * @comment( comment = '单条信息敏感词替换' )
    */
	public function replace( $arrData )
    {
    	if( isset( $arrData ) && !empty( $arrData ) )
    	{
    		$type = $arrData[ 'type' ];
    		$id = $arrData[ 'id' ];
    		
    		$objSens = Sensitive::find( array( 'delsign=' . DBEnums::DELSIGN_NO, 'columns' => 'word,rword' ) );
    		if( false != $objSens && count( $objSens ) > 0 )
    		{
    			$arrSens = $objSens->toArray();
    			$pattern = $replacement = array();
    			foreach( $arrSens as $k => $sen )
    			{
    				$pattern[] = '/' . trim( $sen[ 'word' ] ) . '/';
    				$replacement[] = $sen[ 'rword' ]?:$this->_di[ 'config' ][ 'sensitive_default_replace' ];
    			}
    			switch( intval( $type ) )
    			{
    				case SensitiveEnums::SENSITIVE_REPLACE_PRODUCT:
    					$res = $this->_replaceProduct( $id, $pattern, $replacement );
    					break;
    				case SensitiveEnums::SENSITIVE_REPLACE_COMMENT:
    					$res = $this->_replaceComment( $id, $pattern, $replacement );
    					break;
    				case SensitiveEnums::SENSITIVE_REPLACE_COMMENT_REPLY:
    					$res = $this->_replaceCommentReply( $id, $pattern, $replacement );
    					break;
    				case SensitiveEnums::SENSITIVE_REPLACE_ADVICE:
    					$res = $this->_replaceAdvice( $id, $pattern, $replacement );
    					break;
    				case SensitiveEnums::SENSITIVE_REPLACE_NOTIFY:
    					$res = $this->_replaceNotify( $id, $pattern, $replacement );
    					break;
    				case SensitiveEnums::SENSITIVE_REPLACE_MESSAGE:
    					$res = $this->_replaceMessage( $id, $pattern, $replacement );
    					break;
    			}
    			if( $res != false ){
    				return true;
    			}
    		}
    	}
    	else
    	{
    		return false;
    	}
    	
    }
    
    
    /**
     * @author( author='New' )
     * @date( date = '2016-8-26' )
     * @comment( comment = '产品发布敏感词替换' )
     */
    private function _replaceProduct( $id, $pattern, $replacement )
    {
    	if( isset( $id, $pattern, $replacement ) )
    	{
    		$obj = Products::findFirst( array( 'conditions' => 'id=' . $id ) );
    		if( $obj )
    		{
    			$value = array( 'title' => $obj->title, 'brief' => $obj->brief, 'content' => $obj->content );
    			$value = preg_replace( $pattern, $replacement, $value );
    			if( $value != null )
    			{
    				if( $obj->save( $value ) )
    				{
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }
    
    
    /**
     * @author( author='New' )
     * @date( date = '2016-8-26' )
     * @comment( comment = '评论敏感词替换' )
     */
    private function _replaceComment( $id, $pattern, $replacement )
    {
    	if( isset( $id, $pattern, $replacement ) )
    	{
    		$obj = OrdersComments::findFirst( array( 'conditions' => 'id=' . $id ) );
    		if( $obj )
    		{
    			$value = array( 'comment' => $obj->comment );
    			$value = preg_replace( $pattern, $replacement, $value );
    			if( $value != null )
    			{
    				if( $obj->save( $value ) )
    				{
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }
    
    
    /**
     * @author( author='New' )
     * @date( date = '2016-8-26' )
     * @comment( comment = '评论回复敏感词替换' )
     */
    private function _replaceCommentReply( $id, $pattern, $replacement )
    {
    	if( isset( $id, $pattern, $replacement ) )
    	{
    		$obj = OrdersCommentsReply::findFirst( array( 'conditions' => 'id=' . $id ) );
    		if( $obj )
    		{
    			$value = array( 'reply' => $obj->reply );
    			$value = preg_replace( $pattern, $replacement, $value );
    			if( $value != null )
    			{
    				if( $obj->save( $value ) )
    				{
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }
    
    
    /**
     * @author( author='New' )
     * @date( date = '2016-8-26' )
     * @comment( comment = '咨询与回复敏感词替换' )
     */
    private function _replaceAdvice( $id, $pattern, $replacement )
    {
    	if( isset( $id, $pattern, $replacement ) )
    	{
    		$obj = Advices::findFirst( array( 'conditions' => 'id=' . $id ) );
    		if( $obj )
    		{
    			$value = array( 'question' => $obj->question, 'reply' => $obj->reply );
    			$value = preg_replace( $pattern, $replacement, $value );
    			if( $value != null )
    			{
    				if( $obj->save( $value ) )
    				{
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }
    
    
    /**
     * @author( author='New' )
     * @date( date = '2016-8-26' )
     * @comment( comment = '通知敏感词替换' )
     */
    private function _replaceNotify( $id, $pattern, $replacement )
    {
    	if( isset( $id, $pattern, $replacement ) )
    	{
    		$obj = InternalNotifies::findFirst( array( 'id=' . $id, 'columns' => 'title,content' ) );
    		if( $obj )
    		{
    			$value = array( 'title' => $obj->title, 'content' => $obj->content );
    			$value = preg_replace( $pattern, $replacement, $value );
    			if( $value != null )
    			{
    				if( $obj->save( $value ) )
    				{
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }
    
    
    /**
     * @author( author='New' )
     * @date( date = '2016-8-26' )
     * @comment( comment = '站内信敏感词替换' )
     */
    private function _replaceMessage( $id, $pattern, $replacement )
    {
    	if( isset( $id, $pattern, $replacement ) )
    	{
    		$obj = InternalMessages::findFirst( array( 'id=' . $id, 'columns' => 'title,content' ) );
    		if( $obj )
    		{
    			$value = array( 'title' => $obj->title, 'content' => $obj->content );
    			$value = preg_replace( $pattern, $replacement, $value );
    			if( $value != null )
    			{
    				if( $obj->save( $value ) )
    				{
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }
    
}


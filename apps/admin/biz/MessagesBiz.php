<?php
namespace apps\admin\bizs;

use Phalcon\Di\InjectionAwareInterface;
use apps\admin\models\InternalMessages;
use helpers\TimeUtils;

class MessagesBiz implements InjectionAwareInterface
{
    private $_di;
    
    /* (non-PHPdoc)
     * @see \Phalcon\Di\InjectionAwareInterface::setDI()
     */
    public function setDI( \Phalcon\DiInterface $dependencyInjector) {
        // TODO Auto-generated method stub
        $this->di = $dependencyInjector;
    }
    
    /* (non-PHPdoc)
     * @see \Phalcon\Di\InjectionAwareInterface::getDI()
     */
    public function getDI() {
        // TODO Auto-generated method stub
        return $this->di;
    }
    
    /**
     * @author( author='New' )
     * @date( date = '2016-8-19' )
     * @comment( comment = '发送单条站内信' )	
     * @method( method = 'sendMessage' )
     * @op( op = 'w' )
     * @param senderType 发送人类型
	 * @param senderId 发送人id
	 * @param receiverType 接收人类型
	 * @param receiverId 接收人id
	 * @param msgStatus 站内信状态
	 * @param content 站内信内容
	 * @param title 站内信标题（不必须，默认为Null）
    */
    public function sendMessage( $senderType, $senderId, $receiverType, $receiverId, $msgStatus, $content, $title=null )
    {
    	if( isset( $senderType, $receiverType, $msgStatus ) && !empty( $senderId ) && !empty( $receiverId ) && !empty( $content ) )
    	{
    		$model = new InternalMessages();
    		$time = TimeUtils::getFullTime();
    		$arr = array(
    			'addtime' => $time,
    			'uptime' => $time,
    			'sender_type' => $senderType,
    			'sender_id' => $senderId,
    			'receiver_type' => $receiverType,
    			'receiver_id' => $receiverId,
    			'content' => $content,
    			'status' => $msgStatus,
    		);
    		if( !empty( $title ) )
    		{
    			$arr[ 'title' ] = $title;
    		}
    		if( $model->save( $arr ) )
    		{
    			return true;
    		}
    	}
    	return false;
    }
    
}


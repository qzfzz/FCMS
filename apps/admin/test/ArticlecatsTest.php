<?php
namespace apps\admin\test;

use apps\admin\ext\AUnitTest;
use apps\admin\models\ArticleCats;
use enums\DBEnums;
use enums\ServiceEnums;
use helpers\TimeUtils;

/**
 * @comment( 
 * comment='文章测试类', 
 * author='bruce', 
 * add_date = '2017-04-07', 
 * up_date = '2017-04-07'
 * )
 */
class ArticlecatsTest extends AUnitTest
{
    public function __construct( $di )
    {
        $this->di = $di;
    }
    
    public function run()
    {
        $this->testIndex();
        
        $strName = 'ftest' . time();
        $this->testCheckName( $strName );
        $id = $this->testInsert( $strName );
        
        assert( false !== $id );
        
        $strName =  'fortestyes' . time();
        
        $this->testUpdate( $id, $strName );
        
        $this->testIndex( $strName );
       
        $this->testEdit( $id );
        
//         list( $strReport, $iAll , $iPass )  = $this->di[ServiceEnums::SERVICE_UNIT_TEST]->report();
        
//         echo $strReport;
    }

    private function testIndex( $strName = null )
    {
        $ac = new ArticleCats();
        
        $arrCats = $ac->getCategoryTree();
        
        if( $strName )
        {
            $this->di[ServiceEnums::SERVICE_UNIT_TEST]->run( preg_match( "|$strName|", var_export( $arrCats, true )), true, '文件类型', '文件类型' );
        }
        else
        {
            $this->di[ServiceEnums::SERVICE_UNIT_TEST]->run( $arrCats, 'is_array', '文件类型', '文件类型' );
        }
    }
    
    
    public function testEdit( $id )
    {
        $cateId = $id;
        $articleCats =  ArticleCats::findFirst( array( 'id=?0', 'bind' => array( $id ), 'columns' => 'id,name,parent_id as pid'));
        
        assert( $articleCats );
        
        $this->di[ServiceEnums::SERVICE_UNIT_TEST]->run( preg_match( "|id'(\s)*=>(\s)*'$id'|", var_export( $articleCats, true )), true, '文件类型', '文件类型' );
    }
    
    public function testInsert( $strName )
    {
        $data[ 'name' ] = $strName;//$this->request->getPost( 'cateName', 'string' );
        $data[ 'parent_id' ] = 0;//$this->request->getPost( 'parentId', 'int' );
        $data[ 'addtime' ] = $data[ 'uptime' ] = TimeUtils::getFullTime();
        $data[ 'delsign' ] = DBEnums::DELSIGN_NO;
        $data[ 'title' ] = $data['name'];
        
        //判断一下是否重命名了
        if( ArticleCats::checkExist( $data['name']) )
        {
            $this->di[ServiceEnums::SERVICE_UNIT_TEST]->run( true, true, '名称已存在', '文章分类名已存在' );
        }
        else 
        {
            $articleCats = new ArticleCats();
            $iRet = $articleCats->save( $data );
            
            $this->di[ServiceEnums::SERVICE_UNIT_TEST]->run( $iRet, true, '添加文章分类', '添加文章分类 ' . ( $iRet ? '文件ID为：' . $articleCats->id . '内容为：' . var_export( $articleCats->toArray(), true ): '' ) );
            if( $iRet )
            {
                
                return $articleCats->id;
            }
            else
            {
                return false;
            }
        }
    }
    
    public function testUpdate( $id, $strName )
    {
        $data[ 'id' ] = $id;
        $data[ 'name' ] = $strName;
        $data[ 'parent_id' ] = 0;//$this->request->getPost( 'parentId', 'int' );
        $data[ 'uptime' ] = date( 'Y-m-d H:i:s' ); 
        
        //判断一下是否重命名了
        $where = array(
        		'conditions'=>'delsign=:del: and name=:name: and id <> :optid:',
        		'bind'		=> array( 'del' => DBEnums::DELSIGN_NO, 'name' => $data[ 'name' ], 'optid' => $data[ 'id' ] ),
        );
        $isExist = ArticleCats::findFirst( $where );
        if( $isExist )
        {
            return false;
        }
        
        $articleCats = new ArticleCats();

        $this->di[ServiceEnums::SERVICE_UNIT_TEST]->run( $articleCats->updateCats( $data ), true, '更新记录测试' );
    }
    
    public function testDelete( $id )
    {
        $data[ 'id' ] = $id;
        $data[ 'uptime' ] = date( 'Y-m-d H:i:s' ); 
        
        $articleCats = new ArticleCats();
        $status = $articleCats->deleteCats( $data );
        
    }
    
    public function testCheckName( $strName )
    {
        $this->di[ServiceEnums::SERVICE_UNIT_TEST]->run( ArticleCats::checkExist( $strName ), false, '测试名称是否存在', '用于测试分类名称是否重名' );
        
        //添加顶级分类 
        $ac = new ArticleCats();
        $allCats = $ac->getCategoryTree();
//         $this->di[ServiceEnums::SERVICE_UNIT_TEST]->run( ArticleCats::checkExist( '关于我们' ), false, '分类名称正确性测试', '用于测试分类名称是否重名' );
       
    }
    
}


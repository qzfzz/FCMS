<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/font-awesome/4.6.3/css/font-awesome.min.css" >
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.css">
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/base.css">
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/role.css">
    </head>
    <body class="wrap">
        <ul class="nav nav-tabs" role="tablist" id="tabs">
            <li role="presentation" class=""><a href="{{ url( 'admin/combo/index' ) }}">套餐</a></li>
            <li role="presentation"class="active"><a href="#roleEdit" >{% if objRole.id is empty %}添加套餐{% else %}编辑套餐{% endif %}</a></li>
            <li role="presentation"><a href="#resource">套餐资源</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpannel" class="tab-pane active page-content" id="roleEdit" >
                <form class="form-horizontal">
                    <div class="form-group has-feedback">
                        <label class="col-xs-2 control-label text-right">套餐名</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="text" name="roleName" id="roleName" placeholder="请输入套餐名" value="{% if objRole.name is not empty %}{{ objRole.name | e }}{% endif %}">
                        </div>
<!--                         <div class="col-xs-3"> -->
<!--                         	<button class="btn btn-default btn-sm" type="button" id="copyRole">复制角色</button> -->
<!--                         </div> -->
                    </div>
                    <div class="form-group has-feedback">
                        <label class="col-xs-2 control-label text-right">套餐描述</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="text"  name="roleDescr" id="roleDescr" placeholder="请输入套餐描述" value="{% if objRole.descr is not empty %}{{ objRole.descr | e }}{% endif %}">
                        </div>
                    </div>
                   
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">权限项</label>
                         <div class="col-xs-10 text-left" id="selectRole">
                            <dl>
                                <dt>
                                	<label class="checkbox-inline">
                                		<input class="allSelect" type="checkbox" value="">全选
                                	</label>
                                </dt>
                            </dl>
                            {% if pris | length %}
                            {% for first in pris[1] %}
                            <dl class="all_authorities">
                                <dt> 
                                    <label class='checkbox-inline first'>
                                        <input type="checkbox" class="first" name="pris[]" value="{{ first.id | escape_attr }}" 
                                               {% if rolePris[ first.id ] is not empty %}
                                               data-id="{{ rolePris[ first.id ] | escape_attr }}" checked {% endif %}/>
                                        {{ first.name | e }}
                                    </label>
                                </dt>
                                {% if pris[ first.id ] is not empty %}
                                    {% for second in pris[ first.id ] %}
                                    <dd>
										<label class="checkbox-inline second">
                                            <input type="checkbox" class="second" name="pris[]" value="{{ second.id  | escape_attr }}"
                                                {% if rolePris[ second.id ] is not empty  %} 
                                                data-id="{{ rolePris[ second.id ] | escape_attr }}" checked {% endif %}/>
												{{ second.name | e }}
										</label>
                                        <div>
	                                        {% if pris[ second.id ] is not empty %}
	                                            {% for third in pris[ second.id ]  %}
	                                            <label class="checkbox-inline third">
	                                                <input type="checkbox" class="third" name="pris[]" value="{{ third.id | escape_attr }}"
	                                               {% if rolePris[ third.id ] is not empty %} 
	                                                data-id="{{ rolePris[ third.id ] | escape_attr }}" checked {% endif %}/>
	                                                {{ third.name | e }}
	                                            </label>
	                                            {% endfor %}
	                                        {% endif %}
                                        </div>
                                    </dd>
                                    {% endfor %}
                                {% endif %}
                            </dl>
                            {% endfor %}
                         	{% endif %}
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 30px;">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-success btn-sm" id="roleSave" style="margin-right: 50px;width:70px;">保存</button>
                            <button type="button" class="btn btn-default btn-sm" id="cancel" style="width:70px;" >取消</button>
                        </div>
                        <div class="col-xs-12 text-center loading"  style="margin-top:-20px; display:none;"> 
                            <i class="fa fa-pulse fa-spinner  fa-2x"  > </i>
                        </div>
                    </div>
                    <input type="hidden" id="roleId" name="roleId" value="{% if objRole.id is not empty %}{{ objRole.id | escape_attr }}{% endif %}" />
                </form>
            </div>
            <div class="tab-pane  page-content" role="tabpannel" id="resource" >
            	<form class="form-horizontal">
            		<div class="form-group has-feedback">
            			<label class="col-xs-2 control-label">套餐价格</label>
            			<div class="col-xs-3">
            				<input type="text" class="form-control" id="price" placeholder="请输入价格" value="{% if objResource.price is not empty %}{{ objResource.price | e }}{% endif %}">
            			</div>
            		</div>
            		<div class="form-group has-feedback">
            			<label class="col-xs-2 control-label">活动推荐个数</label>
            			<div class="col-xs-3">
            				<input type="text" class="form-control" id="eventRec" placeholder="请输入活动最大推荐个数" value="{% if objResource.event_rec is not empty %}{{ objResource.event_rec | e }}{% endif %}">
            			</div>
            		</div>
            		<div class="form-group has-feedback">
            			<label class="col-xs-2 control-label">新展推荐个数</label>
            			<div class="col-xs-3">
            				<input type="text" class="form-control" id="tmpExhRec" placeholder="请输入新展最大推荐个数" value="{% if objResource.tmp_exh_rec is not empty %}{{ objResource.tmp_exh_rec | e }}{% endif %}">
            			</div>
            		</div>
            		<div class="form-group has-feedback">
            			<label class="col-xs-2 control-label">是否在首页</label>
            			<div class="col-xs-3">
            				<label class="radio-inline">
            					<input type="radio" class="" value="1" name="isHome" {% if objResource.is_home is not empty %} checked {% endif %} >
            					是
            				</label>
            				<label class="radio-inline">
            					<input type="radio" value="0" name="isHome" {% if objResource.is_home is empty %} checked {% endif %}>
            					否
            				</label>
            			</div>
            		</div>
            		<div class="form-group" style="padding-top:20px;">
            			<div class="col-xs-offset-2 col-xs-10">
	            			<button class="btn btn-success btn-sm operate" type="button" id="saveRes">保存</button>
    	        			<button class="btn btn-default btn-sm operate" type="button" id="cancelRes">取消</button>
            			</div>
            		</div>
            		<input type="hidden" id="resId" value="{% if objResource.id is not empty %}{{ objResource.id | escape_attr }}{% endif%}">
            	</form>
            </div>
        </div>
   		{{ partial( 'public/footer' ) }}
   		<script src="{{ urlAssetsBundle }}/bootstrap/3.3.0/js/bootstrap.min.js"></script>
   		<script src="{{ urlAssetsJs }}/jsut/commonCheck.js"></script>
        <script src="{{ urlAssetsJs }}/admin/combo.js"></script>
        <script>
         var csrf = { key: '{{ security.getTokenKey()}}', token: '{{ security.getToken()}}'};
       	 /*---------------------分页切换--------------------*/
         $('#tabs a').click( function( e ) {
             if( $( this ).parent().index() === 0 )
             {
                 return true;
             }
             e.preventDefault();
            $(this).tab( 'show' );
          });

         $( '#cancelRes' ).click( function() {
             location = '/admin/combo/index';
         });

         function getResource()
         {
            if( isResChange )	
  			{
      			var obj = {};
      			obj.id = $( '#resId' ).val();
      			obj.eventRec = $( '#eventRec' ).val();
      			obj.tmpExhRec = $('#tmpExhRec' ).val();
      			obj.price = $( '#price' ).val();
      			obj.isHome = $( 'input[name="isHome"]:checked' ).val();
  				return obj;
  			}
  			return {};
         }
         //保存角色关联资源
		 $( '#saveRes' ).click( function(){
			$( '#roleSave' ).click(); 
		});	
		//检测是否变化	
		var isResChange = false;
		$( '#resource :input' ).change( function(){
			isResChange = true;
		});
        
        </script>
    </body>
</html>
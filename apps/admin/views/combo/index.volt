{{ partial( 'public/header' )}}
</head>
<body class="wrap">
	<ul class="nav nav-tabs" role="tablist">
		<li class="active" role="presentation"><a>套餐</a></li>
		<li role="presentation"><a href="/admin/combo/add">添加套餐</a></li>
	</ul>
	<div class="tab-content page-content">
		<div class="tab-pane active" role="tabpannel">
			{% if objRole | length %}
			<table class="table table-hover table-bordered" id="tableCombo">
				<thead>
					<tr>
						<th>套餐名</th>
						<th>套餐描述</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					{% for item in objRole %}
					<tr>
						<td>{{ item.name | e }}</td>
						<td>{{ item.descr | e }}</td>
						<td data-id="{{ item.id | escape_attr }}">
							<i class="glyphicon glyphicon-trash delete" title="删除"></i>
							<a class="edit_combo"><i class="glyphicon glyphicon-pencil edit" title="修改"></i></a>
							<a href="/admin/combo/read?id={{ item.id | escape_attr }}"><i class="glyphicon glyphicon-eye-open" title="查看"></i></a>
						</td>
					</tr>
					{% endfor %}
				</tbody>
			</table>
			{% else %}
				<p class="text-center">无套餐</p>
			{% endif %}
		</div>
	</div>

{{ partial( 'public/footer' ) }}
<script>
	$( function(){
		/*-------------删除套餐--------------*/
		$( '#tableCombo' ).on( 'click', '.delete', function(){
			var _this = this;					
			var id = $( this ).parents( 'td' ).attr( 'data-id' );
			$.confirm( { 'title' : '是否要删除该套餐', 'confirm' : function(){
				if( id )
				{
					$.post( '/admin/combo/delete', { 'id': id }, function( ret ){
						if( ! ret.status )
						{
							$( _this ).parents( 'tr' ).remove();
						}
						else
						{
							toastr.error( ret.msg );
						}
					}, 'json');
				}
			}} );
		});
	});
	
	/*-------------------修改（判断锁权限）-------------------*/
	$( 'a.edit_combo' ).click( function(){
		var receiverId = $( this ).parents( 'td' ).attr( 'data-id' );
		$.get( '/admin/combo/lock/id/' + receiverId + '/name/editCombo', function( ret ){
			switch( parseInt( ret.state ) ){
				case 0:
					location.href="/admin/combo/edit?id=" + receiverId;
					break;
				case 1:
					toastr.error( '他人正在编辑，请稍候' );
					break;
				case 2:
					toastr.error( '参数错误' );
					break;
			}
		}, 'json' );
	} );
</script>
</body>
</html>

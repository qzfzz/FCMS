<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/base.css">
        <link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/datetimepicker/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.css">
        <style>

            .goods-pic {
                position:relative;margin:0 20px 20px 0;
                border:1px dashed gray;display:inline-block; vertical-align: middle;text-align: center;color:gray;cursor:pointer;
            }
            .goods-img:nth-child(2) .glyphicon-arrow-left {
                display:none;
            }
            .goods-img:last-child .glyphicon-arrow-right {
                display:none;
            }
            .goods-pic img {
                margin:5px; max-width:500px;max-height:300px;
            }
            .goods-pic-operate {
                position:absolute;bottom: 0; width:100%; text-align: center;background:gainsboro; display:none;
            }
            .goods-address div.col-xs-2  {
                padding-right: 0;
            }

            .ad-pic-operate {
                position:absolute;bottom: 0; width:100%; text-align: center;background:gainsboro; display:none;
            }
            .col-xs-2 {
                padding-right: 0;
            }
        </style>
    </head>
    <body class="wrap">
        <ul class="nav nav-tabs" role="tablist" id="tabs">
            <li role="presentation"><a href="/admin/sel/indexSel">所有配置</a></li>
            <li role="presentation"class="active"><a href="#cacheAdd" >{% if res.id is defined %}修改配置项{% else %}添加配置项{% endif %}</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpannel" class="tab-pane active" id="adAdd" style="padding-top:20px;">
                <form class="form-horizontal" onsubmit="return false;">
                	<div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">配置项分类</label>
                        <div class="col-xs-2">
                            <select class="form-control" id="cateid" name="cateid" value="" required="required">
                                <option value="0" >请选择配置类别</option>
                                {% if category is defined and category is not empty %}
                                	{% for item in category %}
                                		{% if res.cateid is defined and res.cateid == item.id %}
	                                	<option value="{{ item.id }}" selected>{{ item.title }}</option>
	                                	{% else %}
	                                	<option value="{{ item.id }}" >{{ item.title }}</option>
	                                	{% endif %}
	                                {% endfor %}
                                {% endif %}
                            </select>
                        </div>
                    </div>
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">配置项名称</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="text" id="title" name="title" placeholder="请输入分类名称" required="required" value="{% if res.title is defined %}{{res.title}}{% endif %}"/>
                        </div>
                    </div>
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">配置项关键字</label>
                        <div class="col-xs-2">
                            <input class="form-control" type="text" id="key" name="key" value="{% if res.key is defined %}{{res.key}}{% endif %}" />
                        </div>
                        <span class="col-md-2 text-left text-muted" style="padding-left: 15px; font-size: 12px; margin-top: 0.7%;"> 可由英文字母加“_”组成</span>
                    </div>
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">排序</label>
                        <div class="col-xs-2">
                            <input class="form-control" type="text" id="sort" name="sort" value="{% if res.sort is defined %}{{res.sort}}{%else%}0{% endif %}" />
                        </div>
                        <span class="col-md-2 text-left text-muted" style="padding-left: 15px; font-size: 12px; margin-top: 0.7%;"> 可由英文字母加“_”组成</span>
                    </div>
                    
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">配置项值</label>
                        <div class="col-xs-3">
                       		<div class="input-group">
                        		<div class="input-group-btn" id="valtypesel">
						            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">{% if res.valtype is defined and 2 == res.valtype %} String {% elseif res.valtype is defined and 1 == res.valtype %} Float {% else %} Int {% endif %} <span class="caret"></span></button>
						            <ul class="dropdown-menu">
						              <li data-str="Int" data-id="0"><a href="javascript:;">Int</a></li>
						              <li data-str="Float" data-id="1"><a href="javascript:;">Float</a></li>
						              <li data-str="String" data-id="2"><a href="javascript:;">String</a></li>
						            </ul>
					            </div>
					            <input type="hidden" id="valtype" name="valtype" value="{% if res.valtype is defined %}{{res.valtype}}{% else %}0{% endif %}" />
	                            <input class="form-control" type="text" id="value" name="value" value="{% if res.value is defined %}{{res.value }}{% endif %}" />
				          </div>
                        </div>
                    </div>
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">配置项描述</label>
                        <div class="col-xs-3">
                            <textarea id="descr" class="form-control" rows="3" name="descr">{% if res.descr is defined %}{{res.descr}}{% endif %}</textarea>
                        </div>
                    </div>
                    
                    <div class="form-group" style="margin-top: 30px;">
                        <div class="col-sm-offset-2 col-sm-10">
                        	<input type="hidden" name="id" id="id" value="{% if res.id is defined %}{{res.id}}{% endif %}" />
                            <button type="submit" class="btn btn-success btn-sm" style="margin-right: 50px;width:70px;" onclick="submitForm()">保存</button>
                            <button type="button" class="btn btn-default btn-sm" id="cancel" style="width:70px;">取消</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </body>
<script src="{{ urlAssetsJs }}/jquery/jquery-1.11.1.min.js"></script>
<script src="{{ urlAssetsBundle }}/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.js"></script>
<script src="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.js"></script>
<script type="text/javascript">
var sign = {% if res.id is defined %}{{res.id}}{% else %}0{% endif %};
$( 'div#valtypesel' ).find( 'ul li' ).each(function(){
	$( this ).click(function(){
		var key = $.trim( $( this ).find( 'a' ).html() );
		var val = $.trim( $( this ).attr( 'data-id' ) );
		
		$( 'input#valtype' ).val( val );
		$( 'div#valtypesel' ).find( 'button' ).html( key + ' <span class="caret"></span>' );
	});
});

$( 'input#key' ).focus(function(){
	$( this ).parents( 'div.form-group' ).removeClass( 'has-error' );
});

$( 'input#key' ).blur(function(){
	
	var val = $.trim( $( this ).val() );
	var reg = /^[a-zA-Z_]+$/;	// 字母下划线 下划线位置不限
	
	if( !val || val.length <= 0 )
	{
		toastr.error( '请输入配置项关键字' );
		$( 'input#key' ).parents( 'div.form-group' ).addClass( 'has-error' );
		return false;
	} 
	else if( !reg.test( val ) )
	{
		toastr.error( '配置项关键字输入不正确..' );
		$( this ).parents( 'div.form-group' ).addClass( 'has-error' );
		return false;
	}
	
	var iCateID = $.trim( $( 'select#cateid' ).val());
	
	if( iCateID == '' ){
		return false;
	}
	
	if( !sign || false == sign )
	{
		$.get( '/admin/sel/getSelKey/cateid/' + iCateID + '/key/' + val, function( ret ){
			if( ret.status )
			{
				if( ret.msg )
				{
					toastr.error( ret.msg );
				}
				else{
					toastr.error( '该关键字已存在或信息异常,请重新输入或换一个关键字！' );
				}
				
				$( 'input#key' ).parents( 'div.form-group' ).addClass( 'has-error' );
				return false;
			}	
			
		}, 'json' );
	}
	
	
	var newVal = val.toLowerCase();
	$( 'input#key' ).val( newVal );
});


/**
 * 表单验证
 *
 */
function submitForm()
{
	var bStatus = true;
	var cateid = $.trim( $( 'select#cateid' ).val() );
	if( !cateid || false == cateid )
	{
		$( 'select#cateid' ).parents( 'div.form-group' ).addClass( 'has-error' );
		return false;
	}
	
	var key = $.trim( $( 'input#key' ).val() );
	var reg = /^[a-zA-Z_]+$/;	// 字母下划线 下划线位置不限
	
	if( !key || key.length <= 0 )
	{
		toastr.error( '请输入配置项关键字' );
		$( 'input#key' ).parents( 'div.form-group' ).addClass( 'has-error' );
		return false;
	}
	else if ( !reg.test( key ) )
	{
		toastr.error( '配置项关键字输入不正确..' );
		$( 'input#key' ).parents( 'div.form-group' ).addClass( 'has-error' );
		return false;
	}
	
	var iCateID = $.trim( $( 'select#cateid' ).val() );
	
	if( iCateID == '' ){
		return false;
	}
	
 	if( !sign || false == sign ){
 		
		$.get( '/admin/sel/getSelKey/cateid/' + iCateID + '/key/' + key, function( ret ){
			if( ret.status ){
				if( ret.msg ){
					toastr.error( ret.msg );
				}
				else{
					toastr.error( '该关键字已存在或信息异常,请重新输入或换一个关键字！' );
				}
				
				$( 'input#key' ).parents( 'div.form-group' ).addClass( 'has-error' );
				bStatus = false;
			}
			else{
				 var objParam = new Object();
				 objParam.cateid = iCateID;
				 objParam.key = key;
				 objParam.title = $.trim( $( 'input#title' ).val());
				 objParam.sort = $( 'input#sort' ).val();
				 objParam.value = $( 'input#value' ).val();
				 objParam.descr = $.trim( $( 'textarea#descr' ).val());
				 
				$.post( '/admin/sel/saveSel', objParam, function( ret ){
					console.log( ret );
					if( !ret.status ){
						window.location.href= ret.url;
					}
					else{//error
						toastr.error( ret.msg );
					}
				}, 'json' );
			}
			
		}, 'json' );
	}
 	else{//编辑时不用再验证
		 var objParam = new Object();
		 objParam.cateid = iCateID;
		 objParam.key = key;
		 objParam.title = $.trim( $( 'input#title' ).val());
		 objParam.sort = $( 'input#sort' ).val();
		 objParam.value = $( 'input#value' ).val();
		 objParam.descr = $.trim( $( 'textarea#descr' ).val());
		 objParam.id = $( 'input#id' ).val();
		 
		 var strType = $.trim( $( "div#valtypesel button" ).text());
 		 var strSel = "div#valtypesel .dropdown-menu li[data-str='" + strType + "']";
		 objParam.valtype = $( strSel ).attr( 'data-id' ); 
		 
		$.post( '/admin/sel/saveSel', objParam, function( ret ){
			if( !ret.status ){
				window.location.href= ret.url;
			}
			else{//error
				toastr.error( ret.msg );
			}
		}, 'json' );
 	}
	
	return bStatus;
}

/**
 * 判断 key 是否重复
 * key 输入的关键字
 */
function getKeyExists( key )
{
	if( !key )
		return false;
	
	$.get( '/admin/sel/getkey/key/' + key, function( ret ){
		if( 1 != ret.state )
		{
			if( ret.msg )
			{
				toastr.error( ret.msg );
				return false;
			}
			else
				return false;
		}
		else
			return true;
	}, 'json' );
}

/* ----------- 取消操作 ------------*/
$( '#cancel' ).click( function(){
	$.confirm( { 
		title: '确认要离开当前页面吗',
		confirm: function(){
			location.href = "/admin/sel/indexSel";
		}
	} );
} );
</script>
</html>

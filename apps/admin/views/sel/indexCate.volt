<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/base.css">
        <link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.css">
        <style>
            .operate {
                margin-right: 10px;
            }
            .operate:hover {
                cursor:pointer;
                color:green;
            }
        </style>
    </head>
    <body class="wrap">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#cacheList">分类管理</a></li>
            <li role="presentation"><a href="/admin/sel/addcate">添加分类</a></li>
        </ul>
        
        <div class="tab-content" style="padding:20px 0px;">
            <div role="tabpannel" class="tab-pane active" id="userList">
                {% if page.items is defined and page.items is not empty %}
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                        	<th>序号</th>
                            <th>分类名称</th>
                            <th>分类名(英文)</th>
                            <th>枚举名</th>
                            <th>其子项是否为枚举类型</th>
                            <th>getter方法</th>
                            <th>分类描述</th>
                            <th>操作时间</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody id="goodsTable">
                        {% for i,item in page.items %}
                        <tr>
                        	<td>{{ i+1 }}</td>
                            <td>{{ item.title | e }}</td>
                            <td>{{ item.en_name | e }}</td>
                            <td>DIC_CATEGORY_{{ item.en_name | e | upper }}</td>
                            <td>{% if item.is_enum == 0 %}是{% else %}否{%endif%}</td>
                            <td>{{ item.getter | e	 }}</td>
                            <td>{{ item.descr | e }}</td>
                            <td>{{ date( 'Y-m-d H:i' ) | format( item.uptime ) | e }}</td>
                            <td data-id = "{{ item.id | escape_attr }}" >
                                <a href="/admin/sel/editcate/id/{{ item.id | escape_attr }}"><i class="glyphicon glyphicon-pencil operate" title="修改"></i></a>
                                {% if item.is_enum == 0 %}
                                <a href="javascript:void(0)" onclick="generateGetterAndConst({{ item.id | escape_attr }})"><i class="glyphicon glyphicon-refresh operate" title="生成getter与enums"></i></a>
                                {% endif %}
                            </td>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
                {% else %}
                {% endif %}
                {% if page.total_pages > 1 %}
                <nav class="text-right" >
                    <ul class="pagination pagination-sm" style="margin-top:0"> 
                        <li class="{% if page.current == 1 %}disabled{% endif %}"><a href="{{ url( 'admin/sel/indexCate?page=' ) }}{{page.before | escape_attr }}" >&laquo;</a></li>
                        {% if  1 != page.current and 1 != page.before %}
                        <li><a href="{{ url( 'admin/sel/indexCate') | escape_attr }}">1</a></li>
                        {% endif %}
                        {% if page.before != page.current  %}
                        <li><a href="{{ url( 'admin/sel/indexCate?page=') }}{{ page.before | escape_attr }}"><span >{{ page.before |e}}</span></a></li>
                        {% endif %}
                        <li class="active"><a href="{{ url( 'admin/sel/indexCate?page=') }}{{ page.current | escape_attr }}"><span >{{ page.current |e}}</span></a></li>
                        {% if page.next != page.current %}
                        <li><a href="{{ url( 'admin/sel/indexCate?page=') }}{{ page.next | escape_attr }}">{{ page.next |e}}</a></li>
                        {% endif %}
                        {% if page.next  < page.last - 1 %}
                        <li><a href="#">...</a></li>
                        {% endif %}
                        {% if page.last != page.next %}
                        <li><a href="{{ url( 'admin/sel/indexCate?page=') }}{{ page.last | escape_attr }}">{{ page.last |e}}</a></li>
                        {% endif %}
                        <li class="{% if page.current == page.last %}disabled{% endif %}"><a href="{{ url( 'admin/sel/indexCate?page=' ) }}{{page.next | escape_attr }}" >&raquo;</a></li>

                    </ul>
                </nav>
                {% endif %}
            </div>
        </div>
        <div class="alert alert-danger text-center col-xs-2" style="display:none;margin-left: 40%;">
            <span>删除失败</span>
        </div>
        <script src="{{ urlAssetsJs }}/jquery/jquery-1.11.1.min.js"></script>
		<script src="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.js"></script>
        
        <script>
        
        function generateGetterAndConst( id ){
     		$.get( "/admin/sel/generateGetterAndEnums/id/" + id, function( ret ){
     			
     			 var objRet = eval( '(' + ret + ')' );
	                if( !objRet.status ){
	                	toastr.info( objRet.msg )
	                }
	                else{
	                    toastr.error( objRet.msg );
	                }
	                	
    		} );
        }
        
        </script>
    </body>
</html>
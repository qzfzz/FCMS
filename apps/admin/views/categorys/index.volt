<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/base.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/categoryIndex.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.css">
    </head>
    <body class="wrap">
        <div class="tab-content" style="padding:0px 0px;">
            <div role="tabpannel" class="tab-pane active" id="userList">
                {% if firstCats is not empty %}    
                <table class="table table-hover ">
                    <thead>
                        <tr>
                            <th>商品分类</th>
                            <th>操作<i class="glyphicon glyphicon-plus text-muted pull-right" id="first-add"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="template">
                            <td class="cat-name"><i class="glyphicon glyphicon-menu-right text-muted"></i>
                                <span class="old-name"></span>
                                <input class="new-name">
                            </td>
                            <td>
                                <i class="glyphicon glyphicon-trash operate delete text-muted"></i>
                                <i class="glyphicon glyphicon-plus operate add text-muted" ></i>

                            </td>
                        </tr>
                        {% for first in firstCats %}
                        <tr class="first" data-id="{{ first[ 'id' ] | escape_attr }}">
                            <td class="cat-name"><i  class="glyphicon glyphicon-menu-right text-muted" ></i>
                                <span class="old-name">{{ first[ 'name' ] | e }}</span>
                                <input class="new-name" value="{{ first[ 'name' ] | e }}">        
                            </td>
                            <td>
                                <i class="glyphicon glyphicon-trash operate delete text-muted" ></i>
                                <i class="glyphicon glyphicon-plus operate add text-muted" ></i>
                            </td>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
                {% else %}
                <div class="col-xs-12 text-center"> 没有数据 </div>
                {% endif %}
            </div>
        </div>
        <div class="alert alert-danger text-center col-xs-2" style="display:none;margin-left: 40%;">
            <span>删除失败</span>
        </div>
        <script src="{{ urlAssetsJs }}/jquery/jquery-1.11.1.min.js"></script>
        <script src="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.js"></script>
        <script src="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.js"></script>
        <script>
$( function(){
    var csrf = { key: '{{ security.getTokenKey() }}', token: '{{ security.getToken() }}'};
    var table = $( 'table' );
    /*-------------分类的展开--------------------*/
    table.delegate( '.first .cat-name', 'click', function(){
        var parent = $( this ).parent();
        var id = parent.attr( 'data-id' );
        var second = '.second.id' + id;
        var third = '.third.id' + id;

        if( $( second ).length )
        {
            $( second ).toggle();
            if( $( second ).is( ':hidden' ) && $( third ).length )
            {
                $( second ).find( '.cat-name i' ).removeClass( 'glyphicon-menu-down' ).addClass( 'glyphicon-menu-right' );
                $( third ).hide();
            }
        }
        else 
            getCategory( id, parent, 'second' );

        $( this ).find( 'i' ).toggleClass( 'glyphicon-menu-right glyphicon-menu-down' );
        $( '.second' ).not( second ).hide();
        $( '.third' ).hide();
        parent.find( '.add' ).show().end().siblings().find( '.add' ).hide();
        parent.siblings( '.first' ).find( '.cat-name i' ).removeClass( 'glyphicon-menu-down' ).addClass( 'glyphicon-menu-right' );
    } );
    //二级分类展开
    table.delegate( '.second .cat-name', 'click', function(){
        var parent = $( this ).parent();
        var id =  parent.attr( 'data-id' );
        var third = '.third.id' + id;
        if( $( third ).length )
        {
            $( third ).toggle();
            $( this ).find( 'i' ).toggleClass( 'glyphicon-menu-right glyphicon-menu-down' );
        }
        else
            getCategory( id, parent, 'third' );
        $( '.third' ).not( third ).hide();
        parent.siblings( '.second').find( '.cat-name i').removeClass( 'glyphicon-menu-down' ).addClass( 'glyphicon-menu-right' );
        parent.find( '.add' ).show().end().siblings().find( '.add' ).hide();
    } );

    /*----------------删除-----------------*/
    table.delegate( '.delete', 'click', function(){
        var target = $( this ).parents( 'tr' );
        var id = target.attr( 'data-id' );
        if( ! id )
        {
            target.remove();
            return true;
        }
        var _this = this;
        $.confirm( { title: '是否删除该分类', confirm: function(){
            var data = $.extend( csrf, { id: id });
            $.post( '/admin/categorys/delete', data, function( ret ){
                if( !ret.status )
                {
                    $( _this ).parents( 'tr' ).remove();
                    $( '.id' + id ).remove();//子类删除
                }
                else
                { 
                    toastr.success( ret.msg );
                }
                csrf = { key: ret.key, token: ret.token };
            }, 'json' ).error( function(){ //网络不通
                toastr.error( '网络不通' );
            } );
        }});
    } );
    
    /*---------获得下级分类------*/
    var enable = true;
    function getCategory( id, target, level )
    {
        if( ! enable )
        {
            return false;
        }
        enable = false;
        
        $.getJSON( '/admin/categorys/getCategory', { id: id }, function( ret ){
            if( ! ret.status )
            {
                var next = target;
                for( var i in ret.data )
                {
                    var clone = $( '.template' ).clone();
                    clone.removeClass( 'template' ).addClass( level + ' id' + id ).
                            attr( 'data-id', ret.data[ i ].id ).show();
                    clone.find( '.old-name' ).text( ret.data[ i ].name );
                    clone.find( '.new-name' ).val( ret.data[ i ].name );
                    next.after( clone );
                    next = clone;
                }
            }
            enable = true;
        });
    }
    
   /*------------------编辑分类名称-----------------*/
    table.delegate( '.old-name, .new-name', 'click', function(){
        if( $( this ).hasClass( 'new-name'))
        {
            return false;
        }
        var target = $( this ).parents( 'tr' );
        var width = target.find( '.old-name' ).width();
        target.find( '.new-name' ).width( width + 20 ).show();
        target.find( '.old-name' ).hide();
        //相邻的编辑恢复 因为template old-name 是空的
        if( $( '.old-name:empty' ).legth <= 1 ) 
            target.siblings().find( '.old-name' ).show().end().find( '.new-name' ).hide(); 
        return false;
    } );
    
    //修改的名称保存到服务器
    table.delegate( '.new-name', 'keyup', function( e ){
        var obj = $( this ).parents( 'tr' );
        var id = obj.attr( 'data-id' );
        if( e.keyCode === 13 ) //回车代表是保存数据
        {
            var name = $.trim( $( this ).val() );
            if( ! name )
            {
                toastr.error( '输入不能为空' );
                return false;
            }
            var pid = obj.attr( 'data-pid' );
            var data = $.extend( csrf, { id : id, name : name, pid : pid } );
            
            $.post( '/admin/categorys/saveCategory', data, function( ret ){
                if( !ret.status )
                {
                    obj.find( '.new-name' ).hide().siblings( '.old-name' ).text( name ).show();
                    obj.attr( 'data-id', ret.id );
                    toastr.success( ret.msg );
                } 
                else
                {
                    toastr.error( ret.msg );
                }
                csrf = { key: ret.key, token: ret.token };
            }, 'json' ).error( function(){
                toastr.error( '网络不通' );
            } );
            
        } 
        else
        {
            $( this ).siblings( '.old-name' ).text( $.trim( $( this ).val() ) ).hide();
            var width = $( this ).siblings( '.old-name' ).width();
            $( this ).css( 'width', width + 20 ); 
        }
    } );
    
    //修改名称完成
    table.on( 'blur', '.new-name', function(){
       var value = $.trim( $( this ).val()); 
       if( ! value )
       {
           $( this ).parents( 'tr' ).remove(); //没添加内容就删除
       }
    });
    
  //添加一级分类
    $( '#first-add' ).click( function(){
        var clone = $( 'tr.template' ).clone();
        clone.removeClass( 'template' ).addClass( 'first' ).find( '.new-name' ).show();
        $( 'tbody' ).append( clone );
        clone.find( '.new-name').focus();
        $( '.add' ).hide();
        return false;
    });
    /*-------------------添加二级分类或者三级-----------------*/
    table.delegate( '.add', 'click', function(){
        var parent = $( this ).parents( 'tr' );
        var id = parent.attr( 'data-id' );
        var clone = $( '.template' ).clone();
        var css = 'second';
        
        if( parent.hasClass( 'second'))
        {
            css = 'third';
        }
        clone.removeClass( 'template' ).addClass(  css + ' id' + id ).attr( 'data-pid', id );
        clone.find( '.new-name' ).show();
        
        var target = $( 'tr.id' + id );
        if( target.length ) //二级分类或者三级
        {
            target.last().after( clone );
        }
        else 
        {
            parent.after( clone );
        }
        clone.find( '.new-name' ).focus();
    } );
    //隐藏输入框
    $( 'body' ).click( function( e ){
        if( e.target.tagName !== 'INPUT' && e.target.tagName !== 'I' && $( '.old-name:empty' ).length <= 1 )
        {
            $( '.new-name' ).hide();
            $(  '.old-name' ).show();
        }
    });
} );
        </script>
    </body>
</html>
<!-- 对话框选择分类-->
<div class="modal modal-cat" id="catModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h5>请选择</h5>
            </div>
            <div class="modal-body">
                <div class="col-xs-12 text-left category-wrap">
                    <div class="col-xs-4">
                        {% if firstCats is not empty %}
                        <ul class="first">
                            {% for first in firstCats %}
                            <li data-id="{{ first[ 'id' ] | escape_attr }}">
                                <span class="cat-name">{{ first[ 'name' ] | e }} </span>
                                <i class="glyphicon glyphicon-menu-right pull-right"></i></li>
                            {% endfor %}
                        </ul>
                        {% else %}
                        无分类
                        {% endif %}
                    </div>
                    <div class="col-xs-4 second"></div>
                    <div class="col-xs-4 third"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-sm" id="noCategory" data-dismiss="modal">无上级分类</button>
                <button class="btn btn-default btn-sm" data-dismiss="modal">取消</button>
                <button class="btn btn-primary btn-sm" id="saveCat">确定</button>
            </div>
        </div>
    </div>
</div>
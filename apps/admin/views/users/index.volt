	{{ partial( 'public/header' ) }}
	<link rel="stylesheet" href="{{ urlAssetsBundle }}/font-awesome/4.6.3/css/font-awesome.min.css">
    </head>
    <body class="wrap">
    
    	<!-- 发送站内信modal -->
		<div class="modal fade" id="message_detail">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">站内信</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<input class="form-control reply_title" placeholder="标题（选填）"></textarea>
					  	</div>
						<div class="form-group">
							<textarea class="form-control reply_content" rows="3" placeholder="内容（必填）"></textarea>
					  	</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default cancel" style="width:100px;" data-dismiss="modal">取消</button>
						<button type="button" class="btn btn-default save_draft" style="width:100px;" onclick="saveConfirm(0)">存为草稿</button>
						<button type="button" class="btn btn-primary save_confirm" style="width:100px;" onclick="saveConfirm(1)">确定</button>
					</div>
				</div>
			</div>
		</div>
		<!-- 发送站内信modal结束 -->
		
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#userList">用户</a></li>
            <li role="presentation"><a href="/admin/users/add">添加用户</a></li>
        </ul>
        <div class="tab-content" style="padding:20px 0px;">
            <div role="tabpannel" class="tab-pane active" id="userList">
                {% if page.total_items %}
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>用户名</th>
                            <th>昵称</th>
                            <th>姓名</th>
                            <th>角色</th>
                            <th>邮箱</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% for item in page.items %}
                        <tr>
                            <td>{{ item.loginname | e }}</td>
                            <td>{{ item.nickname | e }}</td>
                            <td>{{ item.name | e }}</td>
                            <td>{% if item.roleName %} {{ item.roleName | e }} {% endif %}</td>
                            <td>{{ item.email | e }}</td>
                            <td>{% if item.status == 0  %}正常{% elseif item.status == 1 %}冻结 {% elseif item.status == 2 %}删除{% elseif item.status == 3 %}忘记密码{% endif %}</td>
                            <td data-id="{{ item.id | escape_attr }}">
                            	{% if item.id != myId %}
                            	<i class="glyphicon glyphicon-envelope operate send_message" title="发送站内信"></i>
                            	{% endif %}
                            	{% if item.id not in forbidId %}
                                <i class="glyphicon glyphicon-trash operate userDelete" data-id="{{ item.id | e }}" style="margin-right: 10px;" title="删除"></i>
                            	{% endif %}
                                <a href="/admin/users/edit?id={{ item.id | e }}"><i class="glyphicon glyphicon-pencil operate userEdit" title="修改"></i></a>
                            </td>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
                {% else %}
                	<p class="no_info">无用户</p>
                {% endif %}
                {% if page.total_pages > 1 %}
                <nav class="text-right" >
                    <ul class="pagination pagination-sm" style="margin-top:0"> 
                        <li class="{% if page.current == 1 %}disabled{% endif %}"><a href="{{ url( 'admin/users/index?page=' ) }}{{page.before}}" >&laquo;</a></li>
                        {% if  1 != page.current and 1 != page.before %}
                        <li><a href="{{ url( 'admin/users/index') }}">1</a></li>
                        {% endif %}

                        {% if page.before != page.current  %}
                        <li><a href="{{ url( 'admin/users/index?page=') }}{{ page.before }}"><span >{{ page.before }}</span></a></li>
                        {% endif %}
                        <li class="active"><a href="{{ url( 'admin/users/index?page=') }}{{ page.current }}"><span >{{ page.current }}</span></a></li>
                        {% if page.next != page.current %}
                        <li><a href="{{ url( 'admin/users/index?page=') }}{{ page.next }}">{{ page.next }}</a></li>
                        {% endif %}
                        {% if page.next  < page.last - 1 %}
                        <li><a href="#">...</a></li>
                        {% endif %}
                        {% if page.last != page.next %}
                        <li><a href="{{ url( 'admin/users/index?page=') }}{{ page.last }}">{{ page.last }}</a></li>
                        {% endif %}
                        <li class="{% if page.current == page.last %}disabled{% endif %}"><a href="{{ url( 'admin/users/index?page=' ) }}{{page.next}}" >&raquo;</a></li>

                    </ul>
                </nav>
                {% endif %}
            </div>
        </div>
        
        {{ partial( 'public/footer') }}
        <script>
		$( function(){
			/*----------------删除-----------------*/
            var csrf = { key: '{{ security.getTokenKey() }}', token: '{{ security.getToken()}}'};
			$( 'table' ).on( 'click', '.userDelete', function(){
                var id = $( this ).attr( 'data-id' );
                var data = { id: id, key: csrf.key, token: csrf.token };
                var _this = this;

                $.confirm( { title: '是否删除该用户?', confirm: function(){
                    $.post( '/admin/users/delete', data, function( ret ){
                        if( !ret.status ){
                            $( _this ).parents( 'tr' ).remove();
                        }else{
                            toastr.error( ret.msg );
                        }
                        csrf = { key: ret.key, token: ret.token };
                    }, 'json' ).error( function(){ //网络不通
                        toastr.error( '网络不通' );
                    });
                }});
			} );
        } );
		
		/*----------------点击发送站内信按钮（弹出modal）-----------------*/
		var receiverId;
		$( 'i.send_message' ).click( function(){
			receiverId = $( this ).parents( 'td' ).attr( 'data-id' );
			$( '#message_detail' ).modal( 'show' );
		} );
		
		/*----------------点击Modal确定和保存草稿按钮-----------------*/
		function saveConfirm( status ){
			if( receiverId ){
				if( status == 0 ){//保存草稿
					var msg = '保存草稿';
				}else if( status == 1 ){
					var msg = '发送';
				}
				if( $.trim( $( '.reply_content' ).val() ) == '' ){//回复信息为空
					$( '.reply_content' ).parents( 'div.form-group' ).addClass( 'has-error' );
					toastr.error( '请输入有效回复内容后再' + msg );
				}else{
					$( '.reply_content' ).parents( 'div.form-group' ).addClass( 'has-success' );
					data = { 'receiver_id':receiverId, 'title':$( '.reply_title' ).val(), 'content':$( '.reply_content' ).val(), 'status':status };
					$.post( '/admin/users/saveMessage', data, function( ret ){
						switch( parseInt( ret.state ) ){
							case 0:
								toastr.success( msg + '成功' );
								$( '#message_detail' ).modal( 'hide' );
								location.reload();
								break;
							case 1:
								toastr.error( '参数错误' );
								break;
							case 2:
								toastr.error( msg + '失败' );
								break;
						}
					}, 'json' );
				}
			}else{
				toastr.error( '参数错误' );
				return;
			}
		}
		
        </script>
    </body>
</html>
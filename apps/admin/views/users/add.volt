{{ partial( 'public/header' ) }}
        <style>
            .modal .radio-inline {
                margin-left:10px;
            }
        </style>
    </head>
    <body class="wrap">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class=""><a href="/admin/users/index">用户</a></li>
            <li role="presentation"class="active"><a href="#userAdd" >添加用户</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpannel" class="tab-pane active" id="userAdd" style="padding-top:20px;">
                <form class="form-horizontal">
                    <div class="form-group has-feedback">
                        <label class="col-xs-2 control-label text-right">姓名</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="text" id="name" name="name" placeholder="请输入姓名"/>
                        </div>
                    </div>
                     <div class="form-group has-feedback">
                        <label class="col-xs-2 control-label text-right">昵称</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="text" id="nickname" name="nickname" placeholder="请输入昵称"/>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="col-xs-2 control-label text-right">账号</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="text" id="loginname" name="loginname" placeholder="请输入登录账号" />
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="col-xs-2 control-label text-right">密码</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="password" id="password" name="password" placeholder="请输入密码" />
                        </div>
                    </div>
                     <div class="form-group has-feedback">
                        <label class="col-xs-2 control-label text-right">确认密码</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="password" id="repassword" name="repassword" placeholder="请再次输入密码" />
                        </div>
                    </div>
                    <div class="form-group  has-feedback">
                        <label class="col-xs-2 control-label text-right">邮箱</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="email" id="email" name="email" placeholder="请输入邮箱" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-xs-2 control-label"> 所属角色</label>
                        {% if roles | length  %}
                        <div class="col-xs-8">
                            {% for key,item in roles %}
                            	<label class="radio-inline">
                            		<input type="radio" name="roleId" value="{{ item[ 'id' ] }}"/>{{ item[ 'name' ] | e }}
                            	</label>
                            {% endfor %}
                        </div>
                        {% else %}
                         <div class="col-xs-8 text-danger"><p class="form-control-static">请先添加角色!</p></div>
                        {% endif %}
                    </div>
                    
                    <div class="form-group" style="margin-top: 30px;">
                        <div class="col-sm-offset-2 col-sm-10">
                        	{% if  roles | length %}
                            <button type="button" class="btn btn-success btn-sm" id="userInsert" style="margin-right: 50px;width:70px;">保存</button>
                            {% endif %}
                            <button type="button" class="btn btn-default btn-sm" id="cancel" style="width:70px;">取消</button>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>

        {{ partial( 'public/footer' ) }}
        <script src="{{ urlAssetsJs }}/jsut/commonCheck.js"></script>
        <script>
        	var submitId = false;
            $( function(){
            	 var key =  "<?php echo $this->security->getTokenKey();?>";
     	         var token =  "<?php echo $this->security->getToken();?>";
            	/*---------------防止重复点击-------------*/
        		$( 'input' ).on( 'change keyup', function(){
            		console.log( submitId );
					submitId = false;					
            	});
				
            	
                var is_check = false;
                $( 'input' ).blur( function(){
                    var objParent = $( this ).parents( '.form-group' );
                    objParent.find( 'span' ).remove();
                    var value = $.trim( $( this ).val() );
                    if( value )
                    {
                        if( value.length > 32 )
                        {
                            error( objParent, '输入不可超过32字符' );
                            return false;
                        }
                        var id = $( this ).attr( 'id' );
                        if( 'loginname' === id &&　! is_check ) //是登录名， 就去检验一下是否重复
                        {
                            if( cLength( value ) != 0 )
    	                    {
    		                    error( objParent, '请输入字母或者数字' );
    		                    return false;
    	                    }
                            $.post( '/admin/users/checkLoginName', { 'loginname' : value, 'key':key, 'token':token }, function( ret ){
                                
                                if( ret.status ) //已经存在账户了
                                {
                                    error( objParent, ret.msg );
                                }
                                else
                             	{
                                    is_check = true;
                                }
                                
                                key = ret.key;
                                token = ret.token;
                            }, 'json').error( function(){
                                toastr.error( '网络不通' );
                            });
                        }
                        else if( 'email' === id ) //是邮箱，就去验证邮箱是否正确
                        {
                            var filter  = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;
                            if( ! filter.test( value ) ) 
                            {
                                error( objParent, '邮箱格式非法' );
                                return false;
                            }
                        }
                        else if( 'password' === id ) //判断密码是否符合6-15位且不包含非法字符
                        {
                        	var pattern = /^[\w\`\~\!\@\#\$\%\^\&\*\(\)\-\+\=\|\;\:\,\.\?\/\·\￥\…\（\）]{6,15}$/;
                        	if(  !pattern.test( value ) )
                        	{
                                error( objParent, '请输入6-15位只包含数字、字母、下划线或特殊字符的密码' );
                                $( "#repassword" ).focus().select();
                                return false;
                        	}
                        }
                        else if( 'repassword' === id ) //是二次输入密码，就判断与第一次输入是否一致
                        {
							if( value !== $( '#password' ).val() )
							{
								error( objParent, '两次密码输入不一致' );
								return false;
							}
						}

                        success( objParent );
                     }
					else
					{
                        error( objParent, '输入不可为空' );
					}
                });
                
       	       
                /*-----------添加数据-----------*/
                $( '#userInsert' ).click( function(){
                	
                	if( !submitId )
                	{
                		$( ':text' ).blur(); //重新检验一下数据
                        
                      if( ! $( 'input[name="roleId"]:checked' ).length  )
                        {
                          	toastr.error( '请选择角色' );
                            return false;
                        }
                        
                        if( ! $( 'form span' ).hasClass( 'glyphicon-remove') ) //数据正确可以提交表单了
                        {
                            var data = $( 'form' ).serialize();
                            data += '&key='+ key + '&token='+token;

                            submitId = true;
                            $.post( '/admin/users/insert', data, function( ret ){
                                if( ! ret.status )
                                {
                                	toastr.success( ret.msg );
                                	setTimeout(function(){
	                                    location = '/admin/users/index';
                                	}, 2000 );
                                }
                                else
                                    toastr.error( ret.msg );
                                
                                key = ret.key;
                                token = ret.token;

                            }, 'json').error(function(){
                                toastr.error( '网络不通' );
                            });
                        }
                        else
                        {
                            toastr.error( '数据有错误' );
                        }
                      
                        return false;
                	}
                	else
              		{
                		toastr.error( '数据未修改，请勿提交!' );
              		}
                });
               /* ----------取消---------------*/
               $( '#cancel' ).click( function(){
                    location = '/admin/users/index';
                    return false;
               });
            });
            function success( obj )
            {
                 obj.addClass( 'has-success').removeClass('has-error');
                 obj.find('.form-control').after( '<span class="glyphicon glyphicon-ok form-control-feedback"></span>' );
                 obj.find( '.help-block' ).remove();
            }
            function error( obj, msg )
            {
                 obj.addClass( 'has-error').removeClass('has-success');
                 obj.find( '.glyphicon' ).remove();
                 obj.find('.form-control').after( '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' );
                 if( msg )
                 {
                     obj.find( '.help-block' ).remove();
                     obj.append( '<div class="col-xs-6 help-block">' + msg + '</div>')
                 }
            }
          
        </script>
    </body>
</html>
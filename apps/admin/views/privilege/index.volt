<!DOCTYPE html>
<html>
    <head>
     	{{ partial( 'public/header' )}}
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/privilege.css">
    </head>
    <body class="wrap">
        <ul class="nav nav-tabs" role="tablist" id="tabs"> 
            <li role="presentation" class="active"><a id="list_page">权限</a></li>
            <li role="presentation"><a id="add_page">添加</a></li>
        </ul>
        <div class="tab-content pri_list" style="padding:20px 0;">
            <div role="tabpannel" class="tab-pane active" id="page_list">
                {% if page.items is not empty %}
                <table class="table table-hover ">
                    <thead>
                        <tr>
                            <th>名称</th>
                            <th>排序</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody id="pri_list">
                        <tr class="template">
                            <td class="name"><span class="glyphicon glyphicon-menu-right"></span><span class="pri-name"></span></td>
                            <td><span class="pri-sort"></span></td>
                            <td class="op">
                                <i class="glyphicon glyphicon-trash remove" title="删除"></i>
                                <i class="glyphicon glyphicon-pencil edit" title="编辑"></i>
                                <i class="glyphicon glyphicon-plus add" title="添加"></i>
                            </td>
                        </tr>
                        {% for item in page.items %}
                        <tr class="first pid{{ item.pid | escape_attr }}" data-id="{{ item.id | escape_attr }}" data-pid="{{ item.pid | escape_attr }}">
                            <td class="name"><span class="glyphicon glyphicon-menu-right"></span>{{ item.name | e }}</td>
                            <td>{{ item.sort | e }}</td>
                            <td class="op">
                                <i class="glyphicon glyphicon-trash remove" title="删除"></i>
                                <i class="glyphicon glyphicon-pencil edit" title="编辑"></i>
                                <i class="glyphicon glyphicon-plus add" title="添加"></i>
                            </td>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
                {% else %}
               	  <p class="no_info">无相关数据</p>
                {% endif %}
                  {% if page is  not empty %}
                {% if page.total_pages > 1 %}
                <nav class="text-right" >
                    <ul class="pagination pagination-sm" style="margin-top:0"> 
                        <li class="{% if page.current == 1 %}disabled{% endif %}"><a href="{{ url( 'admin/privilege/index?page=' ) }}{{ page.before | e }}" >&laquo;</a></li>
                        {% if  1 != page.current and 1 != page.before %}
                        <li><a href="{{ url( 'admin/privilege/index') }}">1</a></li>
                        {% endif %}
                        {% if page.before != page.current  %}
                        <li><a href="{{ url( 'admin/privilege/index?page=') }}{{ page.before }}"><span >{{ page.before | e }}</span></a></li>
                        {% endif %}
                        <li class="active"><a href="{{ url( 'admin/privilege/index?page=') }}{{ page.current }}"><span >{{ page.current | e }}</span></a></li>
                        {% if page.next != page.current %}
                        <li><a href="{{ url( 'admin/privilege/index?page=') }}{{ page.next }}">{{ page.next | e }}</a></li>
                        {% endif %}
                        {% if page.next  < page.last - 1 %}
                        <li><a href="#">...</a></li>
                        {% endif %}
                        {% if page.last != page.next %}
                        <li><a href="{{ url( 'admin/privilege/index?page=') }}{{ page.last }}">{{ page.last | e }}</a></li>
                        {% endif %}
                        <li class="{% if page.current == page.last %}disabled{% endif %}"><a href="{{ url( 'admin/privilege/index?page=' ) }}{{ page.next | e }}" >&raquo;</a></li>
                    </ul>
                </nav>
                {% endif %}
                {% endif %}
            </div>
		</div>
        <div role="tabpannel" class="tab-pane active" id="page_add"></div>
		
		<div class="modal fade" id="pri_modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title">关联控制</h5>
						<div class="col-xs-8" style="float: right; margin: -25px 60px 0 0;">
							<input class="form-control input-sm" id="keyword" placeholder="请输入控制器名,以回车结束">
						</div>
					</div>
					<div class="modal-body"
						style="overflow-x: hidden; overflow-y: auto; height: 600px;">
						<div class="text-center" id="loading" style="display: none;">
							<i class="fa fa-spinner fa-pulse fa-2x "></i>
						</div>
						<div id="pri_tree" style=""></div>
						<i class="fa fa-spin fa-pulse fa-3x fa-fw"></i>
					</div>
					<div class="modal-footer">
						<i class="glyphicon glyphicon-refresh pull-left" title="更新数据库"
							style="margin-top: 5px; font-size: 16px; cursor: pointer" id="update_acl"></i>
						<button type="button" class="btn  btn-default" data-dismiss="modal">取消</button>
						<button type="button" class="btn  btn-primary" id="save_acl">确定</button>
					</div>
				</div>
			</div>
		</div>
            
	{{ partial( 'public/footer' ) }}
	<script src="{{ urlAssetsJs }}/admin/privilege/index.js"></script>
	<script src="{{ urlAssetsBundle }}-treeview/1.2.0/bootstrap-treeview.min.js"></script>
	<script src="/bundle/bootstrap/treeview/js/bootstrap-treeview.js"></script>
	<script src="{{ urlAssetsJs }}/jquery/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<script src="{{ urlAssetsJs }}/admin/privilege/add.js"></script>
	<script type="text/javascript" src="{{ urlAssetsJs }}/vue/vue.js"></script>
	<script>
    var csrf = { key : '{{ security.getTokenKey() }}', token :  '{{ security.getToken() }}'};
    var ajaxUrl = '/admin/privilege';

    //更新数据库
    $( '#update_acl' ).on( 'click', function(){
        $.get( ajaxUrl + '/updateAcl/module/admin', function( ret ){
            if( ! ret.status )
            {
                toastr.success( ret.msg );
                setTimeout( function(){
                    isInitTree = false;
                    $( '#add_acl' ).click();
                }, 1000 );
            }
            else
            {
                toastr.error( ret.msg );
            }
        }, 'json').error( function(){
            toastr.error( '更新数据库失败' );
        });
    });
    
	</script>
    </body>
</html>
<form class="form-horizontal" id="form">
	<div class="form-group">
		<label class="col-xs-2 control-label">上级</label>
		<div class="col-xs-3">
			<p class="form-control-static" id="ppri">{% if pname is not empty
				%}{{ pname | escape_attr }}{% else %} 无 {% endif %}</p>
		</div>
	</div>
	<div class="form-group has-feedback">
		<label class="col-xs-2 control-label text-right">菜单名</label>
		<div class="col-xs-4">
			<input class="form-control" type="text" name="name"
				placeholder="请输入权限名"
				value='{% if pri["name"] is not empty %}{{ pri["name"] | escape_attr }}{% endif %}' />
			<span class="glyphicon form-control-feedback"></span>
		</div>
	</div>
	<div class="form-group has-feedback">
		<label class="col-xs-2 control-label text-right">排序</label>
		<div class="col-xs-4">
			<input class="form-control" type="text" name="sort"
				placeholder="请输入数字"
				value='{% if pri["sort"] is not empty %}{{ pri["sort"] | escape_attr }}{% else %}0{% endif %}' />
			<span class="glyphicon form-control-feedback"></span>
		</div>
	</div>
	<div class="form-group has-feedback">
		<label class="col-xs-2 control-label text-right">菜单图标</label>
		<div class="col-xs-4">
			<input class="form-control" type="text" name="icon"
				placeholder="图标名"
				value='{% if pri["icon"] is not empty %}{{ pri["icon"] | escape_attr }}{% else %}{% endif %}' />
			<span style="display:none;" class="glyphicon form-control-feedback"></span>
			<!--<button type="button" style="display:none;" class="btn btn-default" data-toggle="modal" data-target="#icons">选择</button>-->
		</div>
	</div>
	<div class="form-group has-feedback">
		<label class="col-xs-2 control-label text-right">加载模式</label>
		<div class="col-xs-8 text-left">
			<label class="radio-inline"> <input type="radio" name="loadmode"
				value="1" {% if pri["loadmode"] is not empty %} checked {% endif %} />同步
			</label> <label class="radio-inline"> <input type="radio"
				name="loadmode" value="0" {% if pri["loadmode"] is empty %} checked {% endif %} />异步
			</label>
		</div>
	</div>
	<div class="form-group has-feedback">
		<label class="col-xs-2 control-label text-right">是否显示</label>
		<div class="col-xs-8 text-left">
			<label class="radio-inline"> <input type="radio" name="display"
				value="1" {% if pri["display"] is not empty %} checked {% endif %} />是&#12288;
			</label> <label class="radio-inline"> <input type="radio"
				name="display" value="0" {% if pri["display"] is empty %} checked {% endif %} />否&#12288;
			</label>
		</div>
	</div>
	<div class="form-group pri_table">
		<label class="col-xs-2 control-label">关联权限</label>
		<div class="col-xs-8">
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>模块</th>
						<th>控制器</th>
						<th>动作</th>
						<th>是否默认</th>
						<th>操作<i class="glyphicon glyphicon-plus pull-right"
							id="add_acl"></i>
						</th>
					</tr>
				</thead>
				<tbody id="pri_table">
					{% if priAcl is not empty %} {% for item in priAcl %}
					<tr data-paid='{{ item[ "paId" ] | escape_attr }}'
						data-id="{{ item[ 'aclId' ] | escape_attr }}"
						class="{% if item[ 'is_default' ] is not empty %}is_default{% endif %}">
						<td>{{ item[ 'module' ] | e }}</td>
						<td>{{ item[ 'controller' ] | e }}</td>
						<td>{{ item[ 'action' ] | e }}</td>
						<td><i class="glyphicon glyphicon-ok text-success"></i></td>
						<td><i class="glyphicon glyphicon-remove"></i></td>
					</tr>
					{% endfor %} {% endif %}
				</tbody>
			</table>
		</div>
	</div>
	<div class="form-group form-submit">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" class="btn btn-success btn-sm" id="save">保存</button>
			<button type="button" class="btn btn-default btn-sm" id="cancel">取消</button>
		</div>
	</div>
	<input type="hidden" name="id"
		value="{% if id is not empty %}{{ id | escape_attr }}{% endif %}" />
	<input type="hidden" name="pid"
		value="{% if pid is not empty %}{{ pid | escape_attr }}{% endif %}" />
</form>

<style>
</style>

<!-- Modal -->
<div class="modal fade" id="icons" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
		暂不提供选择功能，稍后添加！
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

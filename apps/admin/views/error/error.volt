<!doctype html>
<html>
    <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.css">
    <link>
    <style>
        body {
             font-family: "Microsoft YaHei",sans-serif;
        }
        .glyphicon { 
            top: 2px;
        }
        
    </style>
    <body class="wrap">
        <div class="hidden" id="msg">
            <i class="glyphicon glyphicon-exclamation-sign text-danger"></i> 
            <span class="text-danger" style="margin-left:0.5em;">
            {% if data[ 'msg' ] is not empty %}{{ data[ 'msg' ] | e }}{% endif %}
            </span>
        </div>
    <script src="{{ urlAssetsJs }}/jquery/jquery-1.11.1.min.js"></script>
    <script src="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.js"></script>
    <script>
        var msg = $( '#msg' ).html();
        $.dialog( { title : msg, onOpen :  function(){ 
            $( 'span.title' ).addClass( 'text-left' ).css( 'width', '100%');
            setTimeout( function(){
                location = '/admin/index/fcmshome'; //默认首页
             },  2000 );
        }, onClose:function(){ location = '/admin/index/fcmshome';  } } );
    </script>
    </body>
</html>
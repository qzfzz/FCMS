<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/order.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.css">
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/return.css">
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/base.css">
    </head>
    <body class="wrap" style="">
        <ul class="nav nav-tabs" role="tablist" id="tabs">
            <li role="presentation" class=""><a href="{{ url( 'admin/returns/index' ) }}">退货单</a></li>
            <li role="presentation" class="active"><a href="#returnsRead">退货单详情</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpannel" class="tab-pane fade in active" id="returnsRead" style="padding-top:20px;">
                <div data-target="#step-container" class="row-fluid" id="fuelux-wizard">
                    {% if returns is not empty %}
                    <ul class="wizard-steps" style="margin-bottom:20px;">
                        <li class="active" data-target="#step1">
                            <span class="step">1</span>
                            <span class="title">处理中</span>
                            <span class="time">{{ returns.addtime | e }}</span>
                        </li>
                        <li data-target="#step2"  class="{% if returns.status >= 1 %}active{% endif %}" >
                            <span class="step">2</span>
                            <span class="title">
                              {% if returns.status == 2 %}审核拒绝{% else %}审核通过{% endif %} 
                            </span>
                            {% if track[ 1 ] is not empty  %}
                            <span class="time {% if returns.status == 1 %}current{% endif %}">{{ track[1] | e }}</span>
                            {% endif %}
                            {% if tracke[2] is not empty %}
                            <span class="time {% if  returns.status == 2 %}current{% endif %}">{{ track[2] | e }}</span>
                            {% endif %} 
                        </li>
                        <li data-target="#step3"  class="{% if returns.status >= 3 %}active{% endif %}" >
                            <span class="step">3</span>
                            <span class="title">买家发货</span>
                            {% if track[ 3 ] is not empty %}
                            <span class="time {% if returns.status == 3 %}current{% endif %}">{{ track[3] | e }}</span>
                            {% endif %} 
                        </li>
                        <li data-target="#step4"  class="{% if returns.status >= 4 %}active{% endif %}" >
                            <span class="step">4</span>
                            {% if returns.type == 0 %}
                            <span class="title">卖家退款</span>
                            {% else %}
                             <span class="title">卖家发货</span>
                            {% endif %}
                            {% if track[ 4 ] is not empty %}
                            <span class="time {% if returns.status == 4 %}current{% endif %}">{{ track[ 4 ] | e }}</span>
                            {% endif %} 
                        </li>
                        <li data-target="#step5"  class="{% if returns.status >= 6 %}active{% endif %}"> 
                            <span class="step">5</span>
                            <span class="title">完成</span>
                            {% if track[ 6 ] is not empty %}
                            <span class="time {% if returns.status == 6 %}current{% endif %}">{{ track[ 6 ] | e }}</span>
                            {% endif %} 
                        </li>
                    </ul>
                    {% endif %}
                </div>
                 <div class="panel panel-default" style="position:relative;">
                    <div class="panel-heading">退货信息</div>
                    <table class="table return">
                        <tbody >
                            {% if returns is defined %}
                            <tr>
                                <th>退换货状态</th>
                                <td>
                                    {% if returns.status == 0 %} <span class="text-danger">审核处理中</span> 
                                    {% elseif returns.status == 1 %}<span class="text-success"> 审核通过</span> 
                                    {% elseif returns.status == 2 %}<span class="text-muted"> 审核未通过</span> 
                                    {% elseif returns.status == 3 %}<span class="text-info"> 买家发货 </span>
                                    {% elseif returns.status == 4 %}<span class="text-info"> 卖家发货 </span> 
                                    {% elseif returns.status == 5 %}<span class="text-success"> 退款成功 </span> 
                                    {% elseif returns.status == 6 %}<span class="text-primary"> 退换货完成</span> 
                                    {% endif %}
                                     {% if returns.status == 3  and returns.type == 0 %} 
                                     <button class="btn btn-success btn-sm pull-right" id="refund"> 退款</button></td>
                                     {% endif %}
                                </td>
                                <th> 申请时间 </th>
                                <td colspan='3'><?php if( isset( $receiver[ 'name' ]) ) echo $receiver[ 'name'];?> &nbsp;&nbsp;
                                    {{ returns.addtime }}</td>
                            </tr>
                            <tr>
                                <th>退换货类型</th>
                                <td>{% if returns.type  == 0 %}退货{% elseif returns.type == 1 %}换货{% else %}维修{% endif %}

                                </td>
                                <th>退换货凭证</th>
                                <td colspan='3'>
                                    {% if returns.img is not empty %}
                                    <img style="width:50px; height:50px;" src="{{ returns.img | escape_attr  }}" id="proof">
                                    <img style="position:absolute;width:200px; height:200px; left: 40%; bottom:70px;display:none; z-index: 100;" src="{{ goodsReturn[ 'img' ]}}" >
                                    {% endif %}
                                </td>
                            </tr>
                            <tr>
                                <th>退换货原因</th>
                                <td colspan='5'>{{ returns.reason_content | e }}</td>
                            </tr>
                            <tr>
                                <th>处理意见</th>
                                <td><input class="form-control" type="text" id="content" value="{{ returns.handling_idea }}"></td>
                                <th>
                                    提交处理
                                </th>
                                <td colspan="3" style="color: "> 
                                    <label class="radio-inline" style="" > <input type="radio"  name="agree" value="1" checked/> 同意 </label>
                                    <label class="radio-inline"> <input type="radio"  name="agree" value="2"/> 不同意 </label>
                                    {% if returns.status == 0 %}
                                    <button class="btn btn-success btn-sm pull-right" id="save"> 提交</button>
                                    {% endif %}
                                </td>
                            </tr>
                            {% if returns.status >= 3 %} 
                            <tr>
                                <th>买家快递公司</th>
                                <td>{{ returnGoods.buyerExpressName }}</td>
                                <th>买家快递单号</th>
                                <td colspan='3'>{{ returns.buyer_express_no | e }}</td>
                            </tr>
                            {% endif %}
                            {% if returns.status >= 3  and returns.type is not empty %} 
                            <tr>
                                <th>卖家快递公司</th>
                                <td>
                                    <div class="col-xs-6" style="padding-left:0;">
                                        <select class="form-control" id="seller_express_id" >
                                        {% for item in express %}
                                        <option value="{{ item.id }}" {% if returns.seller_express_id == item.id %} selected {% endif %}>{{ item.name }}</option>
                                        {% endfor %}
                                    </select>
                                    </div>
                                </td>
                                <th>卖家快递单号</th>
                                <td colspan='3'>
                                    <div class='col-xs-6' style='padding-left:0;'>
                                     <input class="form-control" type="text" id="seller_express_no" value="{{ returns.seller_express_no }}">
                                    </div>
                                  {% if returns.status == 3 %} 
                                  <button class="btn btn-success btn-sm pull-right" id="send"> 发货</button>
                                  {% endif %}
                                </td>
                            </tr>
                            {% if seller is not empty %}
                            <tr>
                                <th>卖家地址</th>
                                <td>{{ seller[ 'address'] | e }}</td>
                                <th>卖家手机号</th>
                                <td>{{ seller[ 'tel'] | e }}</td>
                            </tr>
                            {% endif %}    
                            {% endif %}
                         {% endif %}
                        </tbody>
                    </table>
                    <div class="alert alert-danger text-center " style="position:absolute;right:50px; bottom:0px; margin-bottom: -50px; width:200px; display:none;">
                        <i class="glyphicon glyphicon-warning-sign pull-left"></i>
                        <span> 网络不通</span>
                    </div>
                </div>
                 
                <div class="panel panel-default">
                    <div class="panel-heading">商品信息</div>
                    <table class="table">
                        <thead>
                             <tr>
                                <th>商品货号</th>
                                <th>商品名</th>
                                <th>价格</th>
                                <th>数量</th> 
                                <th>小计</th> 
                            </tr>
                        </thead>
                        <tbody>
                            {% if returnGoods is not empty %}
                            <tr>
                                <td>{{ returnGoods.goods_id | e }}</td>
                                <td>{{ returnGoods.name | e }}</td>
                                <td>￥{{ '%.2f'|format( returnGoods.price ) }}</td>
                                <td>{{ returnGoods.goods_num | e }}</td>
                                <td>￥{{  '%.2f' |format( returnGoods.price * returnGoods.goods_num) }}</td>
                            </tr>
                            {% endif %}
                        </tbody>
                    </table>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">订单信息</div>
                    <table class="table receiver">
                        <tbody>
                            {% if order is defined %}
                            <tr>
                                <th>订单号</th>
                                <td>{{ order[ 'order_sn' ] }}</td>
                                <th> 发货时间 </th>
                                <td colspan='3'> {{ order[ 'freight_accesstime' ] }}</td>
                            </tr>
                            {% endif %}
                          
                            {% if  buyer is defined %}
                            <tr>
                                <th> 买家姓名</th>
                                <td>{{ buyer[ 'name' ] | e }}</td>
                                <th> 联系手机</th>
                                <td>{{ buyer[ 'tel' ] | e }}</td>
                            </tr>
                             <tr>
                                <th>收货地址</th>
                                <td colspan="5">{{ buyerAddress | e }}</td>
                            </tr>
                            {% endif %}
                       
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      
        <div class="col-xs-12 text-center" style="padding-bottom:50px;">
            <button class="btn btn-default btn-sm" id="cancel" onclick=""> 返回 </button>
        </div>
        <script src="{{ urlAssetsJs }}/jquery/jquery-1.11.1.min.js"></script>
        <script src="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.js"></script>
        <script>
            $( function(){
                var key = '{{ security.getTokenKey() }}';
                var token = '{{ security.getToken() }}';
                var returnId = "{% if returns.id is not empty %}{{ returns.id }}{% else %} 0 {% endif %}";
                $( '#cancel' ).click( function(){
                     if( document.referrer )
                        location = document.referrer; 
                     else
                        location='/admin/returns/index';
                });
                $( '#save' ).click( function(){
                    $.confirm( { title: '确认处理结果吗？', confirm: function(){  
                        var content = $( '#content' ).val();
                        var agree = $( 'input[name="agree"]:checked').val();
                        $.post( '/admin/returns/save' , { 'id' : returnId , 'content' : content, 'agree': agree, 'key':key, 'token':token }, function( ret ){
                            if( ! ret.status )
                                location.reload();
                            else
                                error( ret.msg );
                            
                            key = ret.key;
                            token = ret.token;
                        }, 'json').error( function(){
                            error( '网络不通' );
                        });
                    }});
                });
                
                $( '#proof' ).hover( function(){
                    $( this ).siblings().show();
                },function(){
                    $( this ).siblings().hide();
                });
                
            /*----------------退款-----------------*/
            $( 'table' ).delegate( '#refund', 'click', function(){
                 $.confirm( { title: '是否要确认退款', confirm: function(){
                         
                     var data = {'id': returnId, 'key':key,'token':token };
                     $.post( '/admin/returns/refund', data, function( ret ){
                         if( !ret.status )
                             location.reload();
                         else
                            error( ret.msg );
                         
                         key = ret.key;
                         token = ret.token;
                     }, 'json').error(function(){ //网络不通
                         error( '网络不通' );
                     });
                 }});
            });
             
            /*----------------发货-----------------*/
            $( 'table' ).delegate( '#send', 'click', function(){
                $.confirm( { title: '是否确认要发货', confirm: function(){
                     var express_id = $( '#seller_express_id' ).val(); //卖家的快递号
                     var express_no = $( '#seller_express_no' ).val();
                     var data = {'id': returnId,
                        'seller_express_id' : express_id,
                        'seller_express_no' : express_no,
                        'key':key,'token':token };
                     $.post( '/admin/returns/send', data, function( ret ){
                         if( !ret.status )
                             location.reload();
                         else
                            error( ret.msg );
                         
                         key = ret.key;
                         token = ret.token;
                     }, 'json').error(function(){ //网络不通
                         error( '网络不通' );
                     });
                }} );
            });
            function error( msg )
            {
                $( '.alert span' ).text( msg );
                $( '.alert' ).show().fadeOut( 3000 );
            }
        });
        </script>
 
    </body>
</html>
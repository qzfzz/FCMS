<div class="detail_top">
	<i class="icon-angle-left" onclick="history.back( -1 );" style="float:left;"></i>
	<i class="icon-list" id="top_more" style="float:right;"></i>
	<div class="top_more_detail">
		<ul>
			<li>首页</li>
			<li>帮助</li>
			<li>分享</li>
		</ul>
	</div>
</div>

<script src="{{ urlAssetsJs }}/jquery/plugins/jquery.animate-colors.js"></script>
<script>
	//下拉菜单显示与隐藏
	$( '#top_more' ).on( 'click', function( e ){
		if( $( '.top_more_detail' ).is( ":hidden" ) ){
			$( '.top_more_detail' ).show();
		}else{
			$( '.top_more_detail' ).hide();
		}
		$( document ).on( "click", function(){
	        $( ".top_more_detail" ).hide();
	    } );
	    e.stopPropagation();
	} );
	
	$( ".top_more_detail" ).on( "click", function( e ){
	    e.stopPropagation();
	} );
	
</script>
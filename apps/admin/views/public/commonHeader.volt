<!-- 手机前台前端页头 -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1,IE=edge" />
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<meta name="renderer" content="webkit">


<link rel='icon' href='/public/favicon.ico' mce_href='/public/favicon.ico' type='image/x-icon' />
<link rel='shortcut icon' href='/public/favicon.ico' mce_href='/public/favicon.ico' type='image/x-icon' />
<link rel="stylesheet" type="text/css" href="{{ urlAssetsCss }}/mobile/index.css" />
<link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/Font-Awesome-3.2.1/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/weui/css/weui.css" />
<link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/weui/css/jquery-weui.css" />
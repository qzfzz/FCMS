<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.css">
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/base.css?n=1">

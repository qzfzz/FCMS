<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/base.css">
        <link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/datetimepicker/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.css">
        <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.css">
        <style>

            .goods-pic {
                position:relative;margin:0 20px 20px 0;
                border:1px dashed gray;display:inline-block; vertical-align: middle;text-align: center;color:gray;cursor:pointer;
            }
            .goods-img:nth-child(2) .glyphicon-arrow-left {
                display:none;
            }
            .goods-img:last-child .glyphicon-arrow-right {
                display:none;
            }
            .goods-pic img {
                margin:5px; max-width:500px;max-height:300px;
            }
            .goods-pic-operate {
                position:absolute;bottom: 0; width:100%; text-align: center;background:gainsboro; display:none;
            }
            .goods-address div.col-xs-2  {
                padding-right: 0;
            }

            .ad-pic-operate {
                position:absolute;bottom: 0; width:100%; text-align: center;background:gainsboro; display:none;
            }
            .col-xs-2 {
                padding-right: 0;
            }
        </style>
    </head>
    <body class="wrap">
        <ul class="nav nav-tabs" role="tablist" id="tabs">
            <li role="presentation"><a href="/admin/config/category">所有分类</a></li>
            <li role="presentation"class="active"><a href="#cacheAdd" >{% if res.id is defined %}修改分类{% else %}添加分类{% endif %}</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpannel" class="tab-pane active" id="adAdd" style="padding-top:20px;">
                <form class="form-horizontal" method="post" action="/admin/config/saveCate">
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">分类名称</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="text" id="name" name="name" placeholder="请输入分类名称" required="required" value="{% if res.name is defined %}{{res.name}}{% endif %}"/>
                        </div>
                    </div>
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">英文名称</label>
                        <div class="col-xs-3">
                            <input class="form-control" type="text" id="en_name" name="en_name" placeholder="请输入分类英文名称(开发用)" required="required" value="{% if res.en_name is defined %}{{res.en_name}}{% endif %}"/>
                        </div>
                    </div>
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">分类描述</label>
                        <div class="col-xs-3">
                            <textarea class="form-control" rows="3" name="descr">{% if res.descr is defined %}{{res.descr}}{% endif %}</textarea>
                        </div>
                    </div>
                    
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">其子项是否为枚举类型</label>
                        <div class="col-xs-3">
                        	<select class="form-control" id="isenum" name="isenum">
                        		<option {% if res.is_enum is defined %}{% if res.is_enum == 1 %} selected="selected" {% endif %}{% endif %} value="1">否</option>
                        		<option {% if res.is_enum is defined %}{% if res.is_enum == 0 %} selected="selected" {% endif %}{% endif %} value="0">是</option>
                        	</select>
                        </div>
                    </div>
                    
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">getter方法</label>
                        <div class="col-xs-3">
                        	<input class="form-control" type="text" id="getter" name="getter" placeholder="getter方法" value="{% if res.getter is defined %}{{res.getter}}{% endif %}"/>
                        </div>
                    </div>
                    
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">getter方法注释</label>
                        <div class="col-xs-3">
                        	<input class="form-control" type="text" id="getter_descr" name="getter_descr" placeholder="getter方法注释" value="{% if res.getter_descr is defined %}{{res.getter_descr}}{% endif %}"/>
                        </div>
                    </div>
                    
                    <div class="form-group has-feedback text-right">
                        <label class="col-xs-2 control-label">排序</label>
                        <div class="col-xs-2">
                            <input class="form-control" type="number" id="sort" name="sort" value="{% if res.sort is defined %}{{res.sort}}{% else %}50{% endif %}" />
                        </div>
                        <span class="col-md-2 text-left text-muted" style="padding-left: 15px; font-size: 12px; margin-top: 0.7%;"> 数字越小越靠前显示</span>
                    </div>
                    <div class="form-group" style="margin-top: 30px;">
                        <div class="col-sm-offset-2 col-sm-10">
                        	<input type="hidden" name="id" id="id" value="{% if res.id is defined %}{{res.id}}{% endif %}" />
                            <button type="submit" class="btn btn-success btn-sm" style="margin-right: 50px;width:70px;">保存</button>
                            <button type="button" class="btn btn-default btn-sm" id="cancel" style="width:70px;">取消</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        <script src="{{ urlAssetsJs }}/jquery/jquery-1.11.1.min.js"></script>
        <script src="{{ urlAssetsBundle }}/bootstrap/3.3.0/js/bootstrap.min.js"></script>
		<script src="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.js"></script>
		<script src="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.js"></script>
		
		<script>
			/* ----------- 取消操作 ------------*/
			$( '#cancel' ).click( function(){
				$.confirm( { 
					title: '确认要离开当前页面吗',
					confirm: function(){
						location.href = "/admin/config/category";
					}
				} );
			} );
		</script>
    </body>
</html>

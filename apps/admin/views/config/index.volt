<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="{{ urlAssetsCss }}/admin/base.css">
        <link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.css">
        <style>
            .operate {
                margin-right: 10px;
            }
            .operate:hover {
                cursor:pointer;
                color:green;
            }
        </style>
    </head>
    <body class="wrap">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#cacheList">配置管理</a></li>
            <li role="presentation"><a href="/admin/config/add">添加配置项</a></li>
            
            <li style="float:right;">
            	<div class="drop" id="selectCategory">
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    	请选择配置分类<span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
				    <li data-id="0"><a href="/admin/config/index?page={% if page.current is defined %}{{page.current}}{% else %}1{% endif%}&cateid=0">全部分类</a></li>
				  	{% if category is defined and category is not empty %}
				  	{% for item in category %}
				    	<li data-id="{{item.id}}"><a href="/admin/config/index?page=1&cateid={{item.id}}">{{item.name}}</a></li>
				    {% endfor %}
				    {% endif %}
				  </ul>
				</div>
        	</li>
        </ul>
        
        <div class="tab-content" style="padding:20px 0px;">
            <div role="tabpannel" class="tab-pane active" id="userList">
                {% if page.items is defined and page.items is not empty %}
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                        	<th>序号</th>
                        	<th>所属分类</th>
                        	<th>配置名称</th>
                        	<th>排序</th>
                            <th>关键字</th>
                            <th>值类型</th>
                            <th>配置值</th>
                            <th>操作时间</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody id="goodsTable">
                        {% for i,item in page.items %}
                        <tr data-id="{{ item.id | escape_attr }}">
                        	<td>{{ i+1 }}</td>
                            <td>{{ item.name | e }}</td>
                            <td>{{ item.title | e }}</td>
                            <td>{{ item.sort | e }}</td>
                            <td class="operate" style="color:green" id="copy{{ item.id | escape_attr }}" data-clipboard-text="{{ item.key | e }}" title="点击复制">{{ item.key | e }}</td>
                            <td>{% if 2 == item.valtype %} String {% elseif 1 == item.valtype %} Float {% else %} Int {% endif %}</td>
                            <td>{{ item.value | e }}</td>
                            <td>{{ date( 'Y-m-d H:i' ) | format( item.uptime ) | e }}</td>
                            <td data-id = "{{ item.id | escape_attr }}" >
                                <a href="/admin/config/edit/id/{{ item.id | escape_attr }}"><i class="glyphicon glyphicon-pencil operate" title="修改"></i></a>
                            </td>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
                {% else %}
                {% endif %}
                {% if page.total_pages > 1 %}
                <nav class="text-right" >
                    <ul class="pagination pagination-sm" style="margin-top:0"> 
                        <li class="{% if page.current == 1 %}disabled{% endif %}"><a href="/admin/config/index?page={{page.before | escape_attr }}&cateid={{cateId}}" >&laquo;</a></li>
                        {% if  1 != page.current and 1 != page.before %}
                        <li><a href="/admin/config/index?page=1&cateid={{cateId}}">1</a></li>
                        {% endif %}
                        {% if page.before != page.current  %}
                        <li><a href="/admin/config/index?page={{ page.before | escape_attr }}&cateid={{cateId}}"><span >{{ page.before |e}}</span></a></li>
                        {% endif %}
                        <li class="active"><a href="/admin/config/index?page={{ page.current | escape_attr }}&cateid={{cateId}}"><span >{{ page.current |e}}</span></a></li>
                        {% if page.next != page.current %}
                        <li><a href="/admin/config/index?page={{ page.next | escape_attr }}&cateid={{cateId}}">{{ page.next |e}}</a></li>
                        {% endif %}
                        {% if page.next  < page.last - 1 %}
                        <li><a href="#">...</a></li>
                        {% endif %}
                        {% if page.last != page.next %}
                        <li><a href="/admin/config/index?page={{ page.last | escape_attr }}&cateid={{cateId}}">{{ page.last |e}}</a></li>
                        {% endif %}
                        <li class="{% if page.current == page.last %}disabled{% endif %}"><a href="/admin/config/index?page={{page.next | escape_attr }}&cateid={{cateId}}" >&raquo;</a></li>

                    </ul>
                </nav>
                {% endif %}
            </div>
        </div>
        <div class="alert alert-danger text-center col-xs-2" style="display:none;margin-left: 40%;">
            <span>删除失败</span>
        </div>
    </body>
    
<script src="{{ urlAssetsJs }}/jquery/jquery-1.11.1.min.js"></script>
<script src="{{ urlAssetsBundle }}/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="{{ urlAssetsBundle }}/bootstrap/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/assets/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript">
var catid = {{cateId}};
$( 'div#selectCategory' ).find( 'ul li' ).each(function(){
	var val = $.trim( $( this ).attr( 'data-id' ) );
	if( val == catid )
	{
		var key = $.trim( $( this ).find( 'a' ).html() );
		$( 'div#selectCategory' ).find( 'button' ).html( key + ' <span class="caret"></span>' );
	}
});

//单击复制关键字
$(function(){
	$( 'tbody#goodsTable tr' ).each(function(){
		var id = $( this ).attr( 'data-id' );
		var clip = new ZeroClipboard( document.getElementById( 'copy'+id ), {
		  moviePath: "/assets/ZeroClipboard/ZeroClipboard.swf"
		} );
		
		// 复制内容到剪贴板成功后的操作
		clip.on( 'complete', function(client, args) {
			toastr.success( '已成功复制至剪切板\n' );
		});
	});
});

</script>
</html>
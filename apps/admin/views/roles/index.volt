{{ partial( 'public/header') }}
</head>
    <body class="wrap">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#rolesList">角色</a></li>
            <li role="presentation"><a href="/admin/roles/add">添加角色</a></li>
        </ul>
        <div class="tab-content page-content">
            <div role="tabpannel" class="tab-pane active" id="rolesList">
                {% if rolesList | length %}
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>角色名</th>
                            <th>角色描述</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% for item in rolesList %}
                        <tr>
                            <td>{{ item[ 'name' ] }}</td>
                            <td>{{ item[ 'descr' ]}}</td>
                            <td>
                                <i class="glyphicon glyphicon-trash operate roleDelete" data-id="{{ item[ 'id'] }}" style="margin-right: 10px;" title="删除"></i>
                                <a href="{{ url( 'admin/roles/edit?id=' ) }}{{ item[ 'id' ] }}" title="编辑" ><i class="glyphicon glyphicon-pencil operate userEdit" style="margin-right: 10px;"></i></a>
                            </td>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
                {% else %}
                	<p class="no_info">无角色</p>
                {% endif %}
            
            </div>
        </div>
     
       {{ partial( 'public/footer' ) }}
        <script>
            $( function(){
               /*----------------删除-----------------*/
               var csrf = { key: '{{ security.getTokenKey()}}', token: '{{ security.getToken()}}'};
               $( 'table' ).on( 'click', '.roleDelete',  function(){
                    var id = $( this ).attr( 'data-id' );
                    var data = $.extend( csrf, { id: id });
                    var _this = this;
                   
					$.confirm( { title: '是否删除角色?', confirm: function(){
						$.post( '/admin/roles/delete', data, function( ret ){
							if( !ret.status ){
                                $( _this ).parents( 'tr' ).remove();
                            }else{
                                toastr.error( ret.msg );
                            }
                            csrf = { key: ret.key, token: ret.token };
                        }, 'json').error(function(){ //网络不通
                            toastr.error( '网络不通' );
						} );
					} } );
				} );
				
			} );
        </script>
    </body>
</html>
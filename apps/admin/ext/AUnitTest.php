<?php
namespace apps\admin\ext;

abstract class AUnitTest implements \Phalcon\Di\InjectionAwareInterface
{
    protected $di;
    
    public abstract function run();
    
    /**
     * {@inheritDoc}
     * @see \Phalcon\Di\InjectionAwareInterface::setDI()
     */
    public function setDI(\Phalcon\DiInterface $dependencyInjector)
    {
       $this->di = $dependencyInjector; 
    }

    /**
     * {@inheritDoc}
     * @see \Phalcon\Di\InjectionAwareInterface::getDI()
     */
    public function getDI()
    {
        return $this->di;
    }
    
    
}


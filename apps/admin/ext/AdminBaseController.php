<?php

/**
 * @comment Admin基类
 * @author fzq
 * @date 2015-1-20
 * @comment 基类不采用统一注释格式
 */
namespace apps\admin\ext;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use apps\admin\models\PriUsers;
use enums\DBEnums;
use enums\PriEnums;
use Phalcon\Mvc\Controller;
use apps\admin\enums\MessageEnums;

class AdminBaseController extends Controller
{
	use \apps\admin\libraries\TAdminAcl;
	
    protected $isSuperAdmin = false;
    protected $userInfo = array();
    
    public function initialize()
	{	
        $this->userInfo = $this->session->get( 'userInfo' ); // 判断是否登录 - session
        if( !empty( $this->userInfo ) )
        {
            //判断数据库是否存在user，不存在则返回登录页面
            if( !PriUsers::findFirst( array( 'id=?0 and delsign=' . DBEnums::DELSIGN_NO,
                    'bind' => [ $this->userInfo[ 'id' ] ]) ) )
            {
                $this->response->redirect( '/admin/login/index' );
            }
            
            $roleId = $this->userInfo[ 'role_id' ]; 
            if( $roleId == PriEnums::SUPER_ADMIN_ID ) //超级管理员不参加角色
            {
            	$this->isSuperAdmin = true;
                return true;
            }
            
            $arrRet = $this->isAllow( $roleId );
            if( $arrRet[ 'status' ] == PriEnums::STUATUS_UNNOEMAL )
            {
                return $this->send( $arrRet[ 'msg' ] );
            }
            return true;
        }
        else
       	{
//       	    return $this->dispatcher->forward( array( 'controller' => 'error', 'action' => 'toLogin', 'params' =>
//       	        array( 'msg' => '登录超时，请重新登录' ) )); //自动登录

            return $this->response->redirect( '/admin/login/index' );
        }
	}
    
    /**
     * csrf 检验
     * return bool
     */
    protected function csrfCheck()
    {
        $key = $this->request->getPost( 'key' );
        $token = $this->request->getPost( 'token' );
        
        if( ( $key && $token && $this->security->checkToken( $key, $token ) ) || $this->security->checkToken() )
        {
            return true;
        }
        else
        {
            return $this->error( 'csrf校验不正确' );
        }
        
    }
    
    /**
     * 验证一下是否是自己
     */
    protected function checkSelf( $userId = 0 )
    {
        if( $userId && $this->userInfo[ 'id' ] != $userId ) //只能编辑自己， 平台可以编辑他人的
        {
            return $this->send( '你没有权限使用其他用户的账号' );
        }
        return true;
    }
    
    
    /**
     * 消息输出,页面跳转和ajax返回
     * param string $msg
     */
    protected function send( $msg )
    {
        return $this->dispatcher->forward( [  'controller' => 'error', 'action' => 'error', 'params' => [ 'msg' => $msg ] ]);
    }
    
    /**
     * 消息的输出，ajax返回
     * param int $status 状态 0：代表成功，1：代表失败，2:代表其
     * param string $msg 消息内容
     * param array $data 其他自定义数据
     */
    protected function message( $status = 0, $msg = '', $data = array() )
    {
        $ret = array();
        if( $this->request->isPost() || $this->request->isAjax() ) //post请求才进行csrf
        {
            $ret[ 'key' ] = $this->security->getTokenKey();
            $ret[ 'token' ] = $this->security->getToken();
        }
        $ret[ 'status' ] = $status;
        $ret[ 'msg' ] = $msg;
        
        if( is_array( $data ) )
        {
           $ret = array_merge( $ret, $data );
        }
        echo json_encode( $ret );
    }
    
    /**
     * 成功消息， ajax返回 
     * param string $msg
     * param array $data 其他自定义数据
     */
    protected function success( $msg = '', $data = array() )
    {
        $this->message( MessageEnums::MESSAGE_SUCCESS, $msg, $data );
        return true;
    }
    
     /**
     * 错误消息, ajax返回 
     * param string $msg
     * param array $data 其他自定义数据
     */
    public function error( $msg = '', $data = array() )
    {
        $this->message( MessageEnums::MESSAGE_ERROR, $msg, $data );
        return false;
    }
    
      
    /**
     * 显示404错误页面
     */
    public function show404( $msg = '' )
    {
        if( ! empty( $_SERVER[ 'HTTP_REFERER']))
        {
            $referer = str_replace( '/', '$', $_SERVER[ 'HTTP_REFERER' ] );
        }
        else
        {
	        $referer = '';
        }
	    $this->response->redirect( '/admin/index/show404/msg/' . $msg . '/referer/'. $referer );
    }
    
    
    public function csrfInfo()
    {
    	if( $this->request->isPost() )//post请求才进行csrf
    	{
    		$ret[ 'key' ] = $this->security->getTokenKey();
    		$ret[ 'token' ] = $this->security->getToken();
    	}
    }
    
//     /**
//      * 返回csrf数组
//      * @return multitype:string
//      */
//     public function getCsrfInfo()
//     {
//     	return array( 'key' => $this->security->getTokenKey(), 'token' => $this->security->getToken() );
//     }
}


<?php
namespace apps\admin;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Events\Manager as EventsManager;
use apps\admin\listeners\DispatcherListener;

use \apis\BackendHomeData;
use \apis\SysInfoData;
use \apis\ArticleData;
use \apis\AdsData;
use \apis\SitesData;
use \apis\FriendLinkData;
use \apis\MenusData;
use \apis\SearchData;
use \apis\SlideData;

class Module implements ModuleDefinitionInterface
{

	public function registerAutoloaders(\Phalcon\DiInterface $di=null)
	{
		$loader = new \Phalcon\Loader();

		$loader->registerNamespaces(array(
			'apps\admin\controllers' => APP_ROOT . 'apps/admin/controllers/',
			'apps\admin\models' => APP_ROOT . 'apps/admin/models/',
			'apps\admin\plugins' => APP_ROOT . 'apps/admin/plugins/',
            'apps\admin\commbiz' => APP_ROOT . 'apps/admin/commbiz/',
			'apps\admin\listeners' => APP_ROOT . 'apps/admin/listeners/',
			'apps\admin\enums' => APP_ROOT . 'apps/admin/enums/',
			'apps\admin\libraries' => APP_ROOT . 'apps/admin/libraries/',
			'apps\admin\vos' => APP_ROOT . 'apps/admin/vos/',
		    'apps\admin\biz' => APP_ROOT . 'apps/admin/biz/',
		    'apps\admin\ext' => APP_ROOT . 'apps/admin/ext/',
		    'apps\admin\test' => APP_ROOT . 'apps/admin/test/',
		));

		$loader->register();
	}

	/**
	 * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
	 */
	public function registerServices(\Phalcon\DiInterface $di=null)
	{
		//Registering a dispatcher
		$di->set('dispatcher', function() {

			$dispatcher = new \Phalcon\Mvc\Dispatcher();

			//Attach a event listener to the dispatcher
			$eventManager = new \Phalcon\Events\Manager();
			//$eventManager->attach('dispatch', new \Acl( 'admin' ));
 			
 			$eventManager->attach( 'dispatch', new DispatcherListener() );
			
			$dispatcher->setEventsManager( $eventManager );
			$dispatcher->setDefaultNamespace( "apps\\admin\\controllers\\" );
			return $dispatcher;
		});

		//Registering the view component
		$di->set('view', function() {
			
			$view = new \Phalcon\Mvc\View();
		
			$view->setViewsDir(APP_ROOT . 'apps/admin/views/');
			$view->registerEngines(array(
					'.volt' => function ($view, $di) {
						$volt = new VoltEngine($view, $di);
						$volt->setOptions(array(
								'compiledPath' => APP_ROOT . 'apps/admin/cache/',
								'compiledSeparator' => '_',
								'compileAlways' => true
						));
						return $volt;
					},
				//	'.phtml' => 'Phalcon\Mvc\View\Engine\Php'
				));
		
			$view->setVars( array( 'urlAssetsJs' => $this[ 'urlCfg' ]->url->cdn_assets_js,//UrlEnums::URL_ASSETS_JS,cdn_assets_img
					'urlAssetsCss' => $this[ 'urlCfg' ]->url->cdn_assets_css,//URLEnums::URL_ASSETS_CSS,
					'urlAssetsBundle' => $this[ 'urlCfg' ]->url->cdn_assets_bundle,//UrlEnums::URL_ASSETS_BUNDLE,
					'urlAssetsImg' => $this[ 'urlCfg' ]->url->cdn_assets_img,//UrlEnums::URL_ASSETS_IMG,
					'domain' => $this[ 'urlCfg' ]->url->url_domain,
			));
			return $view;
		}, true );

		$di->set( 'db', function () {
			
					$db = new \Phalcon\Db\Adapter\Pdo\Mysql( array(
							'adapter'  => $this['dbCfg']->db->adapter,
							'host'     => $this['dbCfg']->db->host,
							'username' => $this['dbCfg']->db->username,
							'password' => $this['dbCfg']->db->password,
							'dbname'   => $this['dbCfg']->db->dbname,
							'charset'  => $this['dbCfg']->db->charset
					) );
		
					$eventsMgr = new EventsManager();
					$dbListener = new \apps\admin\listeners\DbListener();
		
					$eventsMgr->attach( 'db', $dbListener );
		
					$db->setEventsManager( $eventsMgr );
		
					return $db;
				}, true );
		

        $di->set ( 'sysinfo' , function(){ return new SysInfoData(); }, true );
        $di->set ( 'article', function(){ return new ArticleData (); }, true);
        $di->set ( 'ad', function(){ return new AdsData(); }, true );
        $di->set ( 'site' , function(){ return new SitesData(); }, true );
        $di->set ( 'flink', function(){ return new FriendLinkData(); }, true );
        $di->set ( 'menu', function(){ return new MenusData(); }, true );
        $di->set ( 'search', function(){ return new SearchData(); }, true );
        $di->set ( 'slidePics', function(){ return new SlideData(); }, true );
        $di->set ( 'backendHome', function(){ return new BackendHomeData(); }, true );
	}
}
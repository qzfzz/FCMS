<?php

/**
 * 文章分类
 * @author hfc
 * time 2015-7-27
 */

namespace apps\admin\controllers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use apps\admin\models\ArticleCats;
use enums\DBEnums;;
use helpers\TimeUtils;
use apps\admin\ext\AdminBaseController;

class ArticlecatsController extends AdminBaseController
{

    public function initialze()
    {
        parent::initialize();
        //$this->checkPlatform();
    }

    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '文章分类首页' )
     * @method( method = 'indexAction' )
     * @op( op = 'r' )
     */
    public function indexAction( )
    {
        $articleCats = new ArticleCats();
        $this->view->articleCats = $articleCats->getCategoryTree();
    }

    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '添加分类' )
     * @method( method = 'addAction' )
     * @op( op = '' )
     */
    public function addAction()
    {
//        $this->view->articleCats = $this->_getCategoryTree();
        $articleCats = new ArticleCats();
        $this->view->articleCats = $articleCats->getCategoryTree();
    }

    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '编辑分类' )
     * @method( method = 'editAction' )
     * @op( op = '' )
     */
    public function editAction()
    {
        $cateId = $this->request->getQuery( 'id', 'int' );
        $articleCats =  ArticleCats::findFirst( array( 'id=?0', 'bind' => array( $cateId), 'columns' => 'id,name,url,parent_id as pid, descr'));

        if( $articleCats )
        {
            $this->view->articleCats = $articleCats->toArray();
        }

        $articleCats = new ArticleCats();
        $this->view->allCats = $articleCats->getCategoryTree();
//         $this->view->allCats = $this->_getCategoryTree(); //所有分类
    }

    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '添加一个新分类' )
     * @method( method = 'insertAction' )
     * @op( op = 'c' )
     */
    public function insertAction()
    {
        if( ! $this->csrfCheck() ){ return false; }

        $data[ 'name' ] = $this->request->getPost( 'cateName', 'string' );
        $data[ 'parent_id' ] = $this->request->getPost( 'parentId', 'int' );
        $data[ 'addtime' ] = $data[ 'uptime' ] = TimeUtils::getFullTime();
        $data[ 'delsign' ] = DBEnums::DELSIGN_NO;
        $data[ 'title' ] = $data['name'];
        $data[ 'descr' ] = $this->request->getPost( 'descr', 'string' );

        if( !ArticleCats::checkExist( $data[ 'name' ] ) )
        {
            $articleCats = new ArticleCats();
            if( $articleCats->save( $data ) )
            {
                return $this->success( '保存成功' );
            }
            else
            {
                return $this->error( '保存失败' );
            }
        }
        else
        {
            return $this->error( '分类已经存在了' );
        }
    }

    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '更新分类' )
     * @method( method = 'updateAction' )
     * @op( op = 'u' )
     */
    public function updateAction()
    {
        if( ! $this->csrfCheck() ){ return false; }

        $data[ 'id' ] = $this->request->getPost( 'id', 'int' );
        $data[ 'name' ] = $this->request->getPost( 'cateName', 'string' );
        $data[ 'parent_id' ] = $this->request->getPost( 'parentId', 'int' );
        $data[ 'uptime' ] = date( 'Y-m-d H:i:s' );
        $data[ 'url' ] = $this->request->getPost( 'url', 'string' );
        $data[ 'descr' ] = $this->request->getPost( 'descr', 'string' );

        //判断一下是否重命名了
        $where = array(
            'conditions'=>'delsign=:del: and id <> :id:',
            'bind'		=> array( 'del' => DBEnums::DELSIGN_NO, 'id' => $data[ 'id' ] ),
        );
        $articleCat = ArticleCats::findFirst( $where );
        if( $articleCat )
        {
            if( $articleCat->name == $data[ 'name' ] )
            {
                return $this->error( '分类已经存在了' );
            }
        }

        $status = $articleCat->update( $data );

        if( $status )
        {
            return $this->success( '更新成功' );
        }
        else
        {
            return $this->error( '更新失败' );
        }
    }

    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '删除分类' )
     * @method( method = 'deleteAction' )
     * @op( op = 'd' )
     */
    public function deleteAction()
    {
        if( ! $this->csrfCheck() ){ return false; }

        $data[ 'id' ] = $this->request->getPost( 'id', 'int' );
        $data[ 'uptime' ] = date( 'Y-m-d H:i:s' );

        $articleCats = new ArticleCats();
        $status = $articleCats->deleteCats( $data );

        if( $status )
            return $this->success( '删除成功' );
        else
            return $this->error( '删除失败' );
    }

    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '判断分类是否重名' )
     * @method( method = 'checkNameAction' )
     * @op( op = '' )
     */
    public function checkNameAction()
    {
        if( ! $this->csrfCheck() ){ return false; }

        $name = $this->request->getPost( 'name', 'string' );

        if( ArticleCats::checkExist( $name ) )
        {
            return $this->error( '分类名称已经存在' );
        }
        else
        {
            return $this->success( '分类可以添加' );
        }
    }

}

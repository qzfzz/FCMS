<?php

/**
 * 图片管理器
 *
 * @author hfc
 * @time 2015-8-11
 */

namespace apps\admin\controllers;

use enums\DBEnums;;
use MongoId;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use apps\admin\ext\AdminBaseController;

class ImagesController extends AdminBaseController
{
    private $mdb = null;
    
    public function initialize()
    {
        parent::initialize();
        $this->mdb = $this->mongodb->selectCollection( 'space' );
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '首页' )	
     * @method( method = 'indexAction' )
     * @op( op = 'r' )		
    */
    public function indexAction( $limit )
    {
        $pageNum = $this->dispatcher->getParam( 'page' );
        $currentPage = $pageNum ? $pageNum : 1; //默认当前页为 1 
        $limit = $limit ?: 30;

        $pid = $this->dispatcher->getParam( 'pid' ); //当前的目录id ,为字符串类型
        $where[ 'pid' ] = $pid ? $pid : '0';
        $nav = $this->getParent( $where[ 'pid' ]);
        if( $nav )
        {
            $this->view->nav = $nav;
        }
        $where[ 'delsign' ] =  DBEnums::DELSIGN_NO;
        $where[ 'shopid' ] = $this->shopId  ;

        $data =  iterator_to_array( $this->mdb ->find( $where )->sort( array( 'addtime' => -1 ) ) );
        $paginator = new PaginatorArray(
            array(
            "data" => $data,
            "limit" => $limit,
            "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        if( $page )
        {
            $page->pid = $where[ 'pid' ];
            $this->view->page = $page;
        }

    }
    
    /**
     * 选择图片空间的图片
     */
    public function selectAction()
    {
        $this->indexAction( 12 );
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '获得目录下的图片' )	
     * @method( method = 'getFilesAction' )
     * @op( op = '' )		
    */
    public function getFilesAction()
    {
        $id = $this->request->getQuery( 'id', 'string' );
        $data[ 'images' ] = iterator_to_array( $this->mdb->find( array( 'pid' => $id, 'delsign' => DBEnums::DELSIGN_NO )) );

        if( $data[ 'images'] )
        {
            return $this->success( '打开目录成功', $data  );
        }
        else
        {
            return $this->error( '打开目录失败' );
        }
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '重命名 文件夹或者图片' )	
     * @method( method = 'renameAction' )
     * @op( op = 'u' )		
    */
    public function renameAction()
    {
        $id = $this->request->getPost( 'id', 'string' );
        $folderName = $this->request->getPost( 'folderName', 'string' );
  
        $where[ '_id' ] = ( object ) new MongoId( $id );
        $where[ 'shopid' ] =  $this->shopId ; //只可重命名自己的图片或者文件夹
        
        $ret = $this->mdb->update( $where, array( '$set' => array( 'original' => $folderName ) ) );
        if(  $ret[ 'updatedExisting']  )
        {
            return $this->success( '重命名成功' );
        }
        else
        {
            return $this->error( '重命名失败' );
        }
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '新建文件夹' )	
     * @method( method = 'newFolderAction' )
     * @op( op = 'c' )		
    */
    public function newFolderAction()
    {
        $data[ 'pid' ] = $this->request->getPost( 'pid', 'string' ); //当前目录的id
        $data[ 'original' ] = $this->request->getPost( 'folderName', 'string' );
        $this->validation( $data );
        
        $data[ 'type' ] = '.';//代表是目录
        $data[ 'addtime' ] = $data[ 'uptime' ] = time();
        $data[ 'delsign' ] = 0;
        $data[ 'shopid' ] =  $this->shopId;
        
        $this->mdb->insert( $data );
        if( isset( $data[ '_id']) )
        {
            return $this->success( '新建成功', $data );
        }
        else
        {
            return $this->error( '新建失败' );
        }
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '删除文件夹或者图片' )	
     * @method( method = 'deleteAction' )
     * @op( op = 'd' )		
    */
    public function deleteAction()
    {
        $id = $this->request->getPost( 'id', 'string' );
        $where[ '_id' ] = ( object ) new MongoId( $id );
        $where[ 'shopid' ] = intval( $this->shopId ); //只可删除自己的图片
        
        $ret = $this->mdb->update( $where, array( '$set' => array( 'delsign' => 1 )));
        if(  $ret[ 'updatedExisting']  )
        {
            return $this->success( '删除成功' );
        }
        else
        {
            return $this->error( '删除失败' );
        }
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '删除多个文件夹或者图片' )	
     * @method( method = 'deleteMoreAction' )
     * @op( op = 'd' )		
    */
    public function deleteMoreAction()
    {
        $arrId = $this->request->getPost( 'id');

        foreach( $arrId as $id )
        {
            $aId[] = ( object ) new MongoId( $id );
        }
        $where[ '_id' ] =  array( '$in' => $aId );
        $where[ 'shopid' ] = intval( $this->shopId ); //只可删除自己的图片

        $ret = $this->mdb->update( $where, array( '$set' => array( 'delsign' => 1 )), array("multiple" => true)); //删除多个文件
        if(  $ret[ 'updatedExisting']  )
        {
            return $this->success( '删除成功' );
        }
        else
        {
            return $this->error( '删除失败' );
        }
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '上传图片 页面显示' )	
     * @method( method = 'uploadAction' )
     * @op( op = '' )		
    */
    public function uploadAction()
    {
        $this->view->id = $this->request->getQuery( 'id', 'string' );
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '保存图片到数据库中' )	
     * @method( method = 'saveImageAction' )
     * @op( op = 'c' )		
    */
    public function saveImageAction()
    {
        $id = $this->request->getPost( 'pid', 'string' );
        $pid = $id ? $id : '0'; 
        if ($this->request->hasFiles() == true) {
//             $config = include APP_ROOT . '/config/ueditor.php';
            $config = $this->di[ 'ueditorCfg' ];//include APP_ROOT . '/config/ueditor.php';
            
            foreach ($this->request->getUploadedFiles() as $file) {
                $root = $_SERVER[ 'DOCUMENT_ROOT' ];
                if( strpos( $root, 'public') == false )
                {
                    $root .= '/public';
                }
                $originalName  = pathinfo( $file->getName(), PATHINFO_FILENAME );
                if( $config[ 'type'] )   //保存到fastdfs
                {
                    $filePath = $root . '/upload';
                    $originalPath = $this->createThumbImage( $file, $filePath ); //生成缩略图
                    if(  ! $originalPath )
                    {
                       return false;
                    }
                    $extension = $file->getExtension();
                    
                    $arrFileInfo = $this->fastdfs->uploadFile( $originalPath, $extension ); //随机分组
                    if( $arrFileInfo !== false ) //上传成功
                    {
                         $url = '//' . $arrFileInfo[ 'ip_addr' ] . '/' . $arrFileInfo[ 'group_name' ] . '/' . $arrFileInfo[ 'filename' ];
                         $thumbPath = str_replace( '.jpg', '!220x220.jpg', $originalPath );
                         $this->fastdfs->uploadFile( $thumbPath, $extension, 0, $arrFileInfo[ 'group_name']); //同一分组                    }
                         $thumbmPath = str_replace( '.jpg', '!50x50.jpg', $originalPath );
                         $this->fastdfs->uploadFile( $thumbmPath, $extension, 0, $arrFileInfo[ 'group_name']); //同一分组                    }
                    }
                }
                else //保存到本地
                {
                    $uploadDir = '/upload/image/space/' . $this->shopId . '/goods';
                    $url = $uploadDir . '/' . $originalName . '.jpg';
                    $filePath = $root . $uploadDir;
                   
                    if( !file_exists( $filePath ) && ! mkdir( $filePath, 777, true ) )
                    {
                        echo( json_encode( [ 'error' => '创建目录失败'] ) );
                        return true;
                    }
                    
                    $this->createThumbImage( $file, $filePath ); //生成缩略图
                }
                        
                $data[ 'original' ] =  $originalName;
                $data[ 'url' ] = $url;
                $data[ 'pid' ] = $pid;
                $data[ 'size' ] = $file->getSize();
                $data[ 'type'] = 'jpg'; //默认jpg
                $data[ 'addtime' ] = $data[ 'uptime' ] = time();
                $data[ 'shopid' ] = $this->shopId;
                $data[ 'delsign' ] = 0;
                $this->mdb->insert( $data );
                    
                $ret[ 'status' ] =  'success';
                echo json_encode( $ret );
            }
        }
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2016-5-11' )
     * @comment( comment = '生成缩略图' )	
     * @method( method = 'createThumbImage' )
     * @op( op = '' )		
    */
    public function createThumbImage( $image, $imagePath )
    {
        $x =  $this->request->getPost( 'x', 'int', 0 );
        $y =  $this->request->getPost( 'y', 'int', 0 );
        $width =  $this->request->getPost( 'w', 'int', 0 );
        $height =  $this->request->getPost( 'h', 'int', 0 );
        
        $size = $image->getSize();
        if( $size >  2097152 ) //图片的大小不可超过2m
        {
            echo( json_encode( [ 'error' => '图片大小不可超过2m' ] ) );
            return false;
        }

        $tempName = $image->getTempName();
        $imageSize = getimagesize( $tempName ); //获得宽度，高度
        $tw = 400; $thumb = 220; $thumb_m = 50; //前端是按400计算的
        $r =   $imageSize[0] / $tw  ;
              
        $sw = $width * $r ?: $imageSize[0]; //不裁剪
        $sh = $height * $r ?: $imageSize[1];
        $sx = intval( $x * $r ); $sy = intval( $y * $r );  //还原尺寸

        if( $imageSize[0] < $tw || $imageSize[1] < $tw )
        {
            echo( json_encode( [ 'error' => '截取的图片要比400px×400px大' ] ) ); 
            return false;
        }
        //缩略图路径
        $imageName = iconv(  'UTF-8' , 'gb2312', $image->getName());//中文文件名才可以显示正常
        $name = pathinfo( $imageName, PATHINFO_FILENAME );
        
        $thumbPath =   $imagePath .  '/' . $name . '!220x220.jpg'; //此图为220x220
        $minPath = $imagePath . '/' . $name . '!50x50.jpg'; //此图为50x50
        $originPath =  $imagePath . '/' . $name . '.jpg' ;

        $arrSize = [ $tw, $thumb, $thumb_m ];//400, 220  50 图片的生成
        foreach( $arrSize as $item )
        {
            $tw = $th = $item; //生成的图是宽度和高度一致，且已宽度为准  
            $simg = imagecreatefromjpeg( $tempName );
            $dimg = imagecreatetruecolor($tw, $th);
            imagecopyresampled( $dimg, $simg, 0,0, $sx, $sy, $tw, $th, $sw, $sh );
            $ipath = ( $item === $thumb ) ? $thumbPath : ( $item === $thumb_m ? $minPath : $originPath );
            imagejpeg( $dimg, $ipath , 100 );
            imagedestroy( $dimg );
        }
        return $originPath;
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-8-24' )
     * @comment( comment = '对输入的数据进行验证' )	
     * @method( method = 'validation' )
     * @op( op = '' )		
    */
    private  function validation( $data = array() )
    {
        $validation = new Validation();
        $validation->add( 'original', new PresenceOf(array(
            'message' => '文件夹名必填'
        )));
        
        $messages =  $validation->validate( $data );
        if( count( $messages ))
        {
            foreach( $messages as $msg )
            {
                return $this->error( $msg->getMessage() );
            }
        }
    }
    
    /**
     * 根据id，查找自己所有的父
     * param type $id
     */
    private function getParent( $id = '0' )
    {
        $ret = array();
        if( $id != '0' )
        {
            while(1)
            {
                $_id = (object) new \MongoId( $id );
                $data = iterator_to_array( $this->mdb->find( array( '_id' => $_id ))->fields( array( '_id' =>true, 'original' => true, 'pid' => true )));
                foreach( $data as $value )
                {
                    $parent = $value;
                }
                if( ! $parent )
                {
                    return $ret;
                }
              
                array_unshift( $ret, array( 'id' => $parent[ '_id'], 'original' => $parent[ 'original'] )) ;
                if( $parent[ 'pid' ] == '0' )
                {
                    break;
                }
                $id = $parent[ 'pid' ];
            }
        }
        return $ret;
    }
    
    /**
     * @author( author='hfc' )
     * @date( date = '2015-5-10' )
     * @comment( comment = '生成相应尺寸图片' )	
     * @method( method = 'cropImageAction' )
     * @op( op = '' )		
    */
    public function cropImageAction()
    {
        $x = $this->request->getPost( 'x', 'int', 0 ); 
        $y = $this->request->getPost( 'y', 'int', 0 ); 
        $w = $this->request->getPost( 'w', 'int', 0 ); 
        $h = $this->request->getPost( 'h', 'int', 0 ); 
        
        if( empty( $_FILES[ 'image']['tmp_name']))
        {
            return $this->error( '请截取图片' );
        }
       
        $targ_w = $targ_h = 400; //前端是按400截取
        $size = getimagesize($_FILES[ 'image']['tmp_name']); //实际尺寸
        $ratio =   $size[0] / $targ_w ; //宽度比例
        $srx  =  intval( $ratio * $x ); $sry =  intval( $ratio * $y ) ;
        $srw = intval( $ratio * $w );$srh =  intval( $ratio * $h );
        
        if( $size[0] < $targ_w || $size[1] < $targ_h ) //原图一定要比430要大
        {
            return $this->error( '原图宽度和高度都必须大于'. $targ_w. 'px' );
        }
        $jpeg_quality = 100;
        
        $img_r = imagecreatefromjpeg( $_FILES[ 'image'][ 'tmp_name']);
        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
        imagecopyresampled($dst_r,$img_r,0,0,$srx,$sry,$targ_w,$targ_h, $srw,$srh );

        imagejpeg($dst_r,'./upload/crop.jpeg',$jpeg_quality);
        imagedestroy( $dst_r );
        return $this->success( '截取成功' );
    }

}

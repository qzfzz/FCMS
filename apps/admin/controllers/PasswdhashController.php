<?php

namespace apps\admin\controllers;

namespace apps\admin\controllers;


use apps\admin\ext\AdminBaseController;
use apps\admin\models\PriUsers;
/**
 * 密码加密功能
 * @author Carey
 * time 2016-12-23
 */

class PasswdhashController extends AdminBaseController
{
	
	
	public function initialize()
	{
		parent::initialize();
	}
	
	/**
	 * @author( author='carey' )
	 * @date( date = '2016.12.23' )
	 * @comment( comment = '密码重置展示主页' )
	 * @method( method = 'indexAction' )
	 * @op( op = 'r' )
	 */
	public function indexAction()
	{
		
	}
	
	/**
	 * @author( author='carey' )
	 * @date( date = '2016.12.26' )
	 * @comment( comment = '平台用户密码加密功能' )
	 * @method( method = 'puserAction' )
	 * @op( op = 'r' )
	 */
	public function puserAction()
	{
		$arrRet = array();
		$arrRet['log'] = array();
		
		$users =  PriUsers::find();
		if( $users && count( $users ) )
		{
			foreach( $users as $i=>$user )
			{
				if( $user->pwd )
				{
					if( !$user->update( array( 'pwd' => sha1( $user->pwd ) ) ) )
					{
						$strMsg = '平台用户：编号为：' . $user->id . ' 登录名为： ' . $user->loginname . ' 密码加密失败。请检查数据纪录或重新操作。';
						array_push( $arrRet['log'] , $strMsg );
					}
				}
				else
				{
					$strMsg = '平台用户：编号为：' . $user->id . ' 登录名为： ' . $user->loginname . ' 密码加密失败。 该用户未设置密码';
					array_push( $arrRet['log'] , $strMsg );
				}
				
			}
			
			$arrRet['state'] = 0;
			$arrRet['msg'] = '平台用户密码已加密完成';
		}
		else
		{
			$arrRet['state'] = 1;
			$arrRet['msg'] = '暂无平台用户.';
		}
		
		echo json_encode( $arrRet );
	}
	
	
	
}


<?php

/**
 * 系统主框架以及公共显示区域
 * @author Bruce
 * time 2014-10-30
 */
namespace apps\admin\controllers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use enums\DBEnums;
use enums\PriEnums;
use apps\admin\models\SysIndeCfg;
use apps\admin\models\PriUsers;
use helpers\TimeUtils;
use enums\ImgBizEnums;
use apps\admin\ext\AdminBaseController;

class IndexController extends AdminBaseController
{
	private $bInit = false;
    public function initialize()
    {        
        $this->bInit = parent::initialize();
	}

    /**
     * 框架主体（首页）
     */
    public function indexAction( )
    {
        
    	if( !$this->bInit )
    	{
    		$this->view->disable();
    		return;
    	}
    	
        $userInfo = $this->userInfo;
        $leftMenu = $this->dataCache->get( PriEnums::LEFT_MENU_ADMIN . $userInfo[ 'role_id' ]  );

        $this->view->leftMenu = $leftMenu;
        $this->view->loginName = $userInfo[ 'loginname' ];
        $this->view->nickName = $userInfo[ 'nickname' ];
        
        if( isset( $userInfo[ 'avatarSrc' ] ) )
        {
        	$this->view->avatarSrc = $userInfo[ 'avatarSrc' ];
        }
        $this->view->uid = $userInfo[ 'id' ];
    }
    
    /**
     * 点击后台首页，进行的是这个操作
     */
    public function showAction()
    {
    	$userInfo = $this->userInfo;
    	$roleId = $userInfo[ 'role_id' ];
    	$where = array(
    		'column'	 => 'id,delsign,sort,display,name,line,sort,icon,color,size,role_id',
    		'conditions' => 'delsign=:del: and role_id=:roleId: and display=:display: ORDER BY sort DESC',
    		'bind'		 => array( 'del' => DBEnums::DELSIGN_NO, 'roleId' => $roleId, 'display' => 0 ),
    	);
    	$res = SysIndeCfg::find( $where );
    	if( false == $res || count( $res ) <= 0 )
    	{
    		$this->view->pick( 'index/cms' );
    	}
    }
    
    /**
     * 获取模块信息
     */
    public function getmodalsAction()
    {
    	$objRet = new \stdClass();
    	$userInfo = $this->userInfo;
    	$roleId = $userInfo[ 'role_id' ];
    	$where = array(
    			'column'	=> 'id,delsign,sort,display,name,line,sort,icon,color,size,role_id',
    			'conditions'=> 'delsign=:del: and role_id=:roleId: and display=:display: ORDER BY sort DESC',
    			'bind'		=> array( 'del' => DBEnums::DELSIGN_NO , 'roleId' => $roleId, 'display' => 0 ),
    	);
    	$res = SysIndeCfg::find( $where );
    	if( false != $res && count( $res ) > 0 )
    	{
    		$result = $res->toArray();
    		$i=0;
    		$curObj = $this;
    		foreach ( $result as $row )
    		{
    			$newCont = preg_replace_callback( '/%([a-z A-Z]+)-([a-z A-Z]+)%/', function( $matches ) use( $curObj ) {
    				return $curObj->$matches[1]->{ "get". "$matches[2]"}();
    			}, $row[ 'descr' ]);
    			
    			$result[$i][ 'info' ] = $newCont;
    			$i++;
    		}
    		$objRet->state = 0;
    		$objRet->data = $result;
    	}
    	else
    	{
    		$objRet->state = 1;
    	}
    	echo json_encode( $objRet );
    }
    
//    public function ssAction()
//    {
//
//    }
//
//    public function sessAction()
//    {
//        var_dump( $_REQUEST );
//        var_dump( $_POST );
//        var_dump( $_GET );
//    }
    
    /**
    * 错误显示
    */
    public function show404Action( )
    {
        $data = $this->dispatcher->getParams();
        if(  ! empty( $data )  )
        {
            if( !empty( $data['referer'] ) )
            {
                $data['referer'] = str_replace( '$', '/', $data['referer'] );
            }
            $this->view->data = $data;
        }
        
        $this->view->pick( '/index/404' );
    }
        
    /**
     * 框架首页内容（示例）
     */
    public function fcmshomeAction()
    {
        
    }
    
    /**
     ************** 示例页面 ***************** 
     * 示例列表
     */
    public function demoAction()
    {
        $this->view->pick( '/index/demo/list' );
    }
    
    /**
     * 显示添加示例页面
     */
    public function showAddDemoAction()
    {
        $this->view->pick( '/index/demo/add' );
    }
    
    
    /**
     * 帐户设置页面
     */
    public function userInfoAction()
    {
        $userInfo = $this->userInfo;
        
        $this->view->bizType = ImgBizEnums::ADMIN_USER_AVATAR;
        $this->view->userInfo = $userInfo;
    }
    
    /**
     * 帐户设置修改处理
     */
    public function changeUserInfoAction()
    {
        //获取ajax传递过来的值
        if( $this->request->getPost() )
        {
            $name = $this->request->getPost( 'name', 'string' );
            $nickName = $this->request->getPost( 'nickname', 'string' );
            $email = $this->request->getPost( 'email', 'email' );
        }
        else
       	{
            //用户信息发送失败，请重试
            $res[ 'state' ] = 4;
        }
    	
        
        if( $this->userInfo )
        {
            $userInfoArr = $this->userInfo;
            $where = array(
                'id = ?0',
                'bind' => [ $userInfoArr[ 'id' ] ]
            );
            //判空
            if( $userInfo = PriUsers::findFirst( $where ) )
            {
                //更新数据库数据
                $userInfo->name = $name;
                $userInfo->nickname = $nickName;
                $userInfo->email = $email;
                //0:数据更新成功;1:数据更新失败
                $res[ 'state' ] = $userInfo->save()?0:1;
                
                //更新session中的信息数据
                $userInfoArr[ 'name' ] = $name;
                $userInfoArr[ 'nickname' ] = $nickName;
                $userInfoArr[ 'email' ] = $email;
                $this->session->set( 'userInfo', $userInfoArr );
            }
            else
           {
               //数据库中用户信息不存在
               $res[ 'state' ] = 2;
            }
        }
        else
       {
           //登录用户信息不存在，请退出后重新登录'
           $res[ 'state' ] = 3;
        }
        echo json_encode( $res );
    	
    }
    
    /**
     * 修改密码
     */
    public function changePswAction()
    {
    	if( ! $this->csrfCheck() ){ return false; }
    	
    	if( $userId = $this->request->getPost( 'userId' ) )
    	{
    	    if( ! $this->checkSelf( $userId ) )
    	    {
    	        return false;
    	    }
    	}
    	
        if( $this->request->getPost() )
        {
        	//密码加密  md5() + sha1()
            $oldPassword = $this->request->getPost( 'oldPassword' );
            $newPassword = $this->request->getPost( 'newPassword' );
            $rePassword = $this->request->getPost( 'rePassword' );
            
            if( $this->userInfo )
            {
                $userInfoArr = $this->userInfo;
                $where = array(
                    'id = ?0',
                    'bind' => [ $userInfoArr[ 'id' ] ]
                );
                //判空
                if( $userInfo = PriUsers::findFirst( $where ) )
                {
                    //检查原始密码是否正确
                    if( $oldPassword != $userInfo->pwd )
                    {
                        $res[ 'state' ] = 4;
                        $res[ 'key' ] = $this->security->getTokenKey();
                        $res[ 'token' ] = $this->security->getToken();
                        echo json_encode( $res );
                        return;
                    }
                    //更新数据库数据
                    $userInfo->pwd = $newPassword;
                    //0成功1失败
                    $res[ 'state' ] = $userInfo->save() ? 0 : 1;
                }
                else
                {
                    $res[ 'state' ] = 2;//用户信息不存在
                }
            }
            else
            {
                $res[ 'state' ] = 3;//登录用户信息不存在，请退出后重新登录;
            }
        }
       else
       {
        	$ret[ 'state' ] = 5;//信息传递失败，请重试
        }
        
        $res[ 'key' ] = $this->security->getTokenKey();
        $res[ 'token' ] = $this->security->getToken();
        echo json_encode( $res );
    }
    
    /**
     * @author( author='Carey' )
     * @date( date = '2016-03-18' )
     * @comment( comment = '用户修改头像' )
     * @method( method = 'avatarAction' )
     * @op( op = 'r' )
     */
    public function avatarAction()
    {
    	if( ! $this->csrfCheck() ){ return false; }
    	
    	$objRet = new \stdClass();
    	//将上传的图片存储在对应的库中
    	$imgTitle = $this->request->getPost( 'title' );
    	if( $imgTitle && !empty( $imgTitle ) )
    	{
//     	    $imgRes = new PriUsersAvatarPics();
//     	    $resorce_id = $imgRes->addImgs( $imgs, ImgBizEnums::ADMIN_USER_AVATAR, $this->userInfo[ 'id' ], $this->di );
//     	    if( !$resorce_id || false == $resorce_id )
//     	    {
//     	        $objRet->status  = 1;
//     	        $objRet->msg = '数据异常,请刷新后再试...';
//     	        $objRet->key = $this->security->getTokenKey();
//     	        $objRet->token = $this->security->getToken();
    	         
//     	        return json_encode( $objRet );
//     	    }
//     	    else
    	    {
    	        $where	= array(
    	            'conditions'=> 'delsign=:del: and id=:optid:',
    	            'bind'		=> array( 'del' => DBEnums::DELSIGN_NO, 'optid' => $this->userInfo[ 'id' ] ),
    	        );
    	        $result = PriUsers::findFirst( $where );
    	        if( $result )
    	        {
    	            $result->uptime = TimeUtils::getFullTime();
    	            
    	            assert( $this->di['session'] );
    	            
    	            $imgFileInfo = $this->di['session']->get( 'imgList' )[$imgTitle];
    	            
    	            assert( $imgFileInfo );
//     	            if( empty( $imgList ) )
//     	            {
//     	                $imgList = array();
//     	            }
//     	            $imgList[ $fileInfo['title'] ] = $fileInfo;
    	            
//     	            $this->di['session']->set( 'imgList', $imgList );
    	            
    	            $result->avatar = $imgFileInfo['asset_id'];
    	            if( $result->save() )
    	            {
//     	                $userInfo = $this->userInfo;
//     	                $img = new PriUsersAvatarPics();
//     	                $info = $img->getPicById( $result->id, $imgFileInfo['asset_id'] );
//     	                if( $info && count( $info ) > 0 )
//     	                {
//     	                    $userInfo[ 'avatar' ] = $info['url'];
//     	                }

                        $this->userInfo[ 'avatarSrc' ] = $imgFileInfo[ 'url' ];
    	                $this->session->set( 'userInfo', $this->userInfo );
    	                
    	                $objRet->status = 0;
    	                $objRet->msg = '用户头像已修改成功，请刷新页面！';
    	            }
    	            else
    	            {
    	                $objRet->status = 1;
    	                $objRet->msg = '对不起,修改失败请稍后再试..';
    	            }
    	        }
    	        else
    	        {
    	            $objRet->status = 1;
    	            $objRet->msg = '对不起,该用户未找到,修改失败..';
    	        }
    	    }
    	}
    	else 
    	{
    	    $objRet->status  = 1;
    	    $objRet->msg = '图片信息异常...';
    	}
    	
    	$objRet->key = $this->security->getTokenKey();
    	$objRet->token = $this->security->getToken();
    	
    	echo json_encode( $objRet );
    }
}








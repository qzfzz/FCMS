<?php

/**
 * 广告分类
 * @author yyl
 * time 2015-8-24
 */

namespace apps\admin\controllers;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use apps\admin\models\AdCats;
use enums\DBEnums;;
use helpers\TimeUtils;
use apps\admin\ext\AdminBaseController;

class AdcatsController extends AdminBaseController
{

    private $categorys = array();
    
    private $catNames = array();

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.24' )
     * @comment( comment = '广告分类首页' )
     * @method( method = 'indexAction' )		
     * @op( op = 'r' )		
     */
    public function indexAction()
    {
        $allCats = new AdCats();
        $this->view->adCats = $allCats->getCategoryTree();
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.24' )
     * @comment( comment = '添加分类显示' )
     * @method( method = 'addAction' )		
     * @op( op = 'r' )		
     */
    public function addAction()
    {
        $allCats = new AdCats();
        $this->view->adCats = $allCats->getCategoryTree();
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.24' )
     * @comment( comment = '编辑分类显示' )
     * @method( method = 'editAction' )		
     * @op( op = 'r' )		
     */
    public function editAction()
    {
        $cateId = $this->request->getQuery( 'id', 'int' );
        $adCats = AdCats::findFirst( array( 'id=?0', 'bind'    => array(
                        $cateId ),
                    'columns' => 'id,name,pid,width,height' ) );

        if( $adCats )
        {
            $this->view->adCats = $adCats->toArray();
        }
        
        $allCats = new AdCats();
        $this->view->allCats = $allCats->getCategoryTree(); //所有分类
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.24' )
     * @comment( comment = '添加分类' )
     * @method( method = 'insertAction' )		
     * @op( op = 'c' )		
     */
    public function insertAction()
    {
        if( ! $this->csrfCheck() ){ return false; }

        $data[ 'name' ] = $this->request->getPost( 'cateName', 'string' );
        $data[ 'height' ] = $this->request->getPost( 'height', 'int' );
        $data[ 'width' ] = $this->request->getPost( 'width', 'int' );
        $data[ 'pid' ] = $this->request->getPost( 'parentId', 'int' );
        $data[ 'addtime' ] = $data[ 'uptime' ] = TimeUtils::getFullTime();
        $data[ 'delsign' ] = DBEnums::DELSIGN_NO;

        $adCats = new AdCats();
        if( $adCats->save( $data ) )
            return $this->success( '保存成功' );
        else
            return $this->error( '保存失败' );
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.24' )
     * @comment( comment = '更新分类' )
     * @method( method = 'updateAction' )		
     * @op( op = 'u' )		
     */
    public function updateAction()
    {
    	if( ! $this->csrfCheck() ){ return false; }
    	
    	$objRet = new \stdClass();

        if( 0 != count( $this->request->getPost() ) )
        {
            $data = array();
            $data[ 'id' ] = $this->request->getPost( 'id', 'int' );
            $data[ 'name' ] = $this->request->getPost( 'catName', 'string' );
            $data[ 'pid' ] = $this->request->getPost( 'parentId', 'int' );
            $data[ 'height' ] = $this->request->getPost( 'height', 'string' );
            $data[ 'width' ] = $this->request->getPost( 'width', 'string' );
            $data[ 'uptime' ] = TimeUtils::getFullTime();
            
    	    $id = $this->request->getPost( 'id', 'int' );
    	    $where = array(
                "id = $id",
                'columns' => 'name',
    	    );
    	    $adCats = AdCats::findFirst( $where );
    	    if( $adCats )
    	    {
    	        $catsArr = $adCats->toArray();
    	    	if( $data[ 'name' ] != $catsArr[ 'name' ] )
    	    	{
    	    		$adCats = new AdCats();
                    $status = $adCats->updateCats( $data );
                    if( $status )
                    {
                        $objRet->state = 0;
                        $objRet->msg = '更新成功';
                    }
                    else
                    {
                        $objRet->state = 4;
                        $objRet->msg = '更新失败';
                    }
    	    	}
    	    	else
    	    	{
    	    	    $objRet->state = 1;
    	    	    $objRet->msg = '广告分类名称已存在，请更换';
    	    	}
    	    }
    	    else
    	    {
    	        $objRet->state = 2;
    	        $objRet->msg = '该广告分类信息在数据库中不存在';
    	    }
        }
        else
        {
            $objRet->state = 3;
            $objRet->msg = '未找到现有分类的id或参数未传递成功';
        }
        
        $objRet->key = $this->security->getTokenKey();
        $objRet->token = $this->security->getToken();
        
        echo json_encode( $objRet );
        
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.24' )
     * @comment( comment = '删除分类' )
     * @method( method = 'deleteAction' )		
     * @op( op = 'd' )		
     */
    public function deleteAction()
    {
    	if( ! $this->csrfCheck() ){ return false; }
    	
        $data[ 'id' ] = $this->request->getPost( 'id', 'int' );
        $data[ 'uptime' ] = TimeUtils::getFullTime();
        if( $this->shopId ) //只有本店的才可以操作广告
        {
            return $this->error( '你没有权限删除' );
        }

        $adCats = new AdCats();
        $status = $adCats->deleteCats( $data );

        if( $status )
        {
            return $this->success( '删除成功' );
        }
        else
        {
            return $this->error( '删除失败' );
        }
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.24' )
     * @comment( comment = '获取广告分类树' )
     * @method( method = '_getCategoryTree' )		
     * @op( op = 'r' )		
     */
//     private function _getCategoryTree( $pid = 0 )
//     {
//         if( !$this->categorys )
//         {
//             $where = 'delsign=' . DBEnums::DELSIGN_NO;
//             $objCates = AdCats::find( array( $where, 'columns' => 'id,pid,name',
//                         'order'   => 'pid' ) );
//             if( $objCates )
//                 $this->categorys = $objCates->toArray();
//         }

//         $arrCates = array();
//         foreach( $this->categorys as $cate )
//         {
//             if( $cate[ 'pid' ] == $pid )
//             {
//                 $arrCates[ $cate[ 'id' ] ] = $cate;
//                 $children = $this->_getCategoryTree( $cate[ 'id' ] );

//                 if( !empty( $children ) )
//                 {
//                     $arrCates[ $cate[ 'id' ] ][ 'sub' ] = $children;
//                 }
//             }
//         }
//         return $arrCates;
//     }

    
    public function checkUpdateAction()
    {
        $arrRet = array();
        //如果传递过来的数据不为空
        if( 0 !== count( $this->dispatcher->getParams() ) )
        {
            if( 0 != $this->dispatcher->getParam( 'id', 'int' ) )
            {
                $id = $this->dispatcher->getParam( 'id', 'int' );
                $where = array(
                    "id = $id",
                    'columns' => 'name',
                );
                $adCats = AdCats::findFirst( $where );
                if( $adCats )
                {
                    $catsArr = $adCats->toArray();
                    if( $catsArr[ 'name' ] != $this->dispatcher->getParam( 'name', 'string' ) )
                    {
                            
                        $arrRet['state'] = 0;
                        $arrRet['msg'] = '';
                    }
                    else
                    {//广告分类名重复
                        $ret = 1;
                        $arrRet['state'] = 1;
                        $arrRet['msg'] = '广告分类名称已存在，请更换';
                    }
                }
                else
                {//该广告分类信息在数据库中不存在
                    $arrRet['state'] = 2;
                    $arrRet['msg'] = '该广告分类信息在数据库中不存在';
                }
            }
        }
        else
        {//未找到现有分类的id或未传递成功 
            $arrRet['state'] = 3;
            $arrRet['msg'] = '未找到现有分类的id或参数未传递成功';
        }
        
        echo json_encode( $arrRet );
    }
    
    public function getCats( $arr )
    {
        if( is_array( $arr ) )
        {
            foreach( $arr as $value )
            {
                $this->catNames[] = $value[ 'name' ];
                if( array_key_exists( 'sub', $value ) )
                {
                    $this->getCats( $value[ 'sub' ] );
                }
            }
        }
    }
    
    public function checkInsertAction()
    {
        $allCats = new AdCats();
        $catsArr = $allCats->getCategoryTree();
        
        if( !empty( $catsArr ) )
        {
            $this->getCats( $catsArr );
        }
        $catName = $this->dispatcher->getParam( 'name', 'string' );
        
        $arrRet = array();
        
        if( '' != $catName )
        {
        	if( in_array( $catName, $this->catNames ) )
        	{
        	    $arrRet['state'] = 1;
        	    $arrRet['msg'] = '广告分类名称已存在，请更换';
        	}
        	else
        	{
        	    $arrRet['state'] = 0;
        	    $arrRet['msg'] = '';
        	}
        }
        else
        {
            $arrRet['state'] = 2;
            $arrRet['msg'] = '未找到现有分类的id或未传递成功';
        }
        
        echo json_encode( $arrRet );
    }
    
    /**
     * for unit test 
     * this method will be called by the fcms dev tools
     */
    public function _test()
    {
        echo __METHOD__, '<br>';
    }
}

<?php

namespace apps\admin\controllers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use enums\DBEnums;
use Phalcon\Paginator\Adapter\QueryBuilder;
use apps\admin\models\StaticCache;
use helpers\TimeUtils;
use apps\admin\enums\CachecfgEnums;
use apps\admin\enums\DisplayEnums;
use apps\admin\ext\AdminBaseController;

/**
 * 页面静态化配置中心
 * @author Carey
 * @date 2015-10-30
 */
class StaticcacheController extends AdminBaseController{
	
	public function initialize()
	{
		parent::initialize();
		//$this->checkPlatform();
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化栏目/首页配置页面' )
	 * @method( method = 'addAction' )
	 * @op( op = 'r' )
	 */
	public function columnAction()
	{
		$pageNum = $this->request->getQuery( 'page', 'int' );
		$currentPage = $pageNum ? $pageNum : 1;
		
		$builder = $this->modelsManager->createBuilder()
				->columns( 'id,delsign,addtime,uptime,name,cache_time,type,cfgtype,prefix,kind,mold' )
				->from( 'apps\admin\models\StaticCache' )
				->where( 'delsign=' . DBEnums::DELSIGN_NO . ' AND cfgtype=1 ORDER BY uptime DESC' );
		
		$paginator = new  QueryBuilder( array(
				'builder' => $builder,
				'limit' => DisplayEnums::PER_PAGE_LIST_NUM,
				'page' => $currentPage
		));
		$page = $paginator->getPaginate();
		
		$this->view->page = $page;
	}
	
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化栏目/首页配置操作页面' )
	 * @method( method = 'optColumnAction' )
	 * @op( op = 'r' )
	 */
	public function optColumnAction()
	{
		$optid = $this->dispatcher->getParam( 'id' );
		if( false != $optid )
		{
			$where = array(
				'column'	=> 'id,delsign,uptime,addtime,cfgtype,type,kind,mold',
				'conditions'=> 'delsign=:del: and id=:optid: and cfgtype=:cfgtype:',
				'bind'		=> array( 'del' => DBEnums::DELSIGN_NO , 'optid' => $optid , 'cfgtype' => 1 ),
			);
			$res = StaticCache::findFirst( $where );
			if( $res )
            {
                $this->view->res = $res;
            }

			
		}
		
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化栏目/首页配置操作业务' )
	 * @method( method = 'saveColumnActin' )
	 * @op( op = 'r' )
	 */
	public function saveColumnAction()
	{
		if( ! $this->csrfCheck() ){ return false; }
		
		$optid = $this->request->getPost( 'id', 'int' );
		$name	= $this->request->getPost( 'name', 'trim' );
		$prename  = $this->request->getPost( 'pre_name', 'trim' );
		$type	= $this->request->getPost( 'type', 'int' );
		$cacheTime = $this->request->getPost( 'cache_time', 'int' );
		$mold = $this->request->getPost( 'mold', 'int' );
		
		if( false != $optid )
		{
			$where = array(
				'column'	=> 'delsign,addtime,uptime,cname,cache_time,type,is_warm_up,module,kind,mold',
				'conditions'=> 'delsign=:del: and id=:optid: ',
				'bind'		=> array( 'del'=>DBEnums::DELSIGN_NO, 'optid'=> $optid ),
			);
			$res = StaticCache::findFirst( $where );
			if( $res )
			{
				$res->uptime = TimeUtils::getFullTime();
				$res->name	= $name;
				$res->prefix = $prename;
				$res->type   = $type;
				$res->cache_time = $cacheTime?$cacheTime:0;
				$res->mold      = $mold;

				if( $res->save() )
                {
                    $this->response->redirect( '/admin/staticcache/column' );
                }
				else
                {
                    $this->response->redirect( '/admin/staticcache/optcolumn/id/' . $optid );
                }
			}
			else
            {
                $this->response->redirect( '/admin/staticcache/column' );
            }
		}
		else
		{
			$res = new StaticCache();
			$res->delsign = DBEnums::DELSIGN_NO;
			$res->addtime = $res->uptime = TimeUtils::getFullTime();
			$res->name	= $name;
			$res->prefix = $prename;
			$res->type   = $type;
			$res->cache_time = $cacheTime?$cacheTime:0;
			$res->cfgtype	= CachecfgEnums::STATIC_TYPE_COLUMN;
			$res->mold      = $mold;
			
			if( $res->save() )
            {
                $this->response->redirect( '/admin/staticcache/column' );
            }
			else
            {
                $this->response->redirect( '/admin/staticcache/optcolumn' );
            }
		}
		
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '删除静态缓存配置项' )
	 * @method( method = 'deleteAction' )
	 * @op( op = 'r' )
	 */
	public function delColumnAction()
	{
		$objRet = new \stdClass();
		$optid = $this->dispatcher->getParam( 'id' );
		if( false == $optid )
		{
			$objRet->state = 1;
			$objRet->msg = '参数配置错误,请刷新后重试...';
	
			echo json_encode( $objRet );
		}
	
		$where = array(
				'column'	=> 'delsign,addtime,uptime,name,cache_time,cfgtype,type',
				'conditions'=> 'delsign=:del: and id=:optid: and cfgtype=:cfgtype: ',
				'bind'		=> array( 'del'=>DBEnums::DELSIGN_NO, 'optid'=> $optid , 'cfgtype' => 1 ),
		);
		$cache = StaticCache::findFirst( $where );
		if( $cache )
		{
			$cache->delete();
			$objRet->state = 0;
			$objRet->optid = $optid;
			$objRet->msg = '删除成功...';
		}
		else
		{
			$objRet->state = 1;
			$objRet->msg  = '数据未找到,或已被删除..';
		}
		echo json_encode( $objRet );
	}
	
	
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化列表页配置页面' )
	 * @method( method = 'addAction' )
	 * @op( op = 'r' )
	 */
	public function listAction()
	{
		$pageNum = $this->request->getQuery( 'page', 'int' );
		$currentPage = $pageNum ? $pageNum : 1;
		
		$builder = $this->modelsManager->createBuilder()
				->columns( 'id,delsign,addtime,uptime,name,cache_time,type,cfgtype,prefix,kind,mold' )
				->from( 'apps\admin\models\StaticCache' )
				->where( 'delsign=' . DBEnums::DELSIGN_NO . ' AND cfgtype=2 ORDER BY uptime DESC' );
				
		$paginator = new  QueryBuilder( array(
				'builder' => $builder,
				'limit' => DisplayEnums::PER_PAGE_LIST_NUM,
				'page' => $currentPage
		));
		$page = $paginator->getPaginate();
		
		$this->view->page = $page;
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化列表页配置项操作页面' )
	 * @method( method = 'optListAction' )
	 * @op( op = 'r' )
	 */
	public function optListAction()
	{
		$optid = $this->dispatcher->getParam( 'id', 'int' );

		if( false != $optid )
		{
			$where = array(
					'column'	=> 'id,delsign,uptime,addtime,cfgtype,type,kind,mold',
					'conditions'=> 'delsign=:del: and id=:optid: and cfgtype=:cfgtype:',
					'bind'		=> array( 'del' => DBEnums::DELSIGN_NO , 'optid' => $optid , 'cfgtype' => 2 ),
			);
			$res = StaticCache::findFirst( $where );
			if( $res )
            {
                $this->view->res = $res;
            }

		}
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化列表页配置业务' )
	 * @method( method = 'addAction' )
	 * @op( op = 'r' )
	 */
	public function saveListAction()
	{
		if( ! $this->csrfCheck() ){ return false; }
		
		$optid = $this->request->getPost( 'id', 'int' );
		$name	= $this->request->getPost( 'name', 'string' );
		$prename  = $this->request->getPost( 'pre_name', 'string' );
		$type	= $this->request->getPost( 'type', 'int' );
		$cacheTime = $this->request->getPost( 'cache_time', 'int' );
		$mold = $this->request->getPost( 'mold', 'int' );
		$kind = $this->request->getPost( 'kind', 'int' );
		
		if( false != $optid )
		{
			$where = array(
					'column'	=> 'delsign,addtime,uptime,cname,cache_time,type,is_warm_up,module',
					'conditions'=> 'delsign=:del: and id=:optid: ',
					'bind'		=> array( 'del'=>DBEnums::DELSIGN_NO, 'optid'=> $optid ),
			);
			$res = StaticCache::findFirst( $where );
			if( $res )
			{
				$res->uptime = TimeUtils::getFullTime();
				$res->name	= $name;
				$res->prefix = $prename;
				$res->type   = $type;
				$res->cache_time = $cacheTime ? $cacheTime : 0;
				$res->mold      = $mold;
				$res->kind      = $kind;

				if( $res->save() )
					$this->response->redirect( '/admin/staticcache/list' );
				else
					$this->response->redirect( '/admin/staticcache/optlist/id/' . $optid );
			}
			else
				$this->response->redirect( '/admin/staticcache/list' );
		}
		else
		{
			$res = new StaticCache();
			$res->delsign = DBEnums::DELSIGN_NO;
			$res->addtime = $res->uptime = TimeUtils::getFullTime();
			$res->name	= $name;
			$res->prefix = $prename;
			$res->type   = $type;
			$res->cache_time = $cacheTime?$cacheTime:0;
			$res->cfgtype	= CachecfgEnums::STATIC_TYPE_LISR;
			$res->mold      = $mold;
			$res->kind      = $kind;
			
			if( $res->save() )
				$this->response->redirect( '/admin/staticcache/list' );
			else
				$this->response->redirect( '/admin/staticcache/optlist' );
		}
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化列表删除' )
	 * @method( method = 'delListAction' )
	 * @op( op = 'r' )
	 */
	public function delListAction()
	{
		$objRet = new \stdClass();
		$optid = $this->dispatcher->getParam( 'id' );
		if( false == $optid )
		{
			$objRet->state = 1;
			$objRet->msg = '参数配置错误,请刷新后重试...';
		
			echo json_encode( $objRet );
		}
		
		$where = array(
				'column'	=> 'delsign,addtime,uptime,name,cache_time,cfgtype,type',
				'conditions'=> 'delsign=:del: and id=:optid: and cfgtype=:cfgtype: ',
				'bind'		=> array( 'del'=>DBEnums::DELSIGN_NO, 'optid'=> $optid , 'cfgtype' => 2 ),
		);
		$cache = StaticCache::findFirst( $where );
		if( $cache )
		{
			$cache->delete();
			$objRet->state = 0;
			$objRet->optid = $optid;
			$objRet->msg = '删除成功...';
		}
		else
		{
			$objRet->state = 1;
			$objRet->msg  = '数据未找到,或已被删除..';
		}
		echo json_encode( $objRet );
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化详情页配置页面' )
	 * @method( method = 'detailAction' )
	 * @op( op = 'r' )
	 */
	public function detailAction()
	{
		$pageNum = $this->request->getQuery( 'page', 'int' );
		$currentPage = $pageNum ? $pageNum : 1;
		
		$builder = $this->modelsManager->createBuilder()
				->columns( 'id,delsign,addtime,uptime,name,cache_time,type,cfgtype,prefix,kind,mold' )
				->from( 'apps\admin\models\StaticCache' )
				->where( 'delsign=' . DBEnums::DELSIGN_NO . ' AND cfgtype=3 ORDER BY uptime DESC' );
		
		$paginator = new  QueryBuilder( array(
				'builder' => $builder,
				'limit' => DisplayEnums::PER_PAGE_LIST_NUM,
				'page' => $currentPage
		));
		$page = $paginator->getPaginate();
		$this->view->page = $page;
	}
	
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化详情页配置页面' )
	 * @method( method = 'optDetailAction' )
	 * @op( op = 'r' )
	 */
	public function optDetailAction()
	{
		$optid = $this->dispatcher->getParam( 'id' );
		if( false != $optid )
		{
			$where = array(
					'column'	=> 'id,delsign,uptime,addtime,cfgtype,type,name,type,kind,mold',
					'conditions'=> 'delsign=:del: and id=:optid: and cfgtype=:cfgtype:',
					'bind'		=> array( 'del' => DBEnums::DELSIGN_NO , 'optid' => $optid , 'cfgtype' => 3 ),
			);
			$res = StaticCache::findFirst( $where );
			if( $res )
            {
                $this->view->res = $res;
            }

		}

	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '静态化详情页配置业务' )
	 * @method( method = 'saveDetailAction' )
	 * @op( op = 'r' )
	 */
	public function saveDetailAction()
	{
		if( ! $this->csrfCheck() ){ return false; }
		
		$optid = $this->request->getPost( 'id', 'int' );
		$name	= $this->request->getPost( 'name', 'string' );
		$prename  = $this->request->getPost( 'pre_name', 'string' );
		$type	= $this->request->getPost( 'type', 'int' );
		$cacheTime = $this->request->getPost( 'cache_time', 'int' );
		$mold = $this->request->getPost( 'mold', 'int' );
		$kind = $this->request->getPost( 'kind', 'int' );
		
		if( false != $optid )
		{
			$where = array(
					'column'	=> 'delsign,addtime,uptime,cname,cache_time,type,is_warm_up,module',
					'conditions'=> 'delsign=:del: and id=:optid: ',
					'bind'		=> array( 'del'=>DBEnums::DELSIGN_NO, 'optid'=> $optid ),
			);
			$res = StaticCache::findFirst( $where );
			if( $res )
			{
				$res->uptime = TimeUtils::getFullTime();
				$res->name	= $name;
				$res->prefix = $prename;
				$res->type   = $type;
				$res->cache_time = $cacheTime?$cacheTime:0;
				$res->mold      = $mold;
				$res->kind      = $kind;
				
				if( $res->save() )
					$this->response->redirect( '/admin/staticcache/detail' );
				else
					$this->response->redirect( '/admin/staticcache/optdetail/id/' . $optid );
			}
			else
				$this->response->redirect( '/admin/staticcache/detail' );
		}
		else
		{
			$res = new StaticCache();
			$res->delsign = DBEnums::DELSIGN_NO;
			$res->addtime = $res->uptime = TimeUtils::getFullTime();
			$res->name	= $name;
			$res->prefix = $prename;
			$res->type   = $type;
			$res->cache_time = $cacheTime?$cacheTime:0;
			$res->cfgtype	= CachecfgEnums::STATIC_TYPE_DETAIL;
			$res->mold      = $mold;
			$res->kind      = $kind;
			
			if( $res->save() )
				$this->response->redirect( '/admin/staticcache/detail' );
			else
				$this->response->redirect( '/admin/staticcache/optdetail' );
		}
	}
	
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015-11-07' )
	 * @comment( comment = '删除静态化详情页配置' )
	 * @method( method = 'delDetailAction' )
	 * @op( op = 'r' )
	 */
	public function delDetailAction()
	{
		$objRet = new \stdClass();
		$optid = $this->dispatcher->getParam( 'id' );
		if( false == $optid )
		{
			$objRet->state = 1;
			$objRet->msg = '参数配置错误,请刷新后重试...';
		
			echo json_encode( $objRet );
		}
		
		$where = array(
				'column'	=> 'delsign,addtime,uptime,name,cache_time,cfgtype,type,kind,mold',
				'conditions'=> 'delsign=:del: and id=:optid: and cfgtype=:cfgtype: ',
				'bind'		=> array( 'del'=>DBEnums::DELSIGN_NO, 'optid'=> $optid , 'cfgtype' => 3 ),
		);
		$cache = StaticCache::findFirst( $where );
		if( $cache )
		{
			$cache->delete();
			$objRet->state = 0;
			$objRet->optid = $optid;
			$objRet->msg = '删除成功...';
		}
		else
		{
			$objRet->state = 1;
			$objRet->msg  = '数据未找到,或已被删除..';
		}
		echo json_encode( $objRet );
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2016年8月9日' )
	 * @comment( comment = '是否重复设置配置项' )    
	 * @method( method = 'checkIsSetAction' )
	 * @op( op = '' )    
	*/
	public function checkIsSetAction()
	{
	    $mold = $this->dispatcher->getParam( 'mold', 'int' );
	    $kind = $this->dispatcher->getParam( 'kind', 'int' );
	    
	    $objRet = new \stdClass();
	    if( isset( $mold ) )
	    {
    	    $where = array(
    	        'columns'    => 'id,delsign,type,mold,kind',
                'conditions' => 'delsign=:del: and mold=:mold:',
    	        'bind'       => array( 'del' => DBEnums::DELSIGN_NO, 'mold' => $mold ),
    	    );
    	    
    	    if( isset( $kind ) )
    	    {
    	        $where[ 'conditions' ] .= ' and kind=:kind:';
    	        $where[ 'bind' ][ 'kind' ] =  $kind;
    	    }
    	    
    	    $iNums = StaticCache::count( $where );
    	    if( $iNums > 0 )
    	    {
    	        $objRet->state = 1;
    	        $objRet->msg = '您已配置该项,请勿重复配置..';
    	    }
    	    else
    	    {
    	        $objRet->state = 0;
    	    }
	    }
	    else
	    {
	        $objRet->state = 1;
	        $objRet->msg = '参数错误,请刷新以后再试..';
	    }
	    
	    echo json_encode( $objRet );
	}
}

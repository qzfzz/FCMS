<?php

namespace apps\admin\controllers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use enums\DBEnums;
use helpers\TimeUtils;
use apps\admin\models\Sensitive;
use Phalcon\Paginator\Adapter\QueryBuilder;
use enums\CachePrefixEnums;
use apps\admin\enums\DisplayEnums;
use apps\admin\ext\AdminBaseController;

class SensitiveController extends AdminBaseController{
	
	public function initialize()
	{
		parent::initialize();
		//$this->checkPlatform();
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015.10.14' )
	 * @comment( comment = '敏感词列表' )
	 * @method( method = 'indexAction' )
	 * @op( op = 'r' )
	 */
	public function indexAction()
	{
		$pageNum = $this->request->getQuery( 'page', 'int' );
		$currentPage = $pageNum ? $pageNum : 1;
	
		$builder = $this->modelsManager->createBuilder()
				->from( 'apps\admin\models\Sensitive' )
				->where( 'delsign=' . DBEnums::DELSIGN_NO . ' ORDER BY id DESC' );
		 
		$paginator = new QueryBuilder( array(
			'builder' => $builder,
			'limit' => DisplayEnums::PER_PAGE_LIST_NUM,
			'page' => $currentPage
		) );
		$page = $paginator->getPaginate();
			
		$this->view->page = $page;
		$this->view->default = $this->config[ 'sensitive_default_replace' ];
		
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015.10.14' )
	 * @comment( comment = '添加敏感词' )
	 * @method( method = 'addAction' )
	 * @op( op = 'r' )
	 */
	public function addAction()
	{
		
	}
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015.10.14' )
	 * @comment( comment = '保存敏感词' )
	 * @method( method = 'addAction' )
	 * @op( op = 'r' )
	 */
	public function saveAction()
	{
		$loginInfo = $this->userInfo;
		$key = $this->request->getPost( 'key' );
		$arrKeys = explode( "\n", $key );
		
		if( count( $arrKeys ) > 0 )
		{
			$sens = new Sensitive();
			foreach( $arrKeys as $arrKey )
			{
				$val = array(
					'delsign' => DBEnums::DELSIGN_NO,
					'addtime' => TimeUtils::getFullTime(),
					'uptime' => TimeUtils::getFullTime(),
					'word' => $arrKey,
					'uid' => $loginInfo[ 'id' ],
				);
				$sens->insert( $val );
				
				$this->_saveSenCache( $sens->id, $arrKey, '' );
			}
		}
		
		$this->response->redirect( '/admin/sensitive/index' );
	}
	
	
	/**
	 * @author( author='Carey' )
	 * @date( date = '2015.10.14' )
	 * @comment( comment = '删除敏感词' )
	 * @method( method = 'deleteAction' )
	 * @op( op = 'r' )
	 */
	public function deleteAction()
	{
		$objRet = new \stdClass();
		
		$optid = $this->dispatcher->getParam( 'id' );
		if( false == $optid )
		{
			//获取参数出错,请刷新后再试
			$ret = 1;
			//echo json_encode( $objRet );
			echo $ret;
			return false;
		}
		
		$where = array(
			'conditions' => 'delsign=:del: and id=:optid:',
			'bind'		 => array( 'del' => DBEnums::DELSIGN_NO, 'optid' => $optid ),
		);
		$sens = Sensitive::findFirst( $where );
		if( $sens )
		{
			//删除成功
			$this->_delSenCache( $sens->id );
			$sens->delete();
			$ret = 0;
		}
		else
        {//删除失败,未找到该条信息
            $ret = 2;
        }

		
		echo $ret;
	 }
	 
	 /**
	 * @author( author='Carey' )
	 * @date( date = '2015.10.15' )
	 * @comment( comment = '设置敏感词替换词' )
	 * @method( method = 'replaceAction' )
	 * @op( op = 'u' )
	 */
	 public function replaceAction()
	 {
	 	$objRet = new \stdClass();
	 	
	 	$optid  = $this->dispatcher->getParam( 'id' );
	 	$reword = urldecode( $this->dispatcher->getParam( 'strreplace' ) );
	 	
	 	if( false == $optid || false == $reword )
	 	{
	 		$objRet->state = 1;
	 		$objRet->msg = '对不起,参数设置错误,请稍后重试.';
	 		echo json_encode( $objRet );
	 		return false;
	 	}
	 	
	 	$where = array(
	 		'conditions'	=> 'delsign=:del: and id=:optid:',
	 		'bind'			=> array( 'del' => DBEnums::DELSIGN_NO , 'optid' => $optid ),
	 	);
	 	$res = Sensitive::findFirst( $where );
	 	if( $res )
	 	{
	 		$res->rword = $reword;
			$res->save();

			$objRet->state = 0;
			$objRet->rword = $reword;
			$objRet->msg = '设置成功.';
			
			$this->_saveSenCache( $res->id, $res->word, $reword );
	 	}
	 	else 
	 	{
	 		$objRet->state = 1;
	 		$objRet->msg = '信息为找到,设置失败.';
	 	}
	 	
	 	echo json_encode( $objRet );
	 }
	 
	 
	 /**
	  * @author( author='New' )
	  * @date( date = '2016-8-29' )
	  * @comment( comment = '保存敏感词缓存（插入、更新）' )
	  * @method( method = 'setSenCache' )
	  * @op( op = '' )
	  */
	 private function _saveSenCache( $id, $word = '', $rword = '' )
	 {
	 	if( isset( $id ) )
	 	{
	 		
	 		if( $this->nredis->hExists( CachePrefixEnums::SENSITIVE_WORD, 'sensitive_arr' ) )
	 		{
	 			$arrSens = $this->nredis->hGet( CachePrefixEnums::SENSITIVE_WORD, 'sensitive_arr' );
	 			$arrSens[ $id ] = array( 'word' => $word, 'rword' => $rword );
	 			$this->nredis->hSet( CachePrefixEnums::SENSITIVE_WORD, 'sensitive_arr', $arrSens );
	 			return true;
	 		}
	 	}
 		return false;
	 }
	 
	 /**
	  * @author( author='New' )
	  * @date( date = '2016-8-29' )
	  * @comment( comment = '删除敏感词缓存' )
	  * @method( method = 'setSenCache' )
	  * @op( op = '' )
	  */
	 private function _delSenCache( $id )
	 {
	 	if( isset( $id ) )
	 	{
	 		if( $this->nredis->hExists( CachePrefixEnums::SENSITIVE_WORD, 'sensitive_arr' ) )
	 		{
	 			$arrSens = $this->nredis->hGet( CachePrefixEnums::SENSITIVE_WORD, 'sensitive_arr' );
	 			if( $arrSens[ $id ] )
	 			{
	 				unset( $arrSens[ $id ] );
	 				$this->nredis->hSet( CachePrefixEnums::SENSITIVE_WORD, 'sensitive_arr', $arrSens );
	 			}
	 			return true;
	 		}
	 	}
	 	
 		return false;
	 }
	 
}

?>
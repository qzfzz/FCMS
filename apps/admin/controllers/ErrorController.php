<?php
namespace apps\admin\controllers;
use enums\PriEnums;
use apps\admin\enums\MessageEnums;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class ErrorController extends \Phalcon\Mvc\Controller
{
    public function err404Action()
    {
        //echo '<br>Sorry! We cann\'t find this page!<br>';
        $this->view->pick( 'index/404' );
    }
    
    /**
     * 错误显示
     */
    public function errorAction( )
    {
        $data = $this->dispatcher->getParams();
        
        assert( $data );
        
        if( $this->request->isAjax() )
        {
            if( empty( $data[ 'msg'] ) )
            {
                $data[ 'msg' ] = '服务器异常错误';
            }
            echo json_encode( array( 'status' => MessageEnums::MESSAGE_ERROR, 'msg' => $data[ 'msg'] ) );
            return false;
        }
        else
        {
            $this->view->data = $data;
        }
      
    }
    
    /**
     *  跳到登录页面
     */
    public function toLoginAction()
    {
        if( $this->request->isAjax())
        {
            $data = $this->dispatcher->getParams();
            if( empty( $data[ 'msg'] ) )
            {
                $data[ 'msg' ] = '登录超时，请重新登录';
            }
            echo json_encode( array( 'status' => PriEnums::MESSAGE_ERROR, 'msg' => $data[ 'msg'] ) );
        }
        else
        {
            echo '<script>parent.window.location="/admin/login/index"</script>';
        }
        return false;
    }
}


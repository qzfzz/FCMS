<?php

/**
 * 广告
 * @author yyl
 * time 2015-8-24
 */

namespace apps\admin\controllers;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use apps\admin\models\AdCats;
use apps\admin\models\Ad;
use enums\DBEnums;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use helpers\TimeUtils;
use Phalcon\Paginator\Adapter\QueryBuilder;
use enums\ImgBizEnums;
use apps\admin\enums\DisplayEnums;
use apps\admin\ext\AdminBaseController;

class AdController extends AdminBaseController
{

    private $categorys = array();

    public function initialize()
    {
        parent::initialize();
    	//$this->checkPlatform();
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.25' )
     * @comment( comment = '广告首页' )
     * @method( method = 'indexAction' )		
     * @op( op = 'r' )		
     */
    public function indexAction()
    {
        $pageNum = $this->request->getQuery( 'page', 'int' );
        $currentPage = $pageNum ? $pageNum : 1;

        $builder = $this->modelsManager->createBuilder()
        			->columns( 'a.id,a.end_time,a.begin_time,a.media_type,a.name,a.enabled,c.name as cat_name' )
        			->addFrom( 'apps\admin\models\Ad', 'a' )
        			->join( 'apps\admin\models\AdCats' , 'a.cat_id=c.id' , 'c' )
        			->where( 'a.delsign=' . DBEnums::DELSIGN_NO . ' order by a.sort_order desc' );
         
        $paginator = new  QueryBuilder( array(
        		'builder' => $builder,
        		'limit' => DisplayEnums::PER_PAGE_LIST_NUM,
        		'page' => $currentPage
        ));

        $page = $paginator->getPaginate();
        $this->view->page = $page;
        
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.25' )
     * @comment( comment = '编辑商品显示' )
     * @method( method = 'editAction' )		
     * @op( op = 'r' )		
     */
    public function editAction()
    {
        $id = $this->request->getQuery( 'id', 'int' );
        $ad = Ad::findFirst( array( 'id=?0', 'bind' => array( $id ) ) );
        if( $ad )
        {
            $this->view->ad = $ad->toArray();
        }

        
        $allCats = new AdCats();
        $this->view->categorys = $allCats->getCategoryTree();
        $this->view->uid = $this->userInfo[ 'id' ];
        $this->view->bizType = ImgBizEnums::ADMIN_AD_CHIEF_IMG;
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.25' )
     * @comment( comment = '添加商品显示' )
     * @method( method = 'addAction' )		
     * @op( op = 'r' )		
     */
    public function addAction()
    {
        $allCats = new AdCats();
        $this->view->categorys = $allCats->getCategoryTree();
        
        $this->view->uid = $this->userInfo[ 'id' ];
        $this->view->bizType = ImgBizEnums::ADMIN_AD_CHIEF_IMG;
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.25' )
     * @comment( comment = '更新广告' )
     * @method( method = 'updateAction' )		
     * @op( op = 'u' )		
     */
    public function updateAction()
    {
       	if( ! $this->csrfCheck() ){ return false; } //csrf检验
       	
        $adId = $this->request->getPost( 'adId', 'int' );
        $data[ 'title' ] = $this->request->getPost( 'title', 'string' );
        $data[ 'cat_id' ] = $this->request->getPost( 'categoryId', 'int' );
        $data[ 'name' ] = $this->request->getPost( 'name', 'string' );
        $data[ 'begin_time' ] = strtotime( $this->request->getPost( 'begin_time', 'string' ) );
        $data[ 'end_time' ] = strtotime( $this->request->getPost( 'end_time', 'string' ) );
        $data[ 'sort_order' ] = $this->request->getPost( 'sort_order', 'int' );
        $data[ 'media_type' ] = $this->request->getPost( 'media_type', 'int' );
        $data[ 'url' ] = $this->request->getPost( 'pics' );
        $data[ 'src' ] = $this->request->getPost( 'source_url', 'string' );
        $data[ 'enabled' ] = $this->request->getPost( 'enabled', 'int' );
        $this->validation( $data ); //验证输入数据
        $data[ 'user_id' ] = $this->userInfo[ 'id' ];
        $data[ 'uptime' ] = TimeUtils::getFullTime();

        $ad = Ad::findFirst( array( 'id=?0', 'bind' => array( $adId ) ) );

        if( $ad && $ad->update( $data ) )
            return $this->success( '更新成功' );
        else
            return $this->error( '更新失败' );
        
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.25' )
     * @comment( comment = '删除广告' )
     * @method( method = 'deleteAction' )		
     * @op( op = 'd' )		
     */
    public function deleteAction()
    {
    	if( ! $this->csrfCheck() ){ return false; }
    	
        $id = $this->request->getPost( 'id', 'int' );
        $goods = Ad::findFirst( array( 'id=?0', 'bind' => array( $id ) ) );

        if( $goods )
        {
            $status = $goods->update( array( 'delsign' => DBEnums::DELSIGN_YES ,  'uptime'  => date( 'Y-m-d H:i:s' ) ) );
            if( $status )
            {
                return $this->success( '删除成功' );
            }
        }
        return $this->error( '删除失败' );
    }

    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.25' )
     * @comment( comment = '添加广告' )
     * @method( method = 'insertAction' )		
     * @op( op = 'c' )		
     */
    public function insertAction()
    {
       	if( ! $this->csrfCheck() ){ return false; } //csrf检验
        
        $data[ 'title' ] = $this->request->getPost( 'title', 'string' );
        $data[ 'cat_id' ] = $this->request->getPost( 'categoryId', 'int' );
        $data[ 'name' ] = $this->request->getPost( 'name', 'string' );
        $data[ 'begin_time' ] = strtotime( $this->request->getPost( 'begin_time',  'string' ) );
        $data[ 'end_time' ] = strtotime( $this->request->getPost( 'end_time',  'string' ) );
        $data[ 'sort_order' ] = $this->request->getPost( 'sort_order', 'int' );
        $data[ 'media_type' ] = $this->request->getPost( 'media_type', 'int' );
        $data[ 'url' ] = $this->request->getPost( 'pics' );
        $data[ 'src' ] = $this->request->getPost( 'source_url', 'string' );
        $data[ 'enabled' ] = $this->request->getPost( 'enabled', 'int' );

        $this->validation( $data ); //验证输入数据
        $data[ 'delsign' ] = DBEnums::DELSIGN_NO;
        $data[ 'shopid' ] = $this->shopId;
        $data[ 'user_id' ] = $this->userInfo[ 'id' ];
        $data[ 'addtime' ] = $data[ 'uptime' ] = TimeUtils::getFullTime();

        $ad = new Ad();
        
        if( $ad->save( $data ) )
        {
            return $this->success( '保存成功', array( 'id' => $ad->id ) );
        }
        else
        {
            foreach( $ad->getMessages() as $msg )
            {
                echo $msg, PHP_EOL;
            }
            return $this->error( '保存失败', array( 'id' => $ad->id ) );
        }
    }


    /**
     * @author( author='yyl' )
     * @date( date = '2015.8.25' )
     * @comment( comment = '数据验证' )
     * @method( method = 'validation' )		
     * @op( op = '' )		
     */
    private function validation( $data = array() )
    {
        $validation = new Validation();
        $validation->add( 'title',
                new PresenceOf( array(
            'message' => '广告标题必须填写'
        ) ) );
        $validation->add( 'cat_id',
                new PresenceOf( array(
            'message' => '广告分类必须填写'
        ) ) );

        $messages = $validation->validate( $data );
        if( count( $messages ) )
        {
            foreach( $messages as $msg )
            {
                return $this->error( $msg->getMessage() );
            }
        }
    }
    
    public function _test()
    {
        echo __METHOD__, '<br>';
    }

}

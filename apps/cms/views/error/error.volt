<!doctype html>
<html>
    <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.css">
    <link>
    <style>
        body {
             font-family: "Arial","微软雅黑",sans-serif;
        }
        .glyphicon { 
            top: 2px;
        }
        
    </style>
    <body class="wrap">
        <div class="hidden" id="msg">
            <i class="glyphicon glyphicon-info-sign"></i> 
            {% if data[ 'msg' ] is not empty %}{{ data[ 'msg' ] }}{% endif %}
        </div>
    <script src="/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="{{ urlAssetsBundle }}/bootstrap/jqueryConfirm/2.5.0/jquery-confirm.js"></script>
    <script>
        var msg = $( '#msg' ).html();
        $.dialog( { title : msg, onClose : function(){ 
              location = document.referrer;
        } } );
    </script>
    </body>
</html>
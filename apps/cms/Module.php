<?php

namespace apps\cms;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Events\Manager as EventsManager;
use apis\ArticleData;
use apis\AdsData;
use apis\SitesData;
use apis\FriendLinkData;
use apis\MenusData;
use apis\SearchData;
use Phalcon\Cache\Frontend\Output as OutputFrontend; 
use apis\SlideData;

class Module implements ModuleDefinitionInterface {
	public function registerAutoloaders(\Phalcon\DiInterface $di=null) {
		$loader = new \Phalcon\Loader ();
		
		$loader->registerNamespaces ( array (
				'apps\cms\controllers' => APP_ROOT . '/apps/cms/controllers/',
				'apps\cms\models' => APP_ROOT . '/apps/cms/models/',
				'apps\admin\models' => APP_ROOT . '/apps/admin/models/',
                'apps\admin\enums' => APP_ROOT . '/apps/admin/enums/',
				'apps\cms\plugins' => APP_ROOT . '/apps/cms/plugins/',
				'apps\cms\libraries' => APP_ROOT . '/apps/cms/libraries/',
				'apps\cms\listeners' => APP_ROOT . 'apps/cms/listeners/',
				'apps\cms\vos' => APP_ROOT . 'apps/cms/vos/' 
				) );
		
		$loader->register ();
	}
	
	/**
	 * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
	 */
	public function registerServices(\Phalcon\DiInterface $di = null) {
		// Registering a dispatcher
		$di->set ( 'dispatcher', function () {
			
			$dispatcher = new \Phalcon\Mvc\Dispatcher ();
			
			// Attach a event listener to the dispatcher
			$eventManager = new \Phalcon\Events\Manager ();
			// $eventManager->attach('dispatch', new \Acl( 'admin' ));
			
			$eventManager->attach ( 'dispatch:beforeException', function ($event, $dispatcher, $exception) {
				
				if ($exception instanceof \Phalcon\Mvc\Dispatcher\Exception) {
					$dispatcher->forward ( array (
							'module' => 'cms',
							'controller' => 'error',
							'action' => 'err404' 
					) );
					
					return false;
				}
			} );
			
			$eventManager->attach ( 'dispatch:beforeDispatchLoop', function ($event, $dispatcher) {
				$keyParams = array ();
				$params = $dispatcher->getParams ();
				
				foreach ( $params as $k => $v ) {
					if ($k & 1) {
						$keyParams [$params [$k - 1]] = $v;
					}
				}
				
				$dispatcher->setParams ( $keyParams );
			} );
			
			$dispatcher->setEventsManager ( $eventManager );
			$dispatcher->setDefaultNamespace ( "apps\\cms\\controllers\\" );
			return $dispatcher;
		} );
		
		// Registering the view component
		$di->set( 'view', function () {
			$view = new \Phalcon\Mvc\View ();
			$view->setViewsDir ( '../apps/cms/views/' );
			$view->registerEngines ( array (
					'.volt' => function ($view, $di) {
						$volt = new VoltEngine ( $view, $di );
						$volt->setOptions ( array (
								'compiledPath' => APP_ROOT . '/apps/cms/cache/',
								'compiledSeparator' => '_',
								'compileAlways' => false 
						) );
						return $volt;
					} 
			// '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
						) );
                                                
//                         if( !strncmp ( 'nginx', $_SERVER['SERVER_SOFTWARE'], 5 ) )
//                         {//nginx
//                             $view->ASSETS_PATH_PREFIX = '';
//                         }
//                         else
//                         {//apache或其他若这里有错请开发者自己修改为使用的服务器即可
//                             $view->ASSETS_PATH_PREFIX = '';///public
//                         } 
                        
                        
                        $view->setVars( array( 'urlAssetsJs' => $this[ 'urlCfg' ]->url->cdn_assets_js,//UrlEnums::URL_ASSETS_JS,cdn_assets_img
                            'urlAssetsCss' => $this[ 'urlCfg' ]->url->cdn_assets_css,//URLEnums::URL_ASSETS_CSS,
                            'urlAssetsBundle' => $this[ 'urlCfg' ]->url->cdn_assets_bundle,//UrlEnums::URL_ASSETS_BUNDLE,
                            'urlAssetsImg' => $this[ 'urlCfg' ]->url->cdn_assets_img,//UrlEnums::URL_ASSETS_IMG,
                            'domain' => $this[ 'urlCfg' ]->url->url_domain,
                        ));
			return $view;
		} );

		//设置视图缓存
		$di->set( 'viewCache', function(){
			$frontCache = new OutputFrontend( array(
				'lefttime'	=> 	86400,
			) );
			$cache = new \Phalcon\Cache\Backend\File( $frontCache, array( 'cacheDir' => APP_ROOT . 'apps/cms/cache/' ) );
			return $cache;
		} );

		$config = $di->get( 'dbCfg' );
		$di->set ( 'db', function () use($config) {
			$db = new \Phalcon\Db\Adapter\Pdo\Mysql ( array (
							'adapter'  => $this['dbCfg']->db->adapter,
							'host'     => $this['dbCfg']->db->host,
							'username' => $this['dbCfg']->db->username,
							'password' => $this['dbCfg']->db->password,
							'dbname'   => $this['dbCfg']->db->dbname,
							'charset'  => $this['dbCfg']->db->charset,
			                'port'     => $this['dbCfg']->db->port
			) );
			
			$eventsMgr = new EventsManager ();
			$dbListener = new \apps\cms\listeners\DbListener ();
			
			$eventsMgr->attach ( 'db', $dbListener );
			
			$db->setEventsManager ( $eventsMgr );
			
			return $db;
		}, true );
		
		$di->module = 'cms';
		
		$di->set ( 'article', function(){ return new ArticleData (); }, true);
		$di->set ( 'ad', function(){ return new AdsData(); }, true );
		$di->set ( 'site' , function(){ return new SitesData(); }, true );
		$di->set ( 'flink', function(){ return new FriendLinkData(); }, true );
		$di->set ( 'menu', function(){ return new MenusData(); }, true );
		$di->set ( 'search', function(){ return new SearchData(); }, true );
		$di->set ( 'slide', function(){ return new SlideData(); }, true );
		
	}
}
        

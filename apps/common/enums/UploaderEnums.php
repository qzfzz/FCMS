<?php
namespace apps\common\enums;

class UploaderEnums
{
    /**
     * 元保存位置 mongodb
     */
    const FS_METADB_MONGODB = 0;

    /**
     * 元保存位置 redis
     */
    const FS_METADB_REDIS = 1;
    
    /**
     * 元保存位置 mysql
     */
    const FS_METADB_MYSQL = 2;
    
    /**
     * 文件存储在文件系统中 
     */
    const REPO_TYPE_FS = 'fs';
    
    /**
     * 保存文件在fastdfs中
     */
    const REPO_TYPE_FASTDFS = 'fastdfs';

    /**
     * 保存文件在oss中
     */
    const REPO_TYPE_OSS = 'oss';
}


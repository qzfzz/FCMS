<?php
namespace apps\common\controllers;
use enums\ServiceEnums;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class TestController extends \Phalcon\Mvc\Controller
{
    /**
     * 跳转到登录页面
     */
    public function logAction()
    {
        assert( $this->di[ ServiceEnums::SERVICE_LOG_BIZ ]);
    }
    
    public function sAction()
    {
        echo __METHOD__, '<br>';
    }
}


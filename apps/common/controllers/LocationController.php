<?php
namespace apps\common\controllers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

use Phalcon\Mvc\Controller;
use apps\common\models\CommonDistrictDic;
use enums\DBEnums;;
use libraries\IpLocation;
use libraries\DistrictDicUtils;
/**
 * 地址
 * 
 * @author Carey
 * @date 2016-06-24
 */
class LocationController extends Controller
{
    public function ipAction()
    {
    	$objIpLoc = new IpLocation();
    	$objIpLoc->setDI( $this->di );
    	
		var_dump( $objIpLoc->getIpLocation( '114.114.114.114' ));
    }
    
    /**
     * @author( author='Carey' )
     * @date( date = '2016年6月24日' )
     * @comment( comment = '获取省市区' )    
     * @method( method = 'getaddressAction' )
     * @op( op = '' )    
    */
 	public function getaddressAction()
    {
        $id = $this->request->getQuery( 'id', 'int' );
        
        $dist = DistrictDicUtils::getInstance( $this->di );
        
        $address = $dist->getChildrenFromID( $id );
        
        if( $address === false )
        {
        	$address = CommonDistrictDic::find( array( 'upid=?0', 'bind' => array( $id ) ));
        }
        
        if( $address != false )
        {
            $data[ 'address' ] = is_array( $address ) ? $address : $address->toArray();
            $this->success( '获得地址成功', $data );
        }
        else
            $this->error( '获得地址失败');
        
    }
    
    
    
    /**
     *成功消息返回
     * param string $msg
     * param array $data 其他自定义数据
     */
    protected function success( $msg = '', $data = array() )
    {
        $this->message( 0, $msg, $data );
    }
    
    /**
     * 错误消息返回
     * param string $msg
     * param array $data 其他自定义数据
     */
    public function error( $msg = '', $data = array() )
    {
        $this->message( 1, $msg, $data );
    }
    
    /**
     * 消息的输出
     * param int $status 状态 0：代表成功，1：代表失败，2:代表其
     * param string $msg 消息内容
     * param array $data 其他自定义数据
     */
    protected function message( $status = 0, $msg = '', $data = array() )
    {
        if( $this->request->isPost() ) //post请求才进行csrf
        {
            $ret[ 'key' ] = $this->security->getTokenKey();
            $ret[ 'token' ] = $this->security->getToken();
        }
    
        $ret[ 'status' ] = $status;
        $ret[ 'msg' ] = $msg;
        $ret = array_merge( $ret, $data );
        echo json_encode( $ret );
    }
}

?>
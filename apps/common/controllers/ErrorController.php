<?php
namespace apps\common\controllers;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class ErrorController extends \Phalcon\Mvc\Controller
{
    public function err404Action()
    {
        echo '<br>Sorry! We cann\'t find this page!<br>';
    }
    
    /**
     * 跳转到登录页面
     */
    public function toLoginAction()
    {
        echo '<script>parent.window.location="/admin/login/index";</script>';
    }
}


<?php
namespace apps\common\bizs;

interface IImageMeta extends \Phalcon\Di\InjectionAwareInterface
{
    public function saveImg( array $fileInfo );
    
}


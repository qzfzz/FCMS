<?php
namespace apps\common\bizs;

use enums\ImgEnums;
use enums\ServiceEnums;

class ImgMetaBiz  implements \Phalcon\Di\InjectionAwareInterface
{
    private $di;
    private static $_instance = null;
    
    private function __construct( \Phalcon\DiInterface $di )
    {
        $this->di = $di;
    }
    
    public function setDI( \Phalcon\DiInterface $dependencyInjector) 
    {
        $this->di = $dependencyInjector;
    }
    
    /* (non-PHPdoc)
     * @see \Phalcon\Di\InjectionAwareInterface::getDI()
     */
    public function getDI() 
    {
        return $this->di;
    }
    
    private function getMetaRepoType()
    {
        $ueditorCfg = $this->di['ueditorCfg'];
        
        assert( $ueditorCfg );
        
        $defaultMetadb = $ueditorCfg->metadb;
        
        return  $ueditorCfg->{$ueditorCfg->type}->metadb ?? $defaultMetadb;
    }
    
    public function saveMeta( array $fileInfo )
    {
        $metadbType = $this->getMetaRepoType();
        
        $this->di[ ServiceEnums::SERVICE_LOG_DEBUG ]->debug( '$metadbType:' . $metadbType );
        
        switch( $metadbType )
        {
            case ImgEnums::META_DEST_MYSQL:
                $metaDB = new MysqlMeta( $this->di );
                break;
            case ImgEnums::META_DEST_MONGODB:
                $metaDB = new MongoDbMeta($this->di);
                break;
            case ImgEnums::META_DEST_REDIS:
                $metaDB = new RedisMeta($this->di);
                break;
            default:
                $metaDB = new MysqlMeta($this->di);
                break;
        }
        
        return $metaDB->saveImg( $fileInfo );
    }
    
    public static function getInstance( $di )
    {
        assert( $di );
        
        if( self::$_instance == null )
        {
            self::$_instance = new ImgMetaBiz( $di );
        }
        
        return self::$_instance;
    }
}


<?php
namespace apps\common\bizs;

use apps\common\models\ImgAssets;
use enums\ServiceEnums;
use helpers\TimeUtils;
use enums\DBEnums;;

class MysqlMeta implements IImageMeta
{
    private $di;

    public function __construct( $di )
    {
        assert( $di );
        
        $this->setDI( $di );    
    }
    
    public function saveImg( array $fileInfo )
    {
//         var_dump( $fileInfo );
//        assert( $fileInfo );
        
        $img = new ImgAssets();
        
        $request = $this->di[ 'request' ];
        
        assert( $request );
        
        $iBiz = $request->get( 'biz', 'int' );
        
        $img->biz_type = $iBiz;
        $img->filename = $fileInfo[ 'title' ];
        assert( $this->di['session'] );
        
        $img->mem_id = $this->di['session']->userInfo['id'];
        $img->original = $fileInfo['original'];
        $this->di[ ServiceEnums::SERVICE_LOG_DEBUG ]->debug( '$metadbType:' . var_export( $fileInfo, true ) );
        $img->size = $fileInfo['size'];
        $img->type = $fileInfo[ 'type' ];
        
        $img->addtime = TimeUtils::getFullTime();
        $img->delsign = DBEnums::DELSIGN_NO;
        $img->path_name = $fileInfo[ 'pathName' ];
        if( $img->save())
        {
//            assert( $img->id );
            
            return $img->id;
        }
        
        return false;
    }
    
    /**
     * Sets the dependency injector
     *
     * @param \Phalcon\DiInterface $dependencyInjector
     */
    public function setDI(\Phalcon\DiInterface $dependencyInjector)
    {
        $this->di = $dependencyInjector;
    }
    
    /**
     * Returns the internal dependency injector
     *
     * @return \Phalcon\DiInterface
     */
    public function getDI()
    {
        return $this->di;
    }
}


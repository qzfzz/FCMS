<?php

namespace apps\common\libraries;
if( !APP_ROOT  )
    return 'Direct Access Deny!';

/**
 * @author bruce
 * @date 2017-03-22
 */
class FcmsUploader extends Uploader implements \Phalcon\Di\InjectionAwareInterface
{
    private $_di;
    public function setDI(\Phalcon\DiInterface $dependencyInjector)
    {
        $this->_di = $dependencyInjector;
    }

    /**
     */
    public function getDI()
    {
        return $this->_di;
    }
    
    public function __construct( $fileField, $config, $type )
    {
        parent::__construct( $fileField, $config );
        
        
    }

}
<?php
namespace apps\common\libraries;
if( !APP_ROOT  )
    return 'Direct Access Deny!';

class OssServer
{

    private $id;        //  accessid
    private $key;       //  accesskey
    private $host;      //  endpoint 
    private $dir;       // upload file path
    
    public function __construct( $osscfg )
    {
        if( !$osscfg || empty( $osscfg ) )
            return false;
        
        $this->id = $osscfg['OSS_ACCESS_ID'];
        $this->key = $osscfg['OSS_ACCESS_KEY'];
        $this->host = $osscfg[ 'OSS_SERVER' ];
        $this->dir = $osscfg[ 'bizt' ];
    }
    
    public function gmt_iso8601($time)
    {
        $dtStr = date("c", $time);
        $mydatetime = new \DateTime($dtStr);
        $expiration = $mydatetime->format(\DateTime::ISO8601);
        $pos = strpos($expiration, '+');
        $expiration = substr($expiration, 0, $pos);
        return $expiration . "Z";
    }
    
    public function get_config( $params, $callbackUrl = false )
    {
        $now = time();
        $expire = 30; //设置该policy超时时间是10s. 即这个policy过了这个有效时间，将不能访问
        $end = $now + $expire;
        $expiration = $this->gmt_iso8601($end);
    
        //最大文件大小.用户可以自己设置
        $condition = array(0 => 'content-length-range', 1 => 0, 2 => 200000000);
        $conditions[] = $condition;
    
        //表示用户上传的数据,必须是以$dir开始, 不然上传会失败,这一步不是必须项,只是为了安全起见,防止用户通过policy上传到别人的目录
        $start = array(0 => 'starts-with', 1 => '$key', 2 => $this->dir );
        $conditions[] = $start;
    
        $arr = array('expiration' => $expiration, 'conditions' => $conditions);
        $policy = json_encode($arr);
        
        $base64_policy = base64_encode($policy);
        $string_to_sign = $base64_policy;
        $signature = base64_encode(hash_hmac('sha1', $string_to_sign, $this->key , true));
    
        $response = array();
        $response['accessid'] = $this->id;
        $response['host'] = $this->host;
        $response['policy'] = $base64_policy;
        $response['signature'] = $signature;
        $response['expire'] = $end;
        //这个参数是设置用户上传指定的前缀
        $response['dir'] = $this->dir;
        
        if( $callbackUrl )
        {
            $strParams = '';
            if( $params && !empty( $params ) )
            {
                if( isset( $params->biz_type ) )
                {
                    $strParams = '&biz_type='.$params->biz_type;
                }
                if( isset( $params-> mem_id ) )
                {
                    $strParams .= '&mem_id='.$params->mem_id;
                }
                if( isset( $params->mem_type ) )
                {
                    $strParams .= '&mem_type=' . $params->mem_type;
                }
            }
            //callback params
            $callback_param = array(
                'callbackUrl'=> $callbackUrl,
                'callbackBody'  => 'filename=${object}&size=${size}&mimeType=${mimeType}&height=${imageInfo.height}&width=${imageInfo.width}' . $strParams,
                'callbackBodyType'=>"application/x-www-form-urlencoded",
            );
            
            $callback_string = json_encode($callback_param);
            $base64_callback_body = base64_encode($callback_string);
            $response['callback'] = $base64_callback_body;
        }
        
        return $response;
    }
}

?>
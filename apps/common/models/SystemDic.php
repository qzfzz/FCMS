<?php

namespace apps\common\models;

use Phalcon\Mvc\Model\Behavior\SoftDelete;
use enums\DBEnums;;

class SystemDic extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $addtime;
	
	/**
	 *
	 * @var string
	 */
	public $uptime;
	
	/**
	 *
	 * @var integer
	 */
	public $delsign;
	
	/**
	 *
	 * @var string
	 */
	public $descr;
	
	/**
	 *
	 * @var string
	 */
	public $title;
	
	/**
	 *
	 * @var string
	 */
	public $key;
	
	/**
	 *
	 * @var string
	 */
	public $value;
	
	/**
	 *
	 * @var integer
	 */
	public $valtype;
	
	/**
	 *
	 * @var string
	 */
	public $cateid;
	
	/**
	 * Independent Column Mapping.
	 * Keys are the real names in the table and the values their names in the application
	 *
	 * @return array
	 */
	public function columnMap() {
		return array (
				'id' => 'id',
				'addtime' => 'addtime',
				'uptime' => 'uptime',
				'delsign' => 'delsign',
				'descr' => 'descr',
				'title' => 'title',
				'key' => 'key',
				'value' => 'value',
				'valtype' => 'valtype',
				'kind' => 'kind',
		        'cateid' => 'cateid',
		);
	}
	public function initialize() {
		$this->useDynamicUpdate( true );
		
		$this->addBehavior( new SoftDelete( array (
				'field' => 'delsign',
				'value' => DBEnums::DELSIGN_YES
		) ) );
		$this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
	}
}

<?php
namespace apps\common\models;
if( ! APP_ROOT  ) return 'Direct Access Deny!';

use Phalcon\Mvc\Model;
use helpers\TimeUtils;
use enums\DBEnums;;

class ImgAssets extends Model
{
    
    public $id;
    
    public $addtime;
    
    public $delsign;
    
    public $descr;
    
    public $filename;
    
    public $biz_type;
    
    public $size;
    
    public $type;
    
    public $bucket;
    
    public $object;
    
    public $height;
    
    public $width;
    
    public $mem_type;
    
    public $mem_id;
    
    public $original;
    
    public $path_name;
    
//     public function beforeCreate()
//     {
//         if( empty( $this->addtime ))
//         {
//             $this->addtime = TimeUtils::getFullTime();
//             $this->delsign = DBEnums::DELSIGN_NO;
//         }
//     }
    
    public function initialize()
    {
        $this->useDynamicUpdate( true );
         
        $this->setSource( $this->di['dbCfg']['db']['prefix'] . $this->getSource() );
    }
}

?>
<?php
namespace apps\common\models;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class CommonDistrictMongo extends \Phalcon\Mvc\Collection
{
	public function getSource()
	{
		return 'common_district_dic';
	}
}
<?php
namespace uploader;

interface Uploader
{
    public function upload();
    
    public function del();
    
    public function rename();
}


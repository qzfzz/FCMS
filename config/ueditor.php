<?php
if( !APP_ROOT )
    return 'Direct Access Deny!';
    
return new \Phalcon\Config( 
        array(
                'type' => 'oss_server',  // 0 for filesystem 1 for fastdfs 2 for oss_server
                'fs' => array(
                        'cur' => 'f0',
                        'metadb' => 2, //0 for mongodb 1 for redis 2 mysql
                        'paths' => array(
                                'f0' => array(
                                        'key' => 'f0',
                                        'url' => 'http://fshop.xxxx.dev', 
                                        'path' => '/Users/bruce/git/phcms/public/ueditor/sb',
                                        'full' => 0
                                ), 
                                'f1' => array(
                                        'key' => 'f1',
                                        'url' => 'http://img1.xxxx.dev', 
                                        'path' => '/Users/bruce/develop/phpprjs/img1.xxxx.dev', 
                                        'full' => 0
                                ), 
                                'f2' => array(
                                        'key' => 'f2',
                                        'url' => 'http://img2.xxxx.dev', 
                                        'path' => '/Users/bruce/develop/phpprjs/img2.xxxx.dev', 
                                        'full' => 0
                                )
                        )
                ),
                'fastdfs' => array(
                	'metadb' => 0,//0 for mongodb 1 for redis 2 for mysql
                ),
                'oss_server'   => array(
                    'cur' => 'bwdy',
                    'paths' => array(
                        'bwdy' => array(
                            'OSS_ACCESS_ID'     => 'xxxxxxxxxxxxxxxxxxx',
                            'OSS_ACCESS_KEY'    => 'xxxxxxxxxxxxxxxxxxx',
                            'OSS_SERVER'        => 'http://xxxxxxxxxxxxxxxxxxx.oss-cn-beijing.aliyuncs.com',
                            'OSS_ENDPOINT'      => 'http://oss-cn-beijing.aliyuncs.com',
                            'OSS_BUCKET_NAME'   => 'xxxxxxxxxxxxxxxxxxx',
                            'OSS_CALLBACK_URL'  => 'http://admin.xxxx.com/common/upload/cbOss',
                        ),
                    )
                ),
        ) );
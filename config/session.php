<?php
use enums\ServiceEnums;
if( !APP_ROOT )
    return 'Direct Access Deny!';

return new \Phalcon\Config(
    array(
        'server' => \enums\SystemEnums::SESSION_ADAPTER_REDIS,
        \enums\SystemEnums::SESSION_ADAPTER_REDIS   => array(
            'host'       => '127.0.0.1',
            'port'       => 6379,
            'auth'     => 'fzq',
            'persistent' => false,
            'index' => ServiceEnums::REDIS_INDEX_SESSION,
            'lifetime' => 3600,
            'prefix' => 'fcms_',
            'uniqueId' => 'fcms_articles',
        )
    ));
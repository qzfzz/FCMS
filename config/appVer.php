<?php
/**
 * 软件的当前版本号 使用git的版本标识记录
 */
if( !APP_ROOT  )
	return 'Direct Access Deny!';

return new \Phalcon\Config( array(
		'appVersion' => '326ddc120a253871f0f9281937ceea7194da8b76'
) );
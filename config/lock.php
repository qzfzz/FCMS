<?php
use enums\ServiceEnums;
if( !APP_ROOT )
	return 'Direct Access Deny!';

return new \Phalcon\Config(
		array(
				'server' => 'redisServer',
				'redisServer'   => array(
							'host'       => '11.171.113.119',
							'port'       => 6380,
							'auth'     => 'fw1s',
							'persistent' => false,
							'index' => ServiceEnums::REDIS_INDEX_LOCK,
							'lifetime' => 3600,
							'prefix' => 'bwdy_lock_',
							'uniqueId' => 'bwdyOnline'
			)
));
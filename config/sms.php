<?php 
return new \Phalcon\Config(array(
		'default' => 'JuHe',
		'JuHe' => array(
				'limit' => array( '30:1', "60:2", "1800:3" ),
				'tpl_register' => array(
					'id' => '19974',
					'app' => 'asdf',
					'tpl' => '验证码为(#code#)，请尽快完成验证。',
					'params' => array( 'code' => '验证码:string' ),
					'descr' => '注册验证码',
				),
				'tpl_reset_pass' => array(
					'id' => '22663',
					'app' => 'asdf',
					'tpl' => '验证码为(#code#)，您正在进行重置密码操作。请妥善保管。',
					'params' => array( 'code' => '验证码:string' ),
					'descr' => '重置密码',
				),
				'tpl_retrieve_pass' => array(
					'id' => '19975',
					'app' => 'asdf',
					'tpl' => '验证码为(#code#)，您正在进行找回密码操作。请妥善保管。',
					'params' => array( 'code' => '验证码:string' ),
					'descr' => '找回密码',
				),
    		    'tpl_reset_withdraw_password' => array(
    		        'id' => '26532',
    		        'app' => 'asdf',
    		        'tpl' => '验证码为（#code#），您正在进行提现密码重置操作，请妥善保管。',
    		        'params' => array( 'code' => '验证码:string' ),
    		        'descr' => '提现密码验证码',
    		    )
		),
		
		'resend_code_time' => 60,//重新发送短信验证码倒计时时间，单位为秒
		'code_exist_time' => 60 * 60 * 1,//短信验证码有效时间，单位为秒
));


// 'tpl_id'    => '19974', //您申请的短信模板ID，根据实际情况修改
// 'tpl_value' =>'#name#=风知前&#code#=3392&#hour#=1' //您设置的模板变量，根据实际情况修改
<?php
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

$router = new \Phalcon\Mvc\Router( false );

$router->removeExtraSlashes( true );

$router->add( '/admin.php', array(
		'module' => 'admin',
		'controller' => 'index',
		'action' => 'index'
));

$router->add( '/admin', array(
		'module' => 'admin',
		'controller' => 'index',
		'action' => 'index'
));

$router->add( '/admin/index', array(
        'module' => 'admin',
        'controller' => 'index',
        'action' => 'index'
));

$router->add( '/common/upload/ctrl.php', array(
        'module' => 'common',
        'controller' => 'upload',
        'action' => 'ctrl'
));

$router->add('/:module/:controller/:action/:params', array(
		'module' => 1,
		'controller' => 2,
		'action' => 3,
		'params' => 4
));

// url router
$router->setDefaultModule( 'cms' );
$router->setDefaultController( 'index' );
$router->setDefaultAction( 'index' );


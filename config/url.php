<?php
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

return new \Phalcon\Config(array(
		'url' => array(
				'url_domain' => 'xxxx.com',/* 应用域名 */
				'url_mobile_domain' => 'm.xxxx.com',/* 移动端域名 */
				'url_admin_domain' => 'a.xxxx.com',/* 平台管理端域名 */
				'url_ct_domain' => 'ct.xxxx.com:8080',/* 工具服务 */
				
				'url_oss_img' => 'http://xxxxxxxxxxxxxxxxxxx.oss-cn-beijing.aliyuncs.com',/* oss存储域名 */
				'cdn_oss_img' => 'http://xxxxxxxxxxxxxxxxxxx.xxxx.com',/* oss存储CDN */
				
				'url_assets_img' => 'http://assets-img-a.xxxx.com/img',/* image资源域名 */
				'url_assets_js' => 'http://assets-js-a.xxxx.com/js',/* js资源域名 */
				'url_assets_css' => 'http://assets-css-a.xxxx.com/css',/* css资源域名 */
				'url_assets_bundle' => 'http://assets-bundle-a.xxxx.com/bundle',/* bundle资源域名 */
				
				'cdn_assets_img' => 'http://ass-img-a.xxxx.com/img',/* image资源cdn域名 */
				'cdn_assets_js' => 'http://ass-js-a.xxxx.com/js',/* js资源cdn域名 */
				'cdn_assets_css' => 'http://ass-css-a.xxxx.com/css',/* css资源cdn域名 */
				'cdn_assets_bundle' => 'http://ass-bundle-a.xxxx.com/bundle',/* bundle资源域名 */
		))
);
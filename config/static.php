<?php
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

return new \Phalcon\Config( array(
    'base_url'                  => 'http://m.xxxxx.dev',
    'static_project_domain'     => 'http://static.xxxxx.dev',
	'static_project_path'       => '/Users/fzq/git/fcms.fzq.dev/logs/',
    'static_dynamic_index'      => '/cms/index/index'
) );
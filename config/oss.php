<?php
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }
    
 return new \Phalcon\Config( array(
     'cur' => 'bwdy',
     'paths' => array(
         'bwdy' => array(
             'OSS_ACCESS_ID'     => 'xxxxxxxxxxxxxxxxxxx',
             'OSS_ACCESS_KEY'    => 'xxxxxxxxxxxxxxxxxxx',
             'OSS_SERVER'        => 'http://xxxxxxxxxxxxxxxxxxx.oss-cn-beijing.aliyuncs.com',
             'OSS_ENDPOINT'      => 'http://oss-cn-beijing.aliyuncs.com',
             'OSS_BUCKET_NAME'   => 'xxxxxxxxxxxxxxxxxxx',
             'OSS_CALLBACK_URL'  => 'http://m.xxxx.com/common/upload/cbOss',
         ),
     ),
));

<?php
use enums\ServiceEnums;
if( !APP_ROOT )
	return 'Direct Access Deny!';

return new \Phalcon\Config( array(
		'aliRedisServer' => array(
				'host'       => 'r-afqga1234agasdf.redis.rds.aliyuncs.com',
				'port'       => 6379,
				'auth'     => 'fqgqs',
				'persistent' => false,
				'prefix'     => 'gqsdf_'
		),
		'cacheAdapter' => 'redisCache',
		'inmem'        => array( 'frontend' => array( 'lifetime' => 1800 ) ),
		'redisCache'   => array(
				'frontend' => array( 'lifetime' => 14400 ),
				'backend'  => array(
						'host'       => '127.0.1.123',
						'port'       => 6379,
						'auth'     => 'asdfasdf',
						'persistent' => false,
						'index' => ServiceEnums::REDIS_INDEX_CACHE,
						'prefix' => 'bwdy_',
						'uniqueId' => 'bwdy',
				)
		),
		'htmlCache'    => array(
				'frontend' => array( 'lifetime' => 14400 ),
				'backend'  => array( 'cacheDir' => APP_ROOT . 'cache/html/' )
		)
));
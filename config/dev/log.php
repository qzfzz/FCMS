<?php
use enums\ServiceEnums;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

return new \Phalcon\Config(array(
		'log' => array(
				ServiceEnums::SERVICE_LOG_BIZ => '/Users/bruce/log/adminBizLog.log',
		        ServiceEnums::SERVICE_LOG_INFO => '/Users/bruce/log/adminInfoLog.log',
				ServiceEnums::SERVICE_LOG_ERR => '/Users/bruce/log/adminErrLog.log',
				ServiceEnums::SERVICE_LOG_DEBUG	=> '/Users/bruce/log/adminDebugLog.log',
			))
);
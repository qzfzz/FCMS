<?php
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

return new \Phalcon\Config(array(
		'url' => array(
				'url_domain' => 'fzq.me',/* 应用域名 */
				'url_mobile_domain' => 'm.fzq.me',/* 移动端域名 */
				'url_admin_domain' => 'fcms.fzq.me',/* 平台管理端域名 */
				'url_ct_domain' => 'ct.fzq.me:8080',/* 工具服务 */
				
				'url_oss_img' => 'http://xxxxxxxxxxxxxxxxxxx.oss-cn-beijing.aliyuncs.com',/* oss存储域名 */
				'cdn_oss_img' => 'http://xxxxxxxxxxxxxxxxxxx.xxxxx.me',/* oss存储CDN */
				
				'url_assets_img' => 'http://assets-img-fcms.fzq.me',/* image资源域名 */
				'url_assets_js' => 'http://assets-js-fcms.fzq.me',/* js资源域名 */
				'url_assets_css' => 'http://assets-css-fcms.fzq.me',/* css资源域名 */
				'url_assets_bundle' => 'http://assets-bundle-fcms.fzq.me',/* bundle资源域名 */
				
				/* dev模式 不采用CDN加速 */
				'cdn_assets_img' => 'http://fcms.fzq.me/img',/* image资源cdn域名 */
				'cdn_assets_js' => 'http://fcms.fzq.me/js',/* js资源cdn域名 */
				'cdn_assets_css' => 'http://fcms.fzq.me/css',/* css资源cdn域名 */
				'cdn_assets_bundle' => 'http://fcms.fzq.me/bundle',/* bundle资源域名 */
		))
);
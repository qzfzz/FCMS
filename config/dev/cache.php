<?php
use enums\ServiceEnums;
if( !APP_ROOT )
	return 'Direct Access Deny!';

return new \Phalcon\Config( array(
		'aliRedisServer' => array(
				'host'       => 'r-asdf.redis.rds.aliyuncs.com',
				'port'       => 6379,
				'auth'     => 'fzq',
				'persistent' => false,
				'prefix'     => 'bwdy_'
		),
		'cacheAdapter' => 'redisCache',
		'inmem'        => array( 'frontend' => array( 'lifetime' => 1800 ) ),
		'redisCache'   => array(
				'frontend' => array( 'lifetime' => 14400 ),
				'backend'  => array(
						'host'       => '127.0.0.1',
						'port'       => 6379,
						'auth'     => 'fzq',
						'persistent' => false,
						'index'	=> ServiceEnums::REDIS_INDEX_CACHE,
						'prefix' => 'bwdy_',
						'uniqueId' => 'bwdy',
				)
		),
		'htmlCache'    => array(
				'frontend' => array( 'lifetime' => 14400 ),
				'backend'  => array( 'cacheDir' => APP_ROOT . 'cache/html/' )
		)
));
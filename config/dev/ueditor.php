<?php
if( !APP_ROOT )
    return 'Direct Access Deny!';
    
return new \Phalcon\Config( 
        array(
                'type' => 'fs',  // 0 for filesystem 1 for fastdfs 2 for oss_server
                'metadb' => 2, //0 for mongodb 1 for redis 2 mysql
                'fs' => array(
                        'cur' => 'f0',
                        'metadb' => 2, //0 for mongodb 1 for redis 2 mysql
                        'paths' => array(
                                'f0' => array(
                                        'key' => 'f0',
                                        'url' => 'http://fcms.fzq.me/upload',
                                        'path' => '/Volumes/git/www/fcms/public/upload',
                                        'full' => 0
                                ), 
                                'f1' => array(
                                        'key' => 'f1',
                                        'url' => 'http://img1.xxxx.dev', 
                                        'path' => '/Users/bruce/develop/phpprjs/img1.xxxx.dev', 
                                        'full' => 0
                                ), 
                                'f2' => array(
                                        'key' => 'f2',
                                        'url' => 'http://img2.xxxx.dev', 
                                        'path' => '/Users/bruce/develop/phpprjs/img2.xxxx.dev', 
                                        'full' => 0
                                )
                        )
                ),
                'fastdfs' => array(
                	'metadb' => 0,//0 for mongodb 1 for redis 2 for mysql
                ),
                'oss'   => array(
                    'cur' => 'bwdy',
                    'metadb' => 0,//0 for mongodb 1 for redis 2 for mysql
                    'paths' => array(
                        'bwdy' => array(
                            'OSS_ACCESS_ID'     => 'xxxxxx',
                            'OSS_ACCESS_KEY'    => 'xxxx',
                            'OSS_SERVER'        => 'http://img-xxxx.oss-cn-beijing.aliyuncs.com',
                            'OSS_ENDPOINT'      => 'http://oss-cn-beijing.aliyuncs.com',
                            'OSS_BUCKET_NAME'   => 'img-xxxx',
                            'OSS_CALLBACK_URL'  => 'http://xxxx.xxxx.com/common/upload/cbOss',
                        ),
                    )
                ),
        ) );
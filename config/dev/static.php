<?php
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

return new \Phalcon\Config( array(
    'base_url'                  => 'http://m.asdf.dev',
    'static_project_domain'     => 'http://static.asdf.dev',
	'static_project_path'       => '/Users/fzq/git/a.asdf.dev/logs/',
    'static_dynamic_index'      => '/cms/index/index'
) );
<?php
use enums\ServiceEnums;
if( !APP_ROOT )
	return 'Direct Access Deny!';

return new \Phalcon\Config( array(
		'server' => array(
				'host'       => '127.0.0.1',
				'port'       => 6379,
				'auth'     => 'fzq',
				'persistent' => false,
				'prefix'     => 'bwdy_',
				'index'	=> ServiceEnums::REDIS_INDEX_QUEUE
		)
));
<?php
use enums\ServiceEnums;
if( !APP_ROOT )
	return 'Direct Access Deny!';

return new \Phalcon\Config(
		array(
				'server' => 'redisServer',
				'redisServer'   => array(
							'host'       => '127.0.0.1',
							'port'       => 6380,
							'auth'     => 'fzq',
							'persistent' => false,
							'index' => ServiceEnums::REDIS_INDEX_LOCK,
							'lifetime' => 3600,
							'prefix' => 'bwdy_lock_',
							'uniqueId' => 'bwdyOnline'
			)
));
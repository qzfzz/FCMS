<?php
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }
return new \Phalcon\Config(array(
	    'db' => array(
	        'adapter'  => 'Mysql',
	        'host'     => '127.0.0.1',
	        'username' => 'root',
	        'password' => 'mysql',
	        'dbname'   => 'fcms',
	        'charset'  => 'utf8',
	        'prefix'   => 'fcms_',
	        'port'     => 3306
    ))
);

<?php
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }
return new \Phalcon\Config(array(
		'application' => array(
	        'pluginsDir'     => APP_ROOT . 'plugins/',
	        'libraryDir'     => APP_ROOT . 'libraries/',
	        'cacheDir'       => APP_ROOT . 'cache/',
	        'enumsDir'		 => APP_ROOT . 'enums/',
	        'logs' 			 => APP_ROOT . 'logs/',
	        'listenersDir'	 => APP_ROOT . 'listeners/',
	        'baseUri'        => '/',
	    ),
	    'admin_regenrator_time_interval' => 3 * 60,
	    'home_regenarater_time_interval' => 3 * 60,
	    'sensitive_default_replace'		 => '**', // 默认敏感字符替换
		
		'overdue_exchange_time'		=> 7 * 24 * 3600 , //设置换货截至时间
		'overdue_return_time'		=> 10 * 24 * 3600, //设置退货截至时间
		'overdue_repair_time'		=> 3 * 30 * 34 * 3600,//设置维修截至时间
		
		'lock_time' => '600',//锁定时间（单位s，当前锁10分钟）
    )
);

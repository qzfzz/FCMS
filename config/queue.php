<?php
use enums\ServiceEnums;
if( !APP_ROOT )
	return 'Direct Access Deny!';

return new \Phalcon\Config( array(
		'server' => array(
				'host'       => 'r-fqadsfqwf.redis.rds.aliyuncs.com',
				'port'       => 6379,
				'auth'     => 'faqs',
				'persistent' => false,
				'prefix'     => 'fqs_',
				'index'	=> ServiceEnums::REDIS_INDEX_QUEUE,
		)
));
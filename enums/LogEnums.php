<?php
namespace enums;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

/**
 * @author 	zlw
 * @date 	2015.8.19
 * @desc	日志字典
 */
class LogEnums
{
	/**
	 * 后台操作日志
	 */
	const LOG_ADMIN_OPERATION_LOG  = 'Log_adminOperationLog';
	
	/**
	 * 前台操作日志
	 */
	const LOG_HOME_OPERATION_LOG = 'Log_homeOperationLog';
	
	/**
	 * 前台用户访问日志
	 */
	const LOG_HOME_VISIT_LOG = 'Log_homeVisitLog';
	
	/**
	 * 后台session安全验证
	 */
	const LOG_ADMIN_SESSION_LOG  = 'Log_adminSession';
	
	/**
	 * 前台session安全验证
	 */
	const LOG_MEM_SESSION_LOG = 'Log_homeSession';
	
	/**
	 * 前台登录日志
	 */
	const LOG_MEM_LOGIN_LOG = 'Log_memLoginLog';
	
	/**
	 * 记录异常信息
	 */
	const LOG_RECORDEXCEPTION = 'Log_recordException';
	
    /**
     * 添加操作
     */
	const OPERATION_TYPE_ADD = 1;
	
	/**
	 * 修改操作
	 */
	const OPERATION_TYPE_UPDATE = 2;
	
	/**
	 * 删除操作
	 */
	const OPERATION_TYPE_DELETE = 3;
	
	/**
	 * 后台控制器
	 */
	const ADMIN_CONTROLLER_PATH = 'apps\admin\controllers';
	
	/**
	 * 前台控制器
	 */
	const MEM_CONTROLLER_PATH = 'apps\home\controllers';
	
	/**
	 * 后台控制器命名空间
	 */
	const ADMIN_CONTROLLER_NAMESPACE = '\apps\admin\controllers\\';
	
	/**
	 * 前台控制器命名空间
	 */
	const MEM_CONTROLLER_NAMESPACE = '\apps\home\controllers\\';
	
	
	/** ------------------------- 操作日志类别 ---------------------- **/
	/**
	 * 用户类
	 * @var unknown
	 */
	const LOG_TYPE_USER_OPT = 'user_operation';


	
	/**
	 * 取调试的logger格式
	 */
	public static function getDebugLogFormatter()
	{
		return array( 'sid' => 'session id', 'msg' => 'debug message', 'level' => 'debug', 'request' => array() );
	}
	
	/**
	 * 取业务的logger格式
	 */
	public static function getBizLogFormatter()
	{
		return array( 'sid' => 'session id', 'msg' => 'biz message', 'level' => 'info', 'request' => array() );
	}
	
	
	/**
	 * 取出错的信息保存logger格式
	 */
	public static function getErrorLogFormatter()
	{
		return array( 'sid' => 'session id', 'msg' => 'error message', 'level' => 'error', 'request' => array(), 'server' => array() );
	}
}
<?php

namespace enums;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

/**
 * 敏感词
 */
class SensitiveEnums {

	/*-------------------------敏感词替换对象类型------------------------*/
	/**
	 * 文章(product表)
	 */
	const SENSITIVE_REPLACE_PRODUCT = 1;
	
	/**
	 * 评论(orders_comments表)
	 */
	const SENSITIVE_REPLACE_COMMENT = 2;
	
	/**
	 * 评论回复(order_comments_reply表)
	 */
	const SENSITIVE_REPLACE_COMMENT_REPLY = 3;
	
	/**
	 * 咨询(advices表)
	 */
	const SENSITIVE_REPLACE_ADVICE = 4;
	
	/**
	 * 通知(internal_notify表)
	 */
	const SENSITIVE_REPLACE_NOTIFY = 5;
	
	/**
	 * 站内信(internal_message表)
	 */
	const SENSITIVE_REPLACE_MESSAGE = 6;
}

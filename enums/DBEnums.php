<?php
namespace enums;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class DBEnums {
	/**
	 * 数据库返回object
	 * Database result object
	 */
	const DB_RESULT_OBJECT = 'object';
	
	/**
	 * 数据库返回array
	 * Database result array
	 */
	const DB_RESULT_ARRAY = 'array';
	
	/**
	 * 数据库 排序
	 *
	 * asc
	 */
	const DB_SORT_ASC = 'asc';
	
	/**
	 * 数据库 排序
	 *
	 * desc
	 */
	const DB_SORT_DESC = 'desc';
	
	
	/**
	 * delsign delete
	 * 删除 1
	 */
	const DELSIGN_YES = 1;
	
	/**
	 * delsign not delete
	 * 不删除   0
	 */
	const DELSIGN_NO = 0;

	/**
     * page size
     */
	const DEFAULT_BACKEND_PAGE_SIZE = 20;

}


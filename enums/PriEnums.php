<?php

namespace enums;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class PriEnums 
{
	
	/**
	 * 状态 正常  0
	 */
	const STUATUS_NOEMAL = 0;
	
	/**
	 * 状态 异常 1
	 */
	const STUATUS_UNNOEMAL = 1;
	
	
	/**
	 * 超级管理员账户
	 */
	const SUPER_ADMIN_ID = 1;
	
	/**
	 * 系统自带模块
	 */
	const DEFAULT_MODEL_ID = 0;
	
	/**
	 * 不展示模块
	 */
	const IS_MENU_NO = 0;

	/**
	 * 展示模块
	 */
	const IS_MENU_YES = 1;
    
    /**
     *只是菜单
     */
    const PRI_MENU = 3;
    
    /**
     * 操作
     */
    const PRI_ACTION = 0;
    
    /**
     * 控制器
     */
    const PRI_CONTROLLER = 1;
    
    
    /**
     * 模块
     */
    const PRI_MODULE = 2;
	
    
    /**
     * admin 所有acl 缓存前缀
     */
    const PREFIX_ACL_ADMIN = 'acl_admin_';
    
    /**
     * 左菜单 缓存 前缀
     */
    const LEFT_MENU_ADMIN = 'left_menu_admin';
    
	/**
	 * 默认pid
	 */
	const DEFAULT_PID = 0;
	
	/**
	 * 默认ACL
	 */
	const DEFAULT_ACL_YES = 1;
	
	/**
	 * 非默认ACL
	 */
	const DEFAULT_ACL_NOT = 0;
	
	/**
	 * 菜单级别  一级菜单
	 */
	const CAT_LEVEL_FIRST = 1;
	
	/**
	 * 菜单级别  二级菜单
	 */
	const CAT_LEVEL_SECOND = 2;
	
	/**
	 * 菜单级别  三级菜单
	 */
	const CAT_LEVEL_THIRD = 3;
	
	/**
	 * 用户当前状态 正常
	 */
	const USER_STATE_NORMAL = 0;
	
	/**
	 * 用户当前状态 暂停使用
	 */
	const USER_STATE_PAUSE = 1;
	
	/**
	 * 用户当前状态 密码丢失
	 */
	const USER_STATE_LOST = 2;
	
	/**
	 * 用户当前状态 锁定
	 */
	const USER_STATE_LOCK = 3;
    
    /**
	 * 用户当前状态 拉黑
	 */
	const USER_BAN = 4;
	
    
}

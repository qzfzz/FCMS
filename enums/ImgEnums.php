<?php
namespace enums;

class ImgEnums
{
    
    /**
     * mongodb
     */
    const META_DEST_MONGODB = 0;
    
    /**
     * redis
     */
    const META_DEST_REDIS = 1;

    /**
     * mysql
     */
    const META_DEST_MYSQL = 2;
}


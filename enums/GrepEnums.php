<?php

namespace enums;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

/**
 * 正则表达式
 */
class GrepEnums 
{
	
	/**
	 * 产品价格（任意位数并带有两位小数的正浮点数）
	 */
	CONST GREP_PRICE = '/^[1-9]*\d+(\.\d{1,2})?$/';
	
	/**
	 * 任意位数正整数
	 */
	CONST GREP_POSITIVE_INT = '/^[1-9]*\d+$/';
	
	/**
	 * 邮箱
	 */
	CONST GREP_EMAIL = '/^\w+(\.\w+)*@(\w+\.\w+)(\.\w+)*$/';
	
	/**
	 * 中国大陆手机号
	 */
	CONST GREP_CELLPHONE = '/^(?:(\+86[\-\s]?)?)1[34578]{1}\d{1}(?:[\-\s]?)\d{4}(?:[\-\s]?)\d{4}$/';
	
	/**
	 * 年龄（0-130岁）
	 */
	CONST GREP_AGE = '/^[0-9]$|^[1-9][0-9]$|^1[0-2][0-9]$|^130$/';
	
	/**
	 * 用户名（6-16位中文、字母，可重复）
	 */
	CONST GREP_USERNAME = '/^([\u0391-\uFFE5A-Za-z](\s[\u0391-\uFFE5A-Za-z]+)*){6,16}$/';
	
	/**
	 * 真实姓名（2-6个中文）
	 */
	CONST GREP_REAL_NAME = '/^[\u0391-\uFFE5A]{2,6}$/u';
	
	/**
	 * 昵称可中文nickname（6-16位中文、字母、数字或下划线，可重复）
	 */
	CONST GREP_NICKNAME = '/^([\u0391-\uFFE5A-Za-z](\s[\u0391-\uFFE5A-Za-z]+)*){6,16}$/';
	
	/**
	 * 登录名login_name（6-16位数字、字母或下划线，不可重复）
	 */
	CONST GREP_LOGIN_NAME = '/^([\u0391-\uFFE5\w](\s[\u0391-\uFFE5\w]+)*){6,16}$/';
	
	/**
	 * 密码6-12位（6-16位数字、字母或下划线，可重复）
	 */
	CONST GREP_PASSWORD = '/^\w{6,16}$/';
	
	/**
	 * 域名
	 */
	CONST GREP_DOMAIN = '/^(?:https?:\/\/)?(?:(?:[-_\w]+)\.){1,}(?:[-_\w]+)$/';
	
	/**
	 * url
	 */
	CONST GREP_URL = '/^(?:\/[a-zA-Z0-9]+){1,}\/?$/';
	
	/**
	 * 验证码（4位）
	 */
	CONST GREP_CODE = '/^[a-zA-Z0-9]{4}$/';
	
	/**
	 * 邮政编码（6位）
	 */
	CONST GREP_POSTID_INT = '/^[1-9]\d{5}$/';
	
	/**
	 * 匹配身份证（15 位）
	 */
	CONST GREP_ID_CARD1 = '/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/';
	
	/**
	 * 匹配身份证（18 位）
	 */
	CONST GREP_ID_CARE2 = '/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/';
	
	/**
	 * 银行卡号（16-21位）
	 */
	CONST GREP_BANK_CARD = '/^[1-9]\d{15,20}$/';
	
}

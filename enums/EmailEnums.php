<?php
namespace enums;

class EmailEnums
{
    /**
     * 使用本地email服务器发送邮件 
     */
	const MODE_LOCAL = 'local';
	
	/**
	 * 使用其他公司的邮件服务器发送邮件
	 */
	const MODE_ONLINE = 'online';
}
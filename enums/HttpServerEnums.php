<?php
namespace enums;
if( ! APP_ROOT  ) return 'Direct Access Deny!';

/**
 * Http Server Enums
 * @author bruce
 *
 */
class HttpServerEnums
{
	/**
	 * session key
	 */
    const SESSION_KEY = 'FCMS_SESSIONID';
    
    /**
     * php-fpm libphp ... 
     */
    const SERVER_TYPE_COMMON = 0;
    
    /**
     * swoole
     */
    const SERVER_TYPE_SWOOLE = 1;
    
   
}
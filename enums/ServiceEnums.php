<?php
namespace enums;

/**
 * 
 * services.php中的服务名
 * 
 * @author fzq
 * @date 2016-12-12
 * 
 */
class ServiceEnums
{
	/**
	 * lock config
	 */
	const SERVICE_CFG_LOCK = 'lockCfg';
	
	/**
	 * url config
	 */
	const SERVICE_CFG_URL = 'urlCfg';
	
	/**
	 * db config
	 */
	const SERVICE_CFG_DB = 'dbCfg';
	
	/**
	 * queue config
	 */
	const SERVICE_CFG_QUEUE = 'queueCfg';
	
	/**
	 * default config
	 */
	const SERVICE_CFG_CONFIG = 'config';
	
	/**
	 * log config
	 */
	const SERVICE_CFG_LOG = 'logCfg';
	
	/**
	 * cache config
	 */
	const SERVICE_CFG_CACHE = 'cacheCfg';
	
	/**
	 * email config
	 */
	const SERVICE_CFG_EMAIL = 'emailCfg';
	
	/**
	 * juhe config
	 */
	const SERVICE_CFG_JUHE = 'juheCfg';
	
	/**
	 * models cache service
	 */
	const SERVICE_MODELS_CACHE = 'modelsCache';

	/**
	 * benchmark service
	 */
	const SERVICE_BENCHMARK = 'bm';


	/**
	 * cookies service
	 */
	const SERVICE_COOKIES = 'cookies';

	/**
	 * session service
	 */
	const SERVICE_SESSION = 'session';
	
	
	/**
	 * crypt service
	 */
	const SERIVICE_CRYPT = 'crypt';
		
	/**
	 *  service a router
	 */
	const SERVICE_ROUTER = 'router';

	/**
	 * Registering unitTest
	 */
	const SERVICE_UNIT_TEST = 'utest';

	/**
	 * url service
	 */
	const SERVICE_URL = 'url';

	/**
	 * 验证码
	 */ 
	const SERVICE_VERIFY =  'verify';

// //////////////////////////////////////////////////////////////////////////////////////////////////////////
// cache
	/**
	 * for html segment
	 */
	const SERVICE_HTML_CACHE =  'htmlCache';


	/**
	 * apc cache
	 */
	const SERVICE_APC_CACHE = 'apcCache';

	/**
	 * memory 易失性 不能进行序列化 只能在一次请求中有效
	 */
	const SERVICE_MEMORY = 'memory';

	const SERVICE_DATA_CACHE = 'dataCache';

	/**
	 * redis cache
	 */
	const SERVICE_REDIS_CACHE = 'redisCache';

	/**
	 * native redis
	 * 原生native使用phpredis扩展
	 */
	const SERVICE_NATIVE_REDIS = 'nredis';

	/**
	 * 阿里云native redis，只能在aliyun上访问
	 * 原生native使用phpredis扩展
	 */
	const SERVICE_ALI_REDIS = 'aliRedis';
	

	/**
	 * 分布式队列
	 */
	const SERVICE_RQUEUE = 'rqueue';


	/**
	 * upload
	 */
	const SERVICE_UPLOAD = 'upload';


	/**
	 * email
	 */
	const SERVICE_EMAIL = 'email';


	/**
	 * sms service
	 */
	const SERVICE_SMS = 'sms';
    

	/**
	 * xs_museum
	 */
	const SERVICE_XS_MUSEUM = 'xs_museum';
	
	/**
	 * xs_narrator 
	 */
	const SERVICE_SX_NARRATOR = 'xs_narrator';


	/**
	 * modelsMetadata
	 */
	const SERVICE_MODELS_META_DATA = 'modelsMetadata';


	/**
	 * CsrfCheck
	 */
	const SERVICE_SECURITY = 'security';


	
	/**
	 * 互斥锁服务 service
	 */
	const SERVICE_DISTRIBUTION_EXCLUSIVE_LOCK = 'dExclusiveLock';


	/**
	 * oss service
	 */
 	const SERVICE_OSS = 'oss';

 	
 	/**
 	 * info log
 	 */
 	const  SERVICE_LOG_INFO = 'infoLog';
 	
	/**
	 * biz log
	 */
	const  SERVICE_LOG_BIZ = 'bizLog';

	/**
	 * error log
	 */
	const SERVICE_LOG_ERR = 'errLog';


	/**
	 * debug log
	 */
	const SERVICE_LOG_DEBUG = 'debugLog';



	/**
	 * service nredis
	 */
	const SERVICE_REDIS_DB_CACHE = 'nRedisCache';
	
	
	/**
	 * distribution redis lock db
	 */
	const SERVICE_REDIS_DB_LOCK = 'redisLockDB';
	
	
	/**
	 * 默认
	 */
	const REDIS_INDEX_DEFAULT = 0;
	
	/**
	 * 普通cache
	 */
	const REDIS_INDEX_CACHE = 1;
	
	/**
	 * session
	 */
	const REDIS_INDEX_SESSION = 2;
	
	/**
	 * queue
	 */
	const REDIS_INDEX_QUEUE = 3;
	
	/**
	 * lock
	 */
	const REDIS_INDEX_LOCK = 4;
	
}
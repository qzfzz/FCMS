<?php
namespace enums;

class AppEnums
{
	/**
	 * 开发模式
	 */
	const APP_MODE_DEV = 'dev';
	
	/**
	 * 部署模式
	 */
	const APP_MODE_PRODUCTION = 'production';	
	
}
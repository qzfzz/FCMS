<?php

namespace enums;
!defined( 'APP_ROOT' ) && exit( 'Direct Access Deny!' );

class SystemEnums
{

    /**

     * delsign delete

     * 删除 1 ----- 冻结帐户

     */
    const DELSIGN_YES = 1;

    /**

     * delsign not delete

     * 不删除   0 ---- 正常帐户

     */
    const DELSIGN_NO = 0;


    /**

     * 状态 正常  0

     */
    const STUATUS_NOEMAL = 0;

    /**

     * 状态 异常 1

     */
    const STUATUS_UNNOEMAL = 1;


    /**

     * 超级管理员账户

     */
    const SUPER_ADMIN_ID = 1;

    /**

     * 系统自带模块

     */
    const DEFAULT_MODEL_ID = 0;

    /**

     * 不展示模块

     */
    const IS_MENU_NO = 0;

    /**

     * 展示模块

     */
    const IS_MENU_YES = 1;

    /**

     *只是菜单

     */
    const PRI_MENU = 3;

    /**

     * 操作

     */
    const PRI_ACTION = 0;

    /**

     * 控制器

     */
    const PRI_CONTROLLER = 1;


    /**

     * 模块

     */
    const PRI_MODULE = 2;

    /**

     * 默认pid

     */
    const DEFAULT_PID = 0;

    /**

     * 菜单级别  一级菜单

     */
    const CAT_LEVEL_FIRST = 1;

    /**

     * 菜单级别  二级菜单

     */
    const CAT_LEVEL_SECOND = 2;

    /**

     * 菜单级别  三级菜单

     */
    const CAT_LEVEL_THIRD = 3;

    /**

     * 用户当前状态 正常

     */
    const USER_STATE_NORMAL = 0;

    /**

     * 用户当前状态 暂停使用

     */
    const USER_STATE_PAUSE = 1;

    /**

     * 用户当前状态 密码丢失

     */
    const USER_STATE_LOST = 2;

    /**

     * 用户当前状态 锁定

     */
    const USER_STATE_LOCK = 3;

    /**

     * 用户当前状态 账号激活

     */
    const USER_STATE_ACTIVE = 4;

    /**

     * 未读系统通知

     */
    const UNREAD_NOTICE = 1;

    /**

     * 已读系统通知

     */
    const READ_NOTICE = 2;


    /**

     *  图片服务器

     */
    const PIC_HOST = 'http://www.huaer.dev';

    /**
     * session adapter file
     */
    const SESSION_ADAPTER_FILE = 0;

    /**
     * session adapter redis
     */
    const SESSION_ADAPTER_REDIS = 1;
}

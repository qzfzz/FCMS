<?php

namespace enums;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

/**
 * 产品发布者类型
 */
class PromoterTypeEnums 
{
	/**
	 * 普通会员
	 */
	const PROMOTER_COMMOM = 0;
	
	/**
	 * 平台
	 */
	const PROMOTER_PLATFORM = 4;
	
}

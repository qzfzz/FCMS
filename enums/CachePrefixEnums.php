<?php
namespace enums;

/**
 * 
 * @author fzq
 * 
 * 全名规则
 * 第一组
 * 
 * H:Hash
 * STR:STRing
 * L:List
 * S:Sets
 * SS:Sorted Sets
 * B:Bitmaps
 * HLL:HyperLogLogs
 * G:Geospatial
 * 
 * 第二组
 * S for Self 
 * A for Ali
 * 
 * 第三组及其他 更详细的数据结构或业务
 * 
 * 示例： H_S_LOCK_ORDERS = 'h_s_lock_specification';
 */
class CachePrefixEnums
{
	
	//********************************************************************************
	//DLock.php
	/**
	 * 某个资源持有者list前缀
	 */
	const LOCK_RES_LIST_KEYS_PREFIX = 'l_s_res_lock_keys_';
	
	/**
	 * 持锁者前缀
	 */
	const LOCK_RES_ITEM_PREFIX = 'str_s_res_lock_item_';
	
	/**
	 * 资源锁 资源数量hash前缀 主要用来存放资源的个数及剩余数量等
	 */
	const LOCK_RES_PREFIX = 'h_s_res_lock_';
	
	/**
	 * 消费记录前缀
	 */
	const LOCK_RES_LOCK_CONSUME_PREFIX = 'h_s_res_lock_consume_log_';
	
	//********************************************************************************
	//IDGenerator.php
	
	/**
	 * 用于IDGenerator
	 */
	const IDG_PREFIX = 'str_a_idg_';
	
	//********************************************************************************
	//MuseumsBiz.php
	const MEM_OREDER_SCORE_TYPE_PREFIX = 'h_s_m_order_score_type_';
	
	//********************************************************************************
	//敏感词
	const SENSITIVE_WORD = 'h_s_article_sensitive_word';
	
	//********************************************************************************
	//微信
	const WECHAT_ACCESS_TOKEN = 'str_s_wechat_access_token';
	
	const  WX_ACCOUNT_TOKEN_NAME = 'str_s_wx_config_access_token';
	
	const  WX_TICKET_NAME = 'str_s_wx_config_ticket';
	
	//********************************************************************************
	//区域
	
	/**
	 * 以区域名为key
	 */
	const DISTRICTS_NAME = 'h_s_district_name';
	
	/**
	 * 以区域id为key
	 */
	const DISTRICTS_ID = 'h_s_district_id';
	
	/**
	 * 子
	 */
	const DISTRICT_CHILDREN = 'h_s_district_children';
	
	/**
	 * 选择项
	 */
	const DISTRICT_SELECTORS = 'h_s_district_selectors';
	
	
}
<?php
namespace enums;

class MsgType
{
    /**
     * message type email
     */
    const EMAIL_SEND = 'Email_send';
    
    /**
     *  email发送报告 （比如本周发送了多少email等）
     */
    const EMAIL_REPORT = 'Email_report';
    
    /**
     * message type short message
     */
    const SMS_SEND = 'Sms_send';
    
    
    /**
     * message type backup database
     */
    const DB_BACKUP = 'Db_backup';
    
    /**
     * message type security scan
     */
    const SECURITY_SCANSCRIPTSFILES = 'Security_scanCriptsFiles';
    
    
}
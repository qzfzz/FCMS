<?php
namespace enums;
if( ! APP_ROOT  ) return 'Direct Access Deny!';

/**
 * 上传图片业务biz enums
 * ----------------------------------------------
 * biz_type  <= 999
 * ----------------------------------------------
 * @author Carey
 * @date 2016-10-12
 */
class ImgBizEnums
{
    
 /*
     * ----------------------------------------------
     * admin   ( 500~999 )段
     * ----------------------------------------------
     */
    /**
     * 平台 用户-修改头像
     */
    const ADMIN_USER_AVATAR = 500;
    const ADMIN_USER_AVATAR_PREFIX = 'user_avatar';
    
    /**
     * 平台用户 - 上传文章收图
     */
    const ADMIN_ARTICLE_CHIEF_IMG = 501;
    
    /**
     * 平台用户-上传文章内容图片
     */
    const ADMIN_ARTICLE_IMGS = 502;
    
    /**
     * 平台用户 - 上传广告图片
     */
    const ADMIN_AD_CHIEF_IMG = 503;
    
    /**
     * 平台用户 - 上传站点图片
     */
    const ADMIN_SITE_CHIEF_IMG = 504;
    
    /**
     * 平台用户 - 上传友情链接
     */
    const ADMIN_FRIENDLINK_IMG = 505;
    
    /**
     * 平台用户 - 上传幻灯片
     */
    const ADMIN_SLIDE_IMG = 506;
  
    
    /**
     * 平台用户 - museum 导航icon
     */
    const ADMIN_MENU_NAV_IMG = 508;
}


<?php
namespace enums;

class UnitTestEnums
{
//     * @param @iMatch string 0 for simple 1 for match keys 2 for match values 3 for match keys and values 4 for match partial
    
	/**
	 * match simple
	 */
	const MATCH_SIMPLE = 0;	
	
	/**
	 * match keys
	 */
	const MATCH_KEYS = 1;

	/**
	 * match values
	 */
	const MATCH_VALUES = 2;
	
	/**
	 * match keys and values
	 */
	const MATCH_KEYS_AND_VALS= 3;
	
	/**
	 * match paritial
	 */
	const MATCH_PARTIAL = 4;
	
	const EXPECTED_IS_OBJECT = 'is_object';
	
	const EXPECTED_IS_STRING = 'is_string';
	
	const EXPECTED_IS_BOOL = 'is_bool';
	
	const EXPECTED_IS_TRUE = 'is_true';
	
	const EXPECTED_IS_FALSE = 'is_false';
	
	const EXPECTED_IS_INT = 'is_int';
	
	const EXPECTED_IS_NUMERIC = 'is_numeric';
	
	const EXPECTED_IS_FLOAT = 'is_float';
	
	const EXPECTED_IS_DOUBLE = 'is_double';
	
	const EXPECTED_IS_ARRAY = 'is_array';
	
	const EXPECTED_IS_NULL = 'is_null';
	
	const EXPECTED_IS_RESOURCE ='is_resource';
	
}
<?php
namespace enums;

class QueueEnums
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//优先级
	/**
	 * 高优先级
	 */
	const PRIORITY_HIGH = 1;

	/**
	 * 正常优先级 
	 */
	const PRIORITY_MEDIUM = 2;
	
	/**
	 * 低优先级
	 */
	const PRIORITY_LOW = 3;
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//消息类型
	/**
	 * long
	 */
	 const MSGTYPE_LONG = 1;
	
	 /**
	 * String
	 */
	 const MSGTYPE_STRING = 2;
	
	 /**
	 * Array
	  */
	 const MSGTYPE_ARRAY = 3;
	
	
	 /**
	  * Object
	  */
	 const MSGTYPE_OBJEDCT = 4;
	 
	 //////////////////////////////////////////////////////////////////////////////////////////////////////
	 //处理队列方法
	 /**
	  * 默认操作方式队列处理方式
	  */
	 const METHOD_OP_DEFAULT = 'defaultOp';
	 
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//队列
	/**
	 * 待发送短信队列
	 */
	const QUEUE_SMS = 'sms';

	/**
	 * 等发送email队列
	 */
	const QUEUE_EMAIL = 'email';

	/**
	 * 前端操作日志
	 */
	const QUEUE_FRONTEND_LOG = 'f_biz_log';
	
	/**
	 * 管理端操作日志
	 */
	const QUEUE_BACKEND_LOG = 'b_biz_log';
	
	/**
	 * 前端访问日志
	 */
	const QUEUE_VISIT_LOG = 'visit_log';
	
	/**
	 * 结算
	 */
	const QUEUE_SETTLE = 'settle';
	
	/**
	 * 会员升级日志
	 */
	const QUEUE_UPGRANDS = 'grade';
	
	/**
	 * 访问日志
	 */
	const QUEUE_USER_LOG = 'log';
	
	/**
	 * 替换敏感词
	 */
	const QUEUE_SENSITIVE = 'sensitive';
	
	
	/**
	 * 系统重要业务日志
	*/
	const QUEUE_SYSTEM_LOG = 'log';
	
	/**
	* 微信上传图片
 	*/
	const QUEUE_WXUPLOAD_IMAGES = 'uploads';
	
	/**
	 * 每日讲解员得分统计更新
	 */
	const QUEUE_NAR_SCORE = 'nar_score';
	
	const QUEUE_TR = 'tr';
					
}


<?php
namespace enums;

class SmsEnums
{

    /**
     * 发送短信失败，请联系客服解决
     */
    const ERR_SMS_MSG_SEND = '发送短信失败，请联系客服解决';

    /**
     * 重置密码
     */
    const BIZ_RESET_PASS = 'reset_pass';

    /**
     * 会员注册验证码
     */
    const BIZ_REGISTER = 'register';

    /**
     * 找回密码
     */
    const BIZ_RETRIEVE_PASS = 'retrieve_pass';

    /**
     * 重置手机号
     */
    const BIZ_RESET_PHONE = 'reset_phone';

}
<?php
namespace enums;

/**
 * 错误码规则：前三位表示错误族 后三位表示错误码 错误族从100开始到999 错误码从100开始到999
 * 
 * @author fzq
 *
 */
class RetCodeEnums
{

	//********************************************* 成功族 ***********************************************
	//成功族不设族号
	
	/**
	 * 成功
	 */
	const ERR_SUCCESS = 0;
	
	/**
	 * 成功
	 */
	const ERR_SUCCESS_MSG = '成功';
	
	
	//********************************************* 通用族 ***********************************************
	//族号 999
	
	/**
	 * 网络错误
	 */
	const ERR_NETWORK = 999100;
	
	/**
	 * 网络错误
	 */
	const ERR_NETWORK_MSG = '网络错误';
	

	/**
	 * 其他错误
	 */
	const ERR_OTHERS = 999101;
	
	/**
	 * 其他错误
	 */
	const ERR_OTHERS_MSG = '其他错误';
	
	
	/**
	 * 未填写请求参数 
	 */
	const ERR_NO_PARAMS = 999102;
	
	/**
	 * 未填写请求参数
	 */
	const ERR_NO_PARAMS_MSG = '未填写请求参数';
	
	
	
	//********************************************* DISTRICT ***********************************************
	//族号 100

	
	/**
	 * ID不存在
	 */
	const ERR_DISTRICT_ID = 100100;
	
	/**
	 * ID不存在
	 */
	const ERR_DISTRICT_ID_MSG = 'id必须存在';
	
	/**
	 *  name错误
	 */
	const ERR_DISTRICT_NAME = 100101;
	
	/**
	 * name错误
	 */
	const ERR_DISTRICT_NAME_MSG = 'name错误';
	
	/**
	 * 无子嗣
	 */
	const ERR_DISTRICT_NO_CHILD = 100102;
	
	/**
	 * 无子嗣
	 */
	const ERR_DISTRICT_NO_CHILD_MSG = '无子嗣';
	
	/**
	 * 无兄弟
	 */
	const ERR_DISTRICT_NO_SIBLING = 100103;
	
	/**
	 * 无兄弟 
	 */
	const ERR_DISTRICT_NO_SIBLING_MSG = '无兄弟';
	
	/**
	 * 其他错误
	 */
	const ERR_DISTRICT_OTHERS = 100104;
	
	/**
	 * 其他错误
	 */
	const ERR_DISTRICT_OTHERS_MSG = '其他错误';
	
	
	//********************************************* juhe数据三元素检测 ***********************************************
	//族号101
	
	/**
	 * 身份证错误
	 */
	const ERR_JUHE_BANK3_IDCARD = 101100;
	
	/**
	 * 身份证错误
	 */
	const ERR_JUHE_BANK3_IDCARD_MSG = '身份证未输入';
	
	
	/**
	 * 银行卡号错误
	 */
	const ERR_JUHE_BANK3_BANKCARD = 101101;
	
	/**
	 * 银行卡号错误
	 */
	const ERR_JUHE_BANK3_BANKCARD_MSG = '银行卡号未输入';
	
	
	/**
	 * 姓名必须存在
	 */
	const ERR_JUHE_BANK3_NAME = 101102;
	
	/**
	 * 姓名必须存在
	 */
	const ERR_JUHE_BANK3_NAME_MSG = '姓名必须填写'; 
	
	
	/**
	 * 姓名必须存在
	 */
	const ERR_JUHE_BANK3_VERIFY = 101103;
	
	/**
	 * 姓名必须存在
	 */
	const ERR_JUHE_BANK3_VERIFY_MSG = '非本人银行卡或数据填写有误';
	
	//********************************************* juhe数据三元素检测结束 ***********************************************
	
	
}
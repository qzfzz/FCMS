<?php

namespace libraries;
!defined( 'APP_ROOT' ) && exit( 'Direct Access Deny!' );

class FileUtils
{

	function __construct( )
	{
	
	}

	public static function getFileExt( $strFileName )
	{
		$info = pathinfo( $strFileName );
		
		return $info['extension'];
	}
	
	/**
	 * 创建文件夹
	 * @param unknown $fname
	 * @return boolean
	 */
	public static function mkdir( $path, $fname )
	{
		if( false == $path || false == $fname )
			return false;
		
		if( is_dir( $path ) )
			mkdir( $path . $fname, 0777 );
	}

    /**
     *
     * @author( author='bruce' )
     * @date( date = '2018年03月03日' )
     * @comment( comment = '递归创建文件夹' )
     * @op( op = 'w' )
     * @method( method = '' )
     * @param string $dir
     * @return boolean
     */
    public static function mkDirs( $dir )
    {
        if( !is_dir( $dir ) )
        {
            if( !self::mkDirs( dirname( $dir ) ) )
            {
                return false;
            }

            if( !mkdir( $dir, 0777 ) )
            {
                return false;
            }
        }
        return true;
    }
}

?>
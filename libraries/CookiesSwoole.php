<?php

namespace libraries;

use enums\HttpServerEnums;
class CookiesSwoole implements \Phalcon\Http\Response\CookiesInterface, \Phalcon\Di\InjectionAwareInterface
{
	private $_di = null;
	
	private $_useEncryption = false;
	
	private $_crypt = null;
	/*  
	 * @see \Phalcon\Http\Response\CookiesInterface::useEncryption()
	 */
	public function useEncryption( $useEncryption ) 
	{
		$this->_useEncryption = $useEncryption;
	}

	/*
	 * @see \Phalcon\Http\Response\CookiesInterface::isUsingEncryption()
	 */
	public function isUsingEncryption() 
	{
		return $this->_useEncryption;			
	}

	/* 
	 * @see \Phalcon\Http\Response\CookiesInterface::set()
	 */
	public function set( $name, $value = null, $expire = 0, $path = "/", $secure = null, $domain = null, $httpOnly = null) 
	{
// 		$response->cookie( HttpServerEnums::SESSION_KEY , $strSessionID, 0, '/' );
// 		swoole_http_response->cookie(string $key, string $value = '', int $expire = 0 , string $path = '/', string $domain  = '', bool $secure = false , bool $httponly = false);

		$strValue = $value;
		if( $this->_useEncryption && ( $name != HttpServerEnums::SESSION_KEY ))
		{
			if( empty( $this->_crypt ))
			{
				$this->_crypt = $this->_di->get( 'crypt' );
			}
				
			$strValue = $this->_crypt->encryptBase64( $value );
		}
		
		\HttpServer::$response->cookie( $name, $strValue, $expire, $path, $domain, $secure, $httpOnly );
	}

	/* 
	 * @see \Phalcon\Http\Response\CookiesInterface::get()
	 */
	public function get( $name ) 
	{
// 		var_dump( $this->_useEncryption );
// 		var_dump( $_COOKIE, $name );
		if( isset( $_COOKIE[ $name ] ) )
		{
			$cookie = new \Phalcon\Http\Cookie( $name );
			
			if( $this->_useEncryption && ( $name != HttpServerEnums::SESSION_KEY ))
			{
				if( empty( $this->_crypt ))
				{
					$this->_crypt = $this->_di->get( 'crypt' );
				}
				
// 				$cookie->useEncryption( true );
// 				$cookie->setDI( $this->_di );

				$cookie->setValue( $this->_crypt->decryptBase64( $_COOKIE[ $name ] ) );
			}
			
			return $cookie;
		}
		
		
		return null;
	}

	/* 
	 * @see \Phalcon\Http\Response\CookiesInterface::has()
	 */
	public function has($name) 
	{
		if( isset( $_COOKIE[ $name ] ) )
		{
			return true;
		}
		
		return false;
		
	}

	/*  
	 * @see \Phalcon\Http\Response\CookiesInterface::delete()
	 */
	public function delete($name) 
	{
		unset( $_COOKIE[ $name ] );
		
		return setcookie( $name, '', $_SERVER[ 'REQUEST_TIME' ] - 3600 );
	}

	/*  
	 * @see \Phalcon\Http\Response\CookiesInterface::send()
	 */
	public function send() 
	{
		return true;
	}

	/*  
	 * @see \Phalcon\Http\Response\CookiesInterface::reset()
	 */
	public function reset() 
	{
		return true;
	}

	/*  
	 * @see \Phalcon\Di\InjectionAwareInterface::setDI()
	 */
	public function setDI(\Phalcon\DiInterface $dependencyInjector) 
	{
		// TODO Auto-generated method stub
		$this->_di = $dependencyInjector;
	}

	/*  
	 * @see \Phalcon\Di\InjectionAwareInterface::getDI()
	 */
	public function getDI() 
	{
		// TODO Auto-generated method stub
		return $this->_di;
	}

	
}
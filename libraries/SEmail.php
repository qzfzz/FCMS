<?php

namespace libraries;

use enums\ServiceEnums;
use enums\QueueEnums;
use enums\GrepEnums;
if( ! APP_ROOT  ) return 'Direct Access Deny!';


class SEmail implements \Phalcon\Di\InjectionAwareInterface
{
    var $_di;
    
	private static $_instance = null;

	private function __construct( $di )
	{
		$this->_di = $di;
	}
	
	public static function getInstance( $di )
	{
		if( !self::$_instance )
		{
			self::$_instance = new SEmail( $di );
		}	
		
		return self::$_instance;
	}
	
    /**
     *
     * @param string $strSubject
     *            主题
     * @param string $strMessage
     *            正文
     * @param string $strTo
     *            接收者
     * @param array $arrCC
     *            抄送
     * @param array $arrBCC
     *            暗送
     * @param array $arrAttachs
     *            附件
     * return false for error, others for success
     */
    public function sendMail( $strSubject, $strMessage, $strTo, $arrCC = null, 
            $arrBCC = null, $arrAttachs = null )
    {
    	
    	if( !preg_match( GrepEnums::GREP_EMAIL, $strTo ))
    	{
    		return false;	
    	}
    	
    	$queue = $this->_di[ ServiceEnums::SERVICE_RQUEUE ];
    	
    	$params = $queue->getMsgProto( array( 'subject' => $strSubject,
    			'content' => $strMessage,
    			'to' => $strTo ),
    			'email',
    			QueueEnums::MSGTYPE_ARRAY,
    			'Email',
    			'defaultOp',
    			false,
    			QueueEnums::PRIORITY_MEDIUM
    	 );
    	
    	$iRet = $queue->push( $params );
    	
    	if( $iRet )
    	{
    		return true;
    	}
    }
    
    public function setDI( \Phalcon\DiInterface $dependencyInjector )
    {
        $this->_di = $dependencyInjector;
    }
    
    public function getDI()
    {
        return $this->_di;
    }
}
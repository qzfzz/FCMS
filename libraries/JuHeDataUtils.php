<?php
namespace libraries;

if( ! APP_ROOT  ) return 'Direct Access Deny!';

use enums\GrepEnums;
use helpers\CommUtils;

use enums\RetCodeEnums;
use enums\ServiceEnums;

class JuHeDataUtils
{
	/**
	 * 成功
	 */
	const ERR_SUCCESS = 1;
	
	/**
	 * 身份证错误
	 */
	const ERR_JUHE_BANK3_IDCARD = 101100;
	
	/**
	 * 身份证错误
	 */
	const ERR_JUHE_BANK3_IDCARD_MSG = '身份证未输入';
	
	
	/**
	 * 银行卡号错误
	 */
	const ERR_JUHE_BANK3_BANKCARD = 101101;
	
	/**
	 * 银行卡号错误
	 */
	const ERR_JUHE_BANK3_BANKCARD_MSG = '银行卡号未输入';
	
	
	/**
	 * 姓名必须存在
	 */
	const ERR_JUHE_BANK3_NAME = 101102;
	
	/**
	 * 姓名必须存在
	 */
	const ERR_JUHE_BANK3_NAME_MSG = '姓名必须填写';
	
	
	/**
	 * 非本人银行卡或数据填写有误
	 */
	const ERR_JUHE_BANK3_VERIFY = 101103;
	
	/**
	 * 姓名必须存在
	 */
	const ERR_JUHE_BANK3_VERIFY_MSG = '非本人银行卡或数据填写有误';
	
	/**
	 * 其他错误
	 */
	const ERR_OTHERS = 999101;
	
	
	private $_di = null;
	private static $_instance = null;
		
	private function __construct( $di )
	{
		$this->_di = $di;
	}
	
	public static function getInstance( $di )
	{
		if( !self::$_instance )
		{
			self::$_instance = new JuHeDataUtils( $di );
		}
		
		return self::$_instance;
	}
	
	/**
	 * 聚合数据银行卡三元素查询
	 * 
	 * 使用方法
	 * 
	 * http://c.xxxxx.dev/common/juhe/bankcardcheckthreeelem/realname/%20%E9%92%B1%E5%BF%97%E9%94%8B%20/idcard/412726198503156258/bankcard/6216603600001256555
	 * 
	 * return 1 成功 其他失败
	 */
	public function bankCardCheck3Elem( string $strRealName, string $strBankCard, string $strIDCard )
	{
		//身份证号
		if( !$strIDCard || false === CommUtils::validationFilterIDCard( $strIDCard ))
		{
			return self::ERR_JUHE_BANK3_IDCARD;
		}
		
		//银行卡号
		if( !$strBankCard || !preg_match( GrepEnums::GREP_BANK_CARD, $strBankCard ) )
		{
			return self::ERR_JUHE_BANK3_BANKCARD;
		}
		
		//姓名
		if( !$strRealName || ( $strRealName = trim( $strRealName ) ) == '' )
		{
			return self::ERR_JUHE_BANK3_NAME;
		}
		
		$strUrl = $this->_di[ 'juheCfg' ]->bank3e->url . 
					'key=' . $this->_di[ 'juheCfg' ]->bank3e->key . 
					'&realname=' . $strRealName . 
					'&idcard=' . $strIDCard . 
					'&bankcard=' . $strBankCard;
		
		$redis = $this->_di[ ServiceEnums::SERVICE_NATIVE_REDIS ];
		if( $cacheData = $redis->hGet( $this->_di[ 'juheCfg' ]->bank3e->cacheName, $strBankCard ) )
		{
			$objBankInfo = json_decode( $cacheData );
			
			if( $objBankInfo->realname == urldecode( $strRealName ) &&
				$objBankInfo->idcard == $strIDCard && 
				$objBankInfo->bankcard == $strBankCard
			)
			{
				return self::ERR_SUCCESS;
			}
			else
			{
				$this->_di[ServiceEnums::SERVICE_LOG_ERR]->error( RetCodeEnums::ERR_JUHE_BANK3_VERIFY . ' ' . 
						RetCodeEnums::ERR_JUHE_BANK3_VERIFY_MSG  . ' ' . var_export( $objBankInfo, true ) );
				
// 				echo $this->getRet( null, RetCodeEnums::ERR_JUHE_BANK3_VERIFY_MSG, RetCodeEnums::ERR_JUHE_BANK3_VERIFY );
				
				return self::ERR_JUHE_BANK3_VERIFY;
			}
			
		}
		
		$objBankInfo = $this->getResult( $strUrl, true );
		if( $objBankInfo->error_code  == 0 )
		{//查询成功
			if( $objBankInfo->result->res == 1 )
			{//验证成功
// 				echo $this->getRet( null, RetCodeEnums::ERR_SUCCESS_MSG, RetCodeEnums::ERR_SUCCESS );
				
				$arrBankData = array( 
						'bankcard' => $objBankInfo->result->bankcard, 
						'realname' => $objBankInfo->result->realname, 
						'idcard' => $objBankInfo->result->idcard );
				
				
				$redis->hSet( $this->_di[ 'juheCfg' ]->bank3e->cacheName, $strBankCard, json_encode( $arrBankData ) );
				return self::ERR_SUCCESS;
			}
			else
			{
				$this->_di[ServiceEnums::SERVICE_LOG_ERR]->error( RetCodeEnums::ERR_JUHE_BANK3_VERIFY . ' ' . 
						RetCodeEnums::ERR_JUHE_BANK3_VERIFY_MSG  . ' ' . var_export( $objBankInfo, true ) );
				
// 				echo $this->getRet( null, RetCodeEnums::ERR_JUHE_BANK3_VERIFY_MSG, RetCodeEnums::ERR_JUHE_BANK3_VERIFY );

				return self::ERR_JUHE_BANK3_VERIFY;
			}
		}
		else
		{//验证失败
			$this->_di[ServiceEnums::SERVICE_LOG_ERR]->error( RetCodeEnums::ERR_OTHERS . ' ' . RetCodeEnums::ERR_OTHERS_MSG . ' ' . var_export( $objBankInfo, true ) );
			
// 			echo $this->getRet( null, RetCodeEnums::ERR_OTHERS_MSG, RetCodeEnums::ERR_OTHERS );

			return self::ERR_OTHERS;
		}
	}
	
	
	private function getResult( $strUrl, $bDecode = false )
	{
		try
		{
			$curl = curl_init( $strUrl );
			curl_setopt( $curl, CURLOPT_TIMEOUT, 10 );
			curl_setopt( $curl, CURLOPT_HEADER, 0 );
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
			$strRet = curl_exec( $curl );
			curl_close( $curl );
		}
		catch ( \Exception $e )
		{
			$this->_di[ServiceEnums::SERVICE_LOG_ERR]->debug( $e->getMessage() );
			
			return false;
		}
	
		if( $bDecode )
		{
			return json_decode( $strRet );
		}
		else
		{
			return $strRet;
		}
	}

}
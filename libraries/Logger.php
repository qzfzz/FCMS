<?php
namespace libraries;
use enums\ServiceEnums;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class Logger //implements \Phalcon\Di\InjectionAwareInterface
{
    private $di;
    private static $instance = null;
    /**
     * {@inheritDoc}
     * @see \Phalcon\Di\InjectionAwareInterface::setDI()
     */
//     public function setDI(\Phalcon\DiInterface $dependencyInjector)
//     {
//         // TODO Auto-generated method stub
//         $this->di = $dependencyInjector;
//     }

//     /**
//      * {@inheritDoc}
//      * @see \Phalcon\Di\InjectionAwareInterface::getDI()
//      */
//     public function getDI()
//     {
//         // TODO Auto-generated method stub
//         return $this->di;
//     }
    
    public static function getInstance( $di )
    {
        assert( $di );
        
        if( !self::$instance )
        {
            self::$instance = new Logger( $di );
        }
        
        return self::$instance;
    }
    
    public function error( $strMsg = null )
    {
        if( !$strMsg && trim( $strMsg ) != '' )
            $this->di[ServiceEnums::SERVICE_LOG_ERR]->error( $strMsg );
    }
    
    public function biz( $strMsg = null )
    {
        if( !$strMsg && trim( $strMsg ) != '' )
            $this->di[ServiceEnums::SERVICE_LOG_BIZ]->info( $strMsg );
    }
    
    public function info( $strMsg = null )
    {
        if( !$strMsg && trim( $strMsg ) != '' )
            $this->di[ServiceEnums::SERVICE_LOG_INFO]->info( $strMsg );
    }
    
    public function debug( $strMsg = null )
    {
        if( !$strMsg && trim( $strMsg ) != '' )
            $this->di[ServiceEnums::SERVICE_LOG_DEBUG]->debug( $strMsg );
    }

}


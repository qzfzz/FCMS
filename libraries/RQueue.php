<?php
/**
 *
 * @author fzq
 * @date 2016-06-27
 */

namespace libraries;
use enums\QueueEnums;
use helpers\CommUtils;
use enums\CachePrefixEnums;
use enums\ServiceEnums;

if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }

class RQueue implements \Phalcon\Di\InjectionAwareInterface, \libraries\IMsgQueue
{
	//array('mtype' => BIZ_SMS,
	//		'priority' =>  PRIORITY_HIGH,
	//		'cls' => 'Log',
	//		'method' => 'memLoginLog',
	//		'msg' => 'xxxxxxxxx' );
	
// 	private $_config;
	private $_di;
	private $_queue;
	private static $_instance = null;
	
	/* (non-PHPdoc)
	 * @see \Phalcon\Di\InjectionAwareInterface::setDI()
	 */
	public function setDI(\Phalcon\DiInterface $dependencyInjector) 
	{
		// TODO Auto-generated method stub
		$this->_di = $dependencyInjector;
	}

	/* (non-PHPdoc)
	 * @see \Phalcon\Di\InjectionAwareInterface::getDI()
	 */
	public function getDI() 
	{
		// TODO Auto-generated method stub
		return $this->_di;
	}
	
	private function __construct( $di )
	{
		if( $di )
		{
			$this->_di = $di;
			
		}
	}
	
	public static function getInstance( $di )
	{
		if( !self::$_instance )
		{
			self::$_instance = new RQueue( $di );
		}
		
		return self::$_instance;
	}
	/**
	 * 
	 * @param mixed $msg
	 * @param string $strBiz
	 * @param int $msgt  QueueEnums::MSGTYPE_LONG ...
	 * @param string $strCls默认与biz相同
	 * @param string $method默认为defaultOp
	 * @param string $id
	 * @param int $priority
	 * 
	 * return false for error
	 */
	public function getMsgProto( $msg, 
								$strBiz, 
								$msgt = QueueEnums::MSGTYPE_STRING,
								$strCls = false, 
								$strMethod = QueueEnums::METHOD_OP_DEFAULT, 
								$id = false, 
								$priority = QueueEnums::PRIORITY_MEDIUM
								)
	{
		if( !is_string( $strBiz ) || !is_string( $strMethod ) )
		{
			return false;
		}
		
		if( !$strCls )
		{
			$strCls = ucfirst( $strBiz );	
		}
		
		switch( $msgt )
		{
			case QueueEnums::MSGTYPE_STRING:
				$arrParams[ 'msg' ] = $msg;
				break;
			case QueueEnums::MSGTYPE_ARRAY:
			case QueueEnums::MSGTYPE_OBJEDCT:
				$arrParams[ 'msg' ] = serialize( $msg );
				break;
			case QueueEnums::MSGTYPE_LONG:
				$arrParams[ 'msg' ] = '' . $msg;
				break;
			default:
				return false;
		}
		
		$arrParams[ 'biz' ] = $strBiz;
		$arrParams[ 'cls' ] = $strCls;
		$arrParams[ 'method' ] = $strMethod;
		$arrParams[ 'id' ] = $id ? $id : CommUtils::getUNID( $strBiz . $strCls. $strMethod );
		$arrParams[ 'msgt' ] = $msgt;
		$arrParams[ 'priority' ] = $priority;
		
		return $arrParams;
		
	}
	
	//array('biz' => BIZ_SMS,
	//		'priority' =>  PRIORITY_HIGH,
	//		'id'	=> CommUtils::UNID(),
	//		'cls' => 'Log',
	//		'method' => 'memLoginLog',
	//		'msgt'	=> 'string,array,object',
	//		'msg' => 'xxxxxxxxx' );
	/**
	 * send message to message queue
	 * 
	 * @param array $arrParam
	 * return id for true false for error
	 */
	public function push( array $arrParam )
	{
		if( !is_array( $arrParam ) || 
			!isset( $arrParam[ 'biz' ] ) || 
		    !isset( $arrParam[ 'priority' ] ) || 
			!isset( $arrParam[ 'cls' ] ) || 
			!isset( $arrParam[ 'method' ] ) || 
			!isset( $arrParam[ 'msg' ] ) ||
			!isset( $arrParam[ 'msgt' ]) || 
			!isset( $arrParam[ 'id' ]) )
		{
			return false;
		}
		
		$lListLen = $this->_queue->lpush( CachePrefixEnums::RQUEUE_PREFIX . $arrParam[ 'biz' ], serialize( $arrParam ));
		
		if( $lListLen )
		{
			return $arrParam[ 'id' ];
		}
		
		return false;
	}
	

	/**
	 * 从message queue中取数据
	 * @param string $strBiz
	 * @param string $strPriority
	 * @param string $bPush
	 * 
	 * 当使用信号处理时不能使用阻塞方式连接否则会出错
	 * return false for error string for success
	 */
	public function pop( $strBiz, $iPriority = QueueEnums::PRIORITY_MEDIUM, $bBlock = false, $timeout = 10, $bBackup = false )
	{
		if( !is_string( $strBiz ) )
		{
			return false;
		}
		
		if( $bBlock )
		{
			if( $timeout < 0 )
			{
				$timeout = 10;
			}
			
			if( $bBackup )
			{
				$arrMsg = $this->_queue->brpoplpush( CachePrefixEnums::RQUEUE_PREFIX . $strBiz, CachePrefixEnums::RQUEUE_BACKUP_PREFIX . $strBiz, $timeout );
			}
			else
			{
				$arrMsg = $this->_queue->brpop( CachePrefixEnums::RQUEUE_PREFIX . $strBiz, $timeout );
			}

			if( $arrMsg )
			{
				return $arrMsg[1];
			}
			
			return false;
		}
		else
		{
			if( $bBackup )
			{
				return $this->_queue->rpoplpush( CachePrefixEnums::RQUEUE_PREFIX . $strBiz, CachePrefixEnums::RQUEUE_BACKUP_PREFIX . $strBiz );
			}
			else
			{
				return $this->_queue->rpop( CachePrefixEnums::RQUEUE_PREFIX . $strBiz );
			}
		}
		
	}
	
	/**
	 * delete from backup queue
	 * @param array $arrParams
	 * 
	 * return false for error
	 */
	public function delFromBackupQueue( $arrParams )
	{
		return $this->_queue->lrem( CachePrefixEnums::RQUEUE_BACKUP_PREFIX . $arrParams[ 'biz' ], serialize( $arrParams ) );
	}

	public function setServer( $queue )
	{
		if( isset( $this->_queue ))
		{
			unset( $this->_queue );
		}
		
		$this->_queue = $queue;
	}
	
// 	public function reconnect()
// 	{
// 		$redis = new \Redis();
		
// 		$config = $this->_di->get( 'config' );
		
		
// 		try 
// 		{
// 			$redis->pconnect( $config->rqueue->host, $config->rqueue->port /*, 'password'*/ );
// 		}
// 		catch( \Exception $e )
// 		{
// 			echo $e->getMessage();
			
// 			return false;	
// 		}
		
// 		$redis->select( $config->rqueue->db );
// 		$this->_queue->setServer( $redis );
		
// 		return true;
// 	}
	
	public function getServer()
	{
		return $this->_queue;
	}
	
// 	public function getConfig()
// 	{
// 		return $this->_config;
// 	}

// 	public function setConfig( $config )
// 	{
// 		$this->_config = $config;
// 	}
}

<?php
namespace libraries;

class XEMail implements \Phalcon\Di\InjectionAwareInterface
{
	private $_mailer = null;
	private static $_instance = null;
	private $_option = array();
	private $_di = null;
	
	private function __construct( $mailer, $di )
	{
		$this->_mailer = $mailer;
		
		$this->_di = $di;
		
		$this->_option[ 'from' ] = $this->_di['emailCfg']->{$this->_di['emailCfg']->mode}->from;
		$this->_option[ 'defaultSubject' ] = $this->_di['emailCfg']->defaultSubject;
	}
	
	/**
	 * 
	 * 用法 $this->email->sendMail( 'subject', 'this is from bruce!', 'brucetsisen@163.com' );
	 * 
	 * @param string $strSubject
	 * @param string $strMessage
	 * @param string $strTo
	 * @param array $arrCC
	 * @param array $arrBCC
	 * @param array $arrAttachs
	 * @return number
	 * 
     * Send the given Message like it would be sent in a mail client.
     *
     * All recipients (with the exception of Bcc) will be able to see the other
     * recipients this message was sent to.
     *
     * Recipient/sender data will be retrieved from the Message object.
     *
     * The return value is the number of recipients who were accepted for
     * delivery.
     *
     * @param Swift_Mime_Message $message
     * @param array              $failedRecipients An array of failures by-reference
     *
     * @return int 1.正整数表示成功发送的邮件个数 -1：邮件地址为空或是地址格式错误 -2: 消息为空 false:其他错误
	 *
	 */
	public function sendMail( $strSubject, $strMessage, $strTo, $arrCC = null, 
            $arrBCC = null, $arrAttachs = null )
	{ 
		if( !preg_match( "/[\w\-]+\@[\w\-]+\.[\w\-]+/", $strTo ) || is_null( $strTo ) || 0 == strlen( $strTo ))
		{
			return -1;
		}
		
		if( is_null( $strMessage ) || 0 == strlen( $strMessage ) )
		{
			return -2;
		}
		
		if( is_null( $strSubject ) || 0 == strlen( $strSubject ))
		{
			$strSubject = $this->_option['defaultSubject'];
		}
		
        $msg = \Swift_Message::newInstance( $strSubject )->
        	setFrom( $this->_option['from'] )->
        	setTo( $strTo )->
        	setBody( $strMessage );
         
        if( isset( $arrCC ) && is_array( $arrCC ) )
        {
        	foreach( $arrCC as $addr )
        	{
        		$msg->addCc( $addr );
        	}
        }
        
        if( isset( $arrBCC ) && is_array( $arrBCC ) )
        {
        	foreach( $arrBCC as $addr )
        	{
        		$msg->addBcc( $addr );
        	}
        }
        
        
        if( isset( $arrAttachs ) && is_array( $arrAttachs ) )
        {
        	foreach( $arrAttachs as $attachmentPath )
        	{
        		$msg->attach( \Swift_Attachment::fromPath( $attachmentPath ));
        	}
        }
        
        return $this->_mailer->send( $msg );
            	
    }
    
    public static function getInstance( $mailer, $di )
    {
    	if( !self::$_instance )
    	{
    		self::$_instance = new XEMail( $mailer, $di );
    	}
    	
    	return self::$_instance;
    }
    
	/* (non-PHPdoc)
	 * @see \Phalcon\Di\InjectionAwareInterface::setDI()
	 */
	public function setDI(\Phalcon\DiInterface $dependencyInjector) {
		// TODO Auto-generated method stub
		$this->_di = $dependencyInjector;
	}

	/* (non-PHPdoc)
	 * @see \Phalcon\Di\InjectionAwareInterface::getDI()
	 */
	public function getDI() {
		// TODO Auto-generated method stub
		return $this->_di;
	}

    
    
}
<?php
/**
 *
 * @author fzq
 * @date 2016-06-27
 */
namespace libraries;
if( !defined( 'APP_ROOT' ) ){ echo 'Direct Access Deny!'; return; }
use enums\QueueEnums;

interface IMsgQueue
{
	/**
	 * 
	 * @param array $arrParam
	 * 
	 * return false for error
	 */
	public function push( array $arrParam );
	
	/**
	 * 
	 * @param string $strBiz
	 * @param string $strPriority
	 * @param string $bPush
	 * 
	 * return false for error
	 */
	public function pop( $strBiz, $iPriority = QueueEnums::PRIORITY_MEDIUM, $bBlock = false, $timeout = 10, $bBackup = false );
	
	/**
	 * delete from backup queue
	 * @param array $arrParams
	 * 
	 * return false for error
	 */
	public function delFromBackupQueue( $arrParams );
}

